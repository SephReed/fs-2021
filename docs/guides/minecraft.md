# Guide: Minecraft Java Edition

### Availability

Cost: ~$27

- ❌ Web Client
- ✅ Windows
- ✅ MacOS
- ❌ iOS
- ⚠️ Android (bedrock edition only)
- ⚠️ Oculus (bedrock edition only)
- ⚠️ Xbox (bedrock edition only)

-------

<div id="MinecraftInstall">

### Installation

The Flipside Server is hosted for both Minecraft **Java Edition** and Bedrock Edition.

It's kind of confusing, but there's two versions of Minecraft:
1. Java Edition - The original, which allows for extensive modding and large scale servers
2. Bedrock Edition - The thing Microsoft made after they bought Minecraft that *doesn't allow for custom servers or modding*

Although we support Bedrock Edition, if you are buying Minecraft for the first time, we recommend getting the **Java Edition**.  You can follow this link:

[Buy & Download Minecraft Java Edition](https://www.minecraft.net/en-us/store/minecraft-java-edition)

If you are buying Minecraft you will need to create a Microsoft account or use an existing one.
</div>

-------

<div id="MinecraftGettingStarted">

### Getting Started

- Once downloaded and installed, go ahead and start Minecraft
- You will need to login again, and luckily for you there will be two choices: "Microsoft Login" and "Mojang Login". If you just purchased the game, you will be using the "Microsoft Login". If you happened to already have Minecraft from 2012 then you might need to use the
"Mojang Login".
  - ![Minecraft Login](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-minecraft--login.png)
- Once logged in, click the giant green "PLAY" button in the middle of the window.
  - ![Minecraft Play](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-minecraft--play.png)
</div>

-------

<div id="MinecraftEventServer">

### Connect to the Event Server!

- Click "Multiplayer"
  - ![Minecraft Multiplayer](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-minecraft--multiplayer.png)
- If this is your first time playing, you will be warned about 3rd party content, if you consent, click "Proceed"
  - ![Minecraft Proceed](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-minecraft--proceed.png)
- If this is your first time playing and you are using Windows, you may be presented with a scary looking security alert. This is just Minecraft requesting access to the internet. If you have no idea what you are doing then just keep the default setting and click "Allow access"
  - ![Minecraft Windows Defender](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-minecraft--windows-defender.png)
- Click "Add Server"
  - ![Minecraft Add Server](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-minecraft--add-server.png)
- Add the Howdy Fuckers! 2021 Minecraft server
  - For "Server Name", you can choose any name, for example "Howdy Fuckers! 2021"
  - For "Server Address", use "howdy-fuckers.mc.gg"
  - ![Minecraft Add Burn Server](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-minecraft--add-burn-server.png)
- Finally, to join the server, double-click the new server that you just added
  - ![Minecraft Server Ready](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-minecraft--server-ready.png)
- Welcome! Now go play!
  - ![Minecraft Welcome](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-minecraft--welcome.png)
</div>

-------

<div id="MinecraftEventServerBedrock">

### Connect to the Event Server with Bedrock Edition!

If you have Minecraft Bedrock Edition, you can connect to the event server using the following IP address and port:
- IP: `107.175.178.34`
- Port: `62402`

</div>
