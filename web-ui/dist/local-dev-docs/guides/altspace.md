# Guide: Alt Space

*this section is not yet complete*

### Availability

Cost: Free

- ❌ Web Client
- ✅ Windows 
- ⚠️ MacOS (Beta)
- ❌ iOS
- ❌ Android
- ✅ Oculus

-------

### Installation

If you have a VR Headset:
  - For PC:  https://altvr.com/get-altspacevr/
  - For Oculus: https://www.oculus.com/experiences/rift/1072303152793390/

If you have a PC and want to play in "2D Mode":
  - For Windows: https://www.microsoft.com/en-us/p/altspacevr/9nvr7mn2fchq?activetab=pivot:overviewtab
  - For MacOS (buggy & beta version): https://altvr.com/altspacevr-mac/
  
