# If you're just here to read the docs...

### [Follow this link to Welcome Page](./welcome-page.md)


<br><br><br><br>



### If you're here to edit the docs

1. First, get familiar with how these docs are organized.  You can start by exploring the file list at the top (or side) of this page.
2. Once you get the gist, [follow this link](https://gitlab.com/-/ide/project/SephReed/fs-2021/tree/master/-/docs/) to start editing
3. Use the sidebar on the left to navigate to the file you're looking for.   Likely `docs/some-name.md`
    - the files are all written in MarkDown.  You can [learn basic syntax here](https://www.markdownguide.org/basic-syntax)
    - whenever you have a markdown file open, there will be a "Preview Markdown" tab at the top of the editor.  Use this to see the live version.
4. Edit to your hearts content
5. When you are ready to save, there is a button at the bottom left labeled `Commit...`
    - **MAKE SURE YOU CHOOSE** the `Commit to master branch` option.  If you don't, nothing damaging or irreversable will happen.  What will happen
      is that your "commit" will be made on a separate stream, and that stream will have to be merged back into master.
6. Verify your work.  Go back to step 1 and navigate to the file you modified to make sure you like what you see.
