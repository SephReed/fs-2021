# Volunteers

Volunteers are what make any good communal event run, and this is no exception.  The more people put in, the better the yield; so every little bit counts!


-------

<div id="Organizers">

### Organizers & Cat Herders
**Task:** Networking and/or coordinating through the chaos

**Examples:**
- Helping with event scheduling to avoid overlap
- Patrolling the Discord Chats trying to network people into places they can help
- General outreach
- Being a consistent point of contact for a project

[Be a Cat Herder](http://howdyfuckers.online/portal/discord)

</div>

------

<div id="Devs">

### Developers & Tech Admin
**Task:** Building out & maintaining various tech stacks

**Examples:**
- Web Dev (Typescript, NodeJS, etc)
- Minecraft (Server Admin, Java Mods)... we could really use Minecraft Server Admins
- Altspace (Unity, C#, 3D Modeling)

[Join Dev & Tech](http://howdyfuckers.online/portal/discord)
</div>


-------

<div id="AntiTroll">

### Anti-Troll Rangers

**Task:**  Mods for Discord Chat, Minecraft, and/or Altspace

**Examples:**
- De-escalation
- Kicking / banning bad-actors or bots
- Verifying humanity and granting admittance to newcomers

[Join the Rangers](http://howdyfuckers.online/portal/discord)
</div>

-------

<div id="TechSupport">

### Greeters & Basic Tech Support

**Task:** Welcoming, helping, and granting admission to people in various virtual spaces (Discord / Minecraft / AltSpace)

**Examples:**
- Being welcoming
- Looking for people who seem to be confused
- Verifying humanity and granting admittance to newcomers
- Introductory tech-support for newbies

[Become a Greeter](http://howdyfuckers.online/portal/discord)

</div>

-------

<div id="Writers">

### Tutorial Writers
**Task:** Writing up explanations on how to do things

**Examples:**
  - Explaining a problem as simply as possible
  - Adding photos to aid 
  - Covering all platforms (which OS)

[Help Us With am Writing plz](http://howdyfuckers.online/portal/discord)
</div>


-------

<div id="Artists">

### 2D, 3D, & Audio Digital Artists
*note: if you'd like to share your art in the "virtual wander" gallery or as a game asset, [click here](./creators-and-events.md#Gallery)*

**Task:** Create digital assets for this site or to be experienced in game (Minecraft / AltSpace)

**Examples:**
- For this site: logos, background-imagery, icons
- For the Minecraft Custom Resource Pack: 2d pixel art, burner sound effects, new in game background music
- For AltSpace: 3d modeling, textures, sound effects, art to hang on the walls
- For anything: whatever you want to artify, please do it!

[Create Art & Assets](http://howdyfuckers.online/portal/discord)
</div>


-------

<div id="StationManagers">

### Broadcasting Stream Managers
**Task:** Like a TV broadcaster choosing angles, but instead choosing between various streams.

**Responsibilities:**
- Get setup for broadcast management pre-event
- Looking out for and spotlighting the most interesting streams

[Be a Station Manager](http://howdyfuckers.online/portal/discord)
</div>

-------

<div id="Streamers">

### Streamers

**Task:** Attend major events and share footage

**Responsibilities:**
- Having your rig setup for streaming
- Doing your best to capture the moment

[Do Some Streaming](http://howdyfuckers.online/portal/discord)
</div>

