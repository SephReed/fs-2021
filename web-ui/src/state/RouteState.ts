import { HeliumRouteState, IRouteConfig, UrlState } from 'helium-ui';

UrlState.preventRefreshWarningGiven = true;

// const RouteConfigs: Record<string, Omit<IRouteConfig, "id">> = {
const RouteConfigs = {
  "survival-guide": {
    test: /\/survival-guide/
  },
  "gallery-game": {
    test: /\/gallery-game/
  },
  "portal": { test: "/portal" },
  "portal.principles": { test: "/portal/principles" },
  "portal.discord": { test: "/portal/discord" },
  "portal.minecraft": { test: "/portal/minecraft" },
  "portal.topia": { test: "/portal/topia" },
  "portal.altspace": { test: "/portal/altspace" },
  "portal.art-safari": { test: "/portal/art-safari" },
  "portal.calendar": { test: "/portal/calendar" },
  "portal.kflip": { test: "/portal/kflip" },
} as const;

export type RouteId = keyof typeof RouteConfigs;



export class RouteState extends HeliumRouteState<RouteId> {
  constructor() {
    super(RouteConfigs)
  }
}