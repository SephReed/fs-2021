# Creators and Events

Essentially, **anything** that *isn't touch* can become part of this year's event.  If you can
host it on the internet, or share it in a video, or put it in your front lawn; people will
be able to interact with it.  And given we've all got some kind of digital device, there's even some things
that aren't typically burn friendly we can try out (like gaming)!


------

<div id="ArtSafari">

### Burner Art Safari

Hopefully you know about the "Burner Art Safari" already, but if not the basic idea is this:
1. Put something artistic out in public
2. Register its location so it can be put on the map
3. Perhaps put some schwag and hand sanitizer out

[Read More About the Art Safari Here](https://www.burningflipside.com/announcements/art-safari-and-virtual-flipside)
</div>

------

<div id="Streaming">

### Streaming / Online Performances

There will be an event Discord where any audio or video can be shared, a Twitch stream for
spotlighting the best of current audio/video, and an internet radio stream for audio
only.  Anyone can share on any platform they want at any time, but it'll make it easier
for our "station managers" if we know who's doing what and when.

Some performance types include:
- DJs
- VJs: there will be audio djs without any video to go along
- Performers: you can even stream a zoom call if your performance is interactive!
- Streamers: share live footage of cool stuff, virtual or irl

[Share Your Performance Plans Here](http://howdyfuckers.online/portal/discord)
</div>

------

<div id="Gallery">

### The "Virtual Wander" Gallery

Outside of live broadcasting, there will also be a more "peruse as you feel" online gallery. Pretty much
anything that can be shared online is welcome here.  It will be designed to have a
"wandering & discovery" aspect, and will be connected to the Minecraft and Altspace virtual worlds as well.
The gallery is hosted on the Topia platform. For more information on how access the gallery, visit
[Topia User Guide](./guides/topia.md).

Some gallery types include:
- Visual Art
- Music
- Interactive Web Stuff
- Short Stories and Writing

[Share Your Web-Hostable Art Info Here](http://howdyfuckers.online/portal/discord)
</div>

--------

<div id="Other">

### Hosting Other Virtual Events

If you'd like to host a gaming event, or alternate virtual space event, just let us know!  We'll
put it on the event schedule, and try to get a streamer there to share with everyone if you're not able to stream yourself.

[Share Your Virtual Event Plans Here](http://howdyfuckers.online/portal/discord)
</div>

--------

<div id="VirtualWorldContent">

### Virtual World Assets

Some art pieces can even be included in the virtual worlds, such as:

- 2d visual art to be included in world
- background music or sound-effects to immerse players
- 3d models of a large sculpture or project

To read more about this type of content, check out the [artistic volunteer section](./volunteers.md#Artists)
</div>
