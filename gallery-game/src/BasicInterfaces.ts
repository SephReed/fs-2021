export interface IPos {
  x: number;
  y: number;
}

export interface IHeading {
  dx: number;
  dy: number;
}