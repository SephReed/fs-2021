# Guide: Alt Space

### Availability

Cost: Free

- ❌ Web Client
- ✅ Windows
- ⚠️ MacOS (Beta)
- ❌ iOS
- ❌ Android
- ✅ Oculus

-------

<div id="AltspaceInstall">

### Installation

If you have a VR Headset:
  - For PC:  [https://altvr.com/get-altspacevr/](https://altvr.com/get-altspacevr/)
  - For Oculus: [https://www.oculus.com/experiences/rift/1072303152793390/](https://www.oculus.com/experiences/rift/1072303152793390/)

If you have a PC and want to play in "2D Mode":
  - For Windows: [https://www.microsoft.com/en-us/p/altspacevr/9nvr7mn2fchq?activetab=pivot:overviewtab](https://www.microsoft.com/en-us/p/altspacevr/9nvr7mn2fchq?activetab=pivot:overviewtab)
  - For MacOS (buggy & beta version): [https://altvr.com/altspacevr-mac/](https://altvr.com/altspacevr-mac/)

</div>

-------

<div id="AltspaceWorld">

### The World

If you know what you are doing, you can access the event world here: [http://howdyfuckers.online/portal/altspace](http://howdyfuckers.online/portal/altspace)

For a more-detailed guide on getting started with the Windows 2D version, read on...

</div>

-------

<div id="AltspaceWindows2D">

### Windows 2D Version - Getting Started

- Install AltspaceVR from the Windows store by following this link: [https://www.microsoft.com/en-us/p/altspacevr/9nvr7mn2fchq?activetab=pivot:overviewtab](https://www.microsoft.com/en-us/p/altspacevr/9nvr7mn2fchq?activetab=pivot:overviewtab)
- Once installed, go ahead and launch it
- Accept the "Terms of Service"
  - ![AltspaceVR 2D TOS](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-altspace--2d-tos.jpg)
- Accept any other permissions that pop up
  - ![AltspaceVR 2D Permissions](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-altspace--2d-perms.jpg)
- Click "Sign In" and sign in using your Microsoft account
  - ![AltspaceVR 2D Sign In](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-altspace--2d-signin.jpg)
- Once in the lobby, you can customize your avatar if you like, and when you're ready, click "Enter Code" on the left
  - ![AltspaceVR 2D Lobby](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-altspace--2d-lobby.jpg)
- Enter `OAB052`
  - ![AltspaceVR 2D Code](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-altspace--2d-code.png)
- Go ahead and add this world to your favorites
  - ![AltspaceVR 2D Favorite](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-altspace--2d-favorite.png)
- When you're ready to join, click "Enter"
  - ![AltspaceVR 2D Howdy](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-altspace--2d-howdy.png)
- Play time!
  - ![AltspaceVR 2D Play](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-altspace--2d-play.png)

</div>
