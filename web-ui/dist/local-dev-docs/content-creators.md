# Content Creators

If you have any form of content whatsoever you'd like to share in the virtual event, we're 
here to figure out how to make it happen.  Here's some ideas to get your juices flowing.


------

<div id="IRL">

### IRL / Meatspace

Hopefully you know about the "Burner Art Safari" already, but if not the basic idea is this: 
1. put something artistic out in public
2. register its location so it can be put on the map

[You can read more and register here](#)
</div>

------

<div id="Streaming">

### Streaming / Performances

There will be an event Discord where any audio or video can be shared, a Twitch stream for
spotlighting the best of current audio/video, and an internet radio stream for audio 
only.  Anyone can share on any platform they want at any time, but it'll make it easier
for our "station managers" if we know who's doing what and when.

Some performance types include:
- DJs
- VJs: there will be audio djs without any video to go along
- Performers: you can even stream a zoom call if your performance is interactive!
- Streamers: share live footage of cool stuff, virtual or irl

[Share your performance details here](#)
</div>

------

<div id="Gallery">

### Online Gallery Stuff

As well as having streams live broadcasting, there will also be a more "peruse as you feel" gallery 
aspect.  Pretty much anything that can be shared online is welcome here.  Think of it as the front
lawn of your burner camp.

Some gallery types include:
- Visual Art
- Music
- Interactive Web Stuff 

[Share your gallery details here](#)
</div>

--------

<div id="VirtualEvents">

### Hosting Other Virtual Events

If you'd like to host a gaming event, or alternate virtual space event, just let us know!  We'll
put it on the event schedule, and try to get a streamer there.

[Share your virtual event details here](#)
</div>

--------

<div id="ContentVolunteer">

### Virtual World Content

The virtual worlds can use all sort of help to be spruced up.  Including:

- 2d visual art to be included in world
- background music or sound-effects to immerse players

To read more about this type of content, check out the [digital volunteer section](./volunteers.md)
</div>
