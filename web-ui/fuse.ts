import { fusebox, sparky } from 'fuse-box';
import { join } from "path";


 
const fuse = fusebox({
  entry: './src/App.ts',
  devServer: true,
  cache: false,
  webIndex: {
    template: "./index.html"
  },
  target: "browser",
  dependencies: {
    include: ['tslib'],
  },
  watcher: {
    root: join(__dirname, ".."),
    include: [
      join(__dirname, "./src"),
      join(__dirname, "../docs"), 
      join(__dirname, "../gallery-game"), 
    ],
  },
  // stylesheet: {
  //   autoImport: [{
  //     file: "../../editor-ui/src/styleVars.scss"
  //   }]
  // }
});

fuse.runDev();

class Context {
  isProduction;
  runServer;
}
const { task, src } = sparky<Context>(Context);

task("default", async () => {
  await src("../docs/**/**.**")
    .dest("./dist/local-dev-docs/", "/docs/")
    .exec();

  await src("./assets/**/**.**")
    .dest("./dist/assets/", "/assets/")
    .exec();

  // await src("./src/pages/Articles/md/**/**.md")
  //   .dest("./dist/articles/", "/Articles/md/")
  //   .exec();
});