# Getting Connected

There are many different ways to get connected, here's a few

[Going on the Art Safari](./guides/art-safari.md)

[Discord - The main chat center](./guides/discord.md)

[Minecraft - The creativity, building, and burning virtual space](./guides/minecraft.md)

[Altspace - The VR dancing and chatting space](./guides/altspace.md)
