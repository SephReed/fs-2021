(function() {
  var f = window.__fuse = window.__fuse || {};
  var modules = f.modules = f.modules || {}; f.dt = function (x) { return x && x.__esModule ? x : { "default": x }; };

  f.bundle = function(collection, fn) {
    for (var num in collection) {
      modules[num] = collection[num];
    }
    fn ? fn() : void 0;
  };
  f.c = {};
  f.r = function(id) {
    var cached = f.c[id];
    if (cached) return cached.m.exports;
    var module = modules[id];
    if (!module) {
      
      throw new Error('Module ' + id + ' was not found');
    }
    cached = f.c[id] = {};
    cached.exports = {};
    cached.m = { exports: cached.exports };
    module(f.r, cached.exports, cached.m);
    return cached.m.exports;
  }; 
})();
__fuse.bundle({

// src/App.ts @1
1: function(__fusereq, exports, module){
exports.__esModule = true;
__fusereq(3);
var helium_ui_1 = __fusereq(5);
var DocsNav_1 = __fusereq(6);
var Docs_1 = __fusereq(7);
var LandingPage_1 = __fusereq(8);
var helium_ui_tweener_1 = __fusereq(9);
var AppState_1 = __fusereq(10);
var PortalPage_1 = __fusereq(11);
window["RobinHood-License for helium-sdx @ 50M/2K"] = "resolved";
const view = new helium_ui_1.Source();
helium_ui_1.deriverSwitch({
  watch: () => AppState_1.FS.route.id,
  responses: [{
    match: "survival-guide",
    action: () => view.set(Docs_1.renderDocs())
  }, {
    test: async val => val.startsWith("portal"),
    action: () => view.set(PortalPage_1.renderPortalPage())
  }, {
    test: "DEFAULT_RESPONSE",
    action: () => view.set(LandingPage_1.renderLandingPage())
  }]
});
const app = renderApp();
helium_ui_1.onDomReady(() => helium_ui_1.append(document.body, app));
function renderApp() {
  setTimeout(() => {
    !AppState_1.FS.route.id.startsWith("portal") && AppState_1.FS.pointy.show();
  }, 1500);
  const showSidebar = new helium_ui_1.Source(true);
  let cloudHowdy;
  return helium_ui_1.div("App", {
    ddxClass: () => [showSidebar.get() && "--showSidebar", AppState_1.FS.route.id === "survival-guide" && "--hasSidebar"]
  }, [AppState_1.FS.pointy.domNode, helium_ui_1.div("Background", [helium_ui_1.div("Grass"), helium_ui_1.div("MrBlueSkies", {
    onPush: () => {
      cloudHowdy = cloudHowdy || new Audio("/assets/audio/Cloud - HowdyFuckers.mp3");
      cloudHowdy.play();
    }
  }, [helium_ui_1.img("Beams", {
    src: "/assets/img/MrBlueSkies_Beams.png"
  }), helium_ui_1.img("Face", {
    src: "/assets/img/MrBlueSkies_Face.png"
  })]), helium_ui_1.img("Cloud large", {
    src: "/assets/img/cloud-large.png"
  }), helium_ui_1.img("Cloud", {
    src: "/assets/img/cloud-small.png"
  })]), helium_ui_1.div("PageContent", [helium_ui_1.div("Topbar", [helium_ui_1.div("IndexLink", {
    onPush: () => showSidebar.set(!showSidebar.get())
  }), helium_ui_1.anchor("EventLogo", {
    href: "/"
  }, "Howdy Fuckers! 2021")]), helium_ui_1.div("Sidebar", helium_ui_tweener_1.tweener(() => {
    if (AppState_1.FS.route.id === "survival-guide") {
      return [helium_ui_1.anchor("EventLogo", {
        href: "/"
      }, [helium_ui_1.img({
        src: "/assets/img/howdy-fuckers-logo.png"
      })]), DocsNav_1.renderDocsNav()];
    }
    return null;
  })), helium_ui_1.div("MainContent", {
    onPush: () => showSidebar.set(false)
  }, () => view.get())])]);
}

},

// src/app.scss @3
3: function(__fusereq, exports, module){
__fusereq(4)("src/app.scss","html, body {\n  margin: 0px;\n  padding: 0px;\n  font-family: Arial, Helvetica, sans-serif; }\n\n*, *::before, *::after {\n  box-sizing: border-box; }\n\nhtml {\n  background: linear-gradient(-1deg, #61b935, #bb6223 10%, #93d2ff 10%, #007eff 100%);\n  background-color: #42a3ff;\n  overflow-x: hidden; }\n\n@font-face {\n  font-family: Ranchers;\n  src: url(\"/assets/fonts/Ranchers/Ranchers-Regular.ttf\"); }\n\nbody {\n  font-size: 12px; }\n  @media (min-width: 350px) {\n    body {\n      font-size: 13px; } }\n  @media (min-width: 450px) {\n    body {\n      font-size: 14px; } }\n  @media (min-width: 600px) {\n    body {\n      font-size: 15px; } }\n  @media (min-width: 800px) {\n    body {\n      font-size: 16px; } }\n  @media (min-width: 1000px) {\n    body {\n      font-size: 17px; } }\n  @media (min-width: 1200px) {\n    body {\n      font-size: 18px; } }\n  @media (min-width: 1400px) {\n    body {\n      font-size: 19px; } }\n  @media (min-width: 1600px) {\n    body {\n      font-size: 20px; } }\n\n.btn {\n  position: relative;\n  display: inline-flex;\n  width: fit-content;\n  cursor: pointer;\n  font-size: 1em;\n  align-items: center;\n  justify-content: center;\n  text-align: center;\n  z-index: 1;\n  padding: 0.5em 2em;\n  min-width: 8em;\n  border-radius: 0.75em;\n  border: none;\n  color: #0368d2;\n  text-decoration: none; }\n  .btn:hover {\n    text-decoration: none; }\n    .btn:hover::before {\n      filter: drop-shadow(0px 0px 6px #f7941e); }\n  .btn::before, .btn::after {\n    content: \"\";\n    display: block;\n    position: absolute;\n    z-index: -1; }\n  .btn::before {\n    border-radius: 0.8em;\n    top: 0px;\n    left: 0px;\n    right: 0px;\n    bottom: 0px;\n    background: linear-gradient(to bottom right, #f7941e, #9a5b10);\n    box-shadow: 0px 0px 6px #f7941e; }\n  .btn::after {\n    top: 4px;\n    left: 4px;\n    right: 4px;\n    bottom: 4px;\n    background-color: white;\n    border-radius: 1em; }\n\n@media (max-width: 750px) {\n  div.App > .Background > div.MrBlueSkies {\n    left: 2vw; }\n  div.App > div.PageContent > div.Topbar {\n    display: flex; }\n  div.App > div.PageContent > div.Sidebar {\n    position: fixed;\n    transform: translateX(-100%);\n    opacity: 0;\n    padding-top: 3em; }\n  div.App.--hasSidebar > div.PageContent > .Topbar {\n    transform: translateY(0%); }\n  div.App.--hasSidebar > div.PageContent > .MainContent::after {\n    display: block;\n    content: \"\";\n    position: absolute;\n    pointer-events: none;\n    top: 0px;\n    left: 0px;\n    right: 0px;\n    bottom: 0px;\n    background: black;\n    opacity: 0;\n    transition: opacity 0.25s ease-out; }\n  div.App.--hasSidebar.--showSidebar > div.PageContent > .Sidebar {\n    transform: translateX(0%);\n    opacity: 1; }\n  div.App.--hasSidebar.--showSidebar > div.PageContent > .MainContent::after {\n    opacity: 0.7; } }\n\n@media (min-width: 750px) {\n  div.App.--hasSidebar > .Background > .MrBlueSkies {\n    left: 15vw; } }\n\ndiv.App > .Background {\n  position: fixed;\n  height: 100vh;\n  width: 100vw;\n  background: linear-gradient(-1deg, #02a652, #f7941e 10%, #93d2ff 10%, #007eff 100%); }\n  div.App > .Background > .Grass {\n    position: absolute;\n    bottom: 0px;\n    width: 100%;\n    height: 8em;\n    background-image: url(\"/assets/img/grass.png\");\n    background-size: 110% 100%;\n    background-position: 20% 0%; }\n  div.App > .Background > .Cloud {\n    position: absolute;\n    top: 1em;\n    height: 20vh;\n    pointer-events: none;\n    animation: Float-By 120s linear;\n    animation-iteration-count: infinite;\n    animation-delay: -180s; }\n\n@keyframes Float-By {\n  0% {\n    left: 10vw; }\n  50% {\n    left: 100vw; }\n  51% {\n    visibility: hidden;\n    left: 100vw; }\n  52% {\n    visibility: hidden;\n    left: -100vh; }\n  100% {\n    left: 10vw; } }\n    div.App > .Background > .Cloud.large {\n      animation-delay: -30s;\n      animation-duration: 240s; }\n  div.App > .Background > .MrBlueSkies {\n    position: absolute;\n    left: 2vw;\n    transition: left 5s ease-out;\n    animation: Floaty-Bounce 2s linear;\n    animation-iteration-count: infinite; }\n\n@keyframes Floaty-Bounce {\n  0% {\n    top: 0vh; }\n  50% {\n    top: 0.5vh; }\n  100% {\n    top: 0vh; } }\n    div.App > .Background > .MrBlueSkies > .Face {\n      position: absolute;\n      top: 42%;\n      left: 54%;\n      transform: translate(-50%, -50%);\n      height: 69%; }\n    div.App > .Background > .MrBlueSkies > .Beams {\n      height: 24vh;\n      animation: Spin 10s linear;\n      animation-iteration-count: infinite; }\n\n@keyframes Spin {\n  0% {\n    transform: rotate(0deg); }\n  100% {\n    transform: rotate(360deg); } }\n\ndiv.App > .PageContent {\n  display: grid;\n  grid-template-columns: max-content 1fr;\n  min-height: 100vh;\n  width: 100vw;\n  cursor: url(\"/assets/img/texas-tiny.png\") 10 0, default;\n  pointer-events: none; }\n  div.App > .PageContent > .Topbar, div.App > .PageContent > .Sidebar, div.App > .PageContent > .MainContent > * > * {\n    pointer-events: all; }\n  div.App > .PageContent .EventLogo {\n    display: block;\n    font-family: Ranchers;\n    text-align: center;\n    color: #f7941e;\n    text-decoration: none;\n    font-size: 1.5em;\n    padding: 0.25em 0px; }\n  div.App > .PageContent > .Topbar {\n    display: none;\n    justify-content: space-between;\n    align-items: center;\n    padding: 0px 0.5em;\n    position: fixed;\n    top: 0px;\n    width: 100vw;\n    z-index: 2;\n    background: #222222;\n    transition: transform 0.25s ease-out;\n    transform: translateY(-100%); }\n    div.App > .PageContent > .Topbar > .IndexLink {\n      height: 2.5em;\n      width: 2em;\n      background: url(\"/assets/img/icon-hamburger.png\");\n      background-size: contain;\n      background-repeat: no-repeat;\n      background-position: center;\n      filter: invert(1); }\n  div.App > .PageContent > .Sidebar {\n    background: #222;\n    color: white;\n    max-width: 20em;\n    box-shadow: 1px 1px 12px #1116;\n    position: sticky;\n    z-index: 1;\n    top: 0px;\n    overflow-y: auto;\n    height: 100vh;\n    transition: 0.25s ease-out;\n    transition-property: opacity, transform; }\n    div.App > .PageContent > .Sidebar .EventLogo > img {\n      height: 7em;\n      margin: auto; }\n  div.App > .PageContent > .MainContent {\n    position: relative;\n    cursor: inherit; }\n    div.App > .PageContent > .MainContent > .Tweener {\n      height: 100%;\n      width: 100%; }\n    div.App > .PageContent > .MainContent .DocList {\n      display: flex;\n      flex-direction: column;\n      align-items: center;\n      width: 100%;\n      max-width: Min(50em, 100vw);\n      margin: auto;\n      padding: 0px 0.5em; }\n    div.App > .PageContent > .MainContent .Doc {\n      margin: 4em 1em;\n      background: #f9f3e9;\n      padding: 0.5em;\n      border-radius: 0.25em;\n      width: 100%;\n      max-width: Min(50em, 100vw);\n      min-height: 4em;\n      box-shadow: 1px 1px 10px #ffdea7;\n      padding-bottom: 3em;\n      padding-left: 1.5em;\n      line-height: 1.75em;\n      opacity: 0; }\n      div.App > .PageContent > .MainContent .Doc.--loaded {\n        animation: Fade-In 0.25s ease-out;\n        animation-fill-mode: forwards; }\n\n@keyframes Fade-In {\n  0% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n      div.App > .PageContent > .MainContent .Doc #Overview ul > li {\n        margin-bottom: 5em; }\n      div.App > .PageContent > .MainContent .Doc h1, div.App > .PageContent > .MainContent .Doc h2, div.App > .PageContent > .MainContent .Doc h3, div.App > .PageContent > .MainContent .Doc h4, div.App > .PageContent > .MainContent .Doc h5, div.App > .PageContent > .MainContent .Doc p, div.App > .PageContent > .MainContent .Doc ul, div.App > .PageContent > .MainContent .Doc ol {\n        margin: 0px; }\n      div.App > .PageContent > .MainContent .Doc ul, div.App > .PageContent > .MainContent .Doc ol {\n        padding-left: 1.5em; }\n      div.App > .PageContent > .MainContent .Doc li {\n        margin-bottom: 0.5em; }\n      div.App > .PageContent > .MainContent .Doc strong {\n        color: #026934; }\n      div.App > .PageContent > .MainContent .Doc > .helium-md > div {\n        position: relative;\n        padding-left: 1em; }\n        div.App > .PageContent > .MainContent .Doc > .helium-md > div::before {\n          display: block;\n          content: \"\";\n          position: absolute;\n          top: 2em;\n          bottom: 1em;\n          left: 0.5em;\n          width: 3px;\n          transform: translateX(-50%);\n          background: linear-gradient(48deg, transparent, rgba(191, 50, 7, 0.5));\n          border-radius: 1em; }\n        div.App > .PageContent > .MainContent .Doc > .helium-md > div img {\n          position: relative;\n          display: block;\n          max-width: Min(40em, 100%);\n          border-radius: 0.5em;\n          overflow: hidden;\n          border: 1px solid #e0c8a0; }\n        div.App > .PageContent > .MainContent .Doc > .helium-md > div h3 {\n          position: relative;\n          left: -0.7em;\n          color: #bf3207;\n          font-size: 1.5em;\n          margin-bottom: 0.25em; }\n      div.App > .PageContent > .MainContent .Doc h1 {\n        position: relative;\n        font-family: Ranchers;\n        letter-spacing: 0.1em;\n        left: -0.25em;\n        margin-bottom: 0.75em;\n        margin-top: 0.5em;\n        color: #0368d2; }\n      div.App > .PageContent > .MainContent .Doc h1 a, div.App > .PageContent > .MainContent .Doc h3 a {\n        opacity: 0;\n        text-decoration: none;\n        transition: opacity 0.5s;\n        margin-left: 0.5em; }\n        div.App > .PageContent > .MainContent .Doc h1 a.RawLink, div.App > .PageContent > .MainContent .Doc h3 a.RawLink {\n          display: inline-block;\n          margin-left: 0.5em;\n          height: 1em;\n          width: 1em;\n          background-image: url(\"/assets/img/icon-edit.png\");\n          background-size: contain;\n          background-repeat: no-repeat;\n          background-position: center;\n          vertical-align: bottom; }\n      div.App > .PageContent > .MainContent .Doc h1:hover a, div.App > .PageContent > .MainContent .Doc h3:hover a {\n        opacity: 1; }\n        div.App > .PageContent > .MainContent .Doc h1:hover a:hover, div.App > .PageContent > .MainContent .Doc h3:hover a:hover {\n          opacity: 0.5; }\n      div.App > .PageContent > .MainContent .Doc hr {\n        border: none;\n        height: 1.25em;\n        background: #182127;\n        margin: 3em 1em;\n        border-radius: 2em; }\n      div.App > .PageContent > .MainContent .Doc a, div.App > .PageContent > .MainContent .Doc a:visited {\n        color: #0071b5; }\n        div.App > .PageContent > .MainContent .Doc a:hover, div.App > .PageContent > .MainContent .Doc a:visited:hover {\n          opacity: 0.5; }\n\n/*# sourceMappingURL=/resources/css/792d9fa4.css.map */")
},

// src/components/DocsNav/DocsNav.ts @6
6: function(__fusereq, exports, module){
exports.__esModule = true;
__fusereq(18);
var helium_ui_1 = __fusereq(5);
var AppState_1 = __fusereq(10);
function renderDocsNav() {
  return helium_ui_1.div("DocsNav", [helium_ui_1.div("DocItemList", () => {
    return AppState_1.FS.docs.docList.renderMap(docItem => renderDocItem(docItem));
  })]);
}
exports.renderDocsNav = renderDocsNav;
function renderDocItem(item) {
  return helium_ui_1.div({
    class: ["DocItem", ("href" in item) ? "section" : "sheet"],
    innards: [helium_ui_1.anchor("Label", {
      onPush: () => AppState_1.FS.selection.docItem = item.name,
      innards: item.name,
      href: () => ("hash" in item) ? item.hash : "#"
    }), ("sections" in item) && helium_ui_1.div("SubDocs", item.sections.renderMap(subDoc => renderDocItem(subDoc)))]
  });
}
exports.renderDocItem = renderDocItem;

},

// src/pages/Docs/Docs.ts @7
7: function(__fusereq, exports, module){
exports.__esModule = true;
var helium_md_1 = __fusereq(19);
var helium_ui_1 = __fusereq(5);
var AppState_1 = __fusereq(10);
function renderDocs() {
  return helium_ui_1.div("DocList", () => {
    const map = new Map();
    const tryAddDoc = doc => {
      if (doc.filePath && map.has(doc.filePath.replace(/#.+/, "")) === false) {
        map.set(doc.filePath, doc);
      }
      if (doc.sections) {
        doc.sections.forEach(tryAddDoc);
      }
    };
    AppState_1.FS.docs.docList.forEach(tryAddDoc);
    setTimeout(() => {
      const hash = location.hash;
      const target = hash && helium_ui_1.byId(hash.slice(1));
      console.log("target", target);
      target && target.scrollIntoView();
    }, 1000);
    return Array.from(map.values()).map(doc => {
      const docEl = helium_ui_1.div("Doc", {
        id: doc.hash.slice(1)
      }, () => {
        const docText = helium_ui_1.awaitSource("doc", () => AppState_1.FS.docs.fetchDocText(doc.filePath));
        if (!docText) {
          return "Loading Doc...";
        }
        if (docEl) {
          helium_ui_1.addClass("--loaded", docEl);
        }
        ;
        const out = helium_md_1.md(docText);
        const mainHeader = out.querySelector("h1");
        mainHeader && helium_ui_1.append(mainHeader, [helium_ui_1.anchor("HashLink", {
          href: doc.hash
        }, "#"), helium_ui_1.anchor("RawLink", {
          target: "_blank",
          href: `https://gitlab.com/SephReed/fs-2021/-/tree/master/docs${doc.filePath.slice(1)}`
        })]);
        const sections = out.querySelectorAll("div");
        if (sections) {
          sections.forEach(section => {
            const subHeader = section.querySelector("h3");
            subHeader && helium_ui_1.append(subHeader, [helium_ui_1.anchor("HashLink", {
              href: `#${section.id}`
            }, "#")]);
          });
        }
        const links = out.querySelectorAll("a");
        links.forEach(link => {
          const href = link.getAttribute("href");
          const externalLink = (/http/i).test(href);
          if (externalLink) {
            return link.target = "_blank";
          }
          const hashMatch = href.match(/#[\w-]+/g);
          if (hashMatch) {
            return link.href = hashMatch[0];
          }
          const fileNameMatch = href.match(/[\w-]+(?=\.md)/gi);
          if (fileNameMatch) {
            return link.href = `#${fileNameMatch[0]}`;
          }
          console.warn(`${href} matches none of the link use cases`);
        });
        if (doc.hash === "#volunteers" || doc.hash === "#creators-and-events") {
          links.forEach(link => {
            if (link.href.includes("forms.gle") || link.href.includes("announcements/art-safari")) {
              link.classList.add("btn");
            }
          });
        }
        return out;
      });
      return docEl;
    });
  });
}
exports.renderDocs = renderDocs;

},

// src/pages/LandingPage/LandingPage.ts @8
8: function(__fusereq, exports, module){
exports.__esModule = true;
__fusereq(20);
var helium_ui_1 = __fusereq(5);
let howdyFuckersAudio;
function renderLandingPage() {
  howdyFuckersAudio = howdyFuckersAudio || new Audio("/assets/audio/Clovis - HowdyFuckers.mp3");
  howdyFuckersAudio.play();
  return helium_ui_1.div("LandingPage", [helium_ui_1.div("Howdy", {
    onPush: () => howdyFuckersAudio.play(),
    innards: helium_ui_1.img({
      src: "/assets/img/howdy-fuckers-logo.png"
    })
  }), helium_ui_1.div("EventDate", "Virtual Burn :: May 27th - 31st"), helium_ui_1.br(1), helium_ui_1.anchor("GuideLink btn", {
    href: "./portal"
  }, `Click to enter "The Portal"`)]);
}
exports.renderLandingPage = renderLandingPage;

},

// src/state/AppState.ts @10
10: function(__fusereq, exports, module){
exports.__esModule = true;
var helium_ui_1 = __fusereq(5);
var Pointy_1 = __fusereq(22);
var DocsState_1 = __fusereq(23);
var RouteState_1 = __fusereq(24);
class _AppState {
  constructor() {
    this.selection = helium_ui_1.Sourcify({
      docItem: null
    });
    this.docs = new DocsState_1.DocsState();
    this.route = new RouteState_1.RouteState();
    this.pointy = new Pointy_1.Pointy();
  }
}
exports.FS = new _AppState();

},

// src/pages/Portal/PortalPage.ts @11
11: function(__fusereq, exports, module){
exports.__esModule = true;
__fusereq(25);
var helium_ui_1 = __fusereq(5);
var helium_md_1 = __fusereq(19);
var AppState_1 = __fusereq(10);
const portalState = helium_ui_1.Sourcify({
  signedWaiver: false
});
helium_ui_1.syncWithLocalStorage("portalState", portalState);
function renderPortalPage() {
  AppState_1.FS.pointy.hide();
  const portals = [{
    id: "principles",
    link: [helium_ui_1.div("Image", {
      style: `background-image: url("/assets/img/howdy-fuckers-logo.png"); background-color: #02a652`
    }), helium_ui_1.div("Name", "Principles")],
    info: [helium_ui_1.div("Title", "Basic Principles"), helium_ui_1.div("WhatFer", "Participant Expectations"), helium_md_1.md(`
          1. **Decommodification**:  No for-profit ads.  No exchanging money.
          2. **Anti-Abuse**: Respect any boundaries that have been set.  The following boundaries are default:
            - *do not* publish visual records of a persons physical face *without their consent*
            - *do not* expose a persons private information (Dox) *without their consent*
            - *do not* push yourself onto someone romantically *without their consent*
            - *do not* express racial, gendered, or other non-character based observations of a person *without their consent*
            - basically, start off polite and work your way into being more... raw
          3. **Civic Responsibility**: Help others out.  Be the tech support you wish to see in the world.
          4. **Radical Inclusion**: There is no normal.  Always try to find a way to let people be who they truly are.
          5. **Immediacy**: Express yo-self.  Ease around your own personal identity.
            - the more people who let themselves be, the more people will feel comfortable being themselves
          6. **Participation**: If you want to share something, we'll find a place for it.  Everything you see is done by volunteers.
          7. **"Family Friendly"**: Please keep especially lewd behavior in private channels.
            - there is no way (nor attempt) to keep minors out
            - if you want a private channel, just ask
          8. **Edification**: Please do not share the direct links found *in* this portal.  Instead, share links *to* this portal.
            - we want people to have read these principles before entering any space
            - also, there's helpful instructions included, and they'll see all the other stuff they can do
          9. **The Un-principle**: Don't treat these principals as a weapon.  People fuck up.  People change.  Every "rule" has an exception.
        `), helium_ui_1.br(2), helium_ui_1.div({
      onPush: () => {
        console.log("HEY");
        portalState.signedWaiver = !portalState.signedWaiver;
      }
    }, [helium_ui_1.el("input", {
      attr: {
        type: "checkbox",
        name: "waiver"
      },
      ddx: ref => ref.checked = portalState.signedWaiver
    }), helium_ui_1.el("label", {
      attr: {
        for: "waiver"
      }
    }, "I've read the stuff above")])]
  }, {
    id: "discord",
    link: [helium_ui_1.div("Image", {
      style: `background-image: url("/assets/img/discord-thumb.png")`
    }), helium_ui_1.div("Name", "Discord")],
    info: [helium_ui_1.div("Title", "Discord"), helium_ui_1.div("WhatFer", "Main Communications and Support Hub"), helium_ui_1.img({
      src: "/assets/img/discord-thumb.png"
    }), helium_ui_1.anchor("btn", {
      href: `https://discord.gg/cwAdhQ5eDt`
    }, "https://discord.gg/cwAdhQ5eDt"), helium_ui_1.anchor({
      href: `/survival-guide#discord`
    }, "Click here for help/instructions")]
  }, {
    id: "minecraft",
    link: [helium_ui_1.div("Image", {
      style: `background-image: url("/assets/img/minecraft-thumb.png")`
    }), helium_ui_1.div("Name", "Minecraft")],
    info: [helium_ui_1.div("Title", "Minecraft"), helium_ui_1.div("WhatFer", "Building and Burning"), helium_ui_1.img({
      src: "/assets/img/minecraft-thumb.png"
    }), helium_ui_1.anchor("btn", {
      href: `/survival-guide#minecraft`
    }, "Click here for help/instructions"), helium_ui_1.table([["Java Edition:", "howdy-fuckers.mc.gg"], ["Bedrock Edition:", "107.175.178.34 port 62402"]])]
  }, {
    id: "topia",
    link: [helium_ui_1.div("Image", {
      style: `background-image: url("/assets/img/topia-thumb.png")`
    }), helium_ui_1.div("Name", "Topia")],
    info: [helium_ui_1.div("Title", "Topia"), helium_ui_1.div("WhatFer", "Art Gallery and Hangout Space"), helium_ui_1.img({
      src: "/assets/img/topia-thumb.png"
    }), helium_ui_1.anchor("btn", {
      href: `https://topia.io/howdy-fuckers`
    }, "https://topia.io/howdy-fuckers"), helium_ui_1.anchor({
      href: `/survival-guide#topia`
    }, "Click here for help/instructions")]
  }, {
    id: "altspace",
    link: [helium_ui_1.div("Image", {
      style: `background-image: url("https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-altspace--2d-play.png")`
    }), helium_ui_1.div("Name", "AltSpace")],
    info: [helium_ui_1.div("Title", "Altspace"), helium_ui_1.div("WhatFer", "Dance Parties and Virtual Hangout"), helium_ui_1.img({
      src: "https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-altspace--2d-play.png"
    }), helium_ui_1.anchor("btn", {
      href: `/survival-guide#altspace`
    }, "Click here for help/instructions"), helium_ui_1.div([helium_ui_1.anchor({
      href: `https://account.altvr.com/worlds/1742621755333149622/spaces/1742915064203051950`
    }, "World link"), " -- World Code: OAB052"])]
  }, {
    id: "art-safari",
    link: [helium_ui_1.div("Image", {
      style: `background-image: url("/assets/img/art-safari-thumb.png")`
    }), helium_ui_1.div("Name", "Art Safari")],
    info: [helium_ui_1.div("Title", "Art Safari"), helium_ui_1.div("WhatFer", "Real World Outdoor Art Drive"), {
      hackableHTML: `<iframe src="https://www.google.com/maps/d/embed?mid=1RIAE7wbXFCqEEuhVHqTTO7JY3AUwvhDz" width="100%" height="400px"></iframe>`
    }, helium_ui_1.anchor("btn", {
      href: `https://www.google.com/maps/d/u/0/viewer?mid=1RIAE7wbXFCqEEuhVHqTTO7JY3AUwvhDz&ll=30.274643949789976%2C-97.75821316064753&z=10`
    }, "See full map")]
  }, {
    id: "calendar",
    link: [helium_ui_1.div("Image", {
      style: `background-image: url("/assets/img/calendar-thumb.png")`
    }), helium_ui_1.div("Name", "Calendar")],
    info: [helium_ui_1.div("Title", "Calendar"), helium_ui_1.div("WhatFer", "Schedule of Burns and Other Events"), {
      hackableHTML: `<iframe src="https://calendar.google.com/calendar/embed?src=howdy.fawker%40gmail.com" style="border: 0" width="100%" height="400" frameborder="0" scrolling="no"></iframe>`
    }, helium_ui_1.anchor("btn", {
      href: `https://calendar.google.com/calendar/embed?src=howdy.fawker%40gmail.com
        `
    }, "See full calendar")]
  }, {
    id: "kflip",
    link: [helium_ui_1.div("Image", {
      style: `background-image: url("/assets/img/kflip.png")`
    }), helium_ui_1.div("Name", "KFlip Radio")],
    info: [helium_ui_1.div("Title", "KFlip Radio"), helium_ui_1.div("WhatFer", "The 24/7 Radio for this Burn"), helium_ui_1.img({
      src: "/assets/img/kflip.png"
    }), helium_ui_1.anchor("btn", {
      href: `http://www.kflipcamp.org/`
    }, "Tune in")]
  }];
  const portalsById = new Map();
  portals.forEach(portal => portalsById.set(portal.id, portal));
  function getViewId() {
    return AppState_1.FS.route.id.split(".")[1];
  }
  function renderPortalLink(portal) {
    return helium_ui_1.anchor("Link", {
      ddxClass: () => [getViewId() === portal.id && "--selected", portal.id !== "principles" && !portalState.signedWaiver && "--no-access"],
      href: `/portal/${portal.id}`,
      innards: portal.link
    });
  }
  return helium_ui_1.div("PortalPage", [helium_ui_1.div("Links", [portalsById.get("principles"), portalsById.get("discord"), portalsById.get("minecraft"), portalsById.get("art-safari")].map(renderPortalLink)), helium_ui_1.div("Info", () => {
    const viewId = getViewId();
    if (!viewId || viewId !== "principles" && !portalState.signedWaiver) {
      return portalsById.get("principles").info;
    }
    const portal = portalsById.get(viewId);
    return portal && portal.info;
  }), helium_ui_1.div("Links", [portalsById.get("topia"), portalsById.get("altspace"), portalsById.get("calendar"), portalsById.get("kflip")].map(renderPortalLink))]);
}
exports.renderPortalPage = renderPortalPage;

},

// src/components/DocsNav/docsNav.scss @18
18: function(__fusereq, exports, module){
__fusereq(4)("src/components/DocsNav/docsNav.scss","div.DocsNav {\n  margin-bottom: 4em; }\n  div.DocsNav a.Label {\n    display: block;\n    color: inherit;\n    text-decoration: none;\n    padding: 0.25em 0.5em; }\n    div.DocsNav a.Label:hover {\n      background: #bf3207; }\n  div.DocsNav .SubDocs .Label {\n    padding-left: 1.5em; }\n  div.DocsNav .SubDocs .SubDocs .Label {\n    padding-left: 3em; }\n\n/*# sourceMappingURL=/resources/css/027d701f1.css.map */")
},

// src/pages/LandingPage/landingPage.scss @20
20: function(__fusereq, exports, module){
__fusereq(4)("src/pages/LandingPage/landingPage.scss","div.LandingPage {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  text-align: center;\n  min-height: 100vh;\n  width: 100vw; }\n  div.LandingPage > .Howdy {\n    font-family: Ranchers;\n    animation: PopIn 1s ease-in;\n    animation-fill-mode: forwards;\n    font-size: 7em;\n    color: white;\n    margin-top: 18vh;\n    -webkit-text-fill-color: white;\n    -webkit-text-stroke-color: #964e10;\n    -webkit-text-stroke-width: 2px; }\n\n@keyframes PopIn {\n  0% {\n    transform: scale(0.05); }\n  50% {\n    transform: scale(1.1); }\n  80% {\n    transform: scale(0.9); }\n  100% {\n    transform: scale(1); } }\n    div.LandingPage > .Howdy > img {\n      height: 40vh;\n      max-width: 90vw;\n      filter: drop-shadow(0px 0px 10px rgba(0, 0, 0, 0.5)); }\n  div.LandingPage > .EventDate {\n    font-family: Ranchers;\n    color: white;\n    text-shadow: 0px 0px 2px black;\n    letter-spacing: 0.1em; }\n  div.LandingPage > .GuideLink {\n    font-family: Ranchers;\n    letter-spacing: 0.1em; }\n\n/*# sourceMappingURL=/resources/css/29b6e913.css.map */")
},

// src/components/Pointy/Pointy.ts @22
22: function(__fusereq, exports, module){
exports.__esModule = true;
__fusereq(47);
var helium_ui_1 = __fusereq(5);
var helium_md_1 = __fusereq(19);
class Pointy {
  constructor() {
    this.state = helium_ui_1.Sourcify({
      showPointy: false,
      showBubble: true,
      bubbleFullyShown: false,
      fullText: `Howdy Fucker!  I'm Pointy Mc Pointyface!  Looks like you're interested in a virtual burn.
      Well then, come on over and [join the chat](/portal/discord)!
    `.replace(/\s+/g, " ")
    });
    helium_ui_1.deriveAnchorless(() => this.state.showBubble && this.state.fullText && this.state.bubbleFullyShown && helium_ui_1.noDeps(() => this.rollInText()));
  }
  get howdyFuckerAudio() {
    return this._howdyFuckerAudio = this._howdyFuckerAudio || new Audio("/assets/audio/Pointy - HowdyFuckers.mp3");
  }
  show() {
    this.state.showPointy = true;
    setTimeout(() => {
      if (helium_ui_1.noDeps(() => this.state.showPointy)) {
        this.howdyFuckerAudio.play();
      }
    });
  }
  hide() {
    this.state.showPointy = false;
  }
  get domNode() {
    if (this.nodes) {
      return this.nodes.rootNode;
    }
    this.nodes = {};
    this.nodes.rootNode = helium_ui_1.div("PointyContainer", {
      ddxClass: () => [this.state.showPointy && "--showPointy", this.state.showBubble && "--showBubble"]
    }, [helium_ui_1.div("Pointy", {
      onPush: () => this.state.showPointy = false,
      onToucherEnterExit: ev => {
        ev.isEnterEvent && this.howdyFuckerAudio.play();
      }
    }), helium_ui_1.div("BubblePositioner", [this.nodes.speechBubble = helium_ui_1.div("SpeechBubble", {
      on: {
        "animationend": ev => this.state.bubbleFullyShown = this.state.showBubble && this.state.showPointy
      }
    })])]);
    return this.nodes.rootNode;
  }
  clearInterval() {
    if (this.rollInTextInterval) {
      clearInterval(this.rollInTextInterval);
      this.rollInTextInterval = undefined;
    }
  }
  rollInText() {
    const {fullText} = this.state;
    const innards = helium_md_1.md(fullText);
    const textContainers = [];
    const permeate = checkMe => {
      if (checkMe.hasChildNodes()) {
        return checkMe.childNodes.forEach(permeate);
      }
      textContainers.push({
        container: checkMe,
        text: checkMe.textContent
      });
      checkMe.textContent = "";
    };
    permeate(innards);
    this.clearInterval();
    helium_ui_1.setInnards(this.nodes.speechBubble, innards);
    const chunkMatcher = /\s+|\S+/g;
    let skipNext = 0;
    let currentContainer = textContainers.shift();
    const rollInNextChunk = () => {
      let nextChunk;
      while (!nextChunk) {
        if (!currentContainer) {
          return clearInterval();
        }
        const nextMatch = chunkMatcher.exec(currentContainer.text);
        nextChunk = nextMatch && nextMatch[0];
        if (!nextChunk) {
          currentContainer = textContainers.shift();
        }
      }
      let index = 0;
      this.rollInTextInterval = setInterval(() => {
        if (skipNext > 0) {
          return skipNext--;
        }
        const addMe = nextChunk.charAt(index);
        ("!?.,").includes(addMe) && (skipNext = 5);
        currentContainer.container.textContent += addMe;
        index++;
        if (index >= nextChunk.length) {
          clearInterval(this.rollInTextInterval);
          this.rollInTextInterval = undefined;
          rollInNextChunk();
        }
      }, 30);
    };
    rollInNextChunk();
  }
}
exports.Pointy = Pointy;

},

// src/state/DocsState.ts @23
23: function(__fusereq, exports, module){
exports.__esModule = true;
var helium_ui_1 = __fusereq(5);
class DocsState {
  constructor() {
    this.state = helium_ui_1.Sourcify({
      docList: []
    });
    this.docTextPromiseMap = new Map();
  }
  get docList() {
    this.assertDocsFetched();
    return this.state.docList;
  }
  assertDocsFetched() {
    if (this.fetchDocsPromise) {
      return;
    }
    this.fetchDocsPromise = new Promise(async () => {
      const text = await this.fetchDocText("./table-of-contents.md");
      const docItems = [];
      const parentStack = [];
      text.trim().split("\n").forEach(line => {
        const parts = line.split(/(\s*)- *\[([^\]]+)\]\((.+?)(#.+)?\)/);
        const depth = parts[1].length / 2 || 0;
        const hash = parts[4] || `#${parts[3].replace(/(.+\/)|(\.md)/g, "")}`;
        const addMe = {
          name: parts[2],
          filePath: parts[3],
          hash
        };
        console.log(depth, parentStack.length, parts[2]);
        while (parentStack.length > depth) {
          parentStack.pop();
        }
        if (!parentStack.length) {
          parentStack.push(addMe);
          docItems.push(addMe);
          return;
        } else {
          const parent = parentStack[parentStack.length - 1];
          parent.sections = parent.sections || [];
          parent.sections.push(addMe);
          parentStack.push(addMe);
        }
      });
      console.log(docItems);
      this.state.docList = helium_ui_1.Sourcify(docItems);
    });
  }
  async fetchDocText(path) {
    if (this.docTextPromiseMap.has(path) === false) {
      const fullUrl = `https://api.howdyfuckers.online/git-doc/${path.slice(1)}`;
      this.docTextPromiseMap.set(path, (await fetch(fullUrl)).text());
    }
    return this.docTextPromiseMap.get(path);
  }
}
exports.DocsState = DocsState;

},

// src/state/RouteState.ts @24
24: function(__fusereq, exports, module){
exports.__esModule = true;
var helium_ui_1 = __fusereq(5);
helium_ui_1.UrlState.preventRefreshWarningGiven = true;
const RouteConfigs = {
  "survival-guide": {
    test: /\/survival-guide/
  },
  "gallery-game": {
    test: /\/gallery-game/
  },
  "portal": {
    test: "/portal"
  },
  "portal.principles": {
    test: "/portal/principles"
  },
  "portal.discord": {
    test: "/portal/discord"
  },
  "portal.minecraft": {
    test: "/portal/minecraft"
  },
  "portal.topia": {
    test: "/portal/topia"
  },
  "portal.altspace": {
    test: "/portal/altspace"
  },
  "portal.art-safari": {
    test: "/portal/art-safari"
  },
  "portal.calendar": {
    test: "/portal/calendar"
  },
  "portal.kflip": {
    test: "/portal/kflip"
  }
};
class RouteState extends helium_ui_1.HeliumRouteState {
  constructor() {
    super(RouteConfigs);
  }
}
exports.RouteState = RouteState;

},

// src/pages/Portal/portalPage.scss @25
25: function(__fusereq, exports, module){
__fusereq(4)("src/pages/Portal/portalPage.scss","div.PortalPage {\n  min-height: 100vh;\n  width: 100vw;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center; }\n  @media (max-width: 600px) {\n    div.PortalPage > div.Links, div.PortalPage > div.Info {\n      width: 100%; }\n    div.PortalPage > div.Info {\n      border-radius: 0px; } }\n  div.PortalPage > .Links {\n    display: flex;\n    justify-content: space-around;\n    width: 40em;\n    margin: 1em 0px; }\n    div.PortalPage > .Links > .Link {\n      display: flex;\n      flex-direction: column;\n      align-items: center;\n      transition: all ease-out 0.25s;\n      text-decoration: none; }\n      div.PortalPage > .Links > .Link.--selected {\n        filter: drop-shadow(0px 0px 10px gold); }\n        div.PortalPage > .Links > .Link.--selected > .Image {\n          border-color: white; }\n      div.PortalPage > .Links > .Link.--no-access {\n        opacity: 0.5;\n        filter: blur(1px);\n        cursor: not-allowed; }\n      div.PortalPage > .Links > .Link > .Image {\n        border: 2px solid black;\n        border-radius: 40em;\n        overflow: hidden;\n        height: 5em;\n        width: 5em;\n        background-size: cover;\n        background-position: center; }\n      div.PortalPage > .Links > .Link > .Name {\n        background: #06519e;\n        color: white;\n        border-radius: 3em;\n        padding: .125em .5em; }\n  div.PortalPage > .Info {\n    position: relative;\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    width: 40em;\n    background: #f9f3e9;\n    box-shadow: 1px 1px 10px #ffdea7;\n    border-radius: 5em;\n    overflow: hidden;\n    padding: 1em 0px; }\n    div.PortalPage > .Info > .Title {\n      font-size: 1.5em; }\n    div.PortalPage > .Info > .WhatFer {\n      color: #333;\n      font-style: italic;\n      padding-bottom: 0.5em; }\n    div.PortalPage > .Info > img {\n      max-width: 100%; }\n    div.PortalPage > .Info > .btn {\n      margin: 0.5em 0px; }\n    div.PortalPage > .Info .helium-md ol, div.PortalPage > .Info .helium-md ul {\n      margin: 0px; }\n    div.PortalPage > .Info .helium-md ol > li {\n      margin-top: 1em; }\n    div.PortalPage > .Info .helium-md ul > li {\n      margin-top: .25em; }\n\n/*# sourceMappingURL=/resources/css/064dfd6e.css.map */")
},

// src/components/Pointy/pointy.scss @47
47: function(__fusereq, exports, module){
__fusereq(4)("src/components/Pointy/pointy.scss","div.PointyContainer {\n  position: fixed;\n  bottom: 0px;\n  right: 0px;\n  z-index: 2;\n  transition: all ease-out 0.5s;\n  transform: translateX(100%); }\n  div.PointyContainer.--showPointy {\n    transform: translateX(0%); }\n    div.PointyContainer.--showPointy > .Pointy {\n      transform: translate(25%, 5%) rotate(-15deg); }\n      div.PointyContainer.--showPointy > .Pointy:hover {\n        transform: translate(30%, 4%) rotate(-12deg); }\n    div.PointyContainer.--showPointy > .BubblePositioner {\n      opacity: 1; }\n  div.PointyContainer.--showBubble.--showPointy > .BubblePositioner > .SpeechBubble {\n    animation: Pop-In-Bubble 0.3s;\n    animation-fill-mode: forwards;\n    animation-delay: 0.5s; }\n\n@keyframes Pop-In-Bubble {\n  0% {\n    transform: scale(0.05);\n    opacity: 0; }\n  50% {\n    transform: scale(1.1);\n    opacity: 1; }\n  80% {\n    transform: scale(0.9);\n    opacity: 1; }\n  100% {\n    transform: scale(1);\n    opacity: 1; } }\n  div.PointyContainer > .Pointy {\n    height: 20em;\n    width: 7em;\n    max-width: 25vw;\n    background-image: url(\"/assets/img/pointy-mc-pointyface.png\");\n    background-size: contain;\n    background-repeat: no-repeat;\n    background-position: bottom left;\n    transition: all ease-out 0.5s;\n    transform: translate(0%, 0%) rotate(0deg); }\n  div.PointyContainer > .BubblePositioner {\n    position: absolute;\n    bottom: 15em;\n    transform: translate(-100%, 100%);\n    opacity: 0;\n    transition: opacity 0.5s ease-out;\n    pointer-events: none; }\n    div.PointyContainer > .BubblePositioner > .SpeechBubble {\n      pointer-events: all;\n      background: white;\n      border-radius: 1.5em;\n      min-height: 3.5em;\n      width: 20em;\n      max-width: 75vw;\n      padding: 1em;\n      opacity: 0;\n      filter: drop-shadow(0px 0px 5px rgba(0, 0, 0, 0.1));\n      animation: Hide-Bubble 0.5s;\n      animation-fill-mode: forwards; }\n\n@keyframes Hide-Bubble {\n  0% {\n    transform: scale(1);\n    opacity: 1; }\n  50% {\n    transform: scale(0.5);\n    opacity: 1; }\n  100% {\n    transform: scale(0.01);\n    opacity: 0; } }\n      div.PointyContainer > .BubblePositioner > .SpeechBubble p, div.PointyContainer > .BubblePositioner > .SpeechBubble ul, div.PointyContainer > .BubblePositioner > .SpeechBubble ol {\n        margin: 0px; }\n      div.PointyContainer > .BubblePositioner > .SpeechBubble::before {\n        display: block;\n        content: \"\";\n        position: absolute;\n        z-index: -1;\n        right: 0px;\n        top: 1.5em;\n        height: 2em;\n        width: 2em;\n        background: white;\n        transform: translate(50%, -50%) scaleY(0.2) rotate(45deg); }\n\n/*# sourceMappingURL=/resources/css/4d5a6141.css.map */")
}
})
//# sourceMappingURL=app.js.map