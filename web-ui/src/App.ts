import "./app.scss";

import { anchor, append, deriverSwitch, div, img, noDeps, onDomReady, Source, UrlState } from "helium-ui";
import { renderDocsNav } from "./components/DocsNav/DocsNav";
import { renderDocs } from "./pages/Docs/Docs";
import { renderLandingPage } from "./pages/LandingPage/LandingPage";
import { tweener } from "helium-ui-tweener";
import { FS } from "./state/AppState";
import { renderGalleryPage } from "./pages/Gallery/GalleryPage";
import { renderPortalPage } from "./pages/Portal/PortalPage";

(window)["RobinHood-License for helium-sdx @ 50M/2K"] = "resolved";


const view = new Source<HTMLDivElement>();
deriverSwitch({
  watch: () => FS.route.id,
  responses: [
    { match: "survival-guide", action: () => view.set(renderDocs()) },
    { test: async (val) => val.startsWith("portal"), action: () => view.set(renderPortalPage()) },
    { test: "DEFAULT_RESPONSE", action: () => view.set(renderLandingPage()) },
  ]
})


const app = renderApp();
onDomReady(() => append(document.body, app));





function renderApp() {
  setTimeout(() => {
    !FS.route.id.startsWith("portal") && FS.pointy.show()
  }, 1500);

  const showSidebar = new Source(true);
  let cloudHowdy: HTMLAudioElement;

  return div("App", {
    ddxClass: () => [
      showSidebar.get() && "--showSidebar",
      (FS.route.id === "survival-guide") && "--hasSidebar"
    ]
  },[
    FS.pointy.domNode,
    div("Background", [
      div("Grass"),
      div("MrBlueSkies", {
        onPush: () => {
          cloudHowdy = cloudHowdy || new Audio("/assets/audio/Cloud - HowdyFuckers.mp3");
          cloudHowdy.play();
        },
      },[
        img("Beams", { src: "/assets/img/MrBlueSkies_Beams.png"}),
        img("Face", { src: "/assets/img/MrBlueSkies_Face.png"}),
      ]),
      img("Cloud large", { 
        src: "/assets/img/cloud-large.png",
      }),
      img("Cloud", { 
        src: "/assets/img/cloud-small.png",
      }),
    ]),
    div("PageContent", [
      div("Topbar", [
        div("IndexLink", {
          onPush: () => showSidebar.set(!showSidebar.get()),
        }),
        anchor("EventLogo", { href: "/" }, "Howdy Fuckers! 2021"),
      ]),
      div("Sidebar", tweener(() => {
        if (FS.route.id === "survival-guide") {
          return [
            anchor("EventLogo", { href: "/" }, [
              // "Howdy Fuckers! 2021"
              img({ src: "/assets/img/howdy-fuckers-logo.png"})
            ]),
            renderDocsNav(),
          ];
        }
        return null;
      })),
      div("MainContent", {
        onPush: () => showSidebar.set(false)
      }, () => view.get())
    ])
  ]);
}


