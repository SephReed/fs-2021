import { Sourcify } from "helium-ui";


export interface IDocSheetNavItem {
  name: string;
  filePath?: string;
  hash: string;
  sections?: IDocSheetNavItem[];
}


export class DocsState {
  protected state = Sourcify({
    docList: [] as IDocSheetNavItem[]
  })

  public get docList() {
    this.assertDocsFetched();
    return this.state.docList;
  }

  protected fetchDocsPromise: Promise<any>;
  protected assertDocsFetched() {
    if (this.fetchDocsPromise) { return; }
    this.fetchDocsPromise = new Promise(async () => {
      const text = await this.fetchDocText("./table-of-contents.md");
      const docItems: IDocSheetNavItem[] = [];
      const parentStack: IDocSheetNavItem[] = [];
      text.trim().split("\n").forEach((line) => {
        const parts = line.split(/(\s*)- *\[([^\]]+)\]\((.+?)(#.+)?\)/);
        const depth = (parts[1].length / 2) || 0
        const hash = parts[4] || `#${parts[3].replace(/(.+\/)|(\.md)/g, "")}`;
        const addMe = { 
          name: parts[2],
          filePath: parts[3],
          hash
        }
        console.log(depth, parentStack.length, parts[2]);
        while (parentStack.length > depth) {
          parentStack.pop();
        }

        if (!parentStack.length) {
          parentStack.push(addMe);
          docItems.push(addMe);
          return;

        } else {
          const parent = parentStack[parentStack.length - 1];
          parent.sections = parent.sections || [];
          parent.sections.push(addMe);
          parentStack.push(addMe);
        }
      });
      console.log(docItems);

      this.state.docList = Sourcify(docItems);
    })
  }

  protected docTextPromiseMap = new Map<string, Promise<string>>();
  public async fetchDocText(path: string) {
    if (this.docTextPromiseMap.has(path) === false) {
      const fullUrl = `https://api.howdyfuckers.online/git-doc/${path.slice(1)}`;
      // const isLocal = false; // 
      // if (false) {
      //   const prefix = `https://gitlab.com/SephReed/fs-2021/-/raw/master/docs`; //`/local-dev-docs`
      //   const targetPath = prefix + path.slice(1);
      //   fullUrl = `htttps://cors-anywhere.herokuapp.com/${targetPath}`;

      // } else {
      //   fullUrl = `/local-dev-docs${path.slice(1)}`;
      // }
      this.docTextPromiseMap.set(path, 
        (await fetch(fullUrl)).text()
      );
    }
    return this.docTextPromiseMap.get(path);
  }
}