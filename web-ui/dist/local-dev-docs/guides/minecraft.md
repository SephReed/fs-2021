# Guide: Minecraft Java Edition


*this section is not yet complete*

**Very important:  Get Java Edition, not Bedrock Edition.**

### Availability

Cost: ~$27

- ❌ Web Client
- ✅ Windows
- ✅ MacOS
- ❌ iOS
- ❌ Android
- ❌ Oculus

-------

### Installation

The Flipside Server is hosted for Minecraft **Java Edition**.

It's kind of confusing, but there's two versions of Minecraft:
1. Java Edition - The original, which allows for extensive modding and large scale servers
2. Bedrock Edition - The thing Microsoft made after they bought Minecraft that *doesn't allow for custom servers or modding*

So *make sure you get the right one*.  You can follow this link:

[Buy & Download Minecraft Java Edition](https://www.minecraft.net/en-us/store/minecraft-java-edition)
