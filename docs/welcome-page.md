# Welcome Home(page)


<div id="Announcements">

### Announcements!!
  - **The Event Has Begun**: No more announcements
</div>


--------

<div id="Overview">

### Overview
Welcome to the Virtual Event Survival Guide!

Here's the basic gist of whats going on:
  * **The Burner Art Safari** - it's not technically virtual, but [the map part](./guides/art-safari.md) is
    ![The Burner Art Safari Map](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/photo-burner-art-map.jpg)
    *All the strange and interesting, from the comfort of your automobile! (last years map)*
  * **Video / Audio / Chat Streams** - whether chatting in [Discord](./guides/discord.md), tuning in to [KFlip](https://www.kflipcamp.org/), or watching [Twitch](./guides/Twitch.md) streams, anything streamable fits in
    ![Video Chatting](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/photo-silly-discord-call.png)
    *These are just some randos showing off Halloween costumes... but you get the idea*
  * **The Virtual Wander Gallery** - for things that are less live; an online gallery with a wandering, explorative, stumble-upon, no-particular-order aspect
    ![Mock-up of Wandering Gallery](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/photo-wandering-gallery.png)
    *This is just a mock-up of what the wandering gallery could be like*
  * **Virtual Game Spaces** - mostly [Minecraft](./guides/Minecraft.md) and [Altspace](./guides/Altspace.md), but others may pop up
    ![Minecraft Burn Night](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/photo-minecraft-burn-night.png)
    *2020 Flipside Weekend Minecraft on Burn Night*
    ![Altspace Monkeys](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/photo-altspace-monkeys.jpeg)
    *A bunch of monkeys hanging around the AltSpace version of their Burning Man art piece - credit: EspressoBuzz [(see more AltSpace Burner photos here)](https://www.espressobuzz.net/Events/2020/BRCvr/)*
  * **Events** - a calendar of whats happening when and how to connect to it
    ![Event Calendar](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/photo-event-calendar.png)
    *Imagine something like this, but less organized*

Want to participate, or contribute?  Thank you so much!
  1. **Creators and Events** - Which covers pretty much anything you can [host on the internet or in your front yard](./creators-and-events.md).
  2. **Volunteers** - Which covers all the many, many niches that [could use your support](./volunteer.md).  Literally *anyone with an internet connection* can help.
</div>

--------

<div id="Schedule">

### Virtual Burn Schedule

The "Who What When Where" guide for this event is a Google calendar.  If you'd like to add an event, check
out the [Creators and Events section](#creators-and-events).

[Click here to view the (currently mostly empty) calendar](https://calendar.google.com/calendar/embed?src=howdy.fawker%40gmail.com)
</div>
