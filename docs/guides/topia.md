# Guide: Topia

### Availability

Cost: Free

- ✅ Web Client
- ❌ Windows
- ❌ MacOS
- ❌ iOS
- ❌ Android
- ❌ Oculus

-------

<div id="TopiaInstall">

### Installation

With Topia, there is nothing to install besides a web browser. You can use either Chrome, Edge or Brave browser. If you don't have one of those browsers installed already you can install it now. For example, to install Chrome, you can follow this link: [https://www.google.com/chrome/](https://www.google.com/chrome/)

NOTE: Topia is only usable on your desktop web browser - it won't work on your mobile device.

</div>

-------

<div id="TopiaEventServer">

### Connect to the Event Server!

- Open a supported web browser (Chrome, Edge or Brave)
- The link can be found in the portal: [http://howdyfuckers.online/portal/topia](http://howdyfuckers.online/portal/topia)
- When prompted, allow access to your video camera and microphone
- Enter an awesome display name and then click "Enter"
  - ![Topia Enter](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-topia--enter.png)
- Now go explore!

</div>
