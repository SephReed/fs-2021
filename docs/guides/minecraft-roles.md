
# Guide: Minecraft Roles

<div id="McHeirarchy">

### Heirarchy

The Minecraft servers have a permission heirarchy as follows:
* **default** - can walk around, open/close doors
* **participant** - can place/remove blocks
* **greeter** - can promote players from default -> participant
* **ranger** - can promote players from default -> participant -> greeter.  Also, can kick/tempban.  Also, can "rollback" greifer damage.
* **admin** - can run almost all commands
* **op** - all permissions

</div>

-------

<div id="McPromotions">

### Promotions (Rangers and Greeters)

If a user would like to gain build permissions:
1. first, do a tiny amount of vetting.  anything to make sure they aren't a bad actor can help.
- simple example: "Tell me about an interesting experience you had at an art festival"
- extreme example: "Lets have a face to face on discord"
2. run the promote command
- press "t" to open chat
- type `/lp user NAME_GOES_HERE promote`
    - example: `/lp user PointyMcPointyface promote`
- if their name begins with a star "*", you will have issues.  let an admin know.

The instructions above work for both rangers and greeters.

**If you are a ranger promting from `participant -> greeter` please be *extra* dilligent with your vetting**.

Giving the `greeter` permission to a bad actor could let in a floodgate of greifers.

</div>

-------

<div id="McDemotions">

### Demotions (Rangers and Greeters)

If ever a user appears to be acting in bad faith, the first thing to do is `demote` them!

Similar to promotions:  `/lp user NAME_GOES_HERE demote`.

Once they're been demoted to `default` they will no longer be able to cause trouble.  You may want to follow it 
up with kicking, tempbanning, and/or rollbacks.

</div>

-------

<div id="McKickBan">

### Kicking and Temp Banning (Rangers and Admin)

1. Make sure the user has been demoted.  They will come back.
- kicking means they can come back immediately
- tempban will default to 2 hours

2. Run either of the commands like so:
- `/kick NAME_GOES_HERE`
- `/tempban NAME_GOES_HERE`

If you need a permanent ban, admin can run `/ban NAME_GOES_HERE`

</div>

-------

<div id="McRollbacks">

### Rollbacks (Advanced for: Rangers and Admin)

Rollbacks are slightly more complex than any of the other commands.  There are a [ton of different ways of targetting rollbacks](https://prism-bukkit.readthedocs.io/en/latest/commands/parameters.html).

Here are some of the most common examples:

- A user accidentally breaks something they didn't mean to:
    1. Go to the general area
    2. use the command `/pr rollback p:NAME_GOES_HERE r:20 t:20m`
        - this command will rollback everything done by player `p` in a radius `r` of 20 blocks within the last 20 minutes `t`

- A bad actor is discovered after a greifing rampage
    1. use this command **ONLY FOR BAD ACTORS**: `/pr rollback p:NAME_GOES_HERE t:3d`

- Greifing is discovered, but of an unknown origin:
    1. Go to the general area
    2. run the command `/pr lookup a:break,place r:5`
        - this will give you a history of blocks placed and broken actions `a` within a radius `r` of 5 blocks.
        

</div>
