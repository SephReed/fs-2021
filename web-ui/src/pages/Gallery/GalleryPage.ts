import { div } from "helium-ui";
import { GalleryGame } from "../../../../gallery-game/src/Game";


let galleryGame: GalleryGame;
export function renderGalleryPage() {
  if (!galleryGame) { galleryGame = new GalleryGame(); }

  return div("GalleryPage", [
    galleryGame.domNode
  ])
}