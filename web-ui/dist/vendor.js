__fuse.bundle({

// node_modules/tslib/tslib.es6.js @2
2: function(__fusereq, exports, module){
exports.__esModule = true;
var extendStatics = function (d, b) {
  extendStatics = Object.setPrototypeOf || ({
    __proto__: []
  }) instanceof Array && (function (d, b) {
    d.__proto__ = b;
  }) || (function (d, b) {
    for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
  });
  return extendStatics(d, b);
};
function __extends(d, b) {
  if (typeof b !== "function" && b !== null) throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
  extendStatics(d, b);
  function __() {
    this.constructor = d;
  }
  d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}
exports.__extends = __extends;
exports.__assign = function () {
  exports.__assign = Object.assign || (function __assign(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
      s = arguments[i];
      for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
    }
    return t;
  });
  return exports.__assign.apply(this, arguments);
};
function __rest(s, e) {
  var t = {};
  for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
}
exports.__rest = __rest;
function __decorate(decorators, target, key, desc) {
  var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return (c > 3 && r && Object.defineProperty(target, key, r), r);
}
exports.__decorate = __decorate;
function __param(paramIndex, decorator) {
  return function (target, key) {
    decorator(target, key, paramIndex);
  };
}
exports.__param = __param;
function __metadata(metadataKey, metadataValue) {
  if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}
exports.__metadata = __metadata;
function __awaiter(thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }
  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }
    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }
    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }
    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
}
exports.__awaiter = __awaiter;
function __generator(thisArg, body) {
  var _ = {
    label: 0,
    sent: function () {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  }, f, y, t, g;
  return (g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g);
  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }
  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");
    while (_) try {
      if ((f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)) return t;
      if ((y = 0, t)) op = [op[0] & 2, t.value];
      switch (op[0]) {
        case 0:
        case 1:
          t = op;
          break;
        case 4:
          _.label++;
          return {
            value: op[1],
            done: false
          };
        case 5:
          _.label++;
          y = op[1];
          op = [0];
          continue;
        case 7:
          op = _.ops.pop();
          _.trys.pop();
          continue;
        default:
          if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
            _ = 0;
            continue;
          }
          if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
            _.label = op[1];
            break;
          }
          if (op[0] === 6 && _.label < t[1]) {
            _.label = t[1];
            t = op;
            break;
          }
          if (t && _.label < t[2]) {
            _.label = t[2];
            _.ops.push(op);
            break;
          }
          if (t[2]) _.ops.pop();
          _.trys.pop();
          continue;
      }
      op = body.call(thisArg, _);
    } catch (e) {
      op = [6, e];
      y = 0;
    } finally {
      f = t = 0;
    }
    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
}
exports.__generator = __generator;
exports.__createBinding = Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
};
function __exportStar(m, o) {
  for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(o, p)) exports.__createBinding(o, m, p);
}
exports.__exportStar = __exportStar;
function __values(o) {
  var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
  if (m) return m.call(o);
  if (o && typeof o.length === "number") return {
    next: function () {
      if (o && i >= o.length) o = void 0;
      return {
        value: o && o[i++],
        done: !o
      };
    }
  };
  throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}
exports.__values = __values;
function __read(o, n) {
  var m = typeof Symbol === "function" && o[Symbol.iterator];
  if (!m) return o;
  var i = m.call(o), r, ar = [], e;
  try {
    while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
  } catch (error) {
    e = {
      error: error
    };
  } finally {
    try {
      if (r && !r.done && (m = i["return"])) m.call(i);
    } finally {
      if (e) throw e.error;
    }
  }
  return ar;
}
exports.__read = __read;
function __spread() {
  for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
  return ar;
}
exports.__spread = __spread;
function __spreadArrays() {
  for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
  for (var r = Array(s), k = 0, i = 0; i < il; i++) for (var a = arguments[i], j = 0, jl = a.length; j < jl; (j++, k++)) r[k] = a[j];
  return r;
}
exports.__spreadArrays = __spreadArrays;
function __spreadArray(to, from) {
  for (var i = 0, il = from.length, j = to.length; i < il; (i++, j++)) to[j] = from[i];
  return to;
}
exports.__spreadArray = __spreadArray;
function __await(v) {
  return this instanceof __await ? (this.v = v, this) : new __await(v);
}
exports.__await = __await;
function __asyncGenerator(thisArg, _arguments, generator) {
  if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
  var g = generator.apply(thisArg, _arguments || []), i, q = [];
  return (i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
    return this;
  }, i);
  function verb(n) {
    if (g[n]) i[n] = function (v) {
      return new Promise(function (a, b) {
        q.push([n, v, a, b]) > 1 || resume(n, v);
      });
    };
  }
  function resume(n, v) {
    try {
      step(g[n](v));
    } catch (e) {
      settle(q[0][3], e);
    }
  }
  function step(r) {
    r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);
  }
  function fulfill(value) {
    resume("next", value);
  }
  function reject(value) {
    resume("throw", value);
  }
  function settle(f, v) {
    if ((f(v), q.shift(), q.length)) resume(q[0][0], q[0][1]);
  }
}
exports.__asyncGenerator = __asyncGenerator;
function __asyncDelegator(o) {
  var i, p;
  return (i = {}, verb("next"), verb("throw", function (e) {
    throw e;
  }), verb("return"), i[Symbol.iterator] = function () {
    return this;
  }, i);
  function verb(n, f) {
    i[n] = o[n] ? function (v) {
      return (p = !p) ? {
        value: __await(o[n](v)),
        done: n === "return"
      } : f ? f(v) : v;
    } : f;
  }
}
exports.__asyncDelegator = __asyncDelegator;
function __asyncValues(o) {
  if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
  var m = o[Symbol.asyncIterator], i;
  return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
    return this;
  }, i);
  function verb(n) {
    i[n] = o[n] && (function (v) {
      return new Promise(function (resolve, reject) {
        (v = o[n](v), settle(resolve, reject, v.done, v.value));
      });
    });
  }
  function settle(resolve, reject, d, v) {
    Promise.resolve(v).then(function (v) {
      resolve({
        value: v,
        done: d
      });
    }, reject);
  }
}
exports.__asyncValues = __asyncValues;
function __makeTemplateObject(cooked, raw) {
  if (Object.defineProperty) {
    Object.defineProperty(cooked, "raw", {
      value: raw
    });
  } else {
    cooked.raw = raw;
  }
  return cooked;
}
exports.__makeTemplateObject = __makeTemplateObject;
;
var __setModuleDefault = Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
};
function __importStar(mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) exports.__createBinding(result, mod, k);
  __setModuleDefault(result, mod);
  return result;
}
exports.__importStar = __importStar;
function __importDefault(mod) {
  return mod && mod.__esModule ? mod : {
    default: mod
  };
}
exports.__importDefault = __importDefault;
function __classPrivateFieldGet(receiver, privateMap) {
  if (!privateMap.has(receiver)) {
    throw new TypeError("attempted to get private field on non-instance");
  }
  return privateMap.get(receiver);
}
exports.__classPrivateFieldGet = __classPrivateFieldGet;
function __classPrivateFieldSet(receiver, privateMap, value) {
  if (!privateMap.has(receiver)) {
    throw new TypeError("attempted to set private field on non-instance");
  }
  privateMap.set(receiver, value);
  return value;
}
exports.__classPrivateFieldSet = __classPrivateFieldSet;

},

// node_modules/fuse-box/modules/fuse-box-css/index.js @4
4: function(__fusereq, exports, module){
var cssHandler = function (__filename, contents) {
  var styleId = __filename.replace(/[\.\/]+/g, '-');
  if (styleId.charAt(0) === '-') styleId = styleId.substring(1);
  var exists = document.getElementById(styleId);
  if (!exists) {
    var s = document.createElement(contents ? 'style' : 'link');
    s.id = styleId;
    s.type = 'text/css';
    if (contents) {
      s.innerHTML = contents;
    } else {
      s.rel = 'stylesheet';
      s.href = __filename;
    }
    document.getElementsByTagName('head')[0].appendChild(s);
  } else {
    if (contents) exists.innerHTML = contents;
  }
};
module.exports = cssHandler;

},

// node_modules/helium-ui/dist/index.js @5
5: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(12), exports);
__exportStar(__fusereq(13), exports);
__exportStar(__fusereq(14), exports);
__exportStar(__fusereq(15), exports);
__exportStar(__fusereq(16), exports);
__exportStar(__fusereq(17), exports);

},

// node_modules/helium-ui-tweener/src/Tween.ts @9
9: function(__fusereq, exports, module){
exports.__esModule = true;
__fusereq(21);
var helium_ui_1 = __fusereq(5);
exports.tweener = tweener;
exports.tweener = tweener;
function tweener(propsOrDdx, maybeDDx) {
  let props;
  let ddx;
  if (typeof propsOrDdx === "function") {
    ddx = propsOrDdx;
    props = {};
  } else {
    props = propsOrDdx;
    ddx = maybeDDx;
  }
  let ms = props.ms !== undefined ? props.ms : 150;
  let resizeMs = props.resizeMs || 250;
  let type = props.type || "crossfade-on-top";
  if (type === "none") {
    type = undefined;
  }
  let skipFirst = props.skipFirst !== false;
  let tweenSizing = props.disableResize !== true;
  let currentView;
  let currentContent;
  return helium_ui_1.div("Tweener", {
    ddx: parent => {
      const newView = ddx();
      const lastContent = currentContent;
      if (newView === currentView) {
        return;
      }
      currentView = newView;
      const newContent = helium_ui_1.div("Tweener-Content", newView);
      if (!lastContent && skipFirst) {
        return helium_ui_1.setInnards(parent, currentContent = newContent);
      }
      const doSizeTween = newContent && tweenSizing;
      lastContent.style.width = `${lastContent.clientWidth}px`;
      lastContent.style.height = `${lastContent.clientHeight}px`;
      helium_ui_1.setInnards(parent, newContent);
      const targetSize = {
        width: newContent.clientWidth,
        height: newContent.clientHeight
      };
      helium_ui_1.setInnards(parent, lastContent);
      newContent.style.width = `${targetSize.width}px`;
      newContent.style.height = `${targetSize.height}px`;
      let newAnimationComplete = false;
      let oldAnimationComplete = false;
      let sizeTweenComplete = !doSizeTween ? true : false;
      const toBeRemoved = [];
      function removeIfReady(task) {
        task && toBeRemoved.push(task);
        if (newAnimationComplete && oldAnimationComplete && sizeTweenComplete) {
          toBeRemoved.forEach(({item, classes}) => {
            classes.forEach(classnName => item.classList.remove(classnName));
          });
          parent.style.transitionDuration = "";
          newContent.style.width = parent.style.width = "";
          newContent.style.height = parent.style.height = "";
        }
      }
      if (type) {
        [lastContent, newContent].filter(it => !!it).forEach(content => {
          content.style.animationDuration = `${ms}ms`;
          content.classList.remove("--tween_out");
          content.classList.remove("--tween_in");
          const className = `--tween-anim_${type}`;
          const isNewContent = content === newContent;
          const inOutName = isNewContent ? "--tween_in" : "--tween_out";
          const listener = ev => {
            if (ev.animationName.includes(type)) {
              content.removeEventListener("animationend", listener);
              if (!isNewContent) {
                oldAnimationComplete = true;
                content.remove();
              } else {
                newAnimationComplete = true;
              }
              removeIfReady({
                item: content,
                classes: [className, inOutName]
              });
            }
          };
          content.addEventListener("animationend", listener);
          content.classList.add(inOutName);
          content.classList.add(className);
        });
      }
      if (doSizeTween) {
        parent.style.width = `${parent.clientWidth}px`;
        parent.style.height = `${parent.clientHeight}px`;
      }
      if (type) {
        helium_ui_1.setInnards(parent, [lastContent, newContent]);
      } else {
        helium_ui_1.setInnards(parent, newContent);
      }
      currentContent = newContent;
      currentContent.style.minWidth = currentContent.style.minHeight = "";
      if (!doSizeTween) {
        if (type) {
          parent.style.width = `${newContent.clientWidth}px`;
          parent.style.height = `${newContent.clientHeight}px`;
          const animListener = () => {
            parent.removeEventListener("animationend", animListener);
            parent.style.width = parent.style.height = "";
          };
          parent.addEventListener("animationend", animListener);
        }
      } else {
        const transitionListener = () => {
          parent.removeEventListener("transitionend", transitionListener);
          sizeTweenComplete = true;
          removeIfReady();
          parent.style.width = parent.style.height = "";
        };
        parent.addEventListener("transitionend", transitionListener);
        parent.style.transitionDuration = `${resizeMs}ms`;
        requestAnimationFrame(() => requestAnimationFrame(() => {
          const {width, height} = targetSize;
          parent.style.width = `${width + 1}px`;
          parent.style.height = `${height + 1}px`;
        }));
      }
    }
  });
}
exports.tweener = tweener;

},

// node_modules/helium-ui/dist/El-Tool/index.js @12
12: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(26), exports);
__exportStar(__fusereq(27), exports);
__exportStar(__fusereq(28), exports);
__exportStar(__fusereq(29), exports);
__exportStar(__fusereq(30), exports);
__exportStar(__fusereq(31), exports);

},

// node_modules/helium-ui/dist/Derivations/index.js @13
13: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(32), exports);
__exportStar(__fusereq(33), exports);

},

// node_modules/helium-ui/dist/Sourcify/index.js @14
14: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(34), exports);
__exportStar(__fusereq(35), exports);
__exportStar(__fusereq(36), exports);
__exportStar(__fusereq(37), exports);

},

// node_modules/helium-ui/dist/HeliumBase.js @15
15: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.changeFreezeState = exports.isHeliumHazed = exports.heliumHaze = void 0;
function heliumHaze(rookie) {
  rookie._helium = rookie._helium || ({});
  return rookie;
}
exports.heliumHaze = heliumHaze;
function isHeliumHazed(checkMe) {
  return checkMe._helium;
}
exports.isHeliumHazed = isHeliumHazed;
function changeFreezeState(hazed, shouldBeFrozen) {
  const helium = hazed._helium;
  if (helium.frozen === shouldBeFrozen) {
    return;
  }
  helium.frozen = shouldBeFrozen;
  const fns = shouldBeFrozen ? helium.freeze : helium.unfreeze;
  if (fns) {
    fns.forEach(fn => fn());
  }
  if (hazed instanceof HTMLElement) {
    const freezeEv = new Event(helium.frozen ? "helium-freeze" : "helium-unfreeze");
    hazed.dispatchEvent(freezeEv);
  }
}
exports.changeFreezeState = changeFreezeState;

},

// node_modules/helium-ui/dist/Augments/index.js @16
16: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(45), exports);
__exportStar(__fusereq(46), exports);

},

// node_modules/helium-sdx/dist/index.js @17
17: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(38));
__export(__fusereq(39));
__export(__fusereq(40));
const robinhood_license_assert_1 = __fusereq(41);
robinhood_license_assert_1.assertRobinHoodLicense({
  packageName: "helium-sdx",
  version: "v1",
  threshold: {
    num: 50,
    magnitude: "M"
  },
  rate: {
    num: 2,
    magnitude: "K"
  },
  link: ""
});

},

// node_modules/helium-md/dist/index.js @19
19: function(__fusereq, exports, module){
"use strict";
var __importDefault = this && this.__importDefault || (function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.md = void 0;
const helium_ui_1 = __fusereq(5);
const markdown_it_1 = __importDefault(__fusereq(42));
const strip_indent_1 = __importDefault(__fusereq(43));
const dompurify_1 = __importDefault(__fusereq(44));
const _md = new markdown_it_1.default();
function md(propOrChild, childMaybe) {
  const {props, innards} = helium_ui_1.standardizeInput("helium-md", propOrChild, childMaybe);
  let mdText = innards;
  const rules = props.rules || ({});
  const endSpace = rules.endLineSpacesCreateLineBreak;
  if (mdText) {
    if (rules.unindent !== false) {
      mdText = strip_indent_1.default(mdText);
    }
    if (endSpace !== false) {
      const spaceCount = Array(typeof endSpace === "number" ? endSpace + 1 : 3).join(" ");
      const regex = new RegExp(`\\n${spaceCount}+(?=\\n)`, "g");
      mdText = mdText.replace(regex, "\n\nEMPTY_LINE\n");
      const endLineRegex = new RegExp(`${spaceCount}+\\n`, "g");
      mdText = mdText.replace(endLineRegex, "<br>\n");
    }
  }
  let mdHtml = mdText && _md.render(mdText);
  if (mdHtml) {
    if (endSpace !== false) {
      mdHtml = mdHtml.replace(/(<br>\s*<\/p>\s*)(<p>EMPTY_LINE<\/p>\s*)+(<p>)/g, match => {
        return match.match(/EMPTY_LINE/g).map(() => "<br>").join("");
      });
      mdHtml = mdHtml.replace(/<p>EMPTY_LINE<\/p>/g, "<br>");
    }
    if (rules.allowHtml !== false) {
      mdHtml = mdHtml.replace(/&quot;/gi, '"').replace(/&lt;/g, "<").replace(/&gt;/g, ">");
      const config = typeof rules.allowHtml === "object" ? rules.allowHtml : {};
      config.RETURN_DOM_FRAGMENT = false;
      config.RETURN_DOM = false;
      mdHtml = dompurify_1.default.sanitize(mdHtml, config);
    }
  }
  const out = helium_ui_1.div(props, mdHtml && ({
    hackableHTML: mdHtml
  }));
  if (props.templating) {
    permeate(out, props.templating);
  }
  return out;
}
exports.md = md;
function permeate(entry, template) {
  if (!template) {
    return;
  }
  const injects = Array.from(Object.entries(template.inject));
  checkLayer(entry);
  function checkLayer(target) {
    for (let ptr = target.firstChild; !!ptr; ) {
      if (helium_ui_1.isTextNode(ptr)) {
        let chunks = [ptr.textContent];
        injects.forEach(([key, innard]) => {
          const newChunks = [];
          const matcher = `{{${key}}}`;
          chunks.forEach(chunk => {
            if (typeof chunk !== "string") {
              return newChunks.push(chunk);
            }
            const splits = chunk.split(matcher);
            if (splits.length === 1) {
              newChunks.push(splits[0]);
            } else {
              splits.forEach((textSplit, index) => {
                newChunks.push(textSplit);
                if (index < splits.length - 1) {
                  const nodes = helium_ui_1.convertSingleInnardToNodes(innard);
                  nodes.forEach(node => newChunks.push(node));
                }
              });
            }
          });
          chunks = newChunks;
        });
        if (chunks.length) {
          const ogTextNode = ptr;
          chunks.forEach(chunk => {
            const node = typeof chunk === "string" ? new Text(chunk) : chunk;
            ogTextNode.parentElement.insertBefore(node, ogTextNode);
            ptr = node;
          });
          ogTextNode.remove();
        }
      } else if (helium_ui_1.isHTMLElement(ptr)) {
        checkLayer(ptr);
      }
      ptr = ptr.nextSibling;
    }
  }
}

},

// node_modules/helium-ui-tweener/src/tween.scss @21
21: function(__fusereq, exports, module){
__fusereq(4)("node_modules/helium-ui-tweener/src/tween.scss",".Tweener {\n  position: relative;\n  overflow: hidden; }\n  .Tweener > .--tween_in {\n    animation-timing-function: ease-out; }\n  .Tweener > .--tween_out {\n    animation-timing-function: ease-in; }\n  .Tweener > .--tween-anim_fade-on-top.--tween_in {\n    position: absolute;\n    top: 0px;\n    z-index: 1;\n    animation-name: fade-on-top-in; }\n\n@keyframes fade-on-top-in {\n  0% {\n    opacity: 0; }\n  50% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n  .Tweener > .--tween-anim_fade-on-top.--tween_out {\n    animation-name: fade-on-top-out; }\n\n@keyframes fade-on-top-out {\n  0% {\n    opacity: 1; }\n  50% {\n    opacity: 0; }\n  100% {\n    opacity: 0; } }\n  .Tweener > .--tween-anim_crossfade-on-top.--tween_in {\n    position: absolute;\n    top: 0px;\n    z-index: 1;\n    animation-name: crossfade-on-top-in; }\n\n@keyframes crossfade-on-top-in {\n  0% {\n    opacity: 0; }\n  33% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n  .Tweener > .--tween-anim_crossfade-on-top.--tween_out {\n    animation-name: crossfade-on-top-out; }\n\n@keyframes crossfade-on-top-out {\n  0% {\n    opacity: 1; }\n  66% {\n    opacity: 0; }\n  100% {\n    opacity: 0; } }\n  .Tweener > .--tween-anim_shuffle-bottom.--tween_in {\n    position: absolute;\n    z-index: 1;\n    animation-name: shuffle-bottom-in;\n    bottom: 0px;\n    width: 100%; }\n\n@keyframes shuffle-bottom-in {\n  0% {\n    transform: translateY(100%); }\n  100% {\n    transform: translateY(0%); } }\n  .Tweener > .--tween-anim_shuffle-bottom.--tween_out {\n    animation-name: shuffle-bottom-out; }\n\n@keyframes shuffle-bottom-out {\n  0% {\n    transform: translateY(0%); }\n  100% {\n    transform: translateY(100%); } }\n")
},

// node_modules/helium-ui/dist/El-Tool/El-Tool.js @26
26: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.convertSingleInnardToNodes = exports.convertInnardsToElements = exports.append = exports.setInnards = exports.applyProps = exports.standardizeInput = exports.el = exports.isProps = exports.isRawHTML = exports.isTextNode = exports.isHTMLElement = exports.isNode = exports.onUnfreeze = exports.onFreeze = exports.setStyle = exports.removeChildren = exports.ddxClass = exports.haveClass = exports.addClass = exports.classSplice = exports.onDomReady = exports.on = exports.byId = void 0;
const Derivations_1 = __fusereq(13);
const HeliumBase_1 = __fusereq(15);
const ToucherEvents_1 = __fusereq(31);
function byId(id) {
  return document.getElementById(id);
}
exports.byId = byId;
function on(eventName, ref, cb) {
  ref.addEventListener(eventName, cb);
}
exports.on = on;
function onDomReady(cb) {
  if (domIsReady) {
    cb();
    return;
  }
  window.addEventListener("DOMContentLoaded", cb);
}
exports.onDomReady = onDomReady;
let domIsReady = false;
typeof window !== "undefined" && onDomReady(() => domIsReady = true);
function classSplice(element, removeClasses, addClasses) {
  const addList = addClasses && (Array.isArray(addClasses) ? addClasses : addClasses.split(/\s+/g)).filter(it => !!it);
  const remList = removeClasses && (Array.isArray(removeClasses) ? removeClasses : removeClasses.split(/\s+/g)).filter(it => !!it);
  if (remList) {
    remList.filter(it => !addList || !addList.includes(it)).forEach(className => element.classList.remove(className));
  }
  if (addList) {
    addList.filter(it => !remList || !remList.includes(it)).forEach(className => element.classList.add(className));
  }
  return element;
}
exports.classSplice = classSplice;
function addClass(addClasses, element) {
  return classSplice(element, null, addClasses);
}
exports.addClass = addClass;
function haveClass(element, className, addNotRemove) {
  return addNotRemove ? classSplice(element, null, className) : classSplice(element, className, undefined);
}
exports.haveClass = haveClass;
function ddxClass(target, ddx) {
  let lastClassList;
  Derivations_1.deriveDomAnchored(target, () => {
    const removeUs = lastClassList;
    classSplice(target, removeUs, lastClassList = ddx());
  });
}
exports.ddxClass = ddxClass;
function removeChildren(parent) {
  let child = parent.firstChild;
  while (!!child) {
    child.remove();
    child = parent.firstChild;
  }
  return parent;
}
exports.removeChildren = removeChildren;
function setStyle(root, style) {
  if (typeof style === "string") {
    root.style.cssText = style;
  } else {
    Object.keys(style).forEach(styleName => root.style[styleName] = style[styleName]);
  }
  return root;
}
exports.setStyle = setStyle;
function onFreeze(target, cb) {
  void HeliumBase_1.heliumHaze(target);
  target.addEventListener("helium-freeze", cb);
}
exports.onFreeze = onFreeze;
function onUnfreeze(target, cb) {
  void HeliumBase_1.heliumHaze(target);
  target.addEventListener("helium-unfreeze", cb);
}
exports.onUnfreeze = onUnfreeze;
function isNode(checkMe) {
  return checkMe instanceof Node;
}
exports.isNode = isNode;
function isHTMLElement(checkMe) {
  return ("nodeName" in checkMe);
}
exports.isHTMLElement = isHTMLElement;
function isTextNode(checkMe) {
  return isNode(checkMe) && checkMe.nodeType === checkMe.TEXT_NODE;
}
exports.isTextNode = isTextNode;
function isRawHTML(checkMe) {
  return checkMe && typeof checkMe === "object" && checkMe.hasOwnProperty("hackableHTML");
}
exports.isRawHTML = isRawHTML;
function isProps(checkMe) {
  return checkMe && typeof checkMe === "object" && !Array.isArray(checkMe) && !isNode(checkMe) && !isRawHTML(checkMe);
}
exports.isProps = isProps;
function el(tagName, propsOrInnards, innards) {
  const input = standardizeInput(null, propsOrInnards, innards);
  return __createEl(tagName, input.props, input.innards);
}
exports.el = el;
function standardizeInput(nameOrPropsOrInnards, propsOrInnards, maybeInnards, isInnardsRazor) {
  let innards = maybeInnards;
  let props;
  let name;
  if (nameOrPropsOrInnards) {
    if (typeof nameOrPropsOrInnards === "string") {
      name = nameOrPropsOrInnards;
    } else {
      if (maybeInnards !== undefined) {
        throw new Error("Can not define first and third argument if first is not a string");
      }
      maybeInnards = propsOrInnards;
      propsOrInnards = nameOrPropsOrInnards;
    }
  }
  if (propsOrInnards || propsOrInnards === 0) {
    const isInnards = isInnardsRazor ? isInnardsRazor(propsOrInnards) : !isProps(propsOrInnards);
    if (isInnards) {
      if (maybeInnards !== undefined) {
        throw new Error("Can not define third argument if second is innards" + maybeInnards);
      }
      innards = propsOrInnards;
    } else {
      props = propsOrInnards;
      innards = maybeInnards;
    }
  }
  if (props && (props.innards || props.innards === 0)) {
    if (innards && props.innards) {
      console.error(props, innards);
      throw new Error(`Can not specify both props.innards and innards argument`);
    }
    innards = props.innards;
    delete props.innards;
  }
  props = props || ({});
  if (name) {
    props.class = props.class || "";
    if (typeof props.class === "string") {
      props.class = name + (props.class ? " " : "") + props.class;
    } else {
      props.class.unshift(name);
    }
  }
  return {
    props,
    innards
  };
}
exports.standardizeInput = standardizeInput;
function applyProps(props, out, innardsCb) {
  if (props) {
    if (props.on) {
      const eventListeners = props.on || ({});
      Object.keys(eventListeners).forEach(eventType => out.addEventListener(eventType, eventListeners[eventType]));
    }
    if (props.onKeyDown) {
      out.addEventListener("keydown", props.onKeyDown);
    }
    if (props.onPush) {
      out.classList.add("onPush");
      ToucherEvents_1.onPush(out, props.onPush);
    }
    if (props.onTouch) {
      ToucherEvents_1.onTouch(out, props.onTouch);
    }
    if (props.onToucherMove) {
      ToucherEvents_1.onToucherMove(out, props.onToucherMove);
    }
    if (props.onToucherEnterExit) {
      ToucherEvents_1.onToucherEnterExit(out, props.onToucherEnterExit);
    }
    if (props.onHoverChange) {
      out.addEventListener("mouseover", ev => props.onHoverChange(true, ev));
      out.addEventListener("mouseout", ev => props.onHoverChange(false, ev));
    }
    if (props.onFreeze) {
      onFreeze(out, props.onFreeze);
    }
    if (props.onUnfreeze) {
      onUnfreeze(out, props.onUnfreeze);
    }
    if (props.title) {
      out.title = props.title;
    }
    if (props.attr) {
      const attrs = props.attr || ({});
      Object.keys(attrs).forEach(attr => out.setAttribute(attr, attrs[attr]));
    }
    if (props.this) {
      const thisProps = props.this || ({});
      Object.keys(thisProps).forEach(prop => out[prop] = thisProps[prop]);
    }
    if (props.class) {
      addClass(props.class, out);
    }
    if (props.id) {
      out.id = props.id;
    }
    if (props.style) {
      setStyle(out, props.style);
    }
    if (props.innards !== undefined) {
      append(out, props.innards);
    }
  }
  if (innardsCb !== undefined) {
    innardsCb(out);
  }
  if (props) {
    if (props.ref) {
      props.ref(out);
    }
    if (props.ddxClass) {
      ddxClass(out, props.ddxClass);
    }
    if (props.ddx) {
      const keep = {};
      Derivations_1.deriveDomAnchored(out, () => props.ddx(out, keep));
    }
    if (props.augments) {
      let augs = Array.isArray(props.augments) ? props.augments : [props.augments];
      augs.forEach(aug => aug.targetElement(out));
      onFreeze(out, () => augs.forEach(aug => aug.freeze()));
      onUnfreeze(out, () => augs.forEach(aug => aug.unfreeze()));
    }
  }
  return out;
}
exports.applyProps = applyProps;
function __createEl(tagName, props, innards) {
  const out = document.createElement(tagName);
  applyProps(props, out, () => {
    if (innards !== undefined) {
      append(out, innards);
    }
  });
  if (HeliumBase_1.isHeliumHazed(out)) {
    out._helium.frozen = true;
  }
  return out;
}
let innardsWarningGiven;
function setInnards(root, innards) {
  if (root === document.body && !window._heliumNoWarnSetInnardsDocBody) {
    console.warn([`Suggestion: avoid using setInnards() on document body, and instead use append().`, `If you do not, anything added to the page outside of your root render could be removed.  To disable this warning,`, `set window._heliumNoWarnSetInnardsDocBody = true`].join(" "));
  }
  removeChildren(root);
  append(root, innards);
}
exports.setInnards = setInnards;
function append(root, innards, before) {
  const addUs = convertInnardsToElements(innards);
  if (before) {
    addUs.forEach(node => {
      try {
        root.insertBefore(node, before);
      } catch (err) {
        console.error("Can not insert childNode into: ", root);
        console.error("Tried to insert: ", node);
        console.error(err);
      }
    });
  } else {
    addUs.forEach(node => {
      try {
        root.appendChild(node);
      } catch (err) {
        console.error("Can not append childNode to: ", root);
        console.error("Tried to append: ", node);
        console.error(err);
      }
    });
  }
  return root;
}
exports.append = append;
function convertInnardsToElements(innards, flattenFn) {
  if (innards === undefined || innards === null) {
    return [];
  }
  if (typeof innards === "function") {
    const refresher = new Derivations_1.RerenderReplacer(innards, {
      flattenFn
    });
    return refresher.getRender();
  }
  const childList = Array.isArray(innards) ? innards : [innards];
  let out = childList.map(child => convertSingleInnardToNodes(child));
  return flattenFn ? flattenFn(out) : out.flat();
}
exports.convertInnardsToElements = convertInnardsToElements;
function convertSingleInnardToNodes(child) {
  if (typeof child === "function") {
    const replacer = new Derivations_1.RerenderReplacer(child);
    return replacer.getRender();
  } else {
    let out = [];
    if (child === undefined || child === null) {
      return [];
    }
    if (typeof child === "number") {
      child = child + "";
    }
    if (isRawHTML(child)) {
      const template = document.createElement('div');
      template.innerHTML = child.hackableHTML;
      template.getRootNode();
      Array.from(template.childNodes).forEach(child => out.push(child));
    } else if (typeof child === "string") {
      out.push(new Text(child));
    } else if (child) {
      if (Array.isArray(child)) {
        out = out.concat(convertInnardsToElements(child));
      } else {
        out.push(child);
      }
    }
    return out;
  }
}
exports.convertSingleInnardToNodes = convertSingleInnardToNodes;

},

// node_modules/helium-ui/dist/El-Tool/Factories.js @27
27: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.h6 = exports.h5 = exports.h4 = exports.h3 = exports.h2 = exports.h1 = exports.br = exports.hr = exports.bold = exports.italic = exports.inputTextbox = exports.select = exports.button = exports.img = exports.th = exports.td = exports.tr = exports.table = exports.li = exports.ol = exports.ul = exports.pre = exports.span = exports.div = exports._simpleFactory = void 0;
const El_Tool_1 = __fusereq(26);
const helium_sdx_1 = __fusereq(17);
function _simpleFactory(tagName, [arg1, arg2, arg3]) {
  const {props, innards} = El_Tool_1.standardizeInput(arg1, arg2, arg3);
  return El_Tool_1.el(tagName, props, innards);
}
exports._simpleFactory = _simpleFactory;
exports.div = function (...args) {
  return _simpleFactory("div", args);
};
exports.span = function (...args) {
  return _simpleFactory("span", args);
};
exports.pre = function (...args) {
  return _simpleFactory("pre", args);
};
exports.ul = function (...args) {
  const {props, innards} = El_Tool_1.standardizeInput(...args);
  const listItems = Array.isArray(innards) ? innards : [innards];
  return El_Tool_1.el("ul", props, listItems.map(it => {
    if (El_Tool_1.isHTMLElement(it) && (it.tagName === "LI" || it.nodeName === "#comment")) {
      return it;
    }
    return exports.li([it]);
  }));
};
exports.ol = function (...args) {
  const {props, innards} = El_Tool_1.standardizeInput(...args);
  const listItems = Array.isArray(innards) ? innards : [innards];
  return El_Tool_1.el("ol", props, listItems.map(it => {
    if (El_Tool_1.isHTMLElement(it) && (it.tagName === "LI" || it.nodeName === "#comment")) {
      return it;
    }
    return exports.li([it]);
  }));
};
exports.li = function (...args) {
  return _simpleFactory("li", args);
};
exports.table = function (namePropChild, propChild, child) {
  const {props, innards} = El_Tool_1.standardizeInput(namePropChild, propChild, child);
  const rows = El_Tool_1.convertInnardsToElements(innards, rows => rows.map(row => {
    if (Array.isArray(row) && row.length === 1 && !Array.isArray(row[0])) {
      row = row.pop();
    }
    if (El_Tool_1.isHTMLElement(row) && (row.tagName === "TR" || row.nodeName === "#comment")) {
      return row;
    }
    return exports.tr(row);
  }));
  return El_Tool_1.el("table", props, rows.map(toTr));
  function toTr(row) {
    if (Array.isArray(row) && row.length === 1) {
      row = row.pop();
    }
    if (El_Tool_1.isHTMLElement(row) && (row.tagName === "TR" || row.nodeName === "#comment")) {
      return row;
    }
    if (typeof row === "function") {
      return () => toTr(row());
    }
    return exports.tr(row);
  }
};
exports.tr = function (...args) {
  const {props, innards} = El_Tool_1.standardizeInput(...args);
  const cols = El_Tool_1.convertInnardsToElements(innards);
  return El_Tool_1.el("tr", props, cols.map(data => toTd(data)));
  function toTd(data) {
    if (El_Tool_1.isHTMLElement(data) && (data.tagName === "TD" || data.nodeName === "#comment")) {
      return data;
    }
    return El_Tool_1.el("td", data);
  }
};
exports.td = function (...args) {
  return _simpleFactory("td", args);
};
exports.th = function (...args) {
  return _simpleFactory("th", args);
};
exports.img = function (...args) {
  const {props, innards} = El_Tool_1.standardizeInput(...args);
  props.attr = props.attr || ({});
  props.attr.src = props.src;
  return El_Tool_1.el("img", props, innards);
};
exports.button = function (...args) {
  return _simpleFactory("button", args);
};
exports.select = function (classNameOrProps, propsOrInnards, maybeInnards) {
  const {props, innards} = El_Tool_1.standardizeInput(classNameOrProps, propsOrInnards, maybeInnards, a => Array.isArray(a));
  props.on = props.on || ({});
  let currentVal;
  if (props.onInput) {
    const {onInput} = props;
    const onInputListener = props.on.input;
    props.on.input = ev => {
      const newVal = out.value;
      if (newVal !== currentVal) {
        currentVal = newVal;
        onInput(newVal);
      }
      return !onInputListener || onInputListener(ev);
    };
  }
  const out = El_Tool_1.el("select", props, innards.map(innard => {
    let option;
    if (innard instanceof HTMLElement) {
      option = innard;
    } else if (typeof innard === "function") {
      option = El_Tool_1.el("option", {
        ddx: ref => {
          ref.innerText = String(innard());
          if (ref.selected) {}
        }
      });
    } else {
      option = El_Tool_1.el("option", String(innard));
    }
    if (option.selected) {
      currentVal = option.value;
    }
    return option;
  }));
  return out;
};
exports.inputTextbox = function (classNameOrProps, propsOrInnards, maybeInnards) {
  const {props, innards} = El_Tool_1.standardizeInput(classNameOrProps, propsOrInnards, maybeInnards, a => a instanceof helium_sdx_1.Source);
  props.on = props.on || ({});
  if (innards instanceof helium_sdx_1.Source) {
    const onInputArg = props.onInput;
    props.onInput = value => {
      innards.set(value);
      onInputArg && onInputArg(value);
    };
  }
  if (props.onInput) {
    const {onInput} = props;
    const onInputListener = props.on.input;
    let oldVal = innards;
    props.on.input = ev => {
      const newVal = out.value;
      if (newVal !== oldVal) {
        oldVal = newVal;
        onInput(newVal);
      }
      return !onInputListener || onInputListener(ev);
    };
  }
  if (props.onValueEntered) {
    const onChange = props.on.change;
    const {onValueEntered} = props;
    props.on.change = ev => {
      onValueEntered(ev.target.value);
      return !onChange || onChange(ev);
    };
  }
  const out = El_Tool_1.el("input", props);
  out.type = props.type || "text";
  if (innards) {
    if (typeof innards === "string") {
      out.value = innards;
    } else if (innards instanceof helium_sdx_1.Source) {
      helium_sdx_1.derive(() => out.value = innards.get());
    } else {
      helium_sdx_1.derive(() => out.value = innards());
    }
  }
  return out;
};
exports.italic = function (...args) {
  return _simpleFactory("i", args);
};
exports.bold = function (...args) {
  return _simpleFactory("b", args);
};
exports.hr = function (...args) {
  return _simpleFactory("hr", args);
};
let brStylingAdded = false;
function br(size, props = {}) {
  if (!size) {
    return El_Tool_1.el("br", props);
  }
  if (brStylingAdded === false) {
    brStylingAdded = true;
    const style = document.createElement("style");
    style.innerHTML = `em-br { display: block; }`;
    document.head.append(style);
  }
  props.style = {
    height: `${size}em`
  };
  return El_Tool_1.el("em-br", props);
}
exports.br = br;
exports.h1 = function (...args) {
  return _simpleFactory("h1", args);
};
exports.h2 = function (...args) {
  return _simpleFactory("h2", args);
};
exports.h3 = function (...args) {
  return _simpleFactory("h3", args);
};
exports.h4 = function (...args) {
  return _simpleFactory("h4", args);
};
exports.h5 = function (...args) {
  return _simpleFactory("h5", args);
};
exports.h6 = function (...args) {
  return _simpleFactory("h6", args);
};

},

// node_modules/helium-ui/dist/El-Tool/Navigation.js @28
28: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.replaceUrl = exports.followUrl = exports.observeUrl = exports.onUrlChange = exports.UrlState = exports._UrlState = exports.NavigationEvent = exports.externalLink = exports.a = exports.anchor = void 0;
const El_Tool_1 = __fusereq(26);
const Derivations_1 = __fusereq(13);
const helium_sdx_1 = __fusereq(17);
const Sourcify_1 = __fusereq(14);
function anchor(...[classOrProp, propOrChild, child]) {
  const {props, innards} = El_Tool_1.standardizeInput(classOrProp, propOrChild, child);
  props.attr = props.attr || ({});
  const {href} = props;
  if (!href) {
    props.attr.href = "#";
  } else if (typeof href === "string") {
    props.attr.href = href;
  }
  if (props.target) {
    props.attr.target = props.target;
  }
  const ref = El_Tool_1.el("a", props, innards);
  if (typeof href === "function") {
    Derivations_1.deriveDomAnchored(ref, () => {
      ref.href = href();
    });
  }
  if (props.allowTextSelect) {
    El_Tool_1.setStyle(ref, {
      "-webkit-user-drag": "none"
    });
    let selectedText = "";
    El_Tool_1.on("mousedown", ref, ev => selectedText = window.getSelection().toString());
    El_Tool_1.on("click", ref, ev => {
      if (selectedText !== window.getSelection().toString()) {
        ev.preventDefault();
      }
    });
  }
  El_Tool_1.on("click", ref, ev => {
    if (ref.href && !ref.download) {
      if (props.disableHref) {
        return ev.preventDefault();
      }
      if (ev.metaKey || ref.target === "_blank") {
        window.open(ref.href, '_blank');
        return ev.preventDefault();
      } else if (exports.UrlState.update({
        urlPart: ref.href,
        mode: "push",
        deferLocationChange: true
      }) !== "DEFERRED") {
        return ev.preventDefault();
      }
    }
  });
  return ref;
}
exports.anchor = anchor;
function a(...args) {
  console.warn(`helium function "a" has been deprecated.  Please use "anchor" instead.`);
  return anchor(...args);
}
exports.a = a;
function externalLink(...[classOrProp, propOrChild, child]) {
  const {props, innards} = El_Tool_1.standardizeInput(classOrProp, propOrChild, child);
  props.target = "_blank";
  return anchor(props, innards);
}
exports.externalLink = externalLink;
class NavigationEvent extends Event {
  constructor(name, props, newUrl) {
    super(name, props);
    this.pageRefreshCancelled = false;
    this.hashFocusCancelled = false;
    this.preventPageRefresh = () => {
      exports.UrlState.preventRefreshWarningGiven = true;
      this.pageRefreshCancelled = true;
    };
    this.preventHashFocus = () => {
      this.hashFocusCancelled = true;
    };
    this.newUrl = NavigationEvent.createFullUrl(newUrl);
  }
  static createFullUrl(urlPart) {
    try {
      const out = new URL(urlPart);
      if (!out.host) {
        throw "";
      }
      return out;
    } catch (err) {
      return new URL(urlPart, location.origin);
    }
  }
}
exports.NavigationEvent = NavigationEvent;
class _UrlState {
  constructor() {
    this.state = Sourcify_1.Sourcify({
      url: new URL(location.href),
      urlParams: {},
      hash: ""
    });
    this.preventRefreshWarningGiven = false;
    this.initListeners();
  }
  getUrl() {
    if (!this.preventRefreshWarningGiven) {
      console.warn(`If you want to use UrlState for a Single Page App, you must explicitly use 
        "UrlState.observeUrl((ev) => ev.preventPageRefresh())" to stop anchors from refreshing the page.
        If you no longer want to see this warning, you can set "UrlState.preventRefreshWarningGiven = true"
      `.replace(/\s+/g, " "));
      this.preventRefreshWarningGiven = true;
    }
    return this.state.url;
  }
  get href() {
    return this.getUrl().href;
  }
  get host() {
    return this.getUrl().host;
  }
  get hostname() {
    return this.getUrl().hostname;
  }
  get pathname() {
    return this.getUrl().pathname;
  }
  get hash() {
    return this.state.hash;
  }
  get urlParams() {
    return this.state.urlParams;
  }
  followUrl(newUrl) {
    return this.update({
      urlPart: newUrl,
      mode: "push"
    });
  }
  update(argsOrUrl) {
    const args = typeof argsOrUrl === "object" ? argsOrUrl : {
      urlPart: argsOrUrl
    };
    args.mode = args.mode || "push";
    const followResponse = this.newNavEvent("followurl", args.urlPart, true);
    if (followResponse && args.preventHashFocus) {
      followResponse.preventHashFocus();
    }
    let evResponse;
    if (followResponse) {
      const changeEv = this.newNavEvent("locationchange", args.urlPart);
      if (followResponse.hashFocusCancelled) {
        changeEv.preventHashFocus();
      }
      evResponse = window.dispatchEvent(changeEv) && changeEv;
    }
    if (!evResponse) {
      return "BLOCKED";
    }
    if (evResponse.pageRefreshCancelled) {
      if (evResponse.newUrl.href !== location.href) {
        if (location.host !== evResponse.newUrl.host) {
          window.location.href = evResponse.newUrl.href;
          return;
        } else {
          if (args.mode === "push") {
            history.pushState(null, "", args.urlPart);
          } else {
            history.replaceState(null, "", args.urlPart);
          }
        }
      }
      return "CAPTURED";
    }
    if (args.deferLocationChange) {
      return "DEFERRED";
    } else {
      window.location.href = NavigationEvent.createFullUrl(args.urlPart).href;
    }
  }
  replaceUrl(newUrl) {
    return this.update({
      urlPart: newUrl,
      mode: "replace"
    });
  }
  onUrlChange(cb) {
    window.addEventListener("locationchange", cb);
  }
  observeUrl(cb) {
    this.onUrlChange(cb);
    const ev = this.newNavEvent("locationchange", location.href);
    cb(ev);
  }
  newNavEvent(name, newUrl, dispatch = false) {
    const ev = new NavigationEvent(name, {
      cancelable: true
    }, newUrl);
    if (dispatch) {
      window.dispatchEvent(ev);
    }
    return ev;
  }
  initListeners() {
    window.addEventListener('popstate', () => {
      this.newNavEvent("locationchange", location.href, true);
    });
    setTimeout(() => {
      El_Tool_1.onDomReady(() => {
        setTimeout(() => {
          const hash = location.hash;
          const target = El_Tool_1.byId(hash.slice(1));
          if (!!target) {
            target.scrollIntoView({
              behavior: "auto"
            });
          }
        }, 200);
      });
    }, 0);
    let lastHash;
    this.onUrlChange(ev => {
      if (ev.hashFocusCancelled !== true && ev.newUrl.hash !== lastHash) {
        lastHash = ev.newUrl.hash;
        requestAnimationFrame(() => requestAnimationFrame(() => {
          const target = El_Tool_1.byId(lastHash.slice(1));
          if (!!target) {
            target.scrollIntoView({
              behavior: "auto"
            });
          }
        }));
      }
    });
    this.observeUrl(ev => {
      this.state.set("url", ev.newUrl);
    });
    helium_sdx_1.derive({
      anchor: "NO_ANCHOR",
      fn: () => {
        const url = this.getUrl();
        const urlParams = new URLSearchParams(url.search);
        urlParams.forEach((value, key) => {
          this.state.urlParams[key] = value;
        });
        this.state.hash = url.hash;
      }
    });
  }
}
exports._UrlState = _UrlState;
exports.UrlState = new _UrlState();
exports.onUrlChange = exports.UrlState.onUrlChange.bind(exports.UrlState);
exports.observeUrl = exports.UrlState.observeUrl.bind(exports.UrlState);
exports.followUrl = exports.UrlState.followUrl.bind(exports.UrlState);
exports.replaceUrl = exports.UrlState.replaceUrl.bind(exports.UrlState);

},

// node_modules/helium-ui/dist/El-Tool/generateRenderFn.js @29
29: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateRenderFn = void 0;
const DomAnchoredDeriver_1 = __fusereq(48);
const SourceProxyUtilities_1 = __fusereq(37);
function generateRenderFn(render) {
  const renderFn = (stateOrPropsOrCb, stateOrCb) => {
    let stateCb;
    let props;
    let state;
    if (typeof stateOrPropsOrCb === "object") {
      if (("toObjectNoDeps" in stateOrPropsOrCb)) {
        state = stateOrPropsOrCb;
      } else {
        props = stateOrPropsOrCb;
      }
    } else if (typeof stateOrPropsOrCb === "function") {
      stateCb = stateOrPropsOrCb;
    }
    if (typeof stateOrCb === "object") {
      if (!!state) {
        throw new Error(`Can't double define state`);
      }
      state = stateOrCb;
    } else if (typeof stateOrCb === "function") {
      if (!!stateCb) {
        throw new Error(`Can't double define stateCb`);
      }
      stateCb = stateOrCb;
    }
    if (!state) {
      state = SourceProxyUtilities_1.Sourcify({});
    }
    const out = render(props, state);
    stateCb && DomAnchoredDeriver_1.deriveDomAnchored(out, () => stateCb(state));
    return out;
  };
  return renderFn;
}
exports.generateRenderFn = generateRenderFn;

},

// node_modules/helium-ui/dist/El-Tool/RouteState.js @30
30: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HeliumRouteState = void 0;
const SourceProxyUtilities_1 = __fusereq(37);
const Navigation_1 = __fusereq(28);
class HeliumRouteState {
  constructor(configs) {
    this.configMap = new Map();
    this.configList = new Array();
    this.state = SourceProxyUtilities_1.Sourcify({
      currentConfig: null
    });
    Object.entries(configs).forEach(([routeId, config]) => {
      config.id = routeId;
      this.configMap.set(routeId, config);
      this.configList.push(config);
    });
    this.configList.sort((a, b) => {
      if (!a.test || typeof a.test === "function") {
        return 1;
      }
      if (!b.test || typeof b.test === "function") {
        return -1;
      }
      const aLength = typeof a.test === "string" ? a.test.length : a.test.source.length;
      const bLength = typeof b.test === "string" ? b.test.length : b.test.source.length;
      return bLength - aLength;
    });
    console.log(this.configList);
    this.initUrlObserver();
  }
  get id() {
    return this.config ? this.config.id : "NO_ROUTE";
  }
  get config() {
    return this.state.currentConfig;
  }
  getPathSlice(sliceNum) {
    const slices = this.pathname.split(/\/+/g).filter(it => !!it);
    return slices[sliceNum - 1];
  }
  getUrlForRoute(routeId) {
    const {test, getUrl} = this.configMap.get(routeId);
    if (typeof test === "string") {
      return test;
    }
    if (!getUrl) {
      throw new Error(`The "path" property is not defined in the config for "${routeId}"`);
    }
    return typeof getUrl === "string" ? getUrl : getUrl();
  }
  followUrlToRoute(routeId) {
    const url = this.getUrlForRoute(routeId);
    Navigation_1.UrlState.followUrl(url);
  }
  replaceUrlToRoute(routeId) {
    const url = this.getUrlForRoute(routeId);
    Navigation_1.UrlState.replaceUrl(url);
  }
  get pathname() {
    return Navigation_1.UrlState.pathname;
  }
  initUrlObserver() {
    Navigation_1.UrlState.onUrlChange(ev => {
      if (ev.newUrl.host === location.host) {
        ev.preventPageRefresh();
      }
    });
    Navigation_1.UrlState.observeUrl(ev => {
      const configForUrl = this.configList.find(config => this.configMatchesCurrentUrl(config));
      this.state.set("currentConfig", configForUrl || null);
    });
  }
  configMatchesCurrentUrl(config) {
    switch (typeof config.test) {
      case "string":
        return this.pathname.toLowerCase().startsWith(config.test.toLowerCase());
      case "function":
        return config.test(this.pathname);
      case "object":
        return config.test.test(this.pathname);
    }
    return false;
  }
}
exports.HeliumRouteState = HeliumRouteState;

},

// node_modules/helium-ui/dist/El-Tool/ToucherEvents.js @31
31: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.onPush = exports.ToucherSettings = exports.onTouch = exports.onToucherEnterExit = exports.onToucherMove = exports.standardizeToucherEvent = void 0;
function standardizeToucherEvent(ev) {
  const applyMods = obj => {
    Object.entries(obj).forEach(([key, val]) => ev[key] = val);
    return ev;
  };
  if (("clientX" in ev)) {
    if (("touches" in ev)) {
      return ev;
    }
    const fakeTouchList = touches => {
      touches.item = index => touches[index];
      return touches;
    };
    const touch = {
      identifier: 0,
      clientX: ev.clientX,
      clientY: ev.clientY,
      screenX: ev.screenX,
      screenY: ev.screenY,
      pageX: ev.pageX,
      pageY: ev.pageY,
      target: ev.currentTarget,
      touchType: "direct",
      radiusX: 1,
      radiusY: 1,
      rotationAngle: 0,
      force: 0,
      altitudeAngle: 0,
      azimuthAngle: 0
    };
    const mods = {
      touches: fakeTouchList([touch]),
      changedTouches: fakeTouchList([touch]),
      targetTouches: fakeTouchList(ev.currentTarget === ev.target ? [touch] : [])
    };
    return applyMods(mods);
  }
  const mainTouch = ev.targetTouches.item(0) || ev.touches.item(0);
  const clientRect = ev.target.getBoundingClientRect();
  const mods = {
    clientX: mainTouch ? mainTouch.clientX : null,
    clientY: mainTouch ? mainTouch.clientY : null,
    pageX: mainTouch ? mainTouch.pageX : null,
    pageY: mainTouch ? mainTouch.pageY : null,
    offsetX: mainTouch ? mainTouch.clientX - clientRect.x : null,
    offsetY: mainTouch ? mainTouch.clientY - clientRect.y : null,
    screenX: mainTouch ? mainTouch.screenX : null,
    screenY: mainTouch ? mainTouch.screenY : null,
    x: mainTouch ? mainTouch.clientX : null,
    y: mainTouch ? mainTouch.clientY : null,
    button: mainTouch ? 1 : 0,
    buttons: 0,
    movementX: 0,
    movementY: 0,
    relatedTarget: null,
    getModifierState: () => ev.metaKey,
    initMouseEvent: () => null
  };
  return applyMods(mods);
}
exports.standardizeToucherEvent = standardizeToucherEvent;
function onToucherMove(root, cb) {
  const getPosFromClientXY = (x, y) => {
    const clientRect = root.getBoundingClientRect();
    return {
      left: x - clientRect.left,
      top: y - clientRect.top
    };
  };
  const out = {
    mousemove: ev => {
      const toucherMoveEvent = standardizeToucherEvent(ev);
      toucherMoveEvent.positions = [getPosFromClientXY(ev.clientX, ev.clientY)];
      cb(toucherMoveEvent);
    },
    touchmove: ev => {
      const toucherMoveEvent = standardizeToucherEvent(ev);
      toucherMoveEvent.positions = Array.from(ev.touches).map(touch => getPosFromClientXY(touch.clientX, touch.clientY));
      cb(toucherMoveEvent);
      ev.preventDefault();
    }
  };
  Object.entries(out).forEach(([key, cb]) => root.addEventListener(key, cb));
  return out;
}
exports.onToucherMove = onToucherMove;
function onToucherEnterExit(root, cb) {
  const modEvent = (ev, isEnterEvent) => {
    standardizeToucherEvent(ev);
    ev.isEnterEvent = isEnterEvent;
    return ev;
  };
  const out = {
    mouseenter: ev => cb(modEvent(ev, true)),
    mouseleave: ev => cb(modEvent(ev, false)),
    touchstart: ev => {
      cb(modEvent(ev, true));
      ev.preventDefault();
    },
    touchend: ev => {
      cb(modEvent(ev, false));
      ev.preventDefault();
    }
  };
  Object.entries(out).forEach(([key, cb]) => root.addEventListener(key, cb));
  return out;
}
exports.onToucherEnterExit = onToucherEnterExit;
function onTouch(root, cb) {
  const out = {
    mousedown: event => {
      if (event.buttons == 1 || event.buttons == 3) {
        cb(standardizeToucherEvent(event));
      }
    },
    touchstart: event => cb(event)
  };
  Object.entries(out).forEach(([key, cb]) => root.addEventListener(key, cb));
  return out;
}
exports.onTouch = onTouch;
exports.ToucherSettings = {
  maxTouchMsForPushEvent: 800
};
function onPush(root, cb) {
  const target = Element;
  target._onPushEl = {};
  const out = {
    click: ev => {
      target._onPushEl.lastClickTime = Date.now();
      setTimeout(() => {
        const timeSinceTouchEnd = Date.now() - (target._onPushEl.lastTouchEndTime || 0);
        timeSinceTouchEnd > 100 && cb(standardizeToucherEvent(ev));
      }, 0);
    },
    touchstart: ev => {
      setTimeout(() => {
        const timeSinceClick = Date.now() - (target._onPushEl.lastClickTime || 0);
        if (timeSinceClick > 100) {
          target._onPushEl.lastTouchStartTime = Date.now();
          target._onPushEl.lastTouchStartEvent = ev;
        }
      }, 0);
    },
    touchend: ev => {
      const timeSinceTouchStart = Date.now() - (target._onPushEl.lastTouchStartTime || 0);
      if (timeSinceTouchStart <= exports.ToucherSettings.maxTouchMsForPushEvent) {
        target._onPushEl.lastTouchEndTime = Date.now();
        setTimeout(() => cb(standardizeToucherEvent(target._onPushEl.lastTouchStartEvent)), 0);
      }
    }
  };
  Object.entries(out).forEach(([key, cb]) => root.addEventListener(key, cb));
  return out;
}
exports.onPush = onPush;

},

// node_modules/helium-ui/dist/Derivations/Derivers/index.js @32
32: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(52), exports);
__exportStar(__fusereq(48), exports);
__exportStar(__fusereq(53), exports);

},

// node_modules/helium-ui/dist/Derivations/Sources/index.js @33
33: function(__fusereq, exports, module){
"use strict";
var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});
var __exportStar = this && this.__exportStar || (function (m, exports) {
  for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
});
Object.defineProperty(exports, "__esModule", {
  value: true
});
__exportStar(__fusereq(49), exports);
__exportStar(__fusereq(50), exports);
__exportStar(__fusereq(51), exports);

},

// node_modules/helium-ui/dist/Sourcify/SourceProxyObject.js @34
34: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ASSERT_SHALLOW_TYPE_MATCH = exports.SourceProxyObject = void 0;
const SourceMap_1 = __fusereq(51);
const helium_sdx_1 = __fusereq(17);
const SourceProxyUtilities_1 = __fusereq(37);
class SourceProxyObject extends helium_sdx_1.SourceProxyObject {
  constructor(target) {
    SourceProxyUtilities_1.assertSourcificationManagerModded();
    super(target, new SourceMap_1.SourceMap());
  }
  getSrcMap() {
    return this.srcMap;
  }
  renderMap(renderFn) {
    this.refreshAll();
    return this.getSrcMap().renderMap(renderFn);
  }
  renderMapKeyed(renderFn) {
    this.refreshAll();
    return this.getSrcMap().renderMapKeyed(renderFn);
  }
}
exports.SourceProxyObject = SourceProxyObject;
;
exports.ASSERT_SHALLOW_TYPE_MATCH = {};

},

// node_modules/helium-ui/dist/Sourcify/SourceProxyArray.js @35
35: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ASSERT_SHALLOW_TYPE_MATCH_ARRAY = exports.SourceProxyArray = void 0;
const SourceArray_1 = __fusereq(50);
const helium_sdx_1 = __fusereq(17);
const SourceProxyUtilities_1 = __fusereq(37);
class SourceProxyArray extends helium_sdx_1.SourceProxyArray {
  constructor(target) {
    SourceProxyUtilities_1.assertSourcificationManagerModded();
    super(target, new SourceArray_1.SourceArray());
  }
  getSrcArray() {
    return this.srcArray;
  }
  renderMap(renderFn) {
    this.refreshAll();
    return this.getSrcArray().renderMap(renderFn);
  }
  renderMapKeyed(renderFn) {
    this.refreshAll();
    return this.getSrcArray().renderMapKeyed(renderFn);
  }
}
exports.SourceProxyArray = SourceProxyArray;
exports.ASSERT_SHALLOW_TYPE_MATCH_ARRAY = {};

},

// node_modules/helium-ui/dist/Sourcify/SourceProxyShallowTypes.js @36
36: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
function __() {
  ;
  const b = {};
  function testFn(testMe) {
    return null;
  }
  const out = b.a[3];
  const la = b.a.overwrite([]);
  const ba = b.a.sort();
  ba.renderMap(() => null);
  la.renderMap(() => null);
  const a = b.a.filter(() => true);
  const c = a[0];
  const d = b.get("a");
}

},

// node_modules/helium-ui/dist/Sourcify/SourceProxyUtilities.js @37
37: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.renderMapKeyed = exports.renderMap = exports.isProxied = exports.Unsourcify = exports.assertSourcificationManagerModded = exports.srcfy = exports.Sourcify = void 0;
const El_Tool_1 = __fusereq(26);
const helium_sdx_1 = __fusereq(17);
function Sourcify(target) {
  assertSourcificationManagerModded();
  return helium_sdx_1.Sourcify(target);
}
exports.Sourcify = Sourcify;
exports.srcfy = Sourcify;
let SourcificationManagerModded = false;
function assertSourcificationManagerModded() {
  if (!SourcificationManagerModded) {
    const {SourceProxyArray} = __fusereq(35);
    const {SourceProxyObject} = __fusereq(34);
    helium_sdx_1.SourcificationManager.createSourceProxyArray = item => new SourceProxyArray(item);
    helium_sdx_1.SourcificationManager.createSourceProxyObject = item => new SourceProxyObject(item);
    SourcificationManagerModded = true;
  }
}
exports.assertSourcificationManagerModded = assertSourcificationManagerModded;
function Unsourcify(target) {
  return helium_sdx_1.Unsourcify(target);
}
exports.Unsourcify = Unsourcify;
function isProxied(checkMe) {
  return helium_sdx_1.isProxied(checkMe);
}
exports.isProxied = isProxied;
function renderMap(obj, renderFn) {
  return _anyRMap(false, obj, renderFn);
}
exports.renderMap = renderMap;
function renderMapKeyed(obj, renderFn) {
  return _anyRMap(true, obj, renderFn);
}
exports.renderMapKeyed = renderMapKeyed;
function _anyRMap(withKey, obj, renderFn) {
  if (isProxied(obj)) {
    if (withKey) {
      return obj.rkmap(renderFn);
    }
    return obj.rmap(renderFn);
  } else if (obj && typeof obj === "object") {
    const out = [];
    let renders;
    if (Array.isArray(obj)) {
      renders = obj.map((value, index) => renderFn(value, index));
    } else {
      renders = Object.entries(obj).map(([key, value]) => renderFn(value, key));
    }
    renders.forEach(render => {
      const nodesList = El_Tool_1.convertInnardsToElements(render);
      if (!nodesList || !nodesList.length) {
        return;
      }
      nodesList.forEach(node => out.push(node));
    });
    return out;
  }
}

},

// node_modules/helium-sdx/dist/Derivations/index.js @38
38: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(54));
__export(__fusereq(55));
__export(__fusereq(56));
__export(__fusereq(57));
__export(__fusereq(58));
__export(__fusereq(59));

},

// node_modules/helium-sdx/dist/Sourcify/index.js @39
39: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(60));
__export(__fusereq(61));
__export(__fusereq(62));
__export(__fusereq(63));
__export(__fusereq(64));

},

// node_modules/helium-sdx/dist/AnotherLogger.js @40
40: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const logPrepends = {
  freeze: "❄️",
  unfreeze: "🔥",
  dirty: "💩",
  sourceUpdated: "📦",
  treeMutations: "🌲",
  scopes: "🔎",
  sourceRequest: "🧲",
  debugDeriver: "🐞"
};
const showLogTypes = {
  freeze: false,
  unfreeze: false,
  dirty: false,
  sourceUpdated: false,
  treeMutations: false,
  scopes: false,
  sourceRequest: false,
  debugDeriver: false
};
function showLoggingType(type, shouldShow = true) {
  showLogTypes[type] = shouldShow;
}
exports.showLoggingType = showLoggingType;
function showMiscLoggingType(type, shouldShow = true) {
  showLogTypes[type] = shouldShow;
}
exports.showMiscLoggingType = showMiscLoggingType;
function aLog(...args) {
  args = args.map(arg => arg && typeof arg === "object" && arg._static || arg);
  if (args.length <= 1) {
    return console.log(args[0]);
  }
  const type = args[0];
  const shouldShow = showLogTypes[type];
  if (shouldShow === false) {
    return;
  }
  const prepend = logPrepends[type];
  if (prepend) {
    args.shift();
    if (typeof args[0] === "string") {
      args[0] = `${prepend} ${args[0]}`;
    } else {
      args.unshift(prepend);
    }
  }
  console.log(...args);
}
exports.aLog = aLog;
if (typeof window !== "undefined") {
  window.aLog = aLog;
}

},

// node_modules/helium-sdx/node_modules/robinhood-license-assert/dist/index.js @41
41: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
;
;
let licenses = [];
function assertRobinHoodLicense(license) {
  licenses.push(license);
  showLicenseMessage();
}
exports.assertRobinHoodLicense = assertRobinHoodLicense;
let timeout;
function showLicenseMessage() {
  const _mainContext = typeof window !== "undefined" ? window : window;
  if (timeout) {
    clearTimeout(timeout);
  }
  timeout = setTimeout(() => {
    timeout = undefined;
    const listUs = licenses.filter(license => {
      const propName = getPropName(license);
      return (/ *resolved */i).test(_mainContext[propName]) !== true;
    });
    licenses = [];
    if (listUs.length === 0) {
      return;
    }
    const pkg = listUs.length > 1 ? "packages" : "package";
    let out = `POTENTIALLY IMPORTANT LICENSING INFO:
    
    The following npm ${pkg} have un-resolved licenses:\n`;
    listUs.forEach((it, index) => {
      out += `${index + 1}.  ${it.packageName}: ${getLicenseName(it)}\n`;
    });
    out += "\n";
    const minThresh = listUs.sort((a, b) => getThresholdValue(a) - getThresholdValue(b))[0];
    const thresholdWritten = getThresholdLong(minThresh);
    const s = listUs.length > 1;
    out += `Don't Panic!  If you are nowhere near ${thresholdWritten} then the only limitation is that your /
      project must not be a direct competitor to the listed ${pkg} above.  
      
      If you do happen to pass /
      the ${thresholdWritten} threshold (congrats) you will have a 6 month maximum trial period / 
      before needing to buying ${s ? "any (life-time) permits" : "a life-time permit"}.  If you / 
      are creating a project which passes-through /
      the main functionality of the listed ${pkg}, DO NOT resolve this /
      license; you must instead pass it on to your users to resolve. /
      
      Full license information for the ${pkg} can be found at:
    `;
    listUs.forEach((it, index) => {
      out += `${index + 1}.  ${it.packageName}: ${it.link}\n`;
    });
    out += `\nOnce you have resolved each license, you can suppress its message with /
    the following code:\n`;
    listUs.forEach(it => {
      out += `(window || global)["${getPropName(it)}"] = "resolved";\n`;
    });
    out += `
    If any of this seems unfair or unjust, there is contact information in each /
    license for its creator.  In the spirit of Robin Hood.
    `;
    console.warn(out.replace(/\n[ \t]*/g, "\n").replace(/\/ *\n/g, ""));
  }, 400);
}
function getThresholdLong({threshold}) {
  let mag;
  switch (threshold.magnitude) {
    case "M":
      mag = "Million";
      break;
    case "B":
      mag = "Billion";
      break;
    case "T":
      mag = "Trillion";
      break;
  }
  return `$${threshold.num} ${mag}`;
}
function getThresholdValue({threshold}) {
  let mag;
  switch (threshold.magnitude) {
    case "M":
      mag = 1000000;
      break;
    case "B":
      mag = 1000000000;
      break;
    case "T":
      mag = 1000000000000;
      break;
  }
  return threshold.num * mag;
}
function getLicenseName({threshold, rate, version}) {
  return `${version}.RobinHood-License.info/${getSlug(threshold)}/${getSlug(rate)}`;
}
function getPropName({packageName, threshold, rate}) {
  return `RobinHood-License for ${packageName} @ ${getSlug(threshold)}/${getSlug(rate)}`;
}
function getSlug(slugNum) {
  return slugNum.num + (slugNum.magnitude || "");
}

},

// node_modules/markdown-it/index.js @42
42: function(__fusereq, exports, module){
'use strict';
module.exports = __fusereq(65);

},

// node_modules/helium-md/node_modules/strip-indent/index.js @43
43: function(__fusereq, exports, module){
'use strict';
const minIndent = __fusereq(66);
module.exports = string => {
  const indent = minIndent(string);
  if (indent === 0) {
    return string;
  }
  const regex = new RegExp(`^[ \\t]{${indent}}`, 'gm');
  return string.replace(regex, '');
};

},

// node_modules/dompurify/dist/purify.js @44
44: function(__fusereq, exports, module){
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() : typeof define === 'function' && define.amd ? define(factory) : (global = global || self, global.DOMPurify = factory());
})(this, function () {
  'use strict';
  function _toConsumableArray(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
        arr2[i] = arr[i];
      }
      return arr2;
    } else {
      return Array.from(arr);
    }
  }
  var hasOwnProperty = Object.hasOwnProperty, setPrototypeOf = Object.setPrototypeOf, isFrozen = Object.isFrozen, getPrototypeOf = Object.getPrototypeOf, getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
  var freeze = Object.freeze, seal = Object.seal, create = Object.create;
  var _ref = typeof Reflect !== 'undefined' && Reflect, apply = _ref.apply, construct = _ref.construct;
  if (!apply) {
    apply = function apply(fun, thisValue, args) {
      return fun.apply(thisValue, args);
    };
  }
  if (!freeze) {
    freeze = function freeze(x) {
      return x;
    };
  }
  if (!seal) {
    seal = function seal(x) {
      return x;
    };
  }
  if (!construct) {
    construct = function construct(Func, args) {
      return new (Function.prototype.bind.apply(Func, [null].concat(_toConsumableArray(args))))();
    };
  }
  var arrayForEach = unapply(Array.prototype.forEach);
  var arrayPop = unapply(Array.prototype.pop);
  var arrayPush = unapply(Array.prototype.push);
  var stringToLowerCase = unapply(String.prototype.toLowerCase);
  var stringMatch = unapply(String.prototype.match);
  var stringReplace = unapply(String.prototype.replace);
  var stringIndexOf = unapply(String.prototype.indexOf);
  var stringTrim = unapply(String.prototype.trim);
  var regExpTest = unapply(RegExp.prototype.test);
  var typeErrorCreate = unconstruct(TypeError);
  function unapply(func) {
    return function (thisArg) {
      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }
      return apply(func, thisArg, args);
    };
  }
  function unconstruct(func) {
    return function () {
      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }
      return construct(func, args);
    };
  }
  function addToSet(set, array) {
    if (setPrototypeOf) {
      setPrototypeOf(set, null);
    }
    var l = array.length;
    while (l--) {
      var element = array[l];
      if (typeof element === 'string') {
        var lcElement = stringToLowerCase(element);
        if (lcElement !== element) {
          if (!isFrozen(array)) {
            array[l] = lcElement;
          }
          element = lcElement;
        }
      }
      set[element] = true;
    }
    return set;
  }
  function clone(object) {
    var newObject = create(null);
    var property = void 0;
    for (property in object) {
      if (apply(hasOwnProperty, object, [property])) {
        newObject[property] = object[property];
      }
    }
    return newObject;
  }
  function lookupGetter(object, prop) {
    while (object !== null) {
      var desc = getOwnPropertyDescriptor(object, prop);
      if (desc) {
        if (desc.get) {
          return unapply(desc.get);
        }
        if (typeof desc.value === 'function') {
          return unapply(desc.value);
        }
      }
      object = getPrototypeOf(object);
    }
    return null;
  }
  var html = freeze(['a', 'abbr', 'acronym', 'address', 'area', 'article', 'aside', 'audio', 'b', 'bdi', 'bdo', 'big', 'blink', 'blockquote', 'body', 'br', 'button', 'canvas', 'caption', 'center', 'cite', 'code', 'col', 'colgroup', 'content', 'data', 'datalist', 'dd', 'decorator', 'del', 'details', 'dfn', 'dialog', 'dir', 'div', 'dl', 'dt', 'element', 'em', 'fieldset', 'figcaption', 'figure', 'font', 'footer', 'form', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head', 'header', 'hgroup', 'hr', 'html', 'i', 'img', 'input', 'ins', 'kbd', 'label', 'legend', 'li', 'main', 'map', 'mark', 'marquee', 'menu', 'menuitem', 'meter', 'nav', 'nobr', 'ol', 'optgroup', 'option', 'output', 'p', 'picture', 'pre', 'progress', 'q', 'rp', 'rt', 'ruby', 's', 'samp', 'section', 'select', 'shadow', 'small', 'source', 'spacer', 'span', 'strike', 'strong', 'style', 'sub', 'summary', 'sup', 'table', 'tbody', 'td', 'template', 'textarea', 'tfoot', 'th', 'thead', 'time', 'tr', 'track', 'tt', 'u', 'ul', 'var', 'video', 'wbr']);
  var svg = freeze(['svg', 'a', 'altglyph', 'altglyphdef', 'altglyphitem', 'animatecolor', 'animatemotion', 'animatetransform', 'circle', 'clippath', 'defs', 'desc', 'ellipse', 'filter', 'font', 'g', 'glyph', 'glyphref', 'hkern', 'image', 'line', 'lineargradient', 'marker', 'mask', 'metadata', 'mpath', 'path', 'pattern', 'polygon', 'polyline', 'radialgradient', 'rect', 'stop', 'style', 'switch', 'symbol', 'text', 'textpath', 'title', 'tref', 'tspan', 'view', 'vkern']);
  var svgFilters = freeze(['feBlend', 'feColorMatrix', 'feComponentTransfer', 'feComposite', 'feConvolveMatrix', 'feDiffuseLighting', 'feDisplacementMap', 'feDistantLight', 'feFlood', 'feFuncA', 'feFuncB', 'feFuncG', 'feFuncR', 'feGaussianBlur', 'feMerge', 'feMergeNode', 'feMorphology', 'feOffset', 'fePointLight', 'feSpecularLighting', 'feSpotLight', 'feTile', 'feTurbulence']);
  var svgDisallowed = freeze(['animate', 'color-profile', 'cursor', 'discard', 'fedropshadow', 'feimage', 'font-face', 'font-face-format', 'font-face-name', 'font-face-src', 'font-face-uri', 'foreignobject', 'hatch', 'hatchpath', 'mesh', 'meshgradient', 'meshpatch', 'meshrow', 'missing-glyph', 'script', 'set', 'solidcolor', 'unknown', 'use']);
  var mathMl = freeze(['math', 'menclose', 'merror', 'mfenced', 'mfrac', 'mglyph', 'mi', 'mlabeledtr', 'mmultiscripts', 'mn', 'mo', 'mover', 'mpadded', 'mphantom', 'mroot', 'mrow', 'ms', 'mspace', 'msqrt', 'mstyle', 'msub', 'msup', 'msubsup', 'mtable', 'mtd', 'mtext', 'mtr', 'munder', 'munderover']);
  var mathMlDisallowed = freeze(['maction', 'maligngroup', 'malignmark', 'mlongdiv', 'mscarries', 'mscarry', 'msgroup', 'mstack', 'msline', 'msrow', 'semantics', 'annotation', 'annotation-xml', 'mprescripts', 'none']);
  var text = freeze(['#text']);
  var html$1 = freeze(['accept', 'action', 'align', 'alt', 'autocapitalize', 'autocomplete', 'autopictureinpicture', 'autoplay', 'background', 'bgcolor', 'border', 'capture', 'cellpadding', 'cellspacing', 'checked', 'cite', 'class', 'clear', 'color', 'cols', 'colspan', 'controls', 'controlslist', 'coords', 'crossorigin', 'datetime', 'decoding', 'default', 'dir', 'disabled', 'disablepictureinpicture', 'disableremoteplayback', 'download', 'draggable', 'enctype', 'enterkeyhint', 'face', 'for', 'headers', 'height', 'hidden', 'high', 'href', 'hreflang', 'id', 'inputmode', 'integrity', 'ismap', 'kind', 'label', 'lang', 'list', 'loading', 'loop', 'low', 'max', 'maxlength', 'media', 'method', 'min', 'minlength', 'multiple', 'muted', 'name', 'noshade', 'novalidate', 'nowrap', 'open', 'optimum', 'pattern', 'placeholder', 'playsinline', 'poster', 'preload', 'pubdate', 'radiogroup', 'readonly', 'rel', 'required', 'rev', 'reversed', 'role', 'rows', 'rowspan', 'spellcheck', 'scope', 'selected', 'shape', 'size', 'sizes', 'span', 'srclang', 'start', 'src', 'srcset', 'step', 'style', 'summary', 'tabindex', 'title', 'translate', 'type', 'usemap', 'valign', 'value', 'width', 'xmlns']);
  var svg$1 = freeze(['accent-height', 'accumulate', 'additive', 'alignment-baseline', 'ascent', 'attributename', 'attributetype', 'azimuth', 'basefrequency', 'baseline-shift', 'begin', 'bias', 'by', 'class', 'clip', 'clippathunits', 'clip-path', 'clip-rule', 'color', 'color-interpolation', 'color-interpolation-filters', 'color-profile', 'color-rendering', 'cx', 'cy', 'd', 'dx', 'dy', 'diffuseconstant', 'direction', 'display', 'divisor', 'dur', 'edgemode', 'elevation', 'end', 'fill', 'fill-opacity', 'fill-rule', 'filter', 'filterunits', 'flood-color', 'flood-opacity', 'font-family', 'font-size', 'font-size-adjust', 'font-stretch', 'font-style', 'font-variant', 'font-weight', 'fx', 'fy', 'g1', 'g2', 'glyph-name', 'glyphref', 'gradientunits', 'gradienttransform', 'height', 'href', 'id', 'image-rendering', 'in', 'in2', 'k', 'k1', 'k2', 'k3', 'k4', 'kerning', 'keypoints', 'keysplines', 'keytimes', 'lang', 'lengthadjust', 'letter-spacing', 'kernelmatrix', 'kernelunitlength', 'lighting-color', 'local', 'marker-end', 'marker-mid', 'marker-start', 'markerheight', 'markerunits', 'markerwidth', 'maskcontentunits', 'maskunits', 'max', 'mask', 'media', 'method', 'mode', 'min', 'name', 'numoctaves', 'offset', 'operator', 'opacity', 'order', 'orient', 'orientation', 'origin', 'overflow', 'paint-order', 'path', 'pathlength', 'patterncontentunits', 'patterntransform', 'patternunits', 'points', 'preservealpha', 'preserveaspectratio', 'primitiveunits', 'r', 'rx', 'ry', 'radius', 'refx', 'refy', 'repeatcount', 'repeatdur', 'restart', 'result', 'rotate', 'scale', 'seed', 'shape-rendering', 'specularconstant', 'specularexponent', 'spreadmethod', 'startoffset', 'stddeviation', 'stitchtiles', 'stop-color', 'stop-opacity', 'stroke-dasharray', 'stroke-dashoffset', 'stroke-linecap', 'stroke-linejoin', 'stroke-miterlimit', 'stroke-opacity', 'stroke', 'stroke-width', 'style', 'surfacescale', 'systemlanguage', 'tabindex', 'targetx', 'targety', 'transform', 'text-anchor', 'text-decoration', 'text-rendering', 'textlength', 'type', 'u1', 'u2', 'unicode', 'values', 'viewbox', 'visibility', 'version', 'vert-adv-y', 'vert-origin-x', 'vert-origin-y', 'width', 'word-spacing', 'wrap', 'writing-mode', 'xchannelselector', 'ychannelselector', 'x', 'x1', 'x2', 'xmlns', 'y', 'y1', 'y2', 'z', 'zoomandpan']);
  var mathMl$1 = freeze(['accent', 'accentunder', 'align', 'bevelled', 'close', 'columnsalign', 'columnlines', 'columnspan', 'denomalign', 'depth', 'dir', 'display', 'displaystyle', 'encoding', 'fence', 'frame', 'height', 'href', 'id', 'largeop', 'length', 'linethickness', 'lspace', 'lquote', 'mathbackground', 'mathcolor', 'mathsize', 'mathvariant', 'maxsize', 'minsize', 'movablelimits', 'notation', 'numalign', 'open', 'rowalign', 'rowlines', 'rowspacing', 'rowspan', 'rspace', 'rquote', 'scriptlevel', 'scriptminsize', 'scriptsizemultiplier', 'selection', 'separator', 'separators', 'stretchy', 'subscriptshift', 'supscriptshift', 'symmetric', 'voffset', 'width', 'xmlns']);
  var xml = freeze(['xlink:href', 'xml:id', 'xlink:title', 'xml:space', 'xmlns:xlink']);
  var MUSTACHE_EXPR = seal(/\{\{[\s\S]*|[\s\S]*\}\}/gm);
  var ERB_EXPR = seal(/<%[\s\S]*|[\s\S]*%>/gm);
  var DATA_ATTR = seal(/^data-[\-\w.\u00B7-\uFFFF]/);
  var ARIA_ATTR = seal(/^aria-[\-\w]+$/);
  var IS_ALLOWED_URI = seal(/^(?:(?:(?:f|ht)tps?|mailto|tel|callto|cid|xmpp):|[^a-z]|[a-z+.\-]+(?:[^a-z+.\-:]|$))/i);
  var IS_SCRIPT_OR_DATA = seal(/^(?:\w+script|data):/i);
  var ATTR_WHITESPACE = seal(/[\u0000-\u0020\u00A0\u1680\u180E\u2000-\u2029\u205F\u3000]/g);
  var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  };
  function _toConsumableArray$1(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
        arr2[i] = arr[i];
      }
      return arr2;
    } else {
      return Array.from(arr);
    }
  }
  var getGlobal = function getGlobal() {
    return typeof window === 'undefined' ? null : window;
  };
  var _createTrustedTypesPolicy = function _createTrustedTypesPolicy(trustedTypes, document) {
    if ((typeof trustedTypes === 'undefined' ? 'undefined' : _typeof(trustedTypes)) !== 'object' || typeof trustedTypes.createPolicy !== 'function') {
      return null;
    }
    var suffix = null;
    var ATTR_NAME = 'data-tt-policy-suffix';
    if (document.currentScript && document.currentScript.hasAttribute(ATTR_NAME)) {
      suffix = document.currentScript.getAttribute(ATTR_NAME);
    }
    var policyName = 'dompurify' + (suffix ? '#' + suffix : '');
    try {
      return trustedTypes.createPolicy(policyName, {
        createHTML: function createHTML(html$$1) {
          return html$$1;
        }
      });
    } catch (_) {
      console.warn('TrustedTypes policy ' + policyName + ' could not be created.');
      return null;
    }
  };
  function createDOMPurify() {
    var window = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : getGlobal();
    var DOMPurify = function DOMPurify(root) {
      return createDOMPurify(root);
    };
    DOMPurify.version = '2.2.6';
    DOMPurify.removed = [];
    if (!window || !window.document || window.document.nodeType !== 9) {
      DOMPurify.isSupported = false;
      return DOMPurify;
    }
    var originalDocument = window.document;
    var document = window.document;
    var DocumentFragment = window.DocumentFragment, HTMLTemplateElement = window.HTMLTemplateElement, Node = window.Node, Element = window.Element, NodeFilter = window.NodeFilter, _window$NamedNodeMap = window.NamedNodeMap, NamedNodeMap = _window$NamedNodeMap === undefined ? window.NamedNodeMap || window.MozNamedAttrMap : _window$NamedNodeMap, Text = window.Text, Comment = window.Comment, DOMParser = window.DOMParser, trustedTypes = window.trustedTypes;
    var ElementPrototype = Element.prototype;
    var cloneNode = lookupGetter(ElementPrototype, 'cloneNode');
    var getNextSibling = lookupGetter(ElementPrototype, 'nextSibling');
    var getChildNodes = lookupGetter(ElementPrototype, 'childNodes');
    var getParentNode = lookupGetter(ElementPrototype, 'parentNode');
    if (typeof HTMLTemplateElement === 'function') {
      var template = document.createElement('template');
      if (template.content && template.content.ownerDocument) {
        document = template.content.ownerDocument;
      }
    }
    var trustedTypesPolicy = _createTrustedTypesPolicy(trustedTypes, originalDocument);
    var emptyHTML = trustedTypesPolicy && RETURN_TRUSTED_TYPE ? trustedTypesPolicy.createHTML('') : '';
    var _document = document, implementation = _document.implementation, createNodeIterator = _document.createNodeIterator, getElementsByTagName = _document.getElementsByTagName, createDocumentFragment = _document.createDocumentFragment;
    var importNode = originalDocument.importNode;
    var documentMode = {};
    try {
      documentMode = clone(document).documentMode ? document.documentMode : {};
    } catch (_) {}
    var hooks = {};
    DOMPurify.isSupported = implementation && typeof implementation.createHTMLDocument !== 'undefined' && documentMode !== 9;
    var MUSTACHE_EXPR$$1 = MUSTACHE_EXPR, ERB_EXPR$$1 = ERB_EXPR, DATA_ATTR$$1 = DATA_ATTR, ARIA_ATTR$$1 = ARIA_ATTR, IS_SCRIPT_OR_DATA$$1 = IS_SCRIPT_OR_DATA, ATTR_WHITESPACE$$1 = ATTR_WHITESPACE;
    var IS_ALLOWED_URI$$1 = IS_ALLOWED_URI;
    var ALLOWED_TAGS = null;
    var DEFAULT_ALLOWED_TAGS = addToSet({}, [].concat(_toConsumableArray$1(html), _toConsumableArray$1(svg), _toConsumableArray$1(svgFilters), _toConsumableArray$1(mathMl), _toConsumableArray$1(text)));
    var ALLOWED_ATTR = null;
    var DEFAULT_ALLOWED_ATTR = addToSet({}, [].concat(_toConsumableArray$1(html$1), _toConsumableArray$1(svg$1), _toConsumableArray$1(mathMl$1), _toConsumableArray$1(xml)));
    var FORBID_TAGS = null;
    var FORBID_ATTR = null;
    var ALLOW_ARIA_ATTR = true;
    var ALLOW_DATA_ATTR = true;
    var ALLOW_UNKNOWN_PROTOCOLS = false;
    var SAFE_FOR_TEMPLATES = false;
    var WHOLE_DOCUMENT = false;
    var SET_CONFIG = false;
    var FORCE_BODY = false;
    var RETURN_DOM = false;
    var RETURN_DOM_FRAGMENT = false;
    var RETURN_DOM_IMPORT = true;
    var RETURN_TRUSTED_TYPE = false;
    var SANITIZE_DOM = true;
    var KEEP_CONTENT = true;
    var IN_PLACE = false;
    var USE_PROFILES = {};
    var FORBID_CONTENTS = addToSet({}, ['annotation-xml', 'audio', 'colgroup', 'desc', 'foreignobject', 'head', 'iframe', 'math', 'mi', 'mn', 'mo', 'ms', 'mtext', 'noembed', 'noframes', 'noscript', 'plaintext', 'script', 'style', 'svg', 'template', 'thead', 'title', 'video', 'xmp']);
    var DATA_URI_TAGS = null;
    var DEFAULT_DATA_URI_TAGS = addToSet({}, ['audio', 'video', 'img', 'source', 'image', 'track']);
    var URI_SAFE_ATTRIBUTES = null;
    var DEFAULT_URI_SAFE_ATTRIBUTES = addToSet({}, ['alt', 'class', 'for', 'id', 'label', 'name', 'pattern', 'placeholder', 'summary', 'title', 'value', 'style', 'xmlns']);
    var CONFIG = null;
    var formElement = document.createElement('form');
    var _parseConfig = function _parseConfig(cfg) {
      if (CONFIG && CONFIG === cfg) {
        return;
      }
      if (!cfg || (typeof cfg === 'undefined' ? 'undefined' : _typeof(cfg)) !== 'object') {
        cfg = {};
      }
      cfg = clone(cfg);
      ALLOWED_TAGS = ('ALLOWED_TAGS' in cfg) ? addToSet({}, cfg.ALLOWED_TAGS) : DEFAULT_ALLOWED_TAGS;
      ALLOWED_ATTR = ('ALLOWED_ATTR' in cfg) ? addToSet({}, cfg.ALLOWED_ATTR) : DEFAULT_ALLOWED_ATTR;
      URI_SAFE_ATTRIBUTES = ('ADD_URI_SAFE_ATTR' in cfg) ? addToSet(clone(DEFAULT_URI_SAFE_ATTRIBUTES), cfg.ADD_URI_SAFE_ATTR) : DEFAULT_URI_SAFE_ATTRIBUTES;
      DATA_URI_TAGS = ('ADD_DATA_URI_TAGS' in cfg) ? addToSet(clone(DEFAULT_DATA_URI_TAGS), cfg.ADD_DATA_URI_TAGS) : DEFAULT_DATA_URI_TAGS;
      FORBID_TAGS = ('FORBID_TAGS' in cfg) ? addToSet({}, cfg.FORBID_TAGS) : {};
      FORBID_ATTR = ('FORBID_ATTR' in cfg) ? addToSet({}, cfg.FORBID_ATTR) : {};
      USE_PROFILES = ('USE_PROFILES' in cfg) ? cfg.USE_PROFILES : false;
      ALLOW_ARIA_ATTR = cfg.ALLOW_ARIA_ATTR !== false;
      ALLOW_DATA_ATTR = cfg.ALLOW_DATA_ATTR !== false;
      ALLOW_UNKNOWN_PROTOCOLS = cfg.ALLOW_UNKNOWN_PROTOCOLS || false;
      SAFE_FOR_TEMPLATES = cfg.SAFE_FOR_TEMPLATES || false;
      WHOLE_DOCUMENT = cfg.WHOLE_DOCUMENT || false;
      RETURN_DOM = cfg.RETURN_DOM || false;
      RETURN_DOM_FRAGMENT = cfg.RETURN_DOM_FRAGMENT || false;
      RETURN_DOM_IMPORT = cfg.RETURN_DOM_IMPORT !== false;
      RETURN_TRUSTED_TYPE = cfg.RETURN_TRUSTED_TYPE || false;
      FORCE_BODY = cfg.FORCE_BODY || false;
      SANITIZE_DOM = cfg.SANITIZE_DOM !== false;
      KEEP_CONTENT = cfg.KEEP_CONTENT !== false;
      IN_PLACE = cfg.IN_PLACE || false;
      IS_ALLOWED_URI$$1 = cfg.ALLOWED_URI_REGEXP || IS_ALLOWED_URI$$1;
      if (SAFE_FOR_TEMPLATES) {
        ALLOW_DATA_ATTR = false;
      }
      if (RETURN_DOM_FRAGMENT) {
        RETURN_DOM = true;
      }
      if (USE_PROFILES) {
        ALLOWED_TAGS = addToSet({}, [].concat(_toConsumableArray$1(text)));
        ALLOWED_ATTR = [];
        if (USE_PROFILES.html === true) {
          addToSet(ALLOWED_TAGS, html);
          addToSet(ALLOWED_ATTR, html$1);
        }
        if (USE_PROFILES.svg === true) {
          addToSet(ALLOWED_TAGS, svg);
          addToSet(ALLOWED_ATTR, svg$1);
          addToSet(ALLOWED_ATTR, xml);
        }
        if (USE_PROFILES.svgFilters === true) {
          addToSet(ALLOWED_TAGS, svgFilters);
          addToSet(ALLOWED_ATTR, svg$1);
          addToSet(ALLOWED_ATTR, xml);
        }
        if (USE_PROFILES.mathMl === true) {
          addToSet(ALLOWED_TAGS, mathMl);
          addToSet(ALLOWED_ATTR, mathMl$1);
          addToSet(ALLOWED_ATTR, xml);
        }
      }
      if (cfg.ADD_TAGS) {
        if (ALLOWED_TAGS === DEFAULT_ALLOWED_TAGS) {
          ALLOWED_TAGS = clone(ALLOWED_TAGS);
        }
        addToSet(ALLOWED_TAGS, cfg.ADD_TAGS);
      }
      if (cfg.ADD_ATTR) {
        if (ALLOWED_ATTR === DEFAULT_ALLOWED_ATTR) {
          ALLOWED_ATTR = clone(ALLOWED_ATTR);
        }
        addToSet(ALLOWED_ATTR, cfg.ADD_ATTR);
      }
      if (cfg.ADD_URI_SAFE_ATTR) {
        addToSet(URI_SAFE_ATTRIBUTES, cfg.ADD_URI_SAFE_ATTR);
      }
      if (KEEP_CONTENT) {
        ALLOWED_TAGS['#text'] = true;
      }
      if (WHOLE_DOCUMENT) {
        addToSet(ALLOWED_TAGS, ['html', 'head', 'body']);
      }
      if (ALLOWED_TAGS.table) {
        addToSet(ALLOWED_TAGS, ['tbody']);
        delete FORBID_TAGS.tbody;
      }
      if (freeze) {
        freeze(cfg);
      }
      CONFIG = cfg;
    };
    var MATHML_TEXT_INTEGRATION_POINTS = addToSet({}, ['mi', 'mo', 'mn', 'ms', 'mtext']);
    var HTML_INTEGRATION_POINTS = addToSet({}, ['foreignobject', 'desc', 'title', 'annotation-xml']);
    var ALL_SVG_TAGS = addToSet({}, svg);
    addToSet(ALL_SVG_TAGS, svgFilters);
    addToSet(ALL_SVG_TAGS, svgDisallowed);
    var ALL_MATHML_TAGS = addToSet({}, mathMl);
    addToSet(ALL_MATHML_TAGS, mathMlDisallowed);
    var MATHML_NAMESPACE = 'http://www.w3.org/1998/Math/MathML';
    var SVG_NAMESPACE = 'http://www.w3.org/2000/svg';
    var HTML_NAMESPACE = 'http://www.w3.org/1999/xhtml';
    var _checkValidNamespace = function _checkValidNamespace(element) {
      var parent = getParentNode(element);
      if (!parent || !parent.tagName) {
        parent = {
          namespaceURI: HTML_NAMESPACE,
          tagName: 'template'
        };
      }
      var tagName = stringToLowerCase(element.tagName);
      var parentTagName = stringToLowerCase(parent.tagName);
      if (element.namespaceURI === SVG_NAMESPACE) {
        if (parent.namespaceURI === HTML_NAMESPACE) {
          return tagName === 'svg';
        }
        if (parent.namespaceURI === MATHML_NAMESPACE) {
          return tagName === 'svg' && (parentTagName === 'annotation-xml' || MATHML_TEXT_INTEGRATION_POINTS[parentTagName]);
        }
        return Boolean(ALL_SVG_TAGS[tagName]);
      }
      if (element.namespaceURI === MATHML_NAMESPACE) {
        if (parent.namespaceURI === HTML_NAMESPACE) {
          return tagName === 'math';
        }
        if (parent.namespaceURI === SVG_NAMESPACE) {
          return tagName === 'math' && HTML_INTEGRATION_POINTS[parentTagName];
        }
        return Boolean(ALL_MATHML_TAGS[tagName]);
      }
      if (element.namespaceURI === HTML_NAMESPACE) {
        if (parent.namespaceURI === SVG_NAMESPACE && !HTML_INTEGRATION_POINTS[parentTagName]) {
          return false;
        }
        if (parent.namespaceURI === MATHML_NAMESPACE && !MATHML_TEXT_INTEGRATION_POINTS[parentTagName]) {
          return false;
        }
        var commonSvgAndHTMLElements = addToSet({}, ['title', 'style', 'font', 'a', 'script']);
        return !ALL_MATHML_TAGS[tagName] && (commonSvgAndHTMLElements[tagName] || !ALL_SVG_TAGS[tagName]);
      }
      return false;
    };
    var _forceRemove = function _forceRemove(node) {
      arrayPush(DOMPurify.removed, {
        element: node
      });
      try {
        node.parentNode.removeChild(node);
      } catch (_) {
        try {
          node.outerHTML = emptyHTML;
        } catch (_) {
          node.remove();
        }
      }
    };
    var _removeAttribute = function _removeAttribute(name, node) {
      try {
        arrayPush(DOMPurify.removed, {
          attribute: node.getAttributeNode(name),
          from: node
        });
      } catch (_) {
        arrayPush(DOMPurify.removed, {
          attribute: null,
          from: node
        });
      }
      node.removeAttribute(name);
    };
    var _initDocument = function _initDocument(dirty) {
      var doc = void 0;
      var leadingWhitespace = void 0;
      if (FORCE_BODY) {
        dirty = '<remove></remove>' + dirty;
      } else {
        var matches = stringMatch(dirty, /^[\r\n\t ]+/);
        leadingWhitespace = matches && matches[0];
      }
      var dirtyPayload = trustedTypesPolicy ? trustedTypesPolicy.createHTML(dirty) : dirty;
      try {
        doc = new DOMParser().parseFromString(dirtyPayload, 'text/html');
      } catch (_) {}
      if (!doc || !doc.documentElement) {
        doc = implementation.createHTMLDocument('');
        var _doc = doc, body = _doc.body;
        body.parentNode.removeChild(body.parentNode.firstElementChild);
        body.outerHTML = dirtyPayload;
      }
      if (dirty && leadingWhitespace) {
        doc.body.insertBefore(document.createTextNode(leadingWhitespace), doc.body.childNodes[0] || null);
      }
      return getElementsByTagName.call(doc, WHOLE_DOCUMENT ? 'html' : 'body')[0];
    };
    var _createIterator = function _createIterator(root) {
      return createNodeIterator.call(root.ownerDocument || root, root, NodeFilter.SHOW_ELEMENT | NodeFilter.SHOW_COMMENT | NodeFilter.SHOW_TEXT, function () {
        return NodeFilter.FILTER_ACCEPT;
      }, false);
    };
    var _isClobbered = function _isClobbered(elm) {
      if (elm instanceof Text || elm instanceof Comment) {
        return false;
      }
      if (typeof elm.nodeName !== 'string' || typeof elm.textContent !== 'string' || typeof elm.removeChild !== 'function' || !(elm.attributes instanceof NamedNodeMap) || typeof elm.removeAttribute !== 'function' || typeof elm.setAttribute !== 'function' || typeof elm.namespaceURI !== 'string' || typeof elm.insertBefore !== 'function') {
        return true;
      }
      return false;
    };
    var _isNode = function _isNode(object) {
      return (typeof Node === 'undefined' ? 'undefined' : _typeof(Node)) === 'object' ? object instanceof Node : object && (typeof object === 'undefined' ? 'undefined' : _typeof(object)) === 'object' && typeof object.nodeType === 'number' && typeof object.nodeName === 'string';
    };
    var _executeHook = function _executeHook(entryPoint, currentNode, data) {
      if (!hooks[entryPoint]) {
        return;
      }
      arrayForEach(hooks[entryPoint], function (hook) {
        hook.call(DOMPurify, currentNode, data, CONFIG);
      });
    };
    var _sanitizeElements = function _sanitizeElements(currentNode) {
      var content = void 0;
      _executeHook('beforeSanitizeElements', currentNode, null);
      if (_isClobbered(currentNode)) {
        _forceRemove(currentNode);
        return true;
      }
      if (stringMatch(currentNode.nodeName, /[\u0080-\uFFFF]/)) {
        _forceRemove(currentNode);
        return true;
      }
      var tagName = stringToLowerCase(currentNode.nodeName);
      _executeHook('uponSanitizeElement', currentNode, {
        tagName: tagName,
        allowedTags: ALLOWED_TAGS
      });
      if (!_isNode(currentNode.firstElementChild) && (!_isNode(currentNode.content) || !_isNode(currentNode.content.firstElementChild)) && regExpTest(/<[/\w]/g, currentNode.innerHTML) && regExpTest(/<[/\w]/g, currentNode.textContent)) {
        _forceRemove(currentNode);
        return true;
      }
      if (!ALLOWED_TAGS[tagName] || FORBID_TAGS[tagName]) {
        if (KEEP_CONTENT && !FORBID_CONTENTS[tagName]) {
          var parentNode = getParentNode(currentNode);
          var childNodes = getChildNodes(currentNode);
          var childCount = childNodes.length;
          for (var i = childCount - 1; i >= 0; --i) {
            parentNode.insertBefore(cloneNode(childNodes[i], true), getNextSibling(currentNode));
          }
        }
        _forceRemove(currentNode);
        return true;
      }
      if (currentNode instanceof Element && !_checkValidNamespace(currentNode)) {
        _forceRemove(currentNode);
        return true;
      }
      if ((tagName === 'noscript' || tagName === 'noembed') && regExpTest(/<\/no(script|embed)/i, currentNode.innerHTML)) {
        _forceRemove(currentNode);
        return true;
      }
      if (SAFE_FOR_TEMPLATES && currentNode.nodeType === 3) {
        content = currentNode.textContent;
        content = stringReplace(content, MUSTACHE_EXPR$$1, ' ');
        content = stringReplace(content, ERB_EXPR$$1, ' ');
        if (currentNode.textContent !== content) {
          arrayPush(DOMPurify.removed, {
            element: currentNode.cloneNode()
          });
          currentNode.textContent = content;
        }
      }
      _executeHook('afterSanitizeElements', currentNode, null);
      return false;
    };
    var _isValidAttribute = function _isValidAttribute(lcTag, lcName, value) {
      if (SANITIZE_DOM && (lcName === 'id' || lcName === 'name') && ((value in document) || (value in formElement))) {
        return false;
      }
      if (ALLOW_DATA_ATTR && regExpTest(DATA_ATTR$$1, lcName)) ; else if (ALLOW_ARIA_ATTR && regExpTest(ARIA_ATTR$$1, lcName)) ; else if (!ALLOWED_ATTR[lcName] || FORBID_ATTR[lcName]) {
        return false;
      } else if (URI_SAFE_ATTRIBUTES[lcName]) ; else if (regExpTest(IS_ALLOWED_URI$$1, stringReplace(value, ATTR_WHITESPACE$$1, ''))) ; else if ((lcName === 'src' || lcName === 'xlink:href' || lcName === 'href') && lcTag !== 'script' && stringIndexOf(value, 'data:') === 0 && DATA_URI_TAGS[lcTag]) ; else if (ALLOW_UNKNOWN_PROTOCOLS && !regExpTest(IS_SCRIPT_OR_DATA$$1, stringReplace(value, ATTR_WHITESPACE$$1, ''))) ; else if (!value) ; else {
        return false;
      }
      return true;
    };
    var _sanitizeAttributes = function _sanitizeAttributes(currentNode) {
      var attr = void 0;
      var value = void 0;
      var lcName = void 0;
      var l = void 0;
      _executeHook('beforeSanitizeAttributes', currentNode, null);
      var attributes = currentNode.attributes;
      if (!attributes) {
        return;
      }
      var hookEvent = {
        attrName: '',
        attrValue: '',
        keepAttr: true,
        allowedAttributes: ALLOWED_ATTR
      };
      l = attributes.length;
      while (l--) {
        attr = attributes[l];
        var _attr = attr, name = _attr.name, namespaceURI = _attr.namespaceURI;
        value = stringTrim(attr.value);
        lcName = stringToLowerCase(name);
        hookEvent.attrName = lcName;
        hookEvent.attrValue = value;
        hookEvent.keepAttr = true;
        hookEvent.forceKeepAttr = undefined;
        _executeHook('uponSanitizeAttribute', currentNode, hookEvent);
        value = hookEvent.attrValue;
        if (hookEvent.forceKeepAttr) {
          continue;
        }
        _removeAttribute(name, currentNode);
        if (!hookEvent.keepAttr) {
          continue;
        }
        if (regExpTest(/\/>/i, value)) {
          _removeAttribute(name, currentNode);
          continue;
        }
        if (SAFE_FOR_TEMPLATES) {
          value = stringReplace(value, MUSTACHE_EXPR$$1, ' ');
          value = stringReplace(value, ERB_EXPR$$1, ' ');
        }
        var lcTag = currentNode.nodeName.toLowerCase();
        if (!_isValidAttribute(lcTag, lcName, value)) {
          continue;
        }
        try {
          if (namespaceURI) {
            currentNode.setAttributeNS(namespaceURI, name, value);
          } else {
            currentNode.setAttribute(name, value);
          }
          arrayPop(DOMPurify.removed);
        } catch (_) {}
      }
      _executeHook('afterSanitizeAttributes', currentNode, null);
    };
    var _sanitizeShadowDOM = function _sanitizeShadowDOM(fragment) {
      var shadowNode = void 0;
      var shadowIterator = _createIterator(fragment);
      _executeHook('beforeSanitizeShadowDOM', fragment, null);
      while (shadowNode = shadowIterator.nextNode()) {
        _executeHook('uponSanitizeShadowNode', shadowNode, null);
        if (_sanitizeElements(shadowNode)) {
          continue;
        }
        if (shadowNode.content instanceof DocumentFragment) {
          _sanitizeShadowDOM(shadowNode.content);
        }
        _sanitizeAttributes(shadowNode);
      }
      _executeHook('afterSanitizeShadowDOM', fragment, null);
    };
    DOMPurify.sanitize = function (dirty, cfg) {
      var body = void 0;
      var importedNode = void 0;
      var currentNode = void 0;
      var oldNode = void 0;
      var returnNode = void 0;
      if (!dirty) {
        dirty = '<!-->';
      }
      if (typeof dirty !== 'string' && !_isNode(dirty)) {
        if (typeof dirty.toString !== 'function') {
          throw typeErrorCreate('toString is not a function');
        } else {
          dirty = dirty.toString();
          if (typeof dirty !== 'string') {
            throw typeErrorCreate('dirty is not a string, aborting');
          }
        }
      }
      if (!DOMPurify.isSupported) {
        if (_typeof(window.toStaticHTML) === 'object' || typeof window.toStaticHTML === 'function') {
          if (typeof dirty === 'string') {
            return window.toStaticHTML(dirty);
          }
          if (_isNode(dirty)) {
            return window.toStaticHTML(dirty.outerHTML);
          }
        }
        return dirty;
      }
      if (!SET_CONFIG) {
        _parseConfig(cfg);
      }
      DOMPurify.removed = [];
      if (typeof dirty === 'string') {
        IN_PLACE = false;
      }
      if (IN_PLACE) ; else if (dirty instanceof Node) {
        body = _initDocument('<!---->');
        importedNode = body.ownerDocument.importNode(dirty, true);
        if (importedNode.nodeType === 1 && importedNode.nodeName === 'BODY') {
          body = importedNode;
        } else if (importedNode.nodeName === 'HTML') {
          body = importedNode;
        } else {
          body.appendChild(importedNode);
        }
      } else {
        if (!RETURN_DOM && !SAFE_FOR_TEMPLATES && !WHOLE_DOCUMENT && dirty.indexOf('<') === -1) {
          return trustedTypesPolicy && RETURN_TRUSTED_TYPE ? trustedTypesPolicy.createHTML(dirty) : dirty;
        }
        body = _initDocument(dirty);
        if (!body) {
          return RETURN_DOM ? null : emptyHTML;
        }
      }
      if (body && FORCE_BODY) {
        _forceRemove(body.firstChild);
      }
      var nodeIterator = _createIterator(IN_PLACE ? dirty : body);
      while (currentNode = nodeIterator.nextNode()) {
        if (currentNode.nodeType === 3 && currentNode === oldNode) {
          continue;
        }
        if (_sanitizeElements(currentNode)) {
          continue;
        }
        if (currentNode.content instanceof DocumentFragment) {
          _sanitizeShadowDOM(currentNode.content);
        }
        _sanitizeAttributes(currentNode);
        oldNode = currentNode;
      }
      oldNode = null;
      if (IN_PLACE) {
        return dirty;
      }
      if (RETURN_DOM) {
        if (RETURN_DOM_FRAGMENT) {
          returnNode = createDocumentFragment.call(body.ownerDocument);
          while (body.firstChild) {
            returnNode.appendChild(body.firstChild);
          }
        } else {
          returnNode = body;
        }
        if (RETURN_DOM_IMPORT) {
          returnNode = importNode.call(originalDocument, returnNode, true);
        }
        return returnNode;
      }
      var serializedHTML = WHOLE_DOCUMENT ? body.outerHTML : body.innerHTML;
      if (SAFE_FOR_TEMPLATES) {
        serializedHTML = stringReplace(serializedHTML, MUSTACHE_EXPR$$1, ' ');
        serializedHTML = stringReplace(serializedHTML, ERB_EXPR$$1, ' ');
      }
      return trustedTypesPolicy && RETURN_TRUSTED_TYPE ? trustedTypesPolicy.createHTML(serializedHTML) : serializedHTML;
    };
    DOMPurify.setConfig = function (cfg) {
      _parseConfig(cfg);
      SET_CONFIG = true;
    };
    DOMPurify.clearConfig = function () {
      CONFIG = null;
      SET_CONFIG = false;
    };
    DOMPurify.isValidAttribute = function (tag, attr, value) {
      if (!CONFIG) {
        _parseConfig({});
      }
      var lcTag = stringToLowerCase(tag);
      var lcName = stringToLowerCase(attr);
      return _isValidAttribute(lcTag, lcName, value);
    };
    DOMPurify.addHook = function (entryPoint, hookFunction) {
      if (typeof hookFunction !== 'function') {
        return;
      }
      hooks[entryPoint] = hooks[entryPoint] || [];
      arrayPush(hooks[entryPoint], hookFunction);
    };
    DOMPurify.removeHook = function (entryPoint) {
      if (hooks[entryPoint]) {
        arrayPop(hooks[entryPoint]);
      }
    };
    DOMPurify.removeHooks = function (entryPoint) {
      if (hooks[entryPoint]) {
        hooks[entryPoint] = [];
      }
    };
    DOMPurify.removeAllHooks = function () {
      hooks = {};
    };
    return DOMPurify;
  }
  var purify = createDOMPurify();
  return purify;
});

},

// node_modules/helium-ui/dist/Augments/ViewIntersectionAugment.js @45
45: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.intersectionAug = exports.addIntersectionObserver = void 0;
const Factories_1 = __fusereq(27);
const El_Tool_1 = __fusereq(26);
const Augment_1 = __fusereq(67);
function addIntersectionObserver(nodes, init, cb) {
  let thresholds;
  const isObject = init => {
    return init && typeof init === "object" && !Array.isArray(init);
  };
  if (isObject(init)) {
    thresholds = init.threshold;
  }
  if (!Array.isArray(thresholds)) {
    thresholds = [thresholds];
  }
  let startLength = thresholds.length;
  const normalThresholds = thresholds.map(it => {
    switch (it) {
      case "contained":
      case "maxFill":
        return 1;
      case "any":
        return 0;
    }
    return it;
  }).filter(it => typeof it === "number");
  const needsCoverMod = startLength > normalThresholds.length;
  if (!Array.isArray(nodes)) {
    nodes = [nodes];
  }
  if (needsCoverMod) {
    assertStyling();
    const checkUs = nodes.map(node => {
      let checkMe;
      const addMe = Factories_1.div("CoverCheckerPositioner", checkMe = Factories_1.div("CoverChecker"));
      node.classList.add("hintRelative");
      node.prepend(addMe);
      return checkMe;
    });
    const watcher = new IntersectionObserver(cb, {
      threshold: 1
    });
    checkUs.forEach(node => watcher.observe(node));
  }
  if (normalThresholds.length === 0) {
    return;
  }
  const options = isObject(init) ? init : {};
  options.threshold = normalThresholds;
  const watcher = new IntersectionObserver(cb, options);
  setTimeout(() => {
    nodes.forEach(node => watcher.observe(node));
  }, 0);
}
exports.addIntersectionObserver = addIntersectionObserver;
function intersectionAug(init, cb) {
  return new Augment_1.ElementAugment((ref, {modType}) => {
    if (modType !== "init") {
      return;
    }
    addIntersectionObserver(ref, init, (entries, observer) => {
      cb(ref, entries[0], observer);
    });
  });
}
exports.intersectionAug = intersectionAug;
let styleEl;
function assertStyling() {
  if (styleEl) {
    return;
  }
  styleEl = El_Tool_1.el("style");
  styleEl.innerHTML = `
    .hintRelative {
      position: relative;
    }

    .CoverCheckerPositioner {
      position: absolute;
      top: 0px;
      left: 0px;
      right: 0px;
      bottom: 0px;
      pointer-events: none;
      visibility: hidden;
    }
    
    .CoverChecker {
      position: sticky;
      height: 100vh;
      width: 100vw;
      top: 0px; 
    }
  `;
  document.head.prepend(styleEl);
}

},

// node_modules/helium-ui/dist/Augments/BlockEvent.js @46
46: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.blockEventAug = void 0;
const Augment_1 = __fusereq(67);
function blockEventAug(eventType, props = {}) {
  return new Augment_1.ElementAugment((ref, modProps) => {
    if (modProps.modType !== "init") {
      return;
    }
    ref.addEventListener(eventType, ev => {
      if (props.preventDefault !== false) {
        ev.preventDefault();
      }
      if (props.stopPropagation !== false) {
        ev.stopPropagation();
      }
      if (props.stopImmediatePropagation !== false) {
        ev.stopImmediatePropagation();
      }
      if (props.returnFalse !== false) {
        return false;
      }
    });
  });
}
exports.blockEventAug = blockEventAug;

},

// node_modules/helium-ui/dist/Derivations/Derivers/DomAnchoredDeriver.js @48
48: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deriveDomAnchored = void 0;
const HeliumBase_1 = __fusereq(15);
const helium_sdx_1 = __fusereq(17);
function deriveDomAnchored(node, fnOrProps, batching) {
  setupNodeAnchorMutationObserver();
  const helium = HeliumBase_1.heliumHaze(node)._helium;
  let props;
  if (typeof fnOrProps === "function") {
    props = {
      fn: fnOrProps
    };
  } else {
    props = fnOrProps;
  }
  props.batching = props.batching || batching || "singleFrame";
  const anchor = new helium_sdx_1.AnchorNode();
  anchor.details = [`(Right Click > Reveal in Elements Panel).  Issue occurred in render preceding: `, node];
  const fn = () => {
    const onPage = node.getRootNode() === document;
    if (!onPage) {
      HeliumBase_1.changeFreezeState(node, true);
    }
    props.fn();
  };
  const scope = new helium_sdx_1.DeriverScope({
    ...props,
    fn,
    anchor
  });
  (helium.unfreeze = helium.unfreeze || []).push(() => {
    anchor.setStatus(helium_sdx_1.AnchorStatus.NORMAL);
  });
  (helium.freeze = helium.freeze || []).push(() => {
    anchor.setStatus(helium_sdx_1.AnchorStatus.FROZEN);
  });
  scope.runDdx();
  return scope;
}
exports.deriveDomAnchored = deriveDomAnchored;
let mutationsBuffer = [];
let bufferTimeout;
let mutationObserver;
function setupNodeAnchorMutationObserver() {
  if (typeof MutationObserver === "undefined") {
    return;
  }
  if (mutationObserver) {
    return;
  }
  mutationObserver = new MutationObserver(mutationsList => {
    mutationsList.forEach(mutation => {
      if (mutation.type !== 'childList') {
        return;
      }
      mutationsBuffer.push(mutation);
    });
    if (bufferTimeout) {
      clearTimeout(bufferTimeout);
    }
    bufferTimeout = setTimeout(() => {
      bufferTimeout = undefined;
      const oldBuffer = mutationsBuffer;
      mutationsBuffer = [];
      const allNodes = new Map();
      for (const mutation of oldBuffer) {
        mutation.removedNodes.forEach(node => allNodes.set(node));
        mutation.addedNodes.forEach(node => allNodes.set(node));
      }
      const nodeList = Array.from(allNodes.keys()).filter(node => {
        if (HeliumBase_1.isHeliumHazed(node) && node._helium.moveMutation) {
          return node._helium.moveMutation = false;
        }
        return true;
      });
      const permeate = (root, cb) => {
        cb(root);
        root.childNodes.forEach(child => permeate(child, cb));
      };
      nodeList.forEach(root => permeate(root, child => {
        if (HeliumBase_1.isHeliumHazed(child)) {
          const onPage = child.getRootNode() === document;
          HeliumBase_1.changeFreezeState(child, !onPage);
        }
      }));
    }, 10);
  });
  var config = {
    childList: true,
    subtree: true
  };
  if (typeof window !== "undefined") {
    const tryAddObserver = () => mutationObserver.observe(document.body, config);
    if (document && document.body) {
      tryAddObserver();
    } else {
      document.addEventListener("DOMContentLoaded", () => tryAddObserver());
    }
  }
}

},

// node_modules/helium-ui/dist/Derivations/Sources/SourceBase.js @49
49: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});

},

// node_modules/helium-ui/dist/Derivations/Sources/SourceArray.js @50
50: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SourceArray = void 0;
const RerenderReplacer_1 = __fusereq(52);
const helium_sdx_1 = __fusereq(17);
class SourceArray extends helium_sdx_1.SourceArray {
  renderMap(renderFn) {
    return this._mapReplacers(false, renderFn);
  }
  renderMapKeyed(renderFn) {
    return this._mapReplacers(true, renderFn);
  }
  _mapReplacers(withIndex, renderFn) {
    const obsToRerenderMap = new Map();
    const listReplacer = new RerenderReplacer_1.RerenderReplacer(() => {
      this.orderMatters();
      const out = [];
      const length = this.length;
      for (let i = 0; i < length; i++) {
        const observable = withIndex ? this._getIndexObserver(i) : this.getSource(i);
        if (obsToRerenderMap.has(observable) === false) {
          obsToRerenderMap.set(observable, new RerenderReplacer_1.RerenderReplacer(() => {
            const value = observable.get();
            if (withIndex) {
              return renderFn(this.noDeletedConstant(value.get()), i);
            } else {
              return renderFn(this.noDeletedConstant(value));
            }
          }));
        }
        const nodes = obsToRerenderMap.get(observable).getRender();
        nodes.forEach(node => out.push(node));
      }
      return out;
    });
    return listReplacer.getRender();
  }
}
exports.SourceArray = SourceArray;

},

// node_modules/helium-ui/dist/Derivations/Sources/SourceMap.js @51
51: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SourceMap = void 0;
const RerenderReplacer_1 = __fusereq(52);
const helium_sdx_1 = __fusereq(17);
class SourceMap extends helium_sdx_1.SourceMap {
  renderMap(renderFn) {
    return this._mapReplacers(false, renderFn);
  }
  renderMapKeyed(renderFn) {
    return this._mapReplacers(true, renderFn);
  }
  _mapReplacers(withKey, renderFn) {
    const entryReplacers = new Map();
    const orderer = new RerenderReplacer_1.RerenderReplacer(() => {
      const out = [];
      const keys = this.keys();
      for (const key of keys) {
        if (entryReplacers.has(key) === false) {
          const source = this.getSource(key);
          entryReplacers.set(key, new RerenderReplacer_1.RerenderReplacer(() => renderFn(source.get(), withKey ? key : undefined)));
        }
        const nodeList = entryReplacers.get(key).getRender();
        for (const node of nodeList) {
          out.push(node);
        }
      }
      return out;
    });
    return orderer.getRender();
  }
}
exports.SourceMap = SourceMap;

},

// node_modules/helium-ui/dist/Derivations/Derivers/RerenderReplacer.js @52
52: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.recycle = exports.RenderRecycler = exports.RerenderReplacer = void 0;
const El_Tool_1 = __fusereq(26);
const HeliumBase_1 = __fusereq(15);
const DomAnchoredDeriver_1 = __fusereq(48);
const helium_sdx_1 = __fusereq(17);
const REMOVE_NODE = false;
const ADD_NODE = true;
let RerenderReplacer = (() => {
  class RerenderReplacer {
    constructor(rerenderFn, args = {}) {
      this.rerenderFn = rerenderFn;
      this.args = args;
      this.id = "ddx_" + RerenderReplacer.nextId++;
      this.preConstructHook();
      if (helium_sdx_1.DerivationManager.disabled()) {
        this.currentRender = El_Tool_1.convertInnardsToElements(this.rerenderFn(), args.flattenFn);
      } else {
        this.placeHolder = document.createComment(this.id);
        const helium = HeliumBase_1.heliumHaze(this.placeHolder)._helium;
        helium.rerenderReplacer = this;
        helium.isReplacer = true;
        this.deriverScope = DomAnchoredDeriver_1.deriveDomAnchored(this.placeHolder, this.derive.bind(this));
      }
    }
    preConstructHook() {}
    getRender() {
      return this.currentRender;
    }
    derive() {
      const placehold = this.placeHolder;
      const newRender = El_Tool_1.convertInnardsToElements(this.rerenderFn(), this.args.flattenFn);
      newRender.push(this.placeHolder);
      const oldRender = this.currentRender;
      this.currentRender = newRender;
      const parent = placehold.parentElement;
      if (!parent || !oldRender) {
        return;
      }
      const addRemoveMap = new Map();
      oldRender.forEach(node => addRemoveMap.set(node, REMOVE_NODE));
      newRender.forEach(node => {
        if (addRemoveMap.has(node)) {
          addRemoveMap.delete(node);
        } else {
          addRemoveMap.set(node, ADD_NODE);
        }
      });
      Array.from(addRemoveMap.entries()).forEach(([node, addRemove]) => {
        if (addRemove === REMOVE_NODE && node.parentElement) {
          node.parentElement.removeChild(node);
          if (HeliumBase_1.isHeliumHazed(node) && node._helium.isReplacer) {
            node._helium.rerenderReplacer.removeAll();
          }
        }
      });
      const currentNodes = parent.childNodes;
      let parentIndex = Array.from(currentNodes).indexOf(placehold);
      let lastNode;
      const length = newRender.length;
      for (let i = 0; i < length; i++) {
        const addMe = newRender[length - 1 - i];
        const current = currentNodes[parentIndex - i];
        if (addMe === current) {} else {
          parent.insertBefore(addMe, lastNode);
          if (addRemoveMap.get(addMe) !== ADD_NODE) {
            HeliumBase_1.heliumHaze(addMe)._helium.moveMutation = true;
          } else {
            parentIndex++;
          }
        }
        lastNode = addMe;
      }
    }
    removeAll() {
      this.currentRender.forEach(node => {
        if (node.parentElement) {
          node.parentElement.removeChild(node);
        }
      });
    }
  }
  RerenderReplacer.nextId = 0;
  return RerenderReplacer;
})();
exports.RerenderReplacer = RerenderReplacer;
;
class RenderRecycler extends RerenderReplacer {
  preConstructHook() {
    const renders = [];
    const rootRenderFn = this.rerenderFn;
    this.rerenderFn = () => {
      let cachedRender = renders.find(prevRender => {
        const sourceVals = Array.from(prevRender.sourceVals.entries());
        for (const [source, val] of sourceVals) {
          if (val !== source.peek()) {
            return false;
          }
        }
        for (const [source, val] of sourceVals) {
          source.get();
        }
        return true;
      });
      if (!cachedRender) {
        renders.push(cachedRender = {
          sourceVals: new Map(),
          render: rootRenderFn()
        });
        const sources = this.deriverScope.getSourceDependencies();
        sources.forEach(source => cachedRender.sourceVals.set(source, source.peek()));
      } else {
        console.log("Reusing cached render!", cachedRender);
      }
      return cachedRender.render;
    };
  }
}
exports.RenderRecycler = RenderRecycler;
function recycle(renderFn) {
  const replacer = new RenderRecycler(renderFn);
  return replacer.getRender();
}
exports.recycle = recycle;

},

// node_modules/helium-ui/dist/Derivations/Derivers/LocalStorage.js @53
53: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.syncWithLocalStorage = void 0;
const helium_sdx_1 = __fusereq(17);
function syncWithLocalStorage(id, storeMe) {
  const init = localStorage.getItem(id);
  if (init) {
    try {
      const startingData = JSON.parse(init);
      storeMe.upsert(startingData);
    } catch (err) {
      console.warn("Could not parse locally stored data", err);
    }
  }
  helium_sdx_1.derive({
    anchor: "NO_ANCHOR",
    fn: () => {
      const toStore = storeMe.toObject();
      try {
        localStorage.setItem(id, JSON.stringify(toStore));
      } catch (err) {
        console.warn("Could not store", err);
        console.log(toStore);
      }
    }
  });
}
exports.syncWithLocalStorage = syncWithLocalStorage;

},

// node_modules/helium-sdx/dist/Derivations/AnchorNode.js @54
54: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
var AnchorStatus;
(function (AnchorStatus) {
  AnchorStatus["NORMAL"] = "n";
  AnchorStatus["FROZEN"] = "f";
})(AnchorStatus = exports.AnchorStatus || (exports.AnchorStatus = {}));
class AnchorNode {
  constructor() {
    this.status = AnchorStatus.NORMAL;
    this.children = [];
  }
  getStatus() {
    return this.status;
  }
  setStatus(newStat, notifyChildren = true) {
    if (newStat === this.status) {
      return false;
    }
    this.status = newStat;
    if (notifyChildren) {
      this.children.forEach(child => child.setStatus(this.status));
    }
    return true;
  }
  isFrozen() {
    return this.status === AnchorStatus.FROZEN;
  }
  statusIsNormal() {
    return this.status === AnchorStatus.NORMAL;
  }
  addChild(child) {
    this.children.push(child);
  }
}
exports.AnchorNode = AnchorNode;

},

// node_modules/helium-sdx/dist/Derivations/Derivers/index.js @55
55: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(68));
__export(__fusereq(69));

},

// node_modules/helium-sdx/dist/Derivations/DerivationManager.js @56
56: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const AnotherLogger_1 = __fusereq(40);
const DerivationBatcher_1 = __fusereq(58);
class __DerivationManager {
  constructor() {
    this.lastSourceId = 0;
    this.sourceToDeriverMap = new Map();
    this.knownScopes = new Map();
    this.batcher = new DerivationBatcher_1.DerivationBatcher(this);
    const existing = window.DerivationManager;
    if (existing) {
      return existing;
    }
    window.DerivationManager = this;
  }
  disabled() {
    return this.lastSourceId === 0;
  }
  debugDeriver() {
    if (!this.currentDeriver) throw new Error("Current Scope is undefined");
    AnotherLogger_1.showLoggingType("debugDeriver");
    this.targetDebugDeriver = this.currentDeriver;
  }
  isDebugDeriver(deriver = this.targetDebugDeriver) {
    return this.currentDeriver === deriver;
  }
  createSourceId(append) {
    return `s${this.lastSourceId++}` + (append ? `_${append}` : "");
  }
  sourceRequested(source) {
    if (!this.currentDeriver) {
      return;
    }
    const sourceId = source.getSourceId();
    if (this.isDebugDeriver()) {
      AnotherLogger_1.aLog("debugDeriver", `GET: Source requested ${sourceId}`);
    }
    AnotherLogger_1.aLog("sourceRequest", `${this.currentDeriver.id}: Source requested ${sourceId}`);
    this.currentDeriver.addSourceDependency(source);
    this.makeDeriverDependOnSource(this.currentDeriver, sourceId);
  }
  sourceUpdated(source, equalValue) {
    const sourceId = source.getSourceId();
    AnotherLogger_1.aLog("sourceUpdated", `Update: ${sourceId}`);
    const toBeUpdated = this.popDeriversDependentOnSource(sourceId);
    toBeUpdated && toBeUpdated.forEach(deriver => {
      if (deriver.ignoresEqualValues() && equalValue) {
        this.makeDeriverDependOnSource(deriver, sourceId);
        return;
      } else if (deriver.hasStaticDeps()) {
        this.makeDeriverDependOnSource(deriver, sourceId);
      }
      this.batcher.addDeriverWithReason(deriver, source);
    });
  }
  _runAsDeriver(deriver, ddx) {
    let startDeriver = this.currentDeriver;
    if (!deriver || deriver.hasStaticDeps() === false) {
      this.currentDeriver = deriver;
    }
    AnotherLogger_1.aLog("scopes", "Running Scope", deriver && deriver.id);
    if (this.isDebugDeriver()) {
      AnotherLogger_1.aLog("debugDeriver", "<Rerunning Deriver>");
    }
    ddx();
    this.currentDeriver = startDeriver;
  }
  _getCurrentDeriver() {
    return this.currentDeriver;
  }
  _removeDependencyFromSources(deriver, sourceIds) {
    sourceIds.forEach(sourceId => {
      const deriversPerSource = this.sourceToDeriverMap.get(sourceId);
      deriversPerSource && deriversPerSource.delete(deriver);
    });
  }
  popDeriversDependentOnSource(sourceId) {
    const deriversPerSource = this.sourceToDeriverMap.get(sourceId);
    if (!deriversPerSource) {
      return;
    }
    const out = Array.from(deriversPerSource.keys());
    this.sourceToDeriverMap.delete(sourceId);
    return out;
  }
  makeDeriverDependOnSource(deriver, sourceId) {
    if (this.sourceToDeriverMap.has(sourceId) === false) {
      this.sourceToDeriverMap.set(sourceId, new Map());
    }
    return this.sourceToDeriverMap.get(sourceId).set(deriver);
  }
  dropDeriver(deriver) {
    if (deriver.statusIsNormal()) {
      throw Error("Will not drop an status normal scope");
    }
    this.knownScopes.delete(deriver);
  }
}
exports.__DerivationManager = __DerivationManager;
exports.DerivationManager = new __DerivationManager();

},

// node_modules/helium-sdx/dist/Derivations/DeriverScope.js @57
57: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const DerivationManager_1 = __fusereq(56);
const AnchorNode_1 = __fusereq(54);
let SCOPE_ID = 0;
class DeriverScope extends AnchorNode_1.AnchorNode {
  constructor(props) {
    super();
    this.props = props;
    this.sourceDeps = new Map();
    this.id = String(SCOPE_ID++);
    this.creationStack = new Error().stack;
    this.dirty = false;
    this.store = {};
    this.iteration = 0;
    props.anchor = props.anchor || DerivationManager_1.DerivationManager._getCurrentDeriver() || "NO_ANCHOR";
    props.batching = props.batching || "stack";
    if (typeof props.anchor !== "string") {
      props.anchor.addChild(this);
    }
  }
  addSourceDependency(source) {
    this.sourceDeps.set(source);
  }
  runDdx() {
    if (this.iteration >= 0) {
      this.iteration++;
      if (this.iteration > 1000) {
        console.warn(`Deriver scope has ran over 1000 times.  This is usually caused by a bad loop, or too short of batching.`);
        console.warn(this);
        this.iteration = -1;
      }
    }
    this.dropChildren();
    if (this.isFrozen()) {
      DerivationManager_1.DerivationManager.dropDeriver(this);
      return this.dirty = true;
    }
    this.dirty = false;
    try {
      DerivationManager_1.DerivationManager._runAsDeriver(this, () => {
        if (this.hasStaticDeps() === false) {
          this.sourceDeps = new Map();
        }
        this.props.fn({
          scope: this,
          store: this.store
        });
      });
    } catch (err) {
      const details = typeof this.props.anchor === "object" && this.props.anchor.details;
      if (details) {
        if (Array.isArray(details)) {
          console.error(...details);
        } else {
          console.error(details);
        }
      }
      console.error(err);
    }
  }
  getSourceDependencies() {
    return Array.from(this.sourceDeps.keys());
  }
  getSourceDependencyIds() {
    return this.getSourceDependencies().map(source => source.getSourceId());
  }
  hasDep(source) {
    return this.sourceDeps.has(source);
  }
  getDeriver() {
    return this.props.fn;
  }
  getStore() {
    return this.store;
  }
  getBatching() {
    return this.props.batching;
  }
  ignoresEqualValues() {
    return this.props.allowEqualValueUpdates !== true;
  }
  hasStaticDeps() {
    return !!(this.props.dependenciesNeverChange && this.sourceDeps.size);
  }
  unfreeze() {
    return this.setStatus(AnchorNode_1.AnchorStatus.NORMAL);
  }
  cancel() {
    this.setStatus(AnchorNode_1.AnchorStatus.FROZEN);
    DerivationManager_1.DerivationManager._removeDependencyFromSources(this, this.getSourceDependencyIds());
  }
  setStatus(status) {
    const didChange = super.setStatus(status, false);
    if (didChange) {
      if (this.statusIsNormal() && this.dirty) {
        this.runDdx();
      } else {
        this.children.forEach(child => child.setStatus(this.status));
      }
    }
    return didChange;
  }
  dropChildren() {
    this.children.forEach(child => child.setStatus(AnchorNode_1.AnchorStatus.FROZEN));
    this.children = [];
  }
}
exports.DeriverScope = DeriverScope;

},

// node_modules/helium-sdx/dist/Derivations/DerivationBatcher.js @58
58: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const AnotherLogger_1 = __fusereq(40);
class DerivationBatcher {
  constructor(manager) {
    this.manager = manager;
    this.typeToBatchListMap = new Map();
    this.batchTimeouts = new Map();
  }
  addDeriverWithReason(deriver, source) {
    const reasons = this.getDeriverReasons(deriver, true);
    reasons.set(source);
    if (this.manager.isDebugDeriver(deriver)) {
      AnotherLogger_1.aLog("debugDeriver", "DDX: Source dependency updating: ", source.getSourceId());
    }
    this.assertBatchRunning(deriver);
  }
  getDeriverReasons(deriver, force = false) {
    const batchType = deriver.getBatching();
    let batchList = this.typeToBatchListMap.get(batchType);
    if (!batchList) {
      if (!force) {
        return;
      }
      this.typeToBatchListMap.set(batchType, batchList = new Map());
    }
    let reasons = batchList.get(deriver);
    if (!reasons) {
      if (!force) {
        return;
      }
      batchList.set(deriver, reasons = new Map());
    }
    return reasons;
  }
  assertBatchRunning(deriver) {
    const batchType = deriver.getBatching();
    if (batchType === "none") {
      return deriver.runDdx();
    }
    if (this.batchTimeouts.has(batchType)) {
      return;
    }
    const start = timeout => this.batchTimeouts.set(batchType, timeout);
    const end = (() => {
      this.batchTimeouts.delete(batchType);
      this.runBatch(batchType);
    }).bind(this);
    if (typeof batchType === "number") {
      return start(setTimeout(end, batchType));
    }
    switch (batchType) {
      case "singleFrame":
        start(requestAnimationFrame(end));
        return;
      case "doubleFrame":
        start(requestAnimationFrame(() => requestAnimationFrame(end)));
        return;
      case "stack":
        start(setTimeout(end, 0));
        return;
      case "oneSec":
        start(setTimeout(end, 1000));
        return;
      case "fiveSec":
        start(setTimeout(end, 5 * 1000));
        return;
      case "tenSec":
        start(setTimeout(end, 10 * 1000));
        return;
      case "twentySec":
        start(setTimeout(end, 20 * 1000));
        return;
      default:
        throw new Error(`Unknown batchType ${batchType}`);
    }
  }
  runBatch(batchType) {
    AnotherLogger_1.aLog("sourceUpdated", "<Start Source Update Batch>");
    let batchList = this.typeToBatchListMap.get(batchType);
    if (!batchList) {
      return;
    }
    const deriverList = Array.from(batchList.entries()).filter(([deriver, reasons]) => {
      for (const source of reasons.keys()) {
        if (deriver.hasDep(source)) {
          return true;
        }
      }
    }).map(([deriver, reasons]) => {
      if (this.manager.isDebugDeriver(deriver)) {
        AnotherLogger_1.aLog("debugDeriver", `DDX: Deriving for change in source(s): ${Array.from(reasons.keys()).join(", ")}`);
      }
      return deriver;
    });
    batchList.clear();
    deriverList.forEach(deriver => {
      deriver.dropChildren();
    });
    deriverList.forEach(deriver => {
      deriver.runDdx();
    });
  }
}
exports.DerivationBatcher = DerivationBatcher;

},

// node_modules/helium-sdx/dist/Derivations/Sources/index.js @59
59: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(70));
__export(__fusereq(71));
__export(__fusereq(72));
__export(__fusereq(73));
__export(__fusereq(74));

},

// node_modules/helium-sdx/dist/Sourcify/SourceProxyObject.js @60
60: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const SourceProxyBase_1 = __fusereq(62);
const SourceMap_1 = __fusereq(72);
const SourcificationManager_1 = __fusereq(63);
const SourceProxyUtilities_1 = __fusereq(64);
const BasicDerivers_1 = __fusereq(69);
class SourceProxyObject extends SourceProxyBase_1.SourceProxyBase {
  constructor(target, srcMap = new SourceMap_1.SourceMap()) {
    super(target);
    this.srcMap = srcMap;
    this[Symbol.iterator] = () => {
      this.refreshAll();
      return this.srcMap.iterator();
    };
    this._fresh = false;
  }
  bind(key, bindTo) {
    this.assertKey(key);
    if (!bindTo) {
      const src = this.srcMap.getSource(key);
      BasicDerivers_1.derive(() => this.set(key, src.get()));
      return src;
    }
    BasicDerivers_1.derive(() => this.set(key, bindTo.get()));
    BasicDerivers_1.derive(() => bindTo.set(this.get(key)));
  }
  delete(index) {
    this.srcMap.delete(index);
    return delete this.target[index];
  }
  get(key) {
    this.assertKey(key);
    return this.srcMap.get(key);
  }
  has(key) {
    this.assertKey(key);
    return this.srcMap.has(key);
  }
  isEmpty() {
    return this.keys().length === 0;
  }
  keys() {
    this.refreshAll();
    return this.srcMap.keys();
  }
  _propertyList() {
    return this.keys();
  }
  overwrite(newMe) {
    const extraKeys = new Map(Array.from(Object.entries(this.target)));
    Object.keys(newMe).forEach(key => {
      this.set(key, newMe[key]);
      extraKeys.delete(key);
    });
    for (const key of extraKeys.keys()) {
      this.delete(key);
    }
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  peek(key) {
    this.assertKey(key);
    return this.srcMap.peek(key);
  }
  set(key, value) {
    if (SourceProxyUtilities_1.isProxied(value)) {
      this.target[key] = value.toObjectNoDeps();
      this.srcMap.set(key, value);
    } else {
      this.target[key] = value;
      this.refresh(key);
    }
  }
  size() {
    this.refreshAll();
    return this.srcMap.size();
  }
  toObject() {
    const out = {};
    this.refreshAll();
    this.srcMap.forEach((item, key) => {
      if (item && typeof item === "object" && ("toObject" in item)) {
        item = item.toObject();
      }
      out[key] = item;
    });
    return out;
  }
  upsert(items, shallow = false) {
    Object.keys(items).forEach(_key => {
      const key = _key;
      const value = items[key];
      if (!shallow) {
        const current = this.peek(key);
        if (value && typeof value === "object" && !Array.isArray(value) && typeof current === "object" && ("upsert" in current)) {
          current.upsert(value);
          return;
        }
      }
      this.set(key, items[key]);
    });
  }
  assertKey(key) {
    if ((key in this.target) === this.srcMap.peekHas(key)) {
      return;
    }
    this.refresh(key);
  }
  refresh(key) {
    let value = this.target[key];
    if (value && typeof value === "object") {
      const peek = this.srcMap.peek(key);
      if (SourceProxyUtilities_1.isProxied(peek) && peek.toObjectNoDeps() === value) {
        return;
      }
      value = SourceProxyUtilities_1.Sourcify(value);
    }
    this.srcMap.set(key, value);
  }
  refreshAll(type) {
    if (this._fresh && !type) {
      return;
    }
    Object.keys(this.target).forEach(key => {
      if (type === "HARD") {
        this.refresh(key);
      } else {
        this.assertKey(key);
      }
    });
    this._fresh = true;
  }
}
exports.SourceProxyObject = SourceProxyObject;
;
exports.ASSERT_SHALLOW_TYPE_MATCH = {};

},

// node_modules/helium-sdx/dist/Sourcify/SourceProxyArray.js @61
61: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const SourceProxyBase_1 = __fusereq(62);
const SourceArray_1 = __fusereq(71);
const SourcificationManager_1 = __fusereq(63);
const SourceProxyUtilities_1 = __fusereq(64);
class SourceProxyArray extends SourceProxyBase_1.SourceProxyBase {
  constructor(target, srcArray = new SourceArray_1.SourceArray()) {
    super(target);
    this.srcArray = srcArray;
    this[Symbol.iterator] = () => {
      this.refreshAll();
      return this.srcArray.iterator();
    };
    this._fresh = false;
  }
  get length() {
    this.assertIndex(this.target.length - 1);
    return this.srcArray.length;
  }
  concat(items) {
    this.refreshAll();
    const out = this.toObject();
    items.forEach(it => out.push(SourceProxyUtilities_1.Sourcify(it)));
    return SourceProxyUtilities_1.Sourcify(out);
  }
  ddxEachNoIndexDep(cbOrDdxProps, maybeCb) {
    this.refreshAll();
    return this.srcArray.ddxEachNoIndexDep(cbOrDdxProps, maybeCb);
  }
  delete(index) {
    this.srcArray.delete(index);
    return delete this.target[index];
  }
  entries() {
    this.refreshAll();
    return this.srcArray.entries();
  }
  clear() {
    while (this.target.length) {
      this.target.pop();
    }
    this.srcArray.clear();
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  every(cb) {
    this.refreshAll();
    return this.srcArray.every(cb);
  }
  fill(value, start, end) {
    value = SourceProxyUtilities_1.Sourcify(value);
    end = end !== undefined ? end : this.target.length;
    for (let i = start || 0; i < end; i++) {
      this.set(i, value);
    }
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  filter(cb) {
    this.refreshAll();
    return SourceProxyUtilities_1.Sourcify(this.srcArray.filter(cb));
  }
  find(cb) {
    this.refreshAll();
    return this.srcArray.find(cb);
  }
  findIndex(cb) {
    this.refreshAll();
    return this.srcArray.findIndex(cb);
  }
  flat(depth) {
    this.refreshAll();
    return SourceProxyUtilities_1.Sourcify(this.srcArray.flat(depth));
  }
  forEach(cb) {
    this.refreshAll();
    return this.srcArray.forEach(cb);
  }
  forEachNoIndexDep(cb) {
    this.refreshAll();
    return this.srcArray.forEachNoIndexDep(cb);
  }
  get(index) {
    this.assertIndex(index);
    return this.srcArray.get(index);
  }
  getNoIndexDep(index) {
    this.assertIndex(index);
    return this.srcArray.getNoIndexDep(index);
  }
  has(index) {
    this.assertIndex(index);
    return this.srcArray.has(index);
  }
  indexOf(target) {
    this.refreshAll();
    return this.srcArray.indexOf(target);
  }
  includes(target, fromIndex) {
    this.refreshAll();
    return this.srcArray.includes(target, fromIndex);
  }
  join(separator) {
    this.refreshAll();
    return this.srcArray.join(separator);
  }
  keys() {
    this.refreshAll();
    return this.srcArray.keys();
  }
  _propertyList() {
    this.refreshAll();
    const out = this.srcArray.keys().map(it => String(it));
    out.push("length");
    return out;
  }
  map(cb) {
    this.refreshAll();
    return SourceProxyUtilities_1.Sourcify(this.srcArray.map(cb));
  }
  overwrite(newSelf) {
    const oldLength = this.target.length;
    for (let i = 0; i < oldLength; i++) {
      if (i < newSelf.length) {
        this.set(i, newSelf[i]);
      } else {
        this.delete(i);
      }
    }
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  sliceNoIndexDep(start, end) {
    this.refresh(start, end !== undefined ? end : -1);
    return SourceProxyUtilities_1.Sourcify(this.srcArray.sliceNoIndexDep(start, end));
  }
  peekLength() {
    return this.target.length;
  }
  peek(index) {
    this.assertIndex(index);
    return this.srcArray.peek(index);
  }
  push(value) {
    const length = this.target.length;
    this.set(length, value);
  }
  pop() {
    this.assertIndex(this.target.length - 1);
    this.target.pop();
    return this.srcArray.pop();
  }
  reduce(cb) {
    this.refreshAll();
    return this.srcArray.reduce(cb);
  }
  remove(item, count = -1) {
    this.refreshAll();
    let targetItemType = item;
    if (SourceProxyUtilities_1.isProxied(item)) {
      targetItemType = item.toObjectNoDeps();
    }
    let removeCount = 0;
    for (let i = 0; i < this.target.length && (count < 0 || removeCount < count); i++) {
      if (this.target[i] === targetItemType) {
        removeCount++;
        this.target.splice(i, 1);
        i -= 1;
      }
    }
    this.srcArray.remove(item, count);
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  reverse() {
    this.refreshAll();
    this.target.reverse();
    this.srcArray.reverse();
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  slice(start, end) {
    this.refresh(start, end !== undefined ? end : -1);
    return SourceProxyUtilities_1.Sourcify(this.srcArray.slice(start, end));
  }
  set(index, value) {
    if (SourceProxyUtilities_1.isProxied(value)) {
      this.target[index] = value.toObjectNoDeps();
      this.srcArray.set(index, value);
    } else {
      this.target[index] = value;
      this.refresh(index);
    }
  }
  shift() {
    this.assertIndex(0);
    this.target.shift();
    return this.srcArray.shift();
  }
  some(cb) {
    this.refreshAll();
    return this.srcArray.some(cb);
  }
  sort(sortFn) {
    this.refreshAll();
    this.target.sort(sortFn);
    this.srcArray.sort(sortFn);
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  splice(start, deleteCount, ...items) {
    this.target.splice(start, deleteCount, ...(items || []).map(it => SourceProxyUtilities_1.isProxied(it) ? it.toObjectNoDeps() : it));
    return SourceProxyUtilities_1.Sourcify(this.srcArray.splice(start, deleteCount, ...items || []));
  }
  toObject() {
    const out = [];
    this.refreshAll();
    this.srcArray.forEach((item, index) => {
      if (item && typeof item === "object" && ("toObject" in item)) {
        item = item.toObject();
      }
      out[index] = item;
    });
    return out;
  }
  unshift(item) {
    this.target.unshift(SourceProxyUtilities_1.Unsourcify(item));
    return this.srcArray.unshift(SourceProxyUtilities_1.Sourcify(item));
  }
  assertIndex(index) {
    if (index >= this.target.length) {
      return;
    }
    if (!!((index in this.target)) === !!this.srcArray.peekHas(index)) {
      return;
    }
    this.refresh(index);
  }
  refresh(start, end) {
    if (end === undefined) {
      end = start + 1;
    } else if (end === -1) {
      end = this.target.length - 1;
    }
    for (let i = start; i < end; i++) {
      let value = this.target[i];
      if (value && typeof value === "object") {
        const peek = this.srcArray.peek(i);
        if (SourceProxyUtilities_1.isProxied(peek) && peek.toObjectNoDeps() === value) {
          return;
        }
        value = SourceProxyUtilities_1.Sourcify(value);
      }
      this.srcArray.set(i, value);
    }
  }
  refreshAll(type) {
    if (this._fresh && !type) {
      return;
    }
    for (let i = 0; i < this.target.length; i++) {
      if (type === "HARD") {
        this.refresh(i);
      } else {
        this.assertIndex(i);
      }
    }
    this._fresh = true;
  }
}
exports.SourceProxyArray = SourceProxyArray;
exports.ASSERT_SHALLOW_TYPE_MATCH_ARRAY = {};

},

// node_modules/helium-sdx/dist/Sourcify/SourceProxyBase.js @62
62: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
class SourceProxyBase {
  constructor(target) {
    this.target = target;
  }
}
exports.SourceProxyBase = SourceProxyBase;

},

// node_modules/helium-sdx/dist/Sourcify/SourcificationManager.js @63
63: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const SourceProxyArray_1 = __fusereq(61);
const SourceProxyObject_1 = __fusereq(60);
exports.HELIUM_PROXY_OUT = {};
class SourcificationManager {
  constructor(item) {
    this.setProxy(item);
    if (Array.isArray(item)) {
      this.type = "ARR";
      this.srcMgmt = SourcificationManager.createSourceProxyArray(item);
    } else {
      this.type == "OBJ";
      this.srcMgmt = SourcificationManager.createSourceProxyObject(item);
    }
  }
  get proxy() {
    return this._proxy;
  }
  autocorrectPropName(propName) {
    if (typeof propName === "symbol") {
      return undefined;
    }
    if (this.type === "ARR") {
      let asNum;
      if (!isNaN(asNum = parseInt(propName))) {
        return asNum;
      } else {
        return undefined;
      }
    }
    return propName;
  }
  setProxy(item) {
    this._proxy = new Proxy(item, {
      deleteProperty: (_, propName) => {
        propName = this.autocorrectPropName(propName);
        if (propName === undefined) {
          return false;
        }
        return this.srcMgmt.delete(propName);
      },
      get: (_, propName) => {
        switch (propName) {
          case "getProxyManager":
            return () => this;
          case "toObjectNoDeps":
            return () => item;
          case "pretend":
            return () => this._proxy;
        }
        const correctedName = this.autocorrectPropName(propName);
        if (correctedName !== undefined && this.srcMgmt.has(correctedName)) {
          return this.srcMgmt.get(correctedName);
        }
        if ((propName in this.srcMgmt)) {
          if (typeof this.srcMgmt[propName] === "function") {
            return (...args) => {
              const out = this.srcMgmt[propName](...args);
              if (out === exports.HELIUM_PROXY_OUT) {
                return this._proxy;
              }
              return out;
            };
          }
          return this.srcMgmt[propName];
        }
      },
      has: (_, propName) => {
        switch (propName) {
          case "getProxyManager":
          case "toObjectNoDeps":
            return true;
        }
        if ((propName in this.srcMgmt)) {
          return true;
        }
        propName = this.autocorrectPropName(propName);
        if (propName === undefined) {
          return false;
        }
        return this.srcMgmt.has(propName);
      },
      ownKeys: _ => {
        return this.srcMgmt._propertyList();
      },
      set: (_, propName, value) => {
        propName = this.autocorrectPropName(propName);
        if (propName === undefined) {
          throw new Error(`Can not modify array prototype "${propName}" of type: ${typeof propName}`);
        }
        this.srcMgmt.set(propName, value);
        return true;
      }
    });
  }
}
exports.SourcificationManager = SourcificationManager;
SourcificationManager.createSourceProxyArray = item => new SourceProxyArray_1.SourceProxyArray(item);
SourcificationManager.createSourceProxyObject = item => new SourceProxyObject_1.SourceProxyObject(item);

},

// node_modules/helium-sdx/dist/Sourcify/SourceProxyUtilities.js @64
64: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const SourcificationManager_1 = __fusereq(63);
function Sourcify(target) {
  if (isProxied(target)) {
    return target;
  } else if (typeof target !== "object") {
    return target;
  } else if ((/^(object|array)$/i).test(target.constructor.name) === false) {
    return target;
  }
  const mgmt = new SourcificationManager_1.SourcificationManager(target);
  return mgmt.proxy;
}
exports.Sourcify = Sourcify;
function Unsourcify(target) {
  if (isProxied(target)) {
    return target.toObjectNoDeps();
  }
  return target;
}
exports.Unsourcify = Unsourcify;
function isProxied(checkMe) {
  return checkMe && typeof checkMe === "object" && ("getProxyManager" in checkMe);
}
exports.isProxied = isProxied;

},

// node_modules/markdown-it/lib/index.js @65
65: function(__fusereq, exports, module){
'use strict';
var utils = __fusereq(75);
var helpers = __fusereq(76);
var Renderer = __fusereq(77);
var ParserCore = __fusereq(78);
var ParserBlock = __fusereq(79);
var ParserInline = __fusereq(80);
var LinkifyIt = __fusereq(81);
var mdurl = __fusereq(82);
var punycode = __fusereq(83);
var config = {
  'default': __fusereq(84),
  zero: __fusereq(85),
  commonmark: __fusereq(86)
};
var BAD_PROTO_RE = /^(vbscript|javascript|file|data):/;
var GOOD_DATA_RE = /^data:image\/(gif|png|jpeg|webp);/;
function validateLink(url) {
  var str = url.trim().toLowerCase();
  return BAD_PROTO_RE.test(str) ? GOOD_DATA_RE.test(str) ? true : false : true;
}
var RECODE_HOSTNAME_FOR = ['http:', 'https:', 'mailto:'];
function normalizeLink(url) {
  var parsed = mdurl.parse(url, true);
  if (parsed.hostname) {
    if (!parsed.protocol || RECODE_HOSTNAME_FOR.indexOf(parsed.protocol) >= 0) {
      try {
        parsed.hostname = punycode.toASCII(parsed.hostname);
      } catch (er) {}
    }
  }
  return mdurl.encode(mdurl.format(parsed));
}
function normalizeLinkText(url) {
  var parsed = mdurl.parse(url, true);
  if (parsed.hostname) {
    if (!parsed.protocol || RECODE_HOSTNAME_FOR.indexOf(parsed.protocol) >= 0) {
      try {
        parsed.hostname = punycode.toUnicode(parsed.hostname);
      } catch (er) {}
    }
  }
  return mdurl.decode(mdurl.format(parsed));
}
function MarkdownIt(presetName, options) {
  if (!(this instanceof MarkdownIt)) {
    return new MarkdownIt(presetName, options);
  }
  if (!options) {
    if (!utils.isString(presetName)) {
      options = presetName || ({});
      presetName = 'default';
    }
  }
  this.inline = new ParserInline();
  this.block = new ParserBlock();
  this.core = new ParserCore();
  this.renderer = new Renderer();
  this.linkify = new LinkifyIt();
  this.validateLink = validateLink;
  this.normalizeLink = normalizeLink;
  this.normalizeLinkText = normalizeLinkText;
  this.utils = utils;
  this.helpers = utils.assign({}, helpers);
  this.options = {};
  this.configure(presetName);
  if (options) {
    this.set(options);
  }
}
MarkdownIt.prototype.set = function (options) {
  utils.assign(this.options, options);
  return this;
};
MarkdownIt.prototype.configure = function (presets) {
  var self = this, presetName;
  if (utils.isString(presets)) {
    presetName = presets;
    presets = config[presetName];
    if (!presets) {
      throw new Error('Wrong `markdown-it` preset "' + presetName + '", check name');
    }
  }
  if (!presets) {
    throw new Error('Wrong `markdown-it` preset, can\'t be empty');
  }
  if (presets.options) {
    self.set(presets.options);
  }
  if (presets.components) {
    Object.keys(presets.components).forEach(function (name) {
      if (presets.components[name].rules) {
        self[name].ruler.enableOnly(presets.components[name].rules);
      }
      if (presets.components[name].rules2) {
        self[name].ruler2.enableOnly(presets.components[name].rules2);
      }
    });
  }
  return this;
};
MarkdownIt.prototype.enable = function (list, ignoreInvalid) {
  var result = [];
  if (!Array.isArray(list)) {
    list = [list];
  }
  ['core', 'block', 'inline'].forEach(function (chain) {
    result = result.concat(this[chain].ruler.enable(list, true));
  }, this);
  result = result.concat(this.inline.ruler2.enable(list, true));
  var missed = list.filter(function (name) {
    return result.indexOf(name) < 0;
  });
  if (missed.length && !ignoreInvalid) {
    throw new Error('MarkdownIt. Failed to enable unknown rule(s): ' + missed);
  }
  return this;
};
MarkdownIt.prototype.disable = function (list, ignoreInvalid) {
  var result = [];
  if (!Array.isArray(list)) {
    list = [list];
  }
  ['core', 'block', 'inline'].forEach(function (chain) {
    result = result.concat(this[chain].ruler.disable(list, true));
  }, this);
  result = result.concat(this.inline.ruler2.disable(list, true));
  var missed = list.filter(function (name) {
    return result.indexOf(name) < 0;
  });
  if (missed.length && !ignoreInvalid) {
    throw new Error('MarkdownIt. Failed to disable unknown rule(s): ' + missed);
  }
  return this;
};
MarkdownIt.prototype.use = function (plugin) {
  var args = [this].concat(Array.prototype.slice.call(arguments, 1));
  plugin.apply(plugin, args);
  return this;
};
MarkdownIt.prototype.parse = function (src, env) {
  if (typeof src !== 'string') {
    throw new Error('Input data should be a String');
  }
  var state = new this.core.State(src, this, env);
  this.core.process(state);
  return state.tokens;
};
MarkdownIt.prototype.render = function (src, env) {
  env = env || ({});
  return this.renderer.render(this.parse(src, env), this.options, env);
};
MarkdownIt.prototype.parseInline = function (src, env) {
  var state = new this.core.State(src, this, env);
  state.inlineMode = true;
  this.core.process(state);
  return state.tokens;
};
MarkdownIt.prototype.renderInline = function (src, env) {
  env = env || ({});
  return this.renderer.render(this.parseInline(src, env), this.options, env);
};
module.exports = MarkdownIt;

},

// node_modules/min-indent/index.js @66
66: function(__fusereq, exports, module){
'use strict';
module.exports = string => {
  const match = string.match(/^[ \t]*(?=\S)/gm);
  if (!match) {
    return 0;
  }
  return match.reduce((r, a) => Math.min(r, a.length), Infinity);
};

},

// node_modules/helium-ui/dist/Augments/Augment.js @67
67: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ElementAugment = void 0;
class ElementAugment {
  constructor(modFn, store = {}) {
    this.modFn = modFn;
    this._frozen = false;
    this.store = store;
  }
  targetElement(modMe) {
    this.target = modMe;
    this.runMod("init");
  }
  get frozen() {
    return this._frozen;
  }
  freeze() {
    if (this._frozen) {
      return;
    }
    this._frozen = true;
    this.runMod("freeze");
  }
  unfreeze() {
    if (!this._frozen) {
      return;
    }
    this._frozen = false;
    this.runMod("unfreeze");
  }
  runMod(modType) {
    this.modFn(this.target, {
      store: this.store,
      modType
    });
  }
}
exports.ElementAugment = ElementAugment;

},

// node_modules/helium-sdx/dist/Derivations/Derivers/DeriverSwitch.js @68
68: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const BasicDerivers_1 = __fusereq(69);
function deriverSwitch(args) {
  const {watch, responses} = args;
  const noWay = responses.find(it => !(("match" in it) || ("test" in it)));
  if (!!noWay) {
    console.error(noWay);
    throw new Error(`Response has no ability to match item.  Perhaps use "DEFAULT_RESPONSE" as match field.`);
  }
  responses.sort((a, b) => {
    const aIsDefault = a.test === "DEFAULT_RESPONSE";
    const bIsDefault = b.test === "DEFAULT_RESPONSE";
    if (aIsDefault && !bIsDefault) {
      return 1;
    }
    if (!aIsDefault && bIsDefault) {
      return -1;
    }
    return 0;
  });
  let lastResponse;
  BasicDerivers_1.derive({
    ...args.ddxProps,
    fn: () => {
      let value;
      if (typeof watch === "function") {
        value = watch();
      } else {
        value = watch.get();
      }
      BasicDerivers_1.noDeps(async () => {
        for (const resp of responses) {
          const {match, test} = resp;
          let tryMe;
          if (test === "DEFAULT_RESPONSE") {
            tryMe = resp;
          } else if (value === match) {
            tryMe = resp;
          } else if (test instanceof RegExp) {
            if (test.test(String(value))) {
              tryMe = resp;
            }
          } else if (typeof test === "function") {
            if (await test(value)) {
              tryMe = resp;
            }
          }
          if (!!tryMe) {
            if (tryMe === lastResponse && !args.retriggerSameResponse) {
              return;
            }
            const attempt = await tryMe.action(value);
            if (attempt === "NO_SWITCH_NEEDED") {
              return;
            }
            if (attempt !== "TRY_NEXT_ACTION") {
              return lastResponse = resp;
            }
          }
        }
      });
    }
  });
}
exports.deriverSwitch = deriverSwitch;

},

// node_modules/helium-sdx/dist/Derivations/Derivers/BasicDerivers.js @69
69: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const DeriverScope_1 = __fusereq(57);
const DerivationManager_1 = __fusereq(56);
function derive(fnOrProps, batching) {
  let props;
  if (typeof fnOrProps === "function") {
    props = {
      fn: fnOrProps
    };
  } else {
    props = fnOrProps;
  }
  props.batching = props.batching || batching;
  const scope = new DeriverScope_1.DeriverScope(props);
  scope.runDdx();
  return scope;
}
exports.derive = derive;
function deriveAnchorless(fnOrProps, batching) {
  const props = typeof fnOrProps === "object" ? fnOrProps : {
    fn: fnOrProps
  };
  props.anchor = "NO_ANCHOR";
  return derive(props, batching);
}
exports.deriveAnchorless = deriveAnchorless;
function noDeps(cb) {
  let out;
  DerivationManager_1.DerivationManager._runAsDeriver(undefined, () => out = cb());
  return out;
}
exports.noDeps = noDeps;

},

// node_modules/helium-sdx/dist/Derivations/Sources/Source.js @70
70: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const DerivationManager_1 = __fusereq(56);
const BasicDerivers_1 = __fusereq(69);
function source(value) {
  return new Source(value);
}
exports.source = source;
class Source {
  constructor(value, args = {}) {
    this.value = value;
    this.args = args;
    this.sourceId = DerivationManager_1.DerivationManager.createSourceId(args.idAppend);
  }
  get() {
    DerivationManager_1.DerivationManager.sourceRequested(this);
    return this.value;
  }
  peek() {
    return this.value;
  }
  getSourceId() {
    return this.sourceId;
  }
  set(value, forceUpdates = false) {
    const equalValue = !forceUpdates && this.value === value;
    this.value = value;
    this.dispatchUpdateNotifications(equalValue);
  }
  bindTo(bindTo) {
    BasicDerivers_1.derive(() => this.set(bindTo.get()));
    BasicDerivers_1.derive(() => bindTo.set(this.get()));
  }
  dispatchUpdateNotifications(equalValue = false) {
    DerivationManager_1.DerivationManager.sourceUpdated(this, equalValue);
  }
}
exports.Source = Source;

},

// node_modules/helium-sdx/dist/Derivations/Sources/SourceArray.js @71
71: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const Source_1 = __fusereq(70);
const SourceMap_1 = __fusereq(72);
const DerivationManager_1 = __fusereq(56);
const BasicDerivers_1 = __fusereq(69);
class SourceArrayBase {
  constructor(array) {
    this.DELETED = {
      DELETED: true
    };
    if (array) {
      this._array = array.map(value => new Source_1.Source(value, {
        idAppend: "aval"
      }));
      this._length = new Source_1.Source(array.length, {
        idAppend: "alen"
      });
    } else {
      this._array = [];
      this._length = new Source_1.Source(0, {
        idAppend: "alen"
      });
    }
  }
  get length() {
    return this._length.get();
  }
  ddxEachNoIndexDep(cbOrDdxProps, maybeCb) {
    let props;
    let cb;
    if (typeof cbOrDdxProps === "object") {
      props = cbOrDdxProps;
      cb = maybeCb;
    } else {
      props = {};
      cb = cbOrDdxProps;
    }
    if (!cb) {
      throw new Error(`A callback function must be supplied`);
    }
    props.fn = ({store}) => {
      const data = store.ddxEachNoIndexDep = store.ddxEachNoIndexDep || ({
        checkedSources: new Map(),
        checkedLength: 0
      });
      const currentLength = this.length;
      if (currentLength <= data.checkedLength) {
        return;
      }
      for (let i = 0; i < currentLength; i++) {
        const source = this.getSource(i);
        if (!data.checkedSources.has(source)) {
          data.checkedSources.set(source);
          BasicDerivers_1.derive({
            ...props,
            fn: () => cb(this.noDeletedConstant(source.get()))
          });
        }
      }
      data.checkedLength = currentLength;
    };
    props.dependenciesNeverChange = true;
    BasicDerivers_1.derive(props);
  }
  delete(index) {
    this.getSource(index).set(this.DELETED);
  }
  forEachNoIndexDep(cb) {
    for (let i = 0; i < this.length; i++) {
      cb(this.getNoIndexDep(i), i);
    }
  }
  getNoIndexDep(index) {
    return this.noDeletedConstant(this.getSource(index).get());
  }
  noDeletedConstant(out) {
    return out === this.DELETED ? undefined : out;
  }
  getSource(index, noInit = false) {
    if (!this._array[index] && !noInit) {
      this._array[index] = new Source_1.Source(this.DELETED, {
        idAppend: "aval"
      });
    }
    return this._array[index];
  }
  peek(index) {
    if (typeof index === "number" && this._array[index]) {
      return this.noDeletedConstant(this._array[index].peek());
    }
  }
  peekHas(index) {
    return index < this.peekLength() && this._array[index] && this._array[index].peek() !== this.DELETED;
  }
  peekLength() {
    return this._length.peek();
  }
  pop() {
    const index = this.peekLength() - 1;
    const value = this._array.pop().get();
    this._length.set(index);
    this.syncIndex(index);
    return this.noDeletedConstant(value);
  }
  push(value) {
    const index = this.peekLength();
    this.set(index, value);
    this._length.set(index + 1);
    this.syncIndex(index);
  }
  remove(item, limit = -1) {
    let count = 0;
    const length = this._length.peek();
    for (let i = 0; i < length - count; i++) {
      if (limit > -1 && count >= limit) {
        break;
      }
      if (this.peek(i) === item) {
        this._array.splice(i, 1);
        i--;
        count++;
      }
    }
    this._length.set(length - count);
    this.onArrangementUpdate();
  }
  set(index, value, forceUpdates) {
    if (!this._array[index]) {
      this._array[index] = new Source_1.Source(value, {
        idAppend: "aval"
      });
    } else {
      this._array[index].set(value, forceUpdates);
    }
    if (index + 1 > this.peekLength()) {
      this._length.set(index + 1);
      this.syncIndex(index);
    }
  }
  shift() {
    const length = this.peekLength();
    if (!length) {
      return undefined;
    }
    const out = this._array.shift();
    this._length.set(length - 1);
    this.onArrangementUpdate();
    return this.noDeletedConstant(out.get());
  }
  sliceNoIndexDep(start, end) {
    return this._array.slice(start, end).map(it => this.noDeletedConstant(it.get()));
  }
  toObjectNoDeps() {
    return this._array.map(item => item.peek());
  }
  unshift(value) {
    this._array.unshift(new Source_1.Source(value, {
      idAppend: "aval"
    }));
    this._length.set(this.peekLength() + 1);
    this.onArrangementUpdate();
  }
  onArrangementUpdate() {}
  syncIndex(index) {}
}
exports.SourceArrayBase = SourceArrayBase;
class SourceArray extends SourceArrayBase {
  constructor() {
    super(...arguments);
    this.lastUpdate = new Source_1.Source(undefined, {
      idAppend: "aupt"
    });
    this.anchorDeriver = DerivationManager_1.DerivationManager._getCurrentDeriver();
  }
  lastIndexOf() {
    throw new Error("TODO");
  }
  reduceRight() {
    throw new Error("TODO");
  }
  flatMap() {
    throw new Error("TODO");
  }
  concat(items) {
    return this.toObject().concat(items);
  }
  copyWithin(target, start, end) {
    const length = this.peekLength();
    end = end !== undefined ? end : length;
    let copyIndex = start || 0;
    for (let i = target || 0; i < length && copyIndex < end; (i++, copyIndex++)) {
      this.set(i, this.peek(copyIndex));
    }
    return this;
  }
  every(cb) {
    const length = this.peekLength();
    for (let i = 0; i < length; i++) {
      if (cb(this.getNoIndexDep(i)) !== true) {
        return false;
      }
    }
    return true;
  }
  clear() {
    while (this._array.length) {
      this._array.pop();
      this.syncIndex(this._array.length);
    }
    this._length.set(0);
    return this;
  }
  entries() {
    const out = [];
    this.forEach((item, index) => out.push([index, item]));
    return out;
  }
  fill(value, start, end) {
    const length = this.peekLength();
    for (let i = start || 0; i < end || length; i++) {
      this.set(i, value);
    }
  }
  filter(cb) {
    const out = [];
    this.forEach(item => {
      if (cb(item)) {
        out.push(item);
      }
    });
    return out;
  }
  find(cb) {
    const index = this.findIndex(cb);
    return index === -1 ? undefined : this.getNoIndexDep(index);
  }
  findIndex(cb) {
    this.orderMatters();
    const length = this.peekLength();
    for (let i = 0; i < length; i++) {
      const value = this.getNoIndexDep(i);
      if (cb(value)) {
        return i;
      }
    }
    return -1;
  }
  flat(depth) {
    this.orderMatters();
    return this.toObject().flat(depth);
  }
  forEach(cb) {
    this.orderMatters();
    return this.forEachNoIndexDep(cb);
  }
  has(index) {
    const indexSrc = this._getIndexObserver(index).get();
    const value = indexSrc.get();
    return value !== this.DELETED;
  }
  iterator() {
    const self = this;
    void this.length;
    return iterate();
    function* iterate() {
      for (let i = 0; i < self.length; i++) {
        yield self.get(i);
      }
    }
  }
  get(index) {
    return this.noDeletedConstant(this._getIndexObserver(index).get().get());
  }
  includes(item, fromIndex) {
    if (fromIndex !== undefined) {
      throw new Error("TODO: implement fromIndex");
    }
    this.lengthMatters();
    return this.indexOf(item) !== -1;
  }
  indexOf(item) {
    this.orderMatters();
    const length = this.peekLength();
    for (let i = 0; i < length; i++) {
      if (this.getNoIndexDep(i) === item) {
        return i;
      }
    }
    this.lengthMatters();
    return -1;
  }
  join(separator) {
    this.orderMatters();
    this.lengthMatters();
    return this._array.map(it => it.get()).join(separator);
  }
  keys() {
    const out = [];
    const length = this.length;
    for (let i = 0; i < length; i++) {
      let item = this._getIndexObserver(i);
      if (item.get() && item.get().get() !== this.DELETED) {
        out.push(i);
      }
    }
    return out;
  }
  map(cb) {
    const out = [];
    this.forEach((item, index) => out.push(cb(item, index)));
    return out;
  }
  reduce(cb) {
    let out;
    this.forEach((item, index) => {
      if (index === 0) {
        return out = item;
      }
      out = cb(out, item);
    });
    return out;
  }
  reverse() {
    const activeValues = this._array.splice(0, this._length.peek());
    this._array.splice(0, 0, ...activeValues.reverse());
    this.onArrangementUpdate();
    return this;
  }
  slice(start, end) {
    const length = this.peekLength();
    const out = [];
    for (let i = start; end !== undefined && i < end && i < length; i++) {
      out.push(this.get(i));
    }
    return out;
  }
  some(cb) {
    return this.findIndex(cb) !== -1;
  }
  sort(cb) {
    cb = cb || ((l, r) => {
      if (l < r) {
        return -1;
      }
      if (l > r) {
        return 1;
      }
      return 0;
    });
    const activeValues = this._array.splice(0, this._length.peek());
    this._array.splice(0, 0, ...activeValues.sort((a, b) => cb(a.peek(), b.peek())));
    this.onArrangementUpdate();
    return this;
  }
  splice(start, deleteCount, ...items) {
    if (start < 0) {
      throw new Error("TODO");
    }
    const length = this._length.peek();
    if (deleteCount === undefined) {
      deleteCount = length - start;
    }
    start = Math.min(start, length);
    const numDeleted = Math.min(length - start, deleteCount);
    const numAdded = items.length;
    const out = this._array.splice(start, deleteCount, ...items.map(value => new Source_1.Source(value)));
    this._length.set(Math.max(start, length) + (numAdded - numDeleted));
    this.onArrangementUpdate();
    return out.map(it => it.peek());
  }
  toObject() {
    this.orderMatters();
    const out = [];
    this.forEach(item => out.push(item));
    return out;
  }
  values() {
    return this.toObject();
  }
  onArrangementUpdate() {
    this.lastUpdate.set(Date.now());
    if (this.indexMap) {
      BasicDerivers_1.noDeps(() => Array.from(this.indexMap._static().keys()).forEach(key => this.indexMap.set(key, this.getSource(key))));
    }
  }
  syncIndex(index) {
    const indexedSrc = this.indexMap && this.indexMap.peek(this._array.length);
    if (indexedSrc) {
      if (this.peekLength() <= index) {
        indexedSrc.set(this.DELETED);
      } else {
        indexedSrc.set(this.peek(index));
      }
    }
  }
  _getIndexObserver(index) {
    if (!this.indexMap) {
      this.indexMap = new SourceMap_1.SourceMapBase(null, {
        id: "ai"
      });
      BasicDerivers_1.derive({
        fn: () => {
          this.orderMatters();
          Array.from(this.indexMap._static().keys()).forEach(key => this.indexMap.set(key, this.getSource(key)));
        },
        anchor: this.anchorDeriver
      });
    }
    if (!this.indexMap._static().has(index)) {
      BasicDerivers_1.noDeps(() => this.indexMap.set(index, this.getSource(index)));
    }
    return this.indexMap.getSource(index);
  }
  orderMatters() {
    void this.lastUpdate.get();
  }
  lengthMatters() {
    void this.length;
  }
}
exports.SourceArray = SourceArray;

},

// node_modules/helium-sdx/dist/Derivations/Sources/SourceMap.js @72
72: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const Source_1 = __fusereq(70);
const BasicDerivers_1 = __fusereq(69);
class SourceMapBase {
  constructor(entries, args) {
    this.args = args;
    this._map = new Map();
    if (entries) {
      throw new Error("TODO");
    }
  }
  getSource(key) {
    return this.getSetSource({
      key
    });
  }
  get(key) {
    return this.getSource(key).get();
  }
  set(key, value, forceUpdates) {
    this.getSetSource({
      key,
      value
    });
  }
  _static() {
    return this._map;
  }
  peek(key) {
    if (this._map.has(key)) {
      return this._map.get(key).peek();
    }
  }
  getSetSource(args) {
    const key = args.key;
    const setNotGet = ("value" in args);
    const keyDoesNotExist = this._map.has(key) === false;
    if (keyDoesNotExist) {
      let idAppend = "mval";
      if (this.args && this.args.id) {
        idAppend = `${this.args.id}_${idAppend}`;
      }
      if (typeof key === "string" || typeof key === "number") {
        idAppend += `_${key}`;
      }
      this._map.set(key, new Source_1.Source(args.value, {
        idAppend
      }));
      if (setNotGet) {
        return;
      }
    } else if (setNotGet) {
      this._map.get(key).set(args.value);
      return;
    }
    return this._map.get(key);
  }
}
exports.SourceMapBase = SourceMapBase;
class SourceMap extends SourceMapBase {
  constructor() {
    super(...arguments);
    this._keys = new Source_1.Source([], {
      idAppend: "mapKeys"
    });
    this._has = new SourceMapBase(null, {
      id: "has"
    });
    this._size = new Source_1.Source(0);
  }
  forEach(cb) {
    this.keys().forEach(key => cb(this.get(key), key));
  }
  entries() {
    throw new Error("TODO");
  }
  values() {
    throw new Error("TODO");
  }
  iterator() {
    const self = this;
    return iterate();
    function* iterate() {
      for (const it of self.keys()) {
        yield self.get(it);
      }
    }
  }
  toObject() {
    const out = {};
    this.forEach((value, key) => {
      if (typeof key === "string" || typeof key === "number") {
        out[key] = value;
      }
    });
    return out;
  }
  size() {
    return this._size.get();
  }
  set(key, value, forceUpdates) {
    let keysChanged = false;
    if (!this.peekHas(key)) {
      keysChanged = true;
    }
    this._has.set(key, true);
    super.set(key, value, forceUpdates);
    if (keysChanged) {
      this.updateKeys();
    }
  }
  delete(key) {
    if (!this.peekHas(key)) {
      return;
    }
    this._has.set(key, false);
    super.set(key, undefined);
    this.updateKeys();
  }
  clear() {
    this.keys().forEach(key => this.delete(key));
  }
  getnit(key, value) {
    if (this.peekHas(key)) {
      return this.get(key);
    }
    this.set(key, value);
    return value;
  }
  has(key) {
    return !!this._has.get(key);
  }
  keys() {
    return this._keys.get();
  }
  isEmpty() {
    return !this._keys.get().length;
  }
  updateKeys() {
    BasicDerivers_1.noDeps(() => {
      const keys = Array.from(this._map.keys()).filter(key => this.peekHas(key));
      this._keys.set(keys);
      this._size.set(keys.length);
    });
  }
  peekHas(key) {
    return !!this._has.peek(key);
  }
}
exports.SourceMap = SourceMap;

},

// node_modules/helium-sdx/dist/Derivations/Sources/DdxSource.js @73
73: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const Source_1 = __fusereq(70);
const BasicDerivers_1 = __fusereq(69);
class DdxSource {
  constructor(ddx) {
    this.source = new Source_1.Source();
    BasicDerivers_1.derive(() => {
      this.source.set(ddx());
    });
  }
  get() {
    return this.source.get();
  }
  peek() {
    return this.source.peek();
  }
  getSource() {
    return this.source;
  }
}
exports.DdxSource = DdxSource;

},

// node_modules/helium-sdx/dist/Derivations/Sources/PromiseSource.js @74
74: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const Source_1 = __fusereq(70);
const DerivationManager_1 = __fusereq(56);
function awaitSource(propsOrName, promiseOrFn) {
  const props = typeof propsOrName === "object" ? propsOrName : {
    name: propsOrName
  };
  const promise = typeof promiseOrFn === "function" ? promiseOrFn() : promiseOrFn;
  const promSource = new PromiseSource(props, promise);
  return promSource.get();
}
exports.awaitSource = awaitSource;
class PromiseSource {
  constructor(propsOrName, promiseOrFn) {
    const props = typeof propsOrName === "object" ? propsOrName : {
      name: propsOrName
    };
    this.ddxScope = props.ddxScope || DerivationManager_1.DerivationManager._getCurrentDeriver();
    const ddxStore = this.ddxScope.getStore();
    ddxStore.promiseSources = ddxStore.promiseSources || ({});
    let stor = ddxStore.promiseSources[props.name];
    if (stor) {
      this.source = stor.source;
      this.promise = stor.promise;
    } else {
      const promise = typeof promiseOrFn === "function" ? promiseOrFn() : promiseOrFn;
      ddxStore.promiseSources[props.name] = stor = {
        promise: this.promise = promise,
        source: this.source = new Source_1.Source()
      };
      promise.then(value => this.source.set(value)).catch(console.error);
    }
  }
  get() {
    return this.source.get();
  }
}
exports.PromiseSource = PromiseSource;

},

// node_modules/markdown-it/lib/common/utils.js @75
75: function(__fusereq, exports, module){
'use strict';
function _class(obj) {
  return Object.prototype.toString.call(obj);
}
function isString(obj) {
  return _class(obj) === '[object String]';
}
var _hasOwnProperty = Object.prototype.hasOwnProperty;
function has(object, key) {
  return _hasOwnProperty.call(object, key);
}
function assign(obj) {
  var sources = Array.prototype.slice.call(arguments, 1);
  sources.forEach(function (source) {
    if (!source) {
      return;
    }
    if (typeof source !== 'object') {
      throw new TypeError(source + 'must be object');
    }
    Object.keys(source).forEach(function (key) {
      obj[key] = source[key];
    });
  });
  return obj;
}
function arrayReplaceAt(src, pos, newElements) {
  return [].concat(src.slice(0, pos), newElements, src.slice(pos + 1));
}
function isValidEntityCode(c) {
  if (c >= 0xD800 && c <= 0xDFFF) {
    return false;
  }
  if (c >= 0xFDD0 && c <= 0xFDEF) {
    return false;
  }
  if ((c & 0xFFFF) === 0xFFFF || (c & 0xFFFF) === 0xFFFE) {
    return false;
  }
  if (c >= 0x00 && c <= 0x08) {
    return false;
  }
  if (c === 0x0B) {
    return false;
  }
  if (c >= 0x0E && c <= 0x1F) {
    return false;
  }
  if (c >= 0x7F && c <= 0x9F) {
    return false;
  }
  if (c > 0x10FFFF) {
    return false;
  }
  return true;
}
function fromCodePoint(c) {
  if (c > 0xffff) {
    c -= 0x10000;
    var surrogate1 = 0xd800 + (c >> 10), surrogate2 = 0xdc00 + (c & 0x3ff);
    return String.fromCharCode(surrogate1, surrogate2);
  }
  return String.fromCharCode(c);
}
var UNESCAPE_MD_RE = /\\([!"#$%&'()*+,\-.\/:;<=>?@[\\\]^_`{|}~])/g;
var ENTITY_RE = /&([a-z#][a-z0-9]{1,31});/gi;
var UNESCAPE_ALL_RE = new RegExp(UNESCAPE_MD_RE.source + '|' + ENTITY_RE.source, 'gi');
var DIGITAL_ENTITY_TEST_RE = /^#((?:x[a-f0-9]{1,8}|[0-9]{1,8}))/i;
var entities = __fusereq(128);
function replaceEntityPattern(match, name) {
  var code = 0;
  if (has(entities, name)) {
    return entities[name];
  }
  if (name.charCodeAt(0) === 0x23 && DIGITAL_ENTITY_TEST_RE.test(name)) {
    code = name[1].toLowerCase() === 'x' ? parseInt(name.slice(2), 16) : parseInt(name.slice(1), 10);
    if (isValidEntityCode(code)) {
      return fromCodePoint(code);
    }
  }
  return match;
}
function unescapeMd(str) {
  if (str.indexOf('\\') < 0) {
    return str;
  }
  return str.replace(UNESCAPE_MD_RE, '$1');
}
function unescapeAll(str) {
  if (str.indexOf('\\') < 0 && str.indexOf('&') < 0) {
    return str;
  }
  return str.replace(UNESCAPE_ALL_RE, function (match, escaped, entity) {
    if (escaped) {
      return escaped;
    }
    return replaceEntityPattern(match, entity);
  });
}
var HTML_ESCAPE_TEST_RE = /[&<>"]/;
var HTML_ESCAPE_REPLACE_RE = /[&<>"]/g;
var HTML_REPLACEMENTS = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;'
};
function replaceUnsafeChar(ch) {
  return HTML_REPLACEMENTS[ch];
}
function escapeHtml(str) {
  if (HTML_ESCAPE_TEST_RE.test(str)) {
    return str.replace(HTML_ESCAPE_REPLACE_RE, replaceUnsafeChar);
  }
  return str;
}
var REGEXP_ESCAPE_RE = /[.?*+^$[\]\\(){}|-]/g;
function escapeRE(str) {
  return str.replace(REGEXP_ESCAPE_RE, '\\$&');
}
function isSpace(code) {
  switch (code) {
    case 0x09:
    case 0x20:
      return true;
  }
  return false;
}
function isWhiteSpace(code) {
  if (code >= 0x2000 && code <= 0x200A) {
    return true;
  }
  switch (code) {
    case 0x09:
    case 0x0A:
    case 0x0B:
    case 0x0C:
    case 0x0D:
    case 0x20:
    case 0xA0:
    case 0x1680:
    case 0x202F:
    case 0x205F:
    case 0x3000:
      return true;
  }
  return false;
}
var UNICODE_PUNCT_RE = __fusereq(129);
function isPunctChar(ch) {
  return UNICODE_PUNCT_RE.test(ch);
}
function isMdAsciiPunct(ch) {
  switch (ch) {
    case 0x21:
    case 0x22:
    case 0x23:
    case 0x24:
    case 0x25:
    case 0x26:
    case 0x27:
    case 0x28:
    case 0x29:
    case 0x2A:
    case 0x2B:
    case 0x2C:
    case 0x2D:
    case 0x2E:
    case 0x2F:
    case 0x3A:
    case 0x3B:
    case 0x3C:
    case 0x3D:
    case 0x3E:
    case 0x3F:
    case 0x40:
    case 0x5B:
    case 0x5C:
    case 0x5D:
    case 0x5E:
    case 0x5F:
    case 0x60:
    case 0x7B:
    case 0x7C:
    case 0x7D:
    case 0x7E:
      return true;
    default:
      return false;
  }
}
function normalizeReference(str) {
  str = str.trim().replace(/\s+/g, ' ');
  if (('ẞ').toLowerCase() === 'Ṿ') {
    str = str.replace(/ẞ/g, 'ß');
  }
  return str.toLowerCase().toUpperCase();
}
exports.lib = {};
exports.lib.mdurl = __fusereq(82);
exports.lib.ucmicro = __fusereq(130);
exports.assign = assign;
exports.isString = isString;
exports.has = has;
exports.unescapeMd = unescapeMd;
exports.unescapeAll = unescapeAll;
exports.isValidEntityCode = isValidEntityCode;
exports.fromCodePoint = fromCodePoint;
exports.escapeHtml = escapeHtml;
exports.arrayReplaceAt = arrayReplaceAt;
exports.isSpace = isSpace;
exports.isWhiteSpace = isWhiteSpace;
exports.isMdAsciiPunct = isMdAsciiPunct;
exports.isPunctChar = isPunctChar;
exports.escapeRE = escapeRE;
exports.normalizeReference = normalizeReference;

},

// node_modules/markdown-it/lib/helpers/index.js @76
76: function(__fusereq, exports, module){
'use strict';
exports.parseLinkLabel = __fusereq(87);
exports.parseLinkDestination = __fusereq(88);
exports.parseLinkTitle = __fusereq(89);

},

// node_modules/markdown-it/lib/renderer.js @77
77: function(__fusereq, exports, module){
'use strict';
var assign = __fusereq(75).assign;
var unescapeAll = __fusereq(75).unescapeAll;
var escapeHtml = __fusereq(75).escapeHtml;
var default_rules = {};
default_rules.code_inline = function (tokens, idx, options, env, slf) {
  var token = tokens[idx];
  return '<code' + slf.renderAttrs(token) + '>' + escapeHtml(tokens[idx].content) + '</code>';
};
default_rules.code_block = function (tokens, idx, options, env, slf) {
  var token = tokens[idx];
  return '<pre' + slf.renderAttrs(token) + '><code>' + escapeHtml(tokens[idx].content) + '</code></pre>\n';
};
default_rules.fence = function (tokens, idx, options, env, slf) {
  var token = tokens[idx], info = token.info ? unescapeAll(token.info).trim() : '', langName = '', highlighted, i, tmpAttrs, tmpToken;
  if (info) {
    langName = info.split(/\s+/g)[0];
  }
  if (options.highlight) {
    highlighted = options.highlight(token.content, langName) || escapeHtml(token.content);
  } else {
    highlighted = escapeHtml(token.content);
  }
  if (highlighted.indexOf('<pre') === 0) {
    return highlighted + '\n';
  }
  if (info) {
    i = token.attrIndex('class');
    tmpAttrs = token.attrs ? token.attrs.slice() : [];
    if (i < 0) {
      tmpAttrs.push(['class', options.langPrefix + langName]);
    } else {
      tmpAttrs[i][1] += ' ' + options.langPrefix + langName;
    }
    tmpToken = {
      attrs: tmpAttrs
    };
    return '<pre><code' + slf.renderAttrs(tmpToken) + '>' + highlighted + '</code></pre>\n';
  }
  return '<pre><code' + slf.renderAttrs(token) + '>' + highlighted + '</code></pre>\n';
};
default_rules.image = function (tokens, idx, options, env, slf) {
  var token = tokens[idx];
  token.attrs[token.attrIndex('alt')][1] = slf.renderInlineAsText(token.children, options, env);
  return slf.renderToken(tokens, idx, options);
};
default_rules.hardbreak = function (tokens, idx, options) {
  return options.xhtmlOut ? '<br />\n' : '<br>\n';
};
default_rules.softbreak = function (tokens, idx, options) {
  return options.breaks ? options.xhtmlOut ? '<br />\n' : '<br>\n' : '\n';
};
default_rules.text = function (tokens, idx) {
  return escapeHtml(tokens[idx].content);
};
default_rules.html_block = function (tokens, idx) {
  return tokens[idx].content;
};
default_rules.html_inline = function (tokens, idx) {
  return tokens[idx].content;
};
function Renderer() {
  this.rules = assign({}, default_rules);
}
Renderer.prototype.renderAttrs = function renderAttrs(token) {
  var i, l, result;
  if (!token.attrs) {
    return '';
  }
  result = '';
  for ((i = 0, l = token.attrs.length); i < l; i++) {
    result += ' ' + escapeHtml(token.attrs[i][0]) + '="' + escapeHtml(token.attrs[i][1]) + '"';
  }
  return result;
};
Renderer.prototype.renderToken = function renderToken(tokens, idx, options) {
  var nextToken, result = '', needLf = false, token = tokens[idx];
  if (token.hidden) {
    return '';
  }
  if (token.block && token.nesting !== -1 && idx && tokens[idx - 1].hidden) {
    result += '\n';
  }
  result += (token.nesting === -1 ? '</' : '<') + token.tag;
  result += this.renderAttrs(token);
  if (token.nesting === 0 && options.xhtmlOut) {
    result += ' /';
  }
  if (token.block) {
    needLf = true;
    if (token.nesting === 1) {
      if (idx + 1 < tokens.length) {
        nextToken = tokens[idx + 1];
        if (nextToken.type === 'inline' || nextToken.hidden) {
          needLf = false;
        } else if (nextToken.nesting === -1 && nextToken.tag === token.tag) {
          needLf = false;
        }
      }
    }
  }
  result += needLf ? '>\n' : '>';
  return result;
};
Renderer.prototype.renderInline = function (tokens, options, env) {
  var type, result = '', rules = this.rules;
  for (var i = 0, len = tokens.length; i < len; i++) {
    type = tokens[i].type;
    if (typeof rules[type] !== 'undefined') {
      result += rules[type](tokens, i, options, env, this);
    } else {
      result += this.renderToken(tokens, i, options);
    }
  }
  return result;
};
Renderer.prototype.renderInlineAsText = function (tokens, options, env) {
  var result = '';
  for (var i = 0, len = tokens.length; i < len; i++) {
    if (tokens[i].type === 'text') {
      result += tokens[i].content;
    } else if (tokens[i].type === 'image') {
      result += this.renderInlineAsText(tokens[i].children, options, env);
    }
  }
  return result;
};
Renderer.prototype.render = function (tokens, options, env) {
  var i, len, type, result = '', rules = this.rules;
  for ((i = 0, len = tokens.length); i < len; i++) {
    type = tokens[i].type;
    if (type === 'inline') {
      result += this.renderInline(tokens[i].children, options, env);
    } else if (typeof rules[type] !== 'undefined') {
      result += rules[tokens[i].type](tokens, i, options, env, this);
    } else {
      result += this.renderToken(tokens, i, options, env);
    }
  }
  return result;
};
module.exports = Renderer;

},

// node_modules/markdown-it/lib/parser_core.js @78
78: function(__fusereq, exports, module){
'use strict';
var Ruler = __fusereq(90);
var _rules = [['normalize', __fusereq(91)], ['block', __fusereq(92)], ['inline', __fusereq(93)], ['linkify', __fusereq(94)], ['replacements', __fusereq(95)], ['smartquotes', __fusereq(96)]];
function Core() {
  this.ruler = new Ruler();
  for (var i = 0; i < _rules.length; i++) {
    this.ruler.push(_rules[i][0], _rules[i][1]);
  }
}
Core.prototype.process = function (state) {
  var i, l, rules;
  rules = this.ruler.getRules('');
  for ((i = 0, l = rules.length); i < l; i++) {
    rules[i](state);
  }
};
Core.prototype.State = __fusereq(97);
module.exports = Core;

},

// node_modules/markdown-it/lib/parser_block.js @79
79: function(__fusereq, exports, module){
'use strict';
var Ruler = __fusereq(90);
var _rules = [['table', __fusereq(98), ['paragraph', 'reference']], ['code', __fusereq(99)], ['fence', __fusereq(100), ['paragraph', 'reference', 'blockquote', 'list']], ['blockquote', __fusereq(101), ['paragraph', 'reference', 'blockquote', 'list']], ['hr', __fusereq(102), ['paragraph', 'reference', 'blockquote', 'list']], ['list', __fusereq(103), ['paragraph', 'reference', 'blockquote']], ['reference', __fusereq(104)], ['heading', __fusereq(105), ['paragraph', 'reference', 'blockquote']], ['lheading', __fusereq(106)], ['html_block', __fusereq(107), ['paragraph', 'reference', 'blockquote']], ['paragraph', __fusereq(108)]];
function ParserBlock() {
  this.ruler = new Ruler();
  for (var i = 0; i < _rules.length; i++) {
    this.ruler.push(_rules[i][0], _rules[i][1], {
      alt: (_rules[i][2] || []).slice()
    });
  }
}
ParserBlock.prototype.tokenize = function (state, startLine, endLine) {
  var ok, i, rules = this.ruler.getRules(''), len = rules.length, line = startLine, hasEmptyLines = false, maxNesting = state.md.options.maxNesting;
  while (line < endLine) {
    state.line = line = state.skipEmptyLines(line);
    if (line >= endLine) {
      break;
    }
    if (state.sCount[line] < state.blkIndent) {
      break;
    }
    if (state.level >= maxNesting) {
      state.line = endLine;
      break;
    }
    for (i = 0; i < len; i++) {
      ok = rules[i](state, line, endLine, false);
      if (ok) {
        break;
      }
    }
    state.tight = !hasEmptyLines;
    if (state.isEmpty(state.line - 1)) {
      hasEmptyLines = true;
    }
    line = state.line;
    if (line < endLine && state.isEmpty(line)) {
      hasEmptyLines = true;
      line++;
      state.line = line;
    }
  }
};
ParserBlock.prototype.parse = function (src, md, env, outTokens) {
  var state;
  if (!src) {
    return;
  }
  state = new this.State(src, md, env, outTokens);
  this.tokenize(state, state.line, state.lineMax);
};
ParserBlock.prototype.State = __fusereq(109);
module.exports = ParserBlock;

},

// node_modules/markdown-it/lib/parser_inline.js @80
80: function(__fusereq, exports, module){
'use strict';
var Ruler = __fusereq(90);
var _rules = [['text', __fusereq(110)], ['newline', __fusereq(111)], ['escape', __fusereq(112)], ['backticks', __fusereq(113)], ['strikethrough', __fusereq(114).tokenize], ['emphasis', __fusereq(115).tokenize], ['link', __fusereq(116)], ['image', __fusereq(117)], ['autolink', __fusereq(118)], ['html_inline', __fusereq(119)], ['entity', __fusereq(120)]];
var _rules2 = [['balance_pairs', __fusereq(121)], ['strikethrough', __fusereq(114).postProcess], ['emphasis', __fusereq(115).postProcess], ['text_collapse', __fusereq(122)]];
function ParserInline() {
  var i;
  this.ruler = new Ruler();
  for (i = 0; i < _rules.length; i++) {
    this.ruler.push(_rules[i][0], _rules[i][1]);
  }
  this.ruler2 = new Ruler();
  for (i = 0; i < _rules2.length; i++) {
    this.ruler2.push(_rules2[i][0], _rules2[i][1]);
  }
}
ParserInline.prototype.skipToken = function (state) {
  var ok, i, pos = state.pos, rules = this.ruler.getRules(''), len = rules.length, maxNesting = state.md.options.maxNesting, cache = state.cache;
  if (typeof cache[pos] !== 'undefined') {
    state.pos = cache[pos];
    return;
  }
  if (state.level < maxNesting) {
    for (i = 0; i < len; i++) {
      state.level++;
      ok = rules[i](state, true);
      state.level--;
      if (ok) {
        break;
      }
    }
  } else {
    state.pos = state.posMax;
  }
  if (!ok) {
    state.pos++;
  }
  cache[pos] = state.pos;
};
ParserInline.prototype.tokenize = function (state) {
  var ok, i, rules = this.ruler.getRules(''), len = rules.length, end = state.posMax, maxNesting = state.md.options.maxNesting;
  while (state.pos < end) {
    if (state.level < maxNesting) {
      for (i = 0; i < len; i++) {
        ok = rules[i](state, false);
        if (ok) {
          break;
        }
      }
    }
    if (ok) {
      if (state.pos >= end) {
        break;
      }
      continue;
    }
    state.pending += state.src[state.pos++];
  }
  if (state.pending) {
    state.pushPending();
  }
};
ParserInline.prototype.parse = function (str, md, env, outTokens) {
  var i, rules, len;
  var state = new this.State(str, md, env, outTokens);
  this.tokenize(state);
  rules = this.ruler2.getRules('');
  len = rules.length;
  for (i = 0; i < len; i++) {
    rules[i](state);
  }
};
ParserInline.prototype.State = __fusereq(123);
module.exports = ParserInline;

},

// node_modules/linkify-it/index.js @81
81: function(__fusereq, exports, module){
'use strict';
function assign(obj) {
  var sources = Array.prototype.slice.call(arguments, 1);
  sources.forEach(function (source) {
    if (!source) {
      return;
    }
    Object.keys(source).forEach(function (key) {
      obj[key] = source[key];
    });
  });
  return obj;
}
function _class(obj) {
  return Object.prototype.toString.call(obj);
}
function isString(obj) {
  return _class(obj) === '[object String]';
}
function isObject(obj) {
  return _class(obj) === '[object Object]';
}
function isRegExp(obj) {
  return _class(obj) === '[object RegExp]';
}
function isFunction(obj) {
  return _class(obj) === '[object Function]';
}
function escapeRE(str) {
  return str.replace(/[.?*+^$[\]\\(){}|-]/g, '\\$&');
}
var defaultOptions = {
  fuzzyLink: true,
  fuzzyEmail: true,
  fuzzyIP: false
};
function isOptionsObj(obj) {
  return Object.keys(obj || ({})).reduce(function (acc, k) {
    return acc || defaultOptions.hasOwnProperty(k);
  }, false);
}
var defaultSchemas = {
  'http:': {
    validate: function (text, pos, self) {
      var tail = text.slice(pos);
      if (!self.re.http) {
        self.re.http = new RegExp('^\\/\\/' + self.re.src_auth + self.re.src_host_port_strict + self.re.src_path, 'i');
      }
      if (self.re.http.test(tail)) {
        return tail.match(self.re.http)[0].length;
      }
      return 0;
    }
  },
  'https:': 'http:',
  'ftp:': 'http:',
  '//': {
    validate: function (text, pos, self) {
      var tail = text.slice(pos);
      if (!self.re.no_http) {
        self.re.no_http = new RegExp('^' + self.re.src_auth + '(?:localhost|(?:(?:' + self.re.src_domain + ')\\.)+' + self.re.src_domain_root + ')' + self.re.src_port + self.re.src_host_terminator + self.re.src_path, 'i');
      }
      if (self.re.no_http.test(tail)) {
        if (pos >= 3 && text[pos - 3] === ':') {
          return 0;
        }
        if (pos >= 3 && text[pos - 3] === '/') {
          return 0;
        }
        return tail.match(self.re.no_http)[0].length;
      }
      return 0;
    }
  },
  'mailto:': {
    validate: function (text, pos, self) {
      var tail = text.slice(pos);
      if (!self.re.mailto) {
        self.re.mailto = new RegExp('^' + self.re.src_email_name + '@' + self.re.src_host_strict, 'i');
      }
      if (self.re.mailto.test(tail)) {
        return tail.match(self.re.mailto)[0].length;
      }
      return 0;
    }
  }
};
var tlds_2ch_src_re = 'a[cdefgilmnoqrstuwxz]|b[abdefghijmnorstvwyz]|c[acdfghiklmnoruvwxyz]|d[ejkmoz]|e[cegrstu]|f[ijkmor]|g[abdefghilmnpqrstuwy]|h[kmnrtu]|i[delmnoqrst]|j[emop]|k[eghimnprwyz]|l[abcikrstuvy]|m[acdeghklmnopqrstuvwxyz]|n[acefgilopruz]|om|p[aefghklmnrstwy]|qa|r[eosuw]|s[abcdeghijklmnortuvxyz]|t[cdfghjklmnortvwz]|u[agksyz]|v[aceginu]|w[fs]|y[et]|z[amw]';
var tlds_default = ('biz|com|edu|gov|net|org|pro|web|xxx|aero|asia|coop|info|museum|name|shop|рф').split('|');
function resetScanCache(self) {
  self.__index__ = -1;
  self.__text_cache__ = '';
}
function createValidator(re) {
  return function (text, pos) {
    var tail = text.slice(pos);
    if (re.test(tail)) {
      return tail.match(re)[0].length;
    }
    return 0;
  };
}
function createNormalizer() {
  return function (match, self) {
    self.normalize(match);
  };
}
function compile(self) {
  var re = self.re = __fusereq(131)(self.__opts__);
  var tlds = self.__tlds__.slice();
  self.onCompile();
  if (!self.__tlds_replaced__) {
    tlds.push(tlds_2ch_src_re);
  }
  tlds.push(re.src_xn);
  re.src_tlds = tlds.join('|');
  function untpl(tpl) {
    return tpl.replace('%TLDS%', re.src_tlds);
  }
  re.email_fuzzy = RegExp(untpl(re.tpl_email_fuzzy), 'i');
  re.link_fuzzy = RegExp(untpl(re.tpl_link_fuzzy), 'i');
  re.link_no_ip_fuzzy = RegExp(untpl(re.tpl_link_no_ip_fuzzy), 'i');
  re.host_fuzzy_test = RegExp(untpl(re.tpl_host_fuzzy_test), 'i');
  var aliases = [];
  self.__compiled__ = {};
  function schemaError(name, val) {
    throw new Error('(LinkifyIt) Invalid schema "' + name + '": ' + val);
  }
  Object.keys(self.__schemas__).forEach(function (name) {
    var val = self.__schemas__[name];
    if (val === null) {
      return;
    }
    var compiled = {
      validate: null,
      link: null
    };
    self.__compiled__[name] = compiled;
    if (isObject(val)) {
      if (isRegExp(val.validate)) {
        compiled.validate = createValidator(val.validate);
      } else if (isFunction(val.validate)) {
        compiled.validate = val.validate;
      } else {
        schemaError(name, val);
      }
      if (isFunction(val.normalize)) {
        compiled.normalize = val.normalize;
      } else if (!val.normalize) {
        compiled.normalize = createNormalizer();
      } else {
        schemaError(name, val);
      }
      return;
    }
    if (isString(val)) {
      aliases.push(name);
      return;
    }
    schemaError(name, val);
  });
  aliases.forEach(function (alias) {
    if (!self.__compiled__[self.__schemas__[alias]]) {
      return;
    }
    self.__compiled__[alias].validate = self.__compiled__[self.__schemas__[alias]].validate;
    self.__compiled__[alias].normalize = self.__compiled__[self.__schemas__[alias]].normalize;
  });
  self.__compiled__[''] = {
    validate: null,
    normalize: createNormalizer()
  };
  var slist = Object.keys(self.__compiled__).filter(function (name) {
    return name.length > 0 && self.__compiled__[name];
  }).map(escapeRE).join('|');
  self.re.schema_test = RegExp('(^|(?!_)(?:[><\uff5c]|' + re.src_ZPCc + '))(' + slist + ')', 'i');
  self.re.schema_search = RegExp('(^|(?!_)(?:[><\uff5c]|' + re.src_ZPCc + '))(' + slist + ')', 'ig');
  self.re.pretest = RegExp('(' + self.re.schema_test.source + ')|(' + self.re.host_fuzzy_test.source + ')|@', 'i');
  resetScanCache(self);
}
function Match(self, shift) {
  var start = self.__index__, end = self.__last_index__, text = self.__text_cache__.slice(start, end);
  this.schema = self.__schema__.toLowerCase();
  this.index = start + shift;
  this.lastIndex = end + shift;
  this.raw = text;
  this.text = text;
  this.url = text;
}
function createMatch(self, shift) {
  var match = new Match(self, shift);
  self.__compiled__[match.schema].normalize(match, self);
  return match;
}
function LinkifyIt(schemas, options) {
  if (!(this instanceof LinkifyIt)) {
    return new LinkifyIt(schemas, options);
  }
  if (!options) {
    if (isOptionsObj(schemas)) {
      options = schemas;
      schemas = {};
    }
  }
  this.__opts__ = assign({}, defaultOptions, options);
  this.__index__ = -1;
  this.__last_index__ = -1;
  this.__schema__ = '';
  this.__text_cache__ = '';
  this.__schemas__ = assign({}, defaultSchemas, schemas);
  this.__compiled__ = {};
  this.__tlds__ = tlds_default;
  this.__tlds_replaced__ = false;
  this.re = {};
  compile(this);
}
LinkifyIt.prototype.add = function add(schema, definition) {
  this.__schemas__[schema] = definition;
  compile(this);
  return this;
};
LinkifyIt.prototype.set = function set(options) {
  this.__opts__ = assign(this.__opts__, options);
  return this;
};
LinkifyIt.prototype.test = function test(text) {
  this.__text_cache__ = text;
  this.__index__ = -1;
  if (!text.length) {
    return false;
  }
  var m, ml, me, len, shift, next, re, tld_pos, at_pos;
  if (this.re.schema_test.test(text)) {
    re = this.re.schema_search;
    re.lastIndex = 0;
    while ((m = re.exec(text)) !== null) {
      len = this.testSchemaAt(text, m[2], re.lastIndex);
      if (len) {
        this.__schema__ = m[2];
        this.__index__ = m.index + m[1].length;
        this.__last_index__ = m.index + m[0].length + len;
        break;
      }
    }
  }
  if (this.__opts__.fuzzyLink && this.__compiled__['http:']) {
    tld_pos = text.search(this.re.host_fuzzy_test);
    if (tld_pos >= 0) {
      if (this.__index__ < 0 || tld_pos < this.__index__) {
        if ((ml = text.match(this.__opts__.fuzzyIP ? this.re.link_fuzzy : this.re.link_no_ip_fuzzy)) !== null) {
          shift = ml.index + ml[1].length;
          if (this.__index__ < 0 || shift < this.__index__) {
            this.__schema__ = '';
            this.__index__ = shift;
            this.__last_index__ = ml.index + ml[0].length;
          }
        }
      }
    }
  }
  if (this.__opts__.fuzzyEmail && this.__compiled__['mailto:']) {
    at_pos = text.indexOf('@');
    if (at_pos >= 0) {
      if ((me = text.match(this.re.email_fuzzy)) !== null) {
        shift = me.index + me[1].length;
        next = me.index + me[0].length;
        if (this.__index__ < 0 || shift < this.__index__ || shift === this.__index__ && next > this.__last_index__) {
          this.__schema__ = 'mailto:';
          this.__index__ = shift;
          this.__last_index__ = next;
        }
      }
    }
  }
  return this.__index__ >= 0;
};
LinkifyIt.prototype.pretest = function pretest(text) {
  return this.re.pretest.test(text);
};
LinkifyIt.prototype.testSchemaAt = function testSchemaAt(text, schema, pos) {
  if (!this.__compiled__[schema.toLowerCase()]) {
    return 0;
  }
  return this.__compiled__[schema.toLowerCase()].validate(text, pos, this);
};
LinkifyIt.prototype.match = function match(text) {
  var shift = 0, result = [];
  if (this.__index__ >= 0 && this.__text_cache__ === text) {
    result.push(createMatch(this, shift));
    shift = this.__last_index__;
  }
  var tail = shift ? text.slice(shift) : text;
  while (this.test(tail)) {
    result.push(createMatch(this, shift));
    tail = tail.slice(this.__last_index__);
    shift += this.__last_index__;
  }
  if (result.length) {
    return result;
  }
  return null;
};
LinkifyIt.prototype.tlds = function tlds(list, keepOld) {
  list = Array.isArray(list) ? list : [list];
  if (!keepOld) {
    this.__tlds__ = list.slice();
    this.__tlds_replaced__ = true;
    compile(this);
    return this;
  }
  this.__tlds__ = this.__tlds__.concat(list).sort().filter(function (el, idx, arr) {
    return el !== arr[idx - 1];
  }).reverse();
  compile(this);
  return this;
};
LinkifyIt.prototype.normalize = function normalize(match) {
  if (!match.schema) {
    match.url = 'http://' + match.url;
  }
  if (match.schema === 'mailto:' && !(/^mailto:/i).test(match.url)) {
    match.url = 'mailto:' + match.url;
  }
};
LinkifyIt.prototype.onCompile = function onCompile() {};
module.exports = LinkifyIt;

},

// node_modules/mdurl/index.js @82
82: function(__fusereq, exports, module){
'use strict';
module.exports.encode = __fusereq(124);
module.exports.decode = __fusereq(125);
module.exports.format = __fusereq(126);
module.exports.parse = __fusereq(127);

},

// node_modules/punycode/punycode.es6.js @83
83: function(__fusereq, exports, module){
exports.__esModule = true;
'use strict';
const maxInt = 2147483647;
const base = 36;
const tMin = 1;
const tMax = 26;
const skew = 38;
const damp = 700;
const initialBias = 72;
const initialN = 128;
const delimiter = '-';
const regexPunycode = /^xn--/;
const regexNonASCII = /[^\0-\x7E]/;
const regexSeparators = /[\x2E\u3002\uFF0E\uFF61]/g;
const errors = {
  'overflow': 'Overflow: input needs wider integers to process',
  'not-basic': 'Illegal input >= 0x80 (not a basic code point)',
  'invalid-input': 'Invalid input'
};
const baseMinusTMin = base - tMin;
const floor = Math.floor;
const stringFromCharCode = String.fromCharCode;
function error(type) {
  throw new RangeError(errors[type]);
}
function map(array, fn) {
  const result = [];
  let length = array.length;
  while (length--) {
    result[length] = fn(array[length]);
  }
  return result;
}
function mapDomain(string, fn) {
  const parts = string.split('@');
  let result = '';
  if (parts.length > 1) {
    result = parts[0] + '@';
    string = parts[1];
  }
  string = string.replace(regexSeparators, '\x2E');
  const labels = string.split('.');
  const encoded = map(labels, fn).join('.');
  return result + encoded;
}
function ucs2decode(string) {
  const output = [];
  let counter = 0;
  const length = string.length;
  while (counter < length) {
    const value = string.charCodeAt(counter++);
    if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
      const extra = string.charCodeAt(counter++);
      if ((extra & 0xFC00) == 0xDC00) {
        output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
      } else {
        output.push(value);
        counter--;
      }
    } else {
      output.push(value);
    }
  }
  return output;
}
const ucs2encode = array => String.fromCodePoint(...array);
const basicToDigit = function (codePoint) {
  if (codePoint - 0x30 < 0x0A) {
    return codePoint - 0x16;
  }
  if (codePoint - 0x41 < 0x1A) {
    return codePoint - 0x41;
  }
  if (codePoint - 0x61 < 0x1A) {
    return codePoint - 0x61;
  }
  return base;
};
const digitToBasic = function (digit, flag) {
  return digit + 22 + 75 * (digit < 26) - ((flag != 0) << 5);
};
const adapt = function (delta, numPoints, firstTime) {
  let k = 0;
  delta = firstTime ? floor(delta / damp) : delta >> 1;
  delta += floor(delta / numPoints);
  for (; delta > baseMinusTMin * tMax >> 1; k += base) {
    delta = floor(delta / baseMinusTMin);
  }
  return floor(k + (baseMinusTMin + 1) * delta / (delta + skew));
};
const decode = function (input) {
  const output = [];
  const inputLength = input.length;
  let i = 0;
  let n = initialN;
  let bias = initialBias;
  let basic = input.lastIndexOf(delimiter);
  if (basic < 0) {
    basic = 0;
  }
  for (let j = 0; j < basic; ++j) {
    if (input.charCodeAt(j) >= 0x80) {
      error('not-basic');
    }
    output.push(input.charCodeAt(j));
  }
  for (let index = basic > 0 ? basic + 1 : 0; index < inputLength; ) {
    let oldi = i;
    for (let w = 1, k = base; ; k += base) {
      if (index >= inputLength) {
        error('invalid-input');
      }
      const digit = basicToDigit(input.charCodeAt(index++));
      if (digit >= base || digit > floor((maxInt - i) / w)) {
        error('overflow');
      }
      i += digit * w;
      const t = k <= bias ? tMin : k >= bias + tMax ? tMax : k - bias;
      if (digit < t) {
        break;
      }
      const baseMinusT = base - t;
      if (w > floor(maxInt / baseMinusT)) {
        error('overflow');
      }
      w *= baseMinusT;
    }
    const out = output.length + 1;
    bias = adapt(i - oldi, out, oldi == 0);
    if (floor(i / out) > maxInt - n) {
      error('overflow');
    }
    n += floor(i / out);
    i %= out;
    output.splice(i++, 0, n);
  }
  return String.fromCodePoint(...output);
};
const encode = function (input) {
  const output = [];
  input = ucs2decode(input);
  let inputLength = input.length;
  let n = initialN;
  let delta = 0;
  let bias = initialBias;
  for (const currentValue of input) {
    if (currentValue < 0x80) {
      output.push(stringFromCharCode(currentValue));
    }
  }
  let basicLength = output.length;
  let handledCPCount = basicLength;
  if (basicLength) {
    output.push(delimiter);
  }
  while (handledCPCount < inputLength) {
    let m = maxInt;
    for (const currentValue of input) {
      if (currentValue >= n && currentValue < m) {
        m = currentValue;
      }
    }
    const handledCPCountPlusOne = handledCPCount + 1;
    if (m - n > floor((maxInt - delta) / handledCPCountPlusOne)) {
      error('overflow');
    }
    delta += (m - n) * handledCPCountPlusOne;
    n = m;
    for (const currentValue of input) {
      if (currentValue < n && ++delta > maxInt) {
        error('overflow');
      }
      if (currentValue == n) {
        let q = delta;
        for (let k = base; ; k += base) {
          const t = k <= bias ? tMin : k >= bias + tMax ? tMax : k - bias;
          if (q < t) {
            break;
          }
          const qMinusT = q - t;
          const baseMinusT = base - t;
          output.push(stringFromCharCode(digitToBasic(t + qMinusT % baseMinusT, 0)));
          q = floor(qMinusT / baseMinusT);
        }
        output.push(stringFromCharCode(digitToBasic(q, 0)));
        bias = adapt(delta, handledCPCountPlusOne, handledCPCount == basicLength);
        delta = 0;
        ++handledCPCount;
      }
    }
    ++delta;
    ++n;
  }
  return output.join('');
};
const toUnicode = function (input) {
  return mapDomain(input, function (string) {
    return regexPunycode.test(string) ? decode(string.slice(4).toLowerCase()) : string;
  });
};
const toASCII = function (input) {
  return mapDomain(input, function (string) {
    return regexNonASCII.test(string) ? 'xn--' + encode(string) : string;
  });
};
const punycode = {
  'version': '2.1.0',
  'ucs2': {
    'decode': ucs2decode,
    'encode': ucs2encode
  },
  'decode': decode,
  'encode': encode,
  'toASCII': toASCII,
  'toUnicode': toUnicode
};
exports.default = punycode;
exports.ucs2decode = ucs2decode;
exports.ucs2encode = ucs2encode;
exports.decode = decode;
exports.encode = encode;
exports.toASCII = toASCII;
exports.toUnicode = toUnicode;

},

// node_modules/markdown-it/lib/presets/default.js @84
84: function(__fusereq, exports, module){
'use strict';
module.exports = {
  options: {
    html: false,
    xhtmlOut: false,
    breaks: false,
    langPrefix: 'language-',
    linkify: false,
    typographer: false,
    quotes: '\u201c\u201d\u2018\u2019',
    highlight: null,
    maxNesting: 100
  },
  components: {
    core: {},
    block: {},
    inline: {}
  }
};

},

// node_modules/markdown-it/lib/presets/zero.js @85
85: function(__fusereq, exports, module){
'use strict';
module.exports = {
  options: {
    html: false,
    xhtmlOut: false,
    breaks: false,
    langPrefix: 'language-',
    linkify: false,
    typographer: false,
    quotes: '\u201c\u201d\u2018\u2019',
    highlight: null,
    maxNesting: 20
  },
  components: {
    core: {
      rules: ['normalize', 'block', 'inline']
    },
    block: {
      rules: ['paragraph']
    },
    inline: {
      rules: ['text'],
      rules2: ['balance_pairs', 'text_collapse']
    }
  }
};

},

// node_modules/markdown-it/lib/presets/commonmark.js @86
86: function(__fusereq, exports, module){
'use strict';
module.exports = {
  options: {
    html: true,
    xhtmlOut: true,
    breaks: false,
    langPrefix: 'language-',
    linkify: false,
    typographer: false,
    quotes: '\u201c\u201d\u2018\u2019',
    highlight: null,
    maxNesting: 20
  },
  components: {
    core: {
      rules: ['normalize', 'block', 'inline']
    },
    block: {
      rules: ['blockquote', 'code', 'fence', 'heading', 'hr', 'html_block', 'lheading', 'list', 'reference', 'paragraph']
    },
    inline: {
      rules: ['autolink', 'backticks', 'emphasis', 'entity', 'escape', 'html_inline', 'image', 'link', 'newline', 'text'],
      rules2: ['balance_pairs', 'emphasis', 'text_collapse']
    }
  }
};

},

// node_modules/markdown-it/lib/helpers/parse_link_label.js @87
87: function(__fusereq, exports, module){
'use strict';
module.exports = function parseLinkLabel(state, start, disableNested) {
  var level, found, marker, prevPos, labelEnd = -1, max = state.posMax, oldPos = state.pos;
  state.pos = start + 1;
  level = 1;
  while (state.pos < max) {
    marker = state.src.charCodeAt(state.pos);
    if (marker === 0x5D) {
      level--;
      if (level === 0) {
        found = true;
        break;
      }
    }
    prevPos = state.pos;
    state.md.inline.skipToken(state);
    if (marker === 0x5B) {
      if (prevPos === state.pos - 1) {
        level++;
      } else if (disableNested) {
        state.pos = oldPos;
        return -1;
      }
    }
  }
  if (found) {
    labelEnd = state.pos;
  }
  state.pos = oldPos;
  return labelEnd;
};

},

// node_modules/markdown-it/lib/helpers/parse_link_destination.js @88
88: function(__fusereq, exports, module){
'use strict';
var unescapeAll = __fusereq(75).unescapeAll;
module.exports = function parseLinkDestination(str, pos, max) {
  var code, level, lines = 0, start = pos, result = {
    ok: false,
    pos: 0,
    lines: 0,
    str: ''
  };
  if (str.charCodeAt(pos) === 0x3C) {
    pos++;
    while (pos < max) {
      code = str.charCodeAt(pos);
      if (code === 0x0A) {
        return result;
      }
      if (code === 0x3E) {
        result.pos = pos + 1;
        result.str = unescapeAll(str.slice(start + 1, pos));
        result.ok = true;
        return result;
      }
      if (code === 0x5C && pos + 1 < max) {
        pos += 2;
        continue;
      }
      pos++;
    }
    return result;
  }
  level = 0;
  while (pos < max) {
    code = str.charCodeAt(pos);
    if (code === 0x20) {
      break;
    }
    if (code < 0x20 || code === 0x7F) {
      break;
    }
    if (code === 0x5C && pos + 1 < max) {
      pos += 2;
      continue;
    }
    if (code === 0x28) {
      level++;
    }
    if (code === 0x29) {
      if (level === 0) {
        break;
      }
      level--;
    }
    pos++;
  }
  if (start === pos) {
    return result;
  }
  if (level !== 0) {
    return result;
  }
  result.str = unescapeAll(str.slice(start, pos));
  result.lines = lines;
  result.pos = pos;
  result.ok = true;
  return result;
};

},

// node_modules/markdown-it/lib/helpers/parse_link_title.js @89
89: function(__fusereq, exports, module){
'use strict';
var unescapeAll = __fusereq(75).unescapeAll;
module.exports = function parseLinkTitle(str, pos, max) {
  var code, marker, lines = 0, start = pos, result = {
    ok: false,
    pos: 0,
    lines: 0,
    str: ''
  };
  if (pos >= max) {
    return result;
  }
  marker = str.charCodeAt(pos);
  if (marker !== 0x22 && marker !== 0x27 && marker !== 0x28) {
    return result;
  }
  pos++;
  if (marker === 0x28) {
    marker = 0x29;
  }
  while (pos < max) {
    code = str.charCodeAt(pos);
    if (code === marker) {
      result.pos = pos + 1;
      result.lines = lines;
      result.str = unescapeAll(str.slice(start + 1, pos));
      result.ok = true;
      return result;
    } else if (code === 0x0A) {
      lines++;
    } else if (code === 0x5C && pos + 1 < max) {
      pos++;
      if (str.charCodeAt(pos) === 0x0A) {
        lines++;
      }
    }
    pos++;
  }
  return result;
};

},

// node_modules/markdown-it/lib/ruler.js @90
90: function(__fusereq, exports, module){
'use strict';
function Ruler() {
  this.__rules__ = [];
  this.__cache__ = null;
}
Ruler.prototype.__find__ = function (name) {
  for (var i = 0; i < this.__rules__.length; i++) {
    if (this.__rules__[i].name === name) {
      return i;
    }
  }
  return -1;
};
Ruler.prototype.__compile__ = function () {
  var self = this;
  var chains = [''];
  self.__rules__.forEach(function (rule) {
    if (!rule.enabled) {
      return;
    }
    rule.alt.forEach(function (altName) {
      if (chains.indexOf(altName) < 0) {
        chains.push(altName);
      }
    });
  });
  self.__cache__ = {};
  chains.forEach(function (chain) {
    self.__cache__[chain] = [];
    self.__rules__.forEach(function (rule) {
      if (!rule.enabled) {
        return;
      }
      if (chain && rule.alt.indexOf(chain) < 0) {
        return;
      }
      self.__cache__[chain].push(rule.fn);
    });
  });
};
Ruler.prototype.at = function (name, fn, options) {
  var index = this.__find__(name);
  var opt = options || ({});
  if (index === -1) {
    throw new Error('Parser rule not found: ' + name);
  }
  this.__rules__[index].fn = fn;
  this.__rules__[index].alt = opt.alt || [];
  this.__cache__ = null;
};
Ruler.prototype.before = function (beforeName, ruleName, fn, options) {
  var index = this.__find__(beforeName);
  var opt = options || ({});
  if (index === -1) {
    throw new Error('Parser rule not found: ' + beforeName);
  }
  this.__rules__.splice(index, 0, {
    name: ruleName,
    enabled: true,
    fn: fn,
    alt: opt.alt || []
  });
  this.__cache__ = null;
};
Ruler.prototype.after = function (afterName, ruleName, fn, options) {
  var index = this.__find__(afterName);
  var opt = options || ({});
  if (index === -1) {
    throw new Error('Parser rule not found: ' + afterName);
  }
  this.__rules__.splice(index + 1, 0, {
    name: ruleName,
    enabled: true,
    fn: fn,
    alt: opt.alt || []
  });
  this.__cache__ = null;
};
Ruler.prototype.push = function (ruleName, fn, options) {
  var opt = options || ({});
  this.__rules__.push({
    name: ruleName,
    enabled: true,
    fn: fn,
    alt: opt.alt || []
  });
  this.__cache__ = null;
};
Ruler.prototype.enable = function (list, ignoreInvalid) {
  if (!Array.isArray(list)) {
    list = [list];
  }
  var result = [];
  list.forEach(function (name) {
    var idx = this.__find__(name);
    if (idx < 0) {
      if (ignoreInvalid) {
        return;
      }
      throw new Error('Rules manager: invalid rule name ' + name);
    }
    this.__rules__[idx].enabled = true;
    result.push(name);
  }, this);
  this.__cache__ = null;
  return result;
};
Ruler.prototype.enableOnly = function (list, ignoreInvalid) {
  if (!Array.isArray(list)) {
    list = [list];
  }
  this.__rules__.forEach(function (rule) {
    rule.enabled = false;
  });
  this.enable(list, ignoreInvalid);
};
Ruler.prototype.disable = function (list, ignoreInvalid) {
  if (!Array.isArray(list)) {
    list = [list];
  }
  var result = [];
  list.forEach(function (name) {
    var idx = this.__find__(name);
    if (idx < 0) {
      if (ignoreInvalid) {
        return;
      }
      throw new Error('Rules manager: invalid rule name ' + name);
    }
    this.__rules__[idx].enabled = false;
    result.push(name);
  }, this);
  this.__cache__ = null;
  return result;
};
Ruler.prototype.getRules = function (chainName) {
  if (this.__cache__ === null) {
    this.__compile__();
  }
  return this.__cache__[chainName] || [];
};
module.exports = Ruler;

},

// node_modules/markdown-it/lib/rules_core/normalize.js @91
91: function(__fusereq, exports, module){
'use strict';
var NEWLINES_RE = /\r\n?|\n/g;
var NULL_RE = /\0/g;
module.exports = function normalize(state) {
  var str;
  str = state.src.replace(NEWLINES_RE, '\n');
  str = str.replace(NULL_RE, '\uFFFD');
  state.src = str;
};

},

// node_modules/markdown-it/lib/rules_core/block.js @92
92: function(__fusereq, exports, module){
'use strict';
module.exports = function block(state) {
  var token;
  if (state.inlineMode) {
    token = new state.Token('inline', '', 0);
    token.content = state.src;
    token.map = [0, 1];
    token.children = [];
    state.tokens.push(token);
  } else {
    state.md.block.parse(state.src, state.md, state.env, state.tokens);
  }
};

},

// node_modules/markdown-it/lib/rules_core/inline.js @93
93: function(__fusereq, exports, module){
'use strict';
module.exports = function inline(state) {
  var tokens = state.tokens, tok, i, l;
  for ((i = 0, l = tokens.length); i < l; i++) {
    tok = tokens[i];
    if (tok.type === 'inline') {
      state.md.inline.parse(tok.content, state.md, state.env, tok.children);
    }
  }
};

},

// node_modules/markdown-it/lib/rules_core/linkify.js @94
94: function(__fusereq, exports, module){
'use strict';
var arrayReplaceAt = __fusereq(75).arrayReplaceAt;
function isLinkOpen(str) {
  return (/^<a[>\s]/i).test(str);
}
function isLinkClose(str) {
  return (/^<\/a\s*>/i).test(str);
}
module.exports = function linkify(state) {
  var i, j, l, tokens, token, currentToken, nodes, ln, text, pos, lastPos, level, htmlLinkLevel, url, fullUrl, urlText, blockTokens = state.tokens, links;
  if (!state.md.options.linkify) {
    return;
  }
  for ((j = 0, l = blockTokens.length); j < l; j++) {
    if (blockTokens[j].type !== 'inline' || !state.md.linkify.pretest(blockTokens[j].content)) {
      continue;
    }
    tokens = blockTokens[j].children;
    htmlLinkLevel = 0;
    for (i = tokens.length - 1; i >= 0; i--) {
      currentToken = tokens[i];
      if (currentToken.type === 'link_close') {
        i--;
        while (tokens[i].level !== currentToken.level && tokens[i].type !== 'link_open') {
          i--;
        }
        continue;
      }
      if (currentToken.type === 'html_inline') {
        if (isLinkOpen(currentToken.content) && htmlLinkLevel > 0) {
          htmlLinkLevel--;
        }
        if (isLinkClose(currentToken.content)) {
          htmlLinkLevel++;
        }
      }
      if (htmlLinkLevel > 0) {
        continue;
      }
      if (currentToken.type === 'text' && state.md.linkify.test(currentToken.content)) {
        text = currentToken.content;
        links = state.md.linkify.match(text);
        nodes = [];
        level = currentToken.level;
        lastPos = 0;
        for (ln = 0; ln < links.length; ln++) {
          url = links[ln].url;
          fullUrl = state.md.normalizeLink(url);
          if (!state.md.validateLink(fullUrl)) {
            continue;
          }
          urlText = links[ln].text;
          if (!links[ln].schema) {
            urlText = state.md.normalizeLinkText('http://' + urlText).replace(/^http:\/\//, '');
          } else if (links[ln].schema === 'mailto:' && !(/^mailto:/i).test(urlText)) {
            urlText = state.md.normalizeLinkText('mailto:' + urlText).replace(/^mailto:/, '');
          } else {
            urlText = state.md.normalizeLinkText(urlText);
          }
          pos = links[ln].index;
          if (pos > lastPos) {
            token = new state.Token('text', '', 0);
            token.content = text.slice(lastPos, pos);
            token.level = level;
            nodes.push(token);
          }
          token = new state.Token('link_open', 'a', 1);
          token.attrs = [['href', fullUrl]];
          token.level = level++;
          token.markup = 'linkify';
          token.info = 'auto';
          nodes.push(token);
          token = new state.Token('text', '', 0);
          token.content = urlText;
          token.level = level;
          nodes.push(token);
          token = new state.Token('link_close', 'a', -1);
          token.level = --level;
          token.markup = 'linkify';
          token.info = 'auto';
          nodes.push(token);
          lastPos = links[ln].lastIndex;
        }
        if (lastPos < text.length) {
          token = new state.Token('text', '', 0);
          token.content = text.slice(lastPos);
          token.level = level;
          nodes.push(token);
        }
        blockTokens[j].children = tokens = arrayReplaceAt(tokens, i, nodes);
      }
    }
  }
};

},

// node_modules/markdown-it/lib/rules_core/replacements.js @95
95: function(__fusereq, exports, module){
'use strict';
var RARE_RE = /\+-|\.\.|\?\?\?\?|!!!!|,,|--/;
var SCOPED_ABBR_TEST_RE = /\((c|tm|r|p)\)/i;
var SCOPED_ABBR_RE = /\((c|tm|r|p)\)/ig;
var SCOPED_ABBR = {
  c: '©',
  r: '®',
  p: '§',
  tm: '™'
};
function replaceFn(match, name) {
  return SCOPED_ABBR[name.toLowerCase()];
}
function replace_scoped(inlineTokens) {
  var i, token, inside_autolink = 0;
  for (i = inlineTokens.length - 1; i >= 0; i--) {
    token = inlineTokens[i];
    if (token.type === 'text' && !inside_autolink) {
      token.content = token.content.replace(SCOPED_ABBR_RE, replaceFn);
    }
    if (token.type === 'link_open' && token.info === 'auto') {
      inside_autolink--;
    }
    if (token.type === 'link_close' && token.info === 'auto') {
      inside_autolink++;
    }
  }
}
function replace_rare(inlineTokens) {
  var i, token, inside_autolink = 0;
  for (i = inlineTokens.length - 1; i >= 0; i--) {
    token = inlineTokens[i];
    if (token.type === 'text' && !inside_autolink) {
      if (RARE_RE.test(token.content)) {
        token.content = token.content.replace(/\+-/g, '±').replace(/\.{2,}/g, '…').replace(/([?!])…/g, '$1..').replace(/([?!]){4,}/g, '$1$1$1').replace(/,{2,}/g, ',').replace(/(^|[^-])---([^-]|$)/mg, '$1\u2014$2').replace(/(^|\s)--(\s|$)/mg, '$1\u2013$2').replace(/(^|[^-\s])--([^-\s]|$)/mg, '$1\u2013$2');
      }
    }
    if (token.type === 'link_open' && token.info === 'auto') {
      inside_autolink--;
    }
    if (token.type === 'link_close' && token.info === 'auto') {
      inside_autolink++;
    }
  }
}
module.exports = function replace(state) {
  var blkIdx;
  if (!state.md.options.typographer) {
    return;
  }
  for (blkIdx = state.tokens.length - 1; blkIdx >= 0; blkIdx--) {
    if (state.tokens[blkIdx].type !== 'inline') {
      continue;
    }
    if (SCOPED_ABBR_TEST_RE.test(state.tokens[blkIdx].content)) {
      replace_scoped(state.tokens[blkIdx].children);
    }
    if (RARE_RE.test(state.tokens[blkIdx].content)) {
      replace_rare(state.tokens[blkIdx].children);
    }
  }
};

},

// node_modules/markdown-it/lib/rules_core/smartquotes.js @96
96: function(__fusereq, exports, module){
'use strict';
var isWhiteSpace = __fusereq(75).isWhiteSpace;
var isPunctChar = __fusereq(75).isPunctChar;
var isMdAsciiPunct = __fusereq(75).isMdAsciiPunct;
var QUOTE_TEST_RE = /['"]/;
var QUOTE_RE = /['"]/g;
var APOSTROPHE = '\u2019';
function replaceAt(str, index, ch) {
  return str.substr(0, index) + ch + str.substr(index + 1);
}
function process_inlines(tokens, state) {
  var i, token, text, t, pos, max, thisLevel, item, lastChar, nextChar, isLastPunctChar, isNextPunctChar, isLastWhiteSpace, isNextWhiteSpace, canOpen, canClose, j, isSingle, stack, openQuote, closeQuote;
  stack = [];
  for (i = 0; i < tokens.length; i++) {
    token = tokens[i];
    thisLevel = tokens[i].level;
    for (j = stack.length - 1; j >= 0; j--) {
      if (stack[j].level <= thisLevel) {
        break;
      }
    }
    stack.length = j + 1;
    if (token.type !== 'text') {
      continue;
    }
    text = token.content;
    pos = 0;
    max = text.length;
    OUTER: while (pos < max) {
      QUOTE_RE.lastIndex = pos;
      t = QUOTE_RE.exec(text);
      if (!t) {
        break;
      }
      canOpen = canClose = true;
      pos = t.index + 1;
      isSingle = t[0] === "'";
      lastChar = 0x20;
      if (t.index - 1 >= 0) {
        lastChar = text.charCodeAt(t.index - 1);
      } else {
        for (j = i - 1; j >= 0; j--) {
          if (tokens[j].type === 'softbreak' || tokens[j].type === 'hardbreak') break;
          if (tokens[j].type !== 'text') continue;
          lastChar = tokens[j].content.charCodeAt(tokens[j].content.length - 1);
          break;
        }
      }
      nextChar = 0x20;
      if (pos < max) {
        nextChar = text.charCodeAt(pos);
      } else {
        for (j = i + 1; j < tokens.length; j++) {
          if (tokens[j].type === 'softbreak' || tokens[j].type === 'hardbreak') break;
          if (tokens[j].type !== 'text') continue;
          nextChar = tokens[j].content.charCodeAt(0);
          break;
        }
      }
      isLastPunctChar = isMdAsciiPunct(lastChar) || isPunctChar(String.fromCharCode(lastChar));
      isNextPunctChar = isMdAsciiPunct(nextChar) || isPunctChar(String.fromCharCode(nextChar));
      isLastWhiteSpace = isWhiteSpace(lastChar);
      isNextWhiteSpace = isWhiteSpace(nextChar);
      if (isNextWhiteSpace) {
        canOpen = false;
      } else if (isNextPunctChar) {
        if (!(isLastWhiteSpace || isLastPunctChar)) {
          canOpen = false;
        }
      }
      if (isLastWhiteSpace) {
        canClose = false;
      } else if (isLastPunctChar) {
        if (!(isNextWhiteSpace || isNextPunctChar)) {
          canClose = false;
        }
      }
      if (nextChar === 0x22 && t[0] === '"') {
        if (lastChar >= 0x30 && lastChar <= 0x39) {
          canClose = canOpen = false;
        }
      }
      if (canOpen && canClose) {
        canOpen = false;
        canClose = isNextPunctChar;
      }
      if (!canOpen && !canClose) {
        if (isSingle) {
          token.content = replaceAt(token.content, t.index, APOSTROPHE);
        }
        continue;
      }
      if (canClose) {
        for (j = stack.length - 1; j >= 0; j--) {
          item = stack[j];
          if (stack[j].level < thisLevel) {
            break;
          }
          if (item.single === isSingle && stack[j].level === thisLevel) {
            item = stack[j];
            if (isSingle) {
              openQuote = state.md.options.quotes[2];
              closeQuote = state.md.options.quotes[3];
            } else {
              openQuote = state.md.options.quotes[0];
              closeQuote = state.md.options.quotes[1];
            }
            token.content = replaceAt(token.content, t.index, closeQuote);
            tokens[item.token].content = replaceAt(tokens[item.token].content, item.pos, openQuote);
            pos += closeQuote.length - 1;
            if (item.token === i) {
              pos += openQuote.length - 1;
            }
            text = token.content;
            max = text.length;
            stack.length = j;
            continue OUTER;
          }
        }
      }
      if (canOpen) {
        stack.push({
          token: i,
          pos: t.index,
          single: isSingle,
          level: thisLevel
        });
      } else if (canClose && isSingle) {
        token.content = replaceAt(token.content, t.index, APOSTROPHE);
      }
    }
  }
}
module.exports = function smartquotes(state) {
  var blkIdx;
  if (!state.md.options.typographer) {
    return;
  }
  for (blkIdx = state.tokens.length - 1; blkIdx >= 0; blkIdx--) {
    if (state.tokens[blkIdx].type !== 'inline' || !QUOTE_TEST_RE.test(state.tokens[blkIdx].content)) {
      continue;
    }
    process_inlines(state.tokens[blkIdx].children, state);
  }
};

},

// node_modules/markdown-it/lib/rules_core/state_core.js @97
97: function(__fusereq, exports, module){
'use strict';
var Token = __fusereq(132);
function StateCore(src, md, env) {
  this.src = src;
  this.env = env;
  this.tokens = [];
  this.inlineMode = false;
  this.md = md;
}
StateCore.prototype.Token = Token;
module.exports = StateCore;

},

// node_modules/markdown-it/lib/rules_block/table.js @98
98: function(__fusereq, exports, module){
'use strict';
var isSpace = __fusereq(75).isSpace;
function getLine(state, line) {
  var pos = state.bMarks[line] + state.blkIndent, max = state.eMarks[line];
  return state.src.substr(pos, max - pos);
}
function escapedSplit(str) {
  var result = [], pos = 0, max = str.length, ch, escapes = 0, lastPos = 0, backTicked = false, lastBackTick = 0;
  ch = str.charCodeAt(pos);
  while (pos < max) {
    if (ch === 0x60) {
      if (backTicked) {
        backTicked = false;
        lastBackTick = pos;
      } else if (escapes % 2 === 0) {
        backTicked = true;
        lastBackTick = pos;
      }
    } else if (ch === 0x7c && escapes % 2 === 0 && !backTicked) {
      result.push(str.substring(lastPos, pos));
      lastPos = pos + 1;
    }
    if (ch === 0x5c) {
      escapes++;
    } else {
      escapes = 0;
    }
    pos++;
    if (pos === max && backTicked) {
      backTicked = false;
      pos = lastBackTick + 1;
    }
    ch = str.charCodeAt(pos);
  }
  result.push(str.substring(lastPos));
  return result;
}
module.exports = function table(state, startLine, endLine, silent) {
  var ch, lineText, pos, i, nextLine, columns, columnCount, token, aligns, t, tableLines, tbodyLines;
  if (startLine + 2 > endLine) {
    return false;
  }
  nextLine = startLine + 1;
  if (state.sCount[nextLine] < state.blkIndent) {
    return false;
  }
  if (state.sCount[nextLine] - state.blkIndent >= 4) {
    return false;
  }
  pos = state.bMarks[nextLine] + state.tShift[nextLine];
  if (pos >= state.eMarks[nextLine]) {
    return false;
  }
  ch = state.src.charCodeAt(pos++);
  if (ch !== 0x7C && ch !== 0x2D && ch !== 0x3A) {
    return false;
  }
  while (pos < state.eMarks[nextLine]) {
    ch = state.src.charCodeAt(pos);
    if (ch !== 0x7C && ch !== 0x2D && ch !== 0x3A && !isSpace(ch)) {
      return false;
    }
    pos++;
  }
  lineText = getLine(state, startLine + 1);
  columns = lineText.split('|');
  aligns = [];
  for (i = 0; i < columns.length; i++) {
    t = columns[i].trim();
    if (!t) {
      if (i === 0 || i === columns.length - 1) {
        continue;
      } else {
        return false;
      }
    }
    if (!(/^:?-+:?$/).test(t)) {
      return false;
    }
    if (t.charCodeAt(t.length - 1) === 0x3A) {
      aligns.push(t.charCodeAt(0) === 0x3A ? 'center' : 'right');
    } else if (t.charCodeAt(0) === 0x3A) {
      aligns.push('left');
    } else {
      aligns.push('');
    }
  }
  lineText = getLine(state, startLine).trim();
  if (lineText.indexOf('|') === -1) {
    return false;
  }
  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }
  columns = escapedSplit(lineText.replace(/^\||\|$/g, ''));
  columnCount = columns.length;
  if (columnCount > aligns.length) {
    return false;
  }
  if (silent) {
    return true;
  }
  token = state.push('table_open', 'table', 1);
  token.map = tableLines = [startLine, 0];
  token = state.push('thead_open', 'thead', 1);
  token.map = [startLine, startLine + 1];
  token = state.push('tr_open', 'tr', 1);
  token.map = [startLine, startLine + 1];
  for (i = 0; i < columns.length; i++) {
    token = state.push('th_open', 'th', 1);
    token.map = [startLine, startLine + 1];
    if (aligns[i]) {
      token.attrs = [['style', 'text-align:' + aligns[i]]];
    }
    token = state.push('inline', '', 0);
    token.content = columns[i].trim();
    token.map = [startLine, startLine + 1];
    token.children = [];
    token = state.push('th_close', 'th', -1);
  }
  token = state.push('tr_close', 'tr', -1);
  token = state.push('thead_close', 'thead', -1);
  token = state.push('tbody_open', 'tbody', 1);
  token.map = tbodyLines = [startLine + 2, 0];
  for (nextLine = startLine + 2; nextLine < endLine; nextLine++) {
    if (state.sCount[nextLine] < state.blkIndent) {
      break;
    }
    lineText = getLine(state, nextLine).trim();
    if (lineText.indexOf('|') === -1) {
      break;
    }
    if (state.sCount[nextLine] - state.blkIndent >= 4) {
      break;
    }
    columns = escapedSplit(lineText.replace(/^\||\|$/g, ''));
    token = state.push('tr_open', 'tr', 1);
    for (i = 0; i < columnCount; i++) {
      token = state.push('td_open', 'td', 1);
      if (aligns[i]) {
        token.attrs = [['style', 'text-align:' + aligns[i]]];
      }
      token = state.push('inline', '', 0);
      token.content = columns[i] ? columns[i].trim() : '';
      token.children = [];
      token = state.push('td_close', 'td', -1);
    }
    token = state.push('tr_close', 'tr', -1);
  }
  token = state.push('tbody_close', 'tbody', -1);
  token = state.push('table_close', 'table', -1);
  tableLines[1] = tbodyLines[1] = nextLine;
  state.line = nextLine;
  return true;
};

},

// node_modules/markdown-it/lib/rules_block/code.js @99
99: function(__fusereq, exports, module){
'use strict';
module.exports = function code(state, startLine, endLine) {
  var nextLine, last, token;
  if (state.sCount[startLine] - state.blkIndent < 4) {
    return false;
  }
  last = nextLine = startLine + 1;
  while (nextLine < endLine) {
    if (state.isEmpty(nextLine)) {
      nextLine++;
      continue;
    }
    if (state.sCount[nextLine] - state.blkIndent >= 4) {
      nextLine++;
      last = nextLine;
      continue;
    }
    break;
  }
  state.line = last;
  token = state.push('code_block', 'code', 0);
  token.content = state.getLines(startLine, last, 4 + state.blkIndent, true);
  token.map = [startLine, state.line];
  return true;
};

},

// node_modules/markdown-it/lib/rules_block/fence.js @100
100: function(__fusereq, exports, module){
'use strict';
module.exports = function fence(state, startLine, endLine, silent) {
  var marker, len, params, nextLine, mem, token, markup, haveEndMarker = false, pos = state.bMarks[startLine] + state.tShift[startLine], max = state.eMarks[startLine];
  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }
  if (pos + 3 > max) {
    return false;
  }
  marker = state.src.charCodeAt(pos);
  if (marker !== 0x7E && marker !== 0x60) {
    return false;
  }
  mem = pos;
  pos = state.skipChars(pos, marker);
  len = pos - mem;
  if (len < 3) {
    return false;
  }
  markup = state.src.slice(mem, pos);
  params = state.src.slice(pos, max);
  if (marker === 0x60) {
    if (params.indexOf(String.fromCharCode(marker)) >= 0) {
      return false;
    }
  }
  if (silent) {
    return true;
  }
  nextLine = startLine;
  for (; ; ) {
    nextLine++;
    if (nextLine >= endLine) {
      break;
    }
    pos = mem = state.bMarks[nextLine] + state.tShift[nextLine];
    max = state.eMarks[nextLine];
    if (pos < max && state.sCount[nextLine] < state.blkIndent) {
      break;
    }
    if (state.src.charCodeAt(pos) !== marker) {
      continue;
    }
    if (state.sCount[nextLine] - state.blkIndent >= 4) {
      continue;
    }
    pos = state.skipChars(pos, marker);
    if (pos - mem < len) {
      continue;
    }
    pos = state.skipSpaces(pos);
    if (pos < max) {
      continue;
    }
    haveEndMarker = true;
    break;
  }
  len = state.sCount[startLine];
  state.line = nextLine + (haveEndMarker ? 1 : 0);
  token = state.push('fence', 'code', 0);
  token.info = params;
  token.content = state.getLines(startLine + 1, nextLine, len, true);
  token.markup = markup;
  token.map = [startLine, state.line];
  return true;
};

},

// node_modules/markdown-it/lib/rules_block/blockquote.js @101
101: function(__fusereq, exports, module){
'use strict';
var isSpace = __fusereq(75).isSpace;
module.exports = function blockquote(state, startLine, endLine, silent) {
  var adjustTab, ch, i, initial, l, lastLineEmpty, lines, nextLine, offset, oldBMarks, oldBSCount, oldIndent, oldParentType, oldSCount, oldTShift, spaceAfterMarker, terminate, terminatorRules, token, wasOutdented, oldLineMax = state.lineMax, pos = state.bMarks[startLine] + state.tShift[startLine], max = state.eMarks[startLine];
  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }
  if (state.src.charCodeAt(pos++) !== 0x3E) {
    return false;
  }
  if (silent) {
    return true;
  }
  initial = offset = state.sCount[startLine] + pos - (state.bMarks[startLine] + state.tShift[startLine]);
  if (state.src.charCodeAt(pos) === 0x20) {
    pos++;
    initial++;
    offset++;
    adjustTab = false;
    spaceAfterMarker = true;
  } else if (state.src.charCodeAt(pos) === 0x09) {
    spaceAfterMarker = true;
    if ((state.bsCount[startLine] + offset) % 4 === 3) {
      pos++;
      initial++;
      offset++;
      adjustTab = false;
    } else {
      adjustTab = true;
    }
  } else {
    spaceAfterMarker = false;
  }
  oldBMarks = [state.bMarks[startLine]];
  state.bMarks[startLine] = pos;
  while (pos < max) {
    ch = state.src.charCodeAt(pos);
    if (isSpace(ch)) {
      if (ch === 0x09) {
        offset += 4 - (offset + state.bsCount[startLine] + (adjustTab ? 1 : 0)) % 4;
      } else {
        offset++;
      }
    } else {
      break;
    }
    pos++;
  }
  oldBSCount = [state.bsCount[startLine]];
  state.bsCount[startLine] = state.sCount[startLine] + 1 + (spaceAfterMarker ? 1 : 0);
  lastLineEmpty = pos >= max;
  oldSCount = [state.sCount[startLine]];
  state.sCount[startLine] = offset - initial;
  oldTShift = [state.tShift[startLine]];
  state.tShift[startLine] = pos - state.bMarks[startLine];
  terminatorRules = state.md.block.ruler.getRules('blockquote');
  oldParentType = state.parentType;
  state.parentType = 'blockquote';
  wasOutdented = false;
  for (nextLine = startLine + 1; nextLine < endLine; nextLine++) {
    if (state.sCount[nextLine] < state.blkIndent) wasOutdented = true;
    pos = state.bMarks[nextLine] + state.tShift[nextLine];
    max = state.eMarks[nextLine];
    if (pos >= max) {
      break;
    }
    if (state.src.charCodeAt(pos++) === 0x3E && !wasOutdented) {
      initial = offset = state.sCount[nextLine] + pos - (state.bMarks[nextLine] + state.tShift[nextLine]);
      if (state.src.charCodeAt(pos) === 0x20) {
        pos++;
        initial++;
        offset++;
        adjustTab = false;
        spaceAfterMarker = true;
      } else if (state.src.charCodeAt(pos) === 0x09) {
        spaceAfterMarker = true;
        if ((state.bsCount[nextLine] + offset) % 4 === 3) {
          pos++;
          initial++;
          offset++;
          adjustTab = false;
        } else {
          adjustTab = true;
        }
      } else {
        spaceAfterMarker = false;
      }
      oldBMarks.push(state.bMarks[nextLine]);
      state.bMarks[nextLine] = pos;
      while (pos < max) {
        ch = state.src.charCodeAt(pos);
        if (isSpace(ch)) {
          if (ch === 0x09) {
            offset += 4 - (offset + state.bsCount[nextLine] + (adjustTab ? 1 : 0)) % 4;
          } else {
            offset++;
          }
        } else {
          break;
        }
        pos++;
      }
      lastLineEmpty = pos >= max;
      oldBSCount.push(state.bsCount[nextLine]);
      state.bsCount[nextLine] = state.sCount[nextLine] + 1 + (spaceAfterMarker ? 1 : 0);
      oldSCount.push(state.sCount[nextLine]);
      state.sCount[nextLine] = offset - initial;
      oldTShift.push(state.tShift[nextLine]);
      state.tShift[nextLine] = pos - state.bMarks[nextLine];
      continue;
    }
    if (lastLineEmpty) {
      break;
    }
    terminate = false;
    for ((i = 0, l = terminatorRules.length); i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }
    if (terminate) {
      state.lineMax = nextLine;
      if (state.blkIndent !== 0) {
        oldBMarks.push(state.bMarks[nextLine]);
        oldBSCount.push(state.bsCount[nextLine]);
        oldTShift.push(state.tShift[nextLine]);
        oldSCount.push(state.sCount[nextLine]);
        state.sCount[nextLine] -= state.blkIndent;
      }
      break;
    }
    oldBMarks.push(state.bMarks[nextLine]);
    oldBSCount.push(state.bsCount[nextLine]);
    oldTShift.push(state.tShift[nextLine]);
    oldSCount.push(state.sCount[nextLine]);
    state.sCount[nextLine] = -1;
  }
  oldIndent = state.blkIndent;
  state.blkIndent = 0;
  token = state.push('blockquote_open', 'blockquote', 1);
  token.markup = '>';
  token.map = lines = [startLine, 0];
  state.md.block.tokenize(state, startLine, nextLine);
  token = state.push('blockquote_close', 'blockquote', -1);
  token.markup = '>';
  state.lineMax = oldLineMax;
  state.parentType = oldParentType;
  lines[1] = state.line;
  for (i = 0; i < oldTShift.length; i++) {
    state.bMarks[i + startLine] = oldBMarks[i];
    state.tShift[i + startLine] = oldTShift[i];
    state.sCount[i + startLine] = oldSCount[i];
    state.bsCount[i + startLine] = oldBSCount[i];
  }
  state.blkIndent = oldIndent;
  return true;
};

},

// node_modules/markdown-it/lib/rules_block/hr.js @102
102: function(__fusereq, exports, module){
'use strict';
var isSpace = __fusereq(75).isSpace;
module.exports = function hr(state, startLine, endLine, silent) {
  var marker, cnt, ch, token, pos = state.bMarks[startLine] + state.tShift[startLine], max = state.eMarks[startLine];
  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }
  marker = state.src.charCodeAt(pos++);
  if (marker !== 0x2A && marker !== 0x2D && marker !== 0x5F) {
    return false;
  }
  cnt = 1;
  while (pos < max) {
    ch = state.src.charCodeAt(pos++);
    if (ch !== marker && !isSpace(ch)) {
      return false;
    }
    if (ch === marker) {
      cnt++;
    }
  }
  if (cnt < 3) {
    return false;
  }
  if (silent) {
    return true;
  }
  state.line = startLine + 1;
  token = state.push('hr', 'hr', 0);
  token.map = [startLine, state.line];
  token.markup = Array(cnt + 1).join(String.fromCharCode(marker));
  return true;
};

},

// node_modules/markdown-it/lib/rules_block/list.js @103
103: function(__fusereq, exports, module){
'use strict';
var isSpace = __fusereq(75).isSpace;
function skipBulletListMarker(state, startLine) {
  var marker, pos, max, ch;
  pos = state.bMarks[startLine] + state.tShift[startLine];
  max = state.eMarks[startLine];
  marker = state.src.charCodeAt(pos++);
  if (marker !== 0x2A && marker !== 0x2D && marker !== 0x2B) {
    return -1;
  }
  if (pos < max) {
    ch = state.src.charCodeAt(pos);
    if (!isSpace(ch)) {
      return -1;
    }
  }
  return pos;
}
function skipOrderedListMarker(state, startLine) {
  var ch, start = state.bMarks[startLine] + state.tShift[startLine], pos = start, max = state.eMarks[startLine];
  if (pos + 1 >= max) {
    return -1;
  }
  ch = state.src.charCodeAt(pos++);
  if (ch < 0x30 || ch > 0x39) {
    return -1;
  }
  for (; ; ) {
    if (pos >= max) {
      return -1;
    }
    ch = state.src.charCodeAt(pos++);
    if (ch >= 0x30 && ch <= 0x39) {
      if (pos - start >= 10) {
        return -1;
      }
      continue;
    }
    if (ch === 0x29 || ch === 0x2e) {
      break;
    }
    return -1;
  }
  if (pos < max) {
    ch = state.src.charCodeAt(pos);
    if (!isSpace(ch)) {
      return -1;
    }
  }
  return pos;
}
function markTightParagraphs(state, idx) {
  var i, l, level = state.level + 2;
  for ((i = idx + 2, l = state.tokens.length - 2); i < l; i++) {
    if (state.tokens[i].level === level && state.tokens[i].type === 'paragraph_open') {
      state.tokens[i + 2].hidden = true;
      state.tokens[i].hidden = true;
      i += 2;
    }
  }
}
module.exports = function list(state, startLine, endLine, silent) {
  var ch, contentStart, i, indent, indentAfterMarker, initial, isOrdered, itemLines, l, listLines, listTokIdx, markerCharCode, markerValue, max, nextLine, offset, oldListIndent, oldParentType, oldSCount, oldTShift, oldTight, pos, posAfterMarker, prevEmptyEnd, start, terminate, terminatorRules, token, isTerminatingParagraph = false, tight = true;
  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }
  if (state.listIndent >= 0 && state.sCount[startLine] - state.listIndent >= 4 && state.sCount[startLine] < state.blkIndent) {
    return false;
  }
  if (silent && state.parentType === 'paragraph') {
    if (state.tShift[startLine] >= state.blkIndent) {
      isTerminatingParagraph = true;
    }
  }
  if ((posAfterMarker = skipOrderedListMarker(state, startLine)) >= 0) {
    isOrdered = true;
    start = state.bMarks[startLine] + state.tShift[startLine];
    markerValue = Number(state.src.substr(start, posAfterMarker - start - 1));
    if (isTerminatingParagraph && markerValue !== 1) return false;
  } else if ((posAfterMarker = skipBulletListMarker(state, startLine)) >= 0) {
    isOrdered = false;
  } else {
    return false;
  }
  if (isTerminatingParagraph) {
    if (state.skipSpaces(posAfterMarker) >= state.eMarks[startLine]) return false;
  }
  markerCharCode = state.src.charCodeAt(posAfterMarker - 1);
  if (silent) {
    return true;
  }
  listTokIdx = state.tokens.length;
  if (isOrdered) {
    token = state.push('ordered_list_open', 'ol', 1);
    if (markerValue !== 1) {
      token.attrs = [['start', markerValue]];
    }
  } else {
    token = state.push('bullet_list_open', 'ul', 1);
  }
  token.map = listLines = [startLine, 0];
  token.markup = String.fromCharCode(markerCharCode);
  nextLine = startLine;
  prevEmptyEnd = false;
  terminatorRules = state.md.block.ruler.getRules('list');
  oldParentType = state.parentType;
  state.parentType = 'list';
  while (nextLine < endLine) {
    pos = posAfterMarker;
    max = state.eMarks[nextLine];
    initial = offset = state.sCount[nextLine] + posAfterMarker - (state.bMarks[startLine] + state.tShift[startLine]);
    while (pos < max) {
      ch = state.src.charCodeAt(pos);
      if (ch === 0x09) {
        offset += 4 - (offset + state.bsCount[nextLine]) % 4;
      } else if (ch === 0x20) {
        offset++;
      } else {
        break;
      }
      pos++;
    }
    contentStart = pos;
    if (contentStart >= max) {
      indentAfterMarker = 1;
    } else {
      indentAfterMarker = offset - initial;
    }
    if (indentAfterMarker > 4) {
      indentAfterMarker = 1;
    }
    indent = initial + indentAfterMarker;
    token = state.push('list_item_open', 'li', 1);
    token.markup = String.fromCharCode(markerCharCode);
    token.map = itemLines = [startLine, 0];
    oldTight = state.tight;
    oldTShift = state.tShift[startLine];
    oldSCount = state.sCount[startLine];
    oldListIndent = state.listIndent;
    state.listIndent = state.blkIndent;
    state.blkIndent = indent;
    state.tight = true;
    state.tShift[startLine] = contentStart - state.bMarks[startLine];
    state.sCount[startLine] = offset;
    if (contentStart >= max && state.isEmpty(startLine + 1)) {
      state.line = Math.min(state.line + 2, endLine);
    } else {
      state.md.block.tokenize(state, startLine, endLine, true);
    }
    if (!state.tight || prevEmptyEnd) {
      tight = false;
    }
    prevEmptyEnd = state.line - startLine > 1 && state.isEmpty(state.line - 1);
    state.blkIndent = state.listIndent;
    state.listIndent = oldListIndent;
    state.tShift[startLine] = oldTShift;
    state.sCount[startLine] = oldSCount;
    state.tight = oldTight;
    token = state.push('list_item_close', 'li', -1);
    token.markup = String.fromCharCode(markerCharCode);
    nextLine = startLine = state.line;
    itemLines[1] = nextLine;
    contentStart = state.bMarks[startLine];
    if (nextLine >= endLine) {
      break;
    }
    if (state.sCount[nextLine] < state.blkIndent) {
      break;
    }
    if (state.sCount[startLine] - state.blkIndent >= 4) {
      break;
    }
    terminate = false;
    for ((i = 0, l = terminatorRules.length); i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }
    if (terminate) {
      break;
    }
    if (isOrdered) {
      posAfterMarker = skipOrderedListMarker(state, nextLine);
      if (posAfterMarker < 0) {
        break;
      }
    } else {
      posAfterMarker = skipBulletListMarker(state, nextLine);
      if (posAfterMarker < 0) {
        break;
      }
    }
    if (markerCharCode !== state.src.charCodeAt(posAfterMarker - 1)) {
      break;
    }
  }
  if (isOrdered) {
    token = state.push('ordered_list_close', 'ol', -1);
  } else {
    token = state.push('bullet_list_close', 'ul', -1);
  }
  token.markup = String.fromCharCode(markerCharCode);
  listLines[1] = nextLine;
  state.line = nextLine;
  state.parentType = oldParentType;
  if (tight) {
    markTightParagraphs(state, listTokIdx);
  }
  return true;
};

},

// node_modules/markdown-it/lib/rules_block/reference.js @104
104: function(__fusereq, exports, module){
'use strict';
var normalizeReference = __fusereq(75).normalizeReference;
var isSpace = __fusereq(75).isSpace;
module.exports = function reference(state, startLine, _endLine, silent) {
  var ch, destEndPos, destEndLineNo, endLine, href, i, l, label, labelEnd, oldParentType, res, start, str, terminate, terminatorRules, title, lines = 0, pos = state.bMarks[startLine] + state.tShift[startLine], max = state.eMarks[startLine], nextLine = startLine + 1;
  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }
  if (state.src.charCodeAt(pos) !== 0x5B) {
    return false;
  }
  while (++pos < max) {
    if (state.src.charCodeAt(pos) === 0x5D && state.src.charCodeAt(pos - 1) !== 0x5C) {
      if (pos + 1 === max) {
        return false;
      }
      if (state.src.charCodeAt(pos + 1) !== 0x3A) {
        return false;
      }
      break;
    }
  }
  endLine = state.lineMax;
  terminatorRules = state.md.block.ruler.getRules('reference');
  oldParentType = state.parentType;
  state.parentType = 'reference';
  for (; nextLine < endLine && !state.isEmpty(nextLine); nextLine++) {
    if (state.sCount[nextLine] - state.blkIndent > 3) {
      continue;
    }
    if (state.sCount[nextLine] < 0) {
      continue;
    }
    terminate = false;
    for ((i = 0, l = terminatorRules.length); i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }
    if (terminate) {
      break;
    }
  }
  str = state.getLines(startLine, nextLine, state.blkIndent, false).trim();
  max = str.length;
  for (pos = 1; pos < max; pos++) {
    ch = str.charCodeAt(pos);
    if (ch === 0x5B) {
      return false;
    } else if (ch === 0x5D) {
      labelEnd = pos;
      break;
    } else if (ch === 0x0A) {
      lines++;
    } else if (ch === 0x5C) {
      pos++;
      if (pos < max && str.charCodeAt(pos) === 0x0A) {
        lines++;
      }
    }
  }
  if (labelEnd < 0 || str.charCodeAt(labelEnd + 1) !== 0x3A) {
    return false;
  }
  for (pos = labelEnd + 2; pos < max; pos++) {
    ch = str.charCodeAt(pos);
    if (ch === 0x0A) {
      lines++;
    } else if (isSpace(ch)) {} else {
      break;
    }
  }
  res = state.md.helpers.parseLinkDestination(str, pos, max);
  if (!res.ok) {
    return false;
  }
  href = state.md.normalizeLink(res.str);
  if (!state.md.validateLink(href)) {
    return false;
  }
  pos = res.pos;
  lines += res.lines;
  destEndPos = pos;
  destEndLineNo = lines;
  start = pos;
  for (; pos < max; pos++) {
    ch = str.charCodeAt(pos);
    if (ch === 0x0A) {
      lines++;
    } else if (isSpace(ch)) {} else {
      break;
    }
  }
  res = state.md.helpers.parseLinkTitle(str, pos, max);
  if (pos < max && start !== pos && res.ok) {
    title = res.str;
    pos = res.pos;
    lines += res.lines;
  } else {
    title = '';
    pos = destEndPos;
    lines = destEndLineNo;
  }
  while (pos < max) {
    ch = str.charCodeAt(pos);
    if (!isSpace(ch)) {
      break;
    }
    pos++;
  }
  if (pos < max && str.charCodeAt(pos) !== 0x0A) {
    if (title) {
      title = '';
      pos = destEndPos;
      lines = destEndLineNo;
      while (pos < max) {
        ch = str.charCodeAt(pos);
        if (!isSpace(ch)) {
          break;
        }
        pos++;
      }
    }
  }
  if (pos < max && str.charCodeAt(pos) !== 0x0A) {
    return false;
  }
  label = normalizeReference(str.slice(1, labelEnd));
  if (!label) {
    return false;
  }
  if (silent) {
    return true;
  }
  if (typeof state.env.references === 'undefined') {
    state.env.references = {};
  }
  if (typeof state.env.references[label] === 'undefined') {
    state.env.references[label] = {
      title: title,
      href: href
    };
  }
  state.parentType = oldParentType;
  state.line = startLine + lines + 1;
  return true;
};

},

// node_modules/markdown-it/lib/rules_block/heading.js @105
105: function(__fusereq, exports, module){
'use strict';
var isSpace = __fusereq(75).isSpace;
module.exports = function heading(state, startLine, endLine, silent) {
  var ch, level, tmp, token, pos = state.bMarks[startLine] + state.tShift[startLine], max = state.eMarks[startLine];
  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }
  ch = state.src.charCodeAt(pos);
  if (ch !== 0x23 || pos >= max) {
    return false;
  }
  level = 1;
  ch = state.src.charCodeAt(++pos);
  while (ch === 0x23 && pos < max && level <= 6) {
    level++;
    ch = state.src.charCodeAt(++pos);
  }
  if (level > 6 || pos < max && !isSpace(ch)) {
    return false;
  }
  if (silent) {
    return true;
  }
  max = state.skipSpacesBack(max, pos);
  tmp = state.skipCharsBack(max, 0x23, pos);
  if (tmp > pos && isSpace(state.src.charCodeAt(tmp - 1))) {
    max = tmp;
  }
  state.line = startLine + 1;
  token = state.push('heading_open', 'h' + String(level), 1);
  token.markup = ('########').slice(0, level);
  token.map = [startLine, state.line];
  token = state.push('inline', '', 0);
  token.content = state.src.slice(pos, max).trim();
  token.map = [startLine, state.line];
  token.children = [];
  token = state.push('heading_close', 'h' + String(level), -1);
  token.markup = ('########').slice(0, level);
  return true;
};

},

// node_modules/markdown-it/lib/rules_block/lheading.js @106
106: function(__fusereq, exports, module){
'use strict';
module.exports = function lheading(state, startLine, endLine) {
  var content, terminate, i, l, token, pos, max, level, marker, nextLine = startLine + 1, oldParentType, terminatorRules = state.md.block.ruler.getRules('paragraph');
  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }
  oldParentType = state.parentType;
  state.parentType = 'paragraph';
  for (; nextLine < endLine && !state.isEmpty(nextLine); nextLine++) {
    if (state.sCount[nextLine] - state.blkIndent > 3) {
      continue;
    }
    if (state.sCount[nextLine] >= state.blkIndent) {
      pos = state.bMarks[nextLine] + state.tShift[nextLine];
      max = state.eMarks[nextLine];
      if (pos < max) {
        marker = state.src.charCodeAt(pos);
        if (marker === 0x2D || marker === 0x3D) {
          pos = state.skipChars(pos, marker);
          pos = state.skipSpaces(pos);
          if (pos >= max) {
            level = marker === 0x3D ? 1 : 2;
            break;
          }
        }
      }
    }
    if (state.sCount[nextLine] < 0) {
      continue;
    }
    terminate = false;
    for ((i = 0, l = terminatorRules.length); i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }
    if (terminate) {
      break;
    }
  }
  if (!level) {
    return false;
  }
  content = state.getLines(startLine, nextLine, state.blkIndent, false).trim();
  state.line = nextLine + 1;
  token = state.push('heading_open', 'h' + String(level), 1);
  token.markup = String.fromCharCode(marker);
  token.map = [startLine, state.line];
  token = state.push('inline', '', 0);
  token.content = content;
  token.map = [startLine, state.line - 1];
  token.children = [];
  token = state.push('heading_close', 'h' + String(level), -1);
  token.markup = String.fromCharCode(marker);
  state.parentType = oldParentType;
  return true;
};

},

// node_modules/markdown-it/lib/rules_block/html_block.js @107
107: function(__fusereq, exports, module){
'use strict';
var block_names = __fusereq(133);
var HTML_OPEN_CLOSE_TAG_RE = __fusereq(134).HTML_OPEN_CLOSE_TAG_RE;
var HTML_SEQUENCES = [[/^<(script|pre|style)(?=(\s|>|$))/i, /<\/(script|pre|style)>/i, true], [/^<!--/, /-->/, true], [/^<\?/, /\?>/, true], [/^<![A-Z]/, />/, true], [/^<!\[CDATA\[/, /\]\]>/, true], [new RegExp('^</?(' + block_names.join('|') + ')(?=(\\s|/?>|$))', 'i'), /^$/, true], [new RegExp(HTML_OPEN_CLOSE_TAG_RE.source + '\\s*$'), /^$/, false]];
module.exports = function html_block(state, startLine, endLine, silent) {
  var i, nextLine, token, lineText, pos = state.bMarks[startLine] + state.tShift[startLine], max = state.eMarks[startLine];
  if (state.sCount[startLine] - state.blkIndent >= 4) {
    return false;
  }
  if (!state.md.options.html) {
    return false;
  }
  if (state.src.charCodeAt(pos) !== 0x3C) {
    return false;
  }
  lineText = state.src.slice(pos, max);
  for (i = 0; i < HTML_SEQUENCES.length; i++) {
    if (HTML_SEQUENCES[i][0].test(lineText)) {
      break;
    }
  }
  if (i === HTML_SEQUENCES.length) {
    return false;
  }
  if (silent) {
    return HTML_SEQUENCES[i][2];
  }
  nextLine = startLine + 1;
  if (!HTML_SEQUENCES[i][1].test(lineText)) {
    for (; nextLine < endLine; nextLine++) {
      if (state.sCount[nextLine] < state.blkIndent) {
        break;
      }
      pos = state.bMarks[nextLine] + state.tShift[nextLine];
      max = state.eMarks[nextLine];
      lineText = state.src.slice(pos, max);
      if (HTML_SEQUENCES[i][1].test(lineText)) {
        if (lineText.length !== 0) {
          nextLine++;
        }
        break;
      }
    }
  }
  state.line = nextLine;
  token = state.push('html_block', '', 0);
  token.map = [startLine, nextLine];
  token.content = state.getLines(startLine, nextLine, state.blkIndent, true);
  return true;
};

},

// node_modules/markdown-it/lib/rules_block/paragraph.js @108
108: function(__fusereq, exports, module){
'use strict';
module.exports = function paragraph(state, startLine) {
  var content, terminate, i, l, token, oldParentType, nextLine = startLine + 1, terminatorRules = state.md.block.ruler.getRules('paragraph'), endLine = state.lineMax;
  oldParentType = state.parentType;
  state.parentType = 'paragraph';
  for (; nextLine < endLine && !state.isEmpty(nextLine); nextLine++) {
    if (state.sCount[nextLine] - state.blkIndent > 3) {
      continue;
    }
    if (state.sCount[nextLine] < 0) {
      continue;
    }
    terminate = false;
    for ((i = 0, l = terminatorRules.length); i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }
    if (terminate) {
      break;
    }
  }
  content = state.getLines(startLine, nextLine, state.blkIndent, false).trim();
  state.line = nextLine;
  token = state.push('paragraph_open', 'p', 1);
  token.map = [startLine, state.line];
  token = state.push('inline', '', 0);
  token.content = content;
  token.map = [startLine, state.line];
  token.children = [];
  token = state.push('paragraph_close', 'p', -1);
  state.parentType = oldParentType;
  return true;
};

},

// node_modules/markdown-it/lib/rules_block/state_block.js @109
109: function(__fusereq, exports, module){
'use strict';
var Token = __fusereq(132);
var isSpace = __fusereq(75).isSpace;
function StateBlock(src, md, env, tokens) {
  var ch, s, start, pos, len, indent, offset, indent_found;
  this.src = src;
  this.md = md;
  this.env = env;
  this.tokens = tokens;
  this.bMarks = [];
  this.eMarks = [];
  this.tShift = [];
  this.sCount = [];
  this.bsCount = [];
  this.blkIndent = 0;
  this.line = 0;
  this.lineMax = 0;
  this.tight = false;
  this.ddIndent = -1;
  this.listIndent = -1;
  this.parentType = 'root';
  this.level = 0;
  this.result = '';
  s = this.src;
  indent_found = false;
  for ((start = pos = indent = offset = 0, len = s.length); pos < len; pos++) {
    ch = s.charCodeAt(pos);
    if (!indent_found) {
      if (isSpace(ch)) {
        indent++;
        if (ch === 0x09) {
          offset += 4 - offset % 4;
        } else {
          offset++;
        }
        continue;
      } else {
        indent_found = true;
      }
    }
    if (ch === 0x0A || pos === len - 1) {
      if (ch !== 0x0A) {
        pos++;
      }
      this.bMarks.push(start);
      this.eMarks.push(pos);
      this.tShift.push(indent);
      this.sCount.push(offset);
      this.bsCount.push(0);
      indent_found = false;
      indent = 0;
      offset = 0;
      start = pos + 1;
    }
  }
  this.bMarks.push(s.length);
  this.eMarks.push(s.length);
  this.tShift.push(0);
  this.sCount.push(0);
  this.bsCount.push(0);
  this.lineMax = this.bMarks.length - 1;
}
StateBlock.prototype.push = function (type, tag, nesting) {
  var token = new Token(type, tag, nesting);
  token.block = true;
  if (nesting < 0) this.level--;
  token.level = this.level;
  if (nesting > 0) this.level++;
  this.tokens.push(token);
  return token;
};
StateBlock.prototype.isEmpty = function isEmpty(line) {
  return this.bMarks[line] + this.tShift[line] >= this.eMarks[line];
};
StateBlock.prototype.skipEmptyLines = function skipEmptyLines(from) {
  for (var max = this.lineMax; from < max; from++) {
    if (this.bMarks[from] + this.tShift[from] < this.eMarks[from]) {
      break;
    }
  }
  return from;
};
StateBlock.prototype.skipSpaces = function skipSpaces(pos) {
  var ch;
  for (var max = this.src.length; pos < max; pos++) {
    ch = this.src.charCodeAt(pos);
    if (!isSpace(ch)) {
      break;
    }
  }
  return pos;
};
StateBlock.prototype.skipSpacesBack = function skipSpacesBack(pos, min) {
  if (pos <= min) {
    return pos;
  }
  while (pos > min) {
    if (!isSpace(this.src.charCodeAt(--pos))) {
      return pos + 1;
    }
  }
  return pos;
};
StateBlock.prototype.skipChars = function skipChars(pos, code) {
  for (var max = this.src.length; pos < max; pos++) {
    if (this.src.charCodeAt(pos) !== code) {
      break;
    }
  }
  return pos;
};
StateBlock.prototype.skipCharsBack = function skipCharsBack(pos, code, min) {
  if (pos <= min) {
    return pos;
  }
  while (pos > min) {
    if (code !== this.src.charCodeAt(--pos)) {
      return pos + 1;
    }
  }
  return pos;
};
StateBlock.prototype.getLines = function getLines(begin, end, indent, keepLastLF) {
  var i, lineIndent, ch, first, last, queue, lineStart, line = begin;
  if (begin >= end) {
    return '';
  }
  queue = new Array(end - begin);
  for (i = 0; line < end; (line++, i++)) {
    lineIndent = 0;
    lineStart = first = this.bMarks[line];
    if (line + 1 < end || keepLastLF) {
      last = this.eMarks[line] + 1;
    } else {
      last = this.eMarks[line];
    }
    while (first < last && lineIndent < indent) {
      ch = this.src.charCodeAt(first);
      if (isSpace(ch)) {
        if (ch === 0x09) {
          lineIndent += 4 - (lineIndent + this.bsCount[line]) % 4;
        } else {
          lineIndent++;
        }
      } else if (first - lineStart < this.tShift[line]) {
        lineIndent++;
      } else {
        break;
      }
      first++;
    }
    if (lineIndent > indent) {
      queue[i] = new Array(lineIndent - indent + 1).join(' ') + this.src.slice(first, last);
    } else {
      queue[i] = this.src.slice(first, last);
    }
  }
  return queue.join('');
};
StateBlock.prototype.Token = Token;
module.exports = StateBlock;

},

// node_modules/markdown-it/lib/rules_inline/text.js @110
110: function(__fusereq, exports, module){
'use strict';
function isTerminatorChar(ch) {
  switch (ch) {
    case 0x0A:
    case 0x21:
    case 0x23:
    case 0x24:
    case 0x25:
    case 0x26:
    case 0x2A:
    case 0x2B:
    case 0x2D:
    case 0x3A:
    case 0x3C:
    case 0x3D:
    case 0x3E:
    case 0x40:
    case 0x5B:
    case 0x5C:
    case 0x5D:
    case 0x5E:
    case 0x5F:
    case 0x60:
    case 0x7B:
    case 0x7D:
    case 0x7E:
      return true;
    default:
      return false;
  }
}
module.exports = function text(state, silent) {
  var pos = state.pos;
  while (pos < state.posMax && !isTerminatorChar(state.src.charCodeAt(pos))) {
    pos++;
  }
  if (pos === state.pos) {
    return false;
  }
  if (!silent) {
    state.pending += state.src.slice(state.pos, pos);
  }
  state.pos = pos;
  return true;
};

},

// node_modules/markdown-it/lib/rules_inline/newline.js @111
111: function(__fusereq, exports, module){
'use strict';
var isSpace = __fusereq(75).isSpace;
module.exports = function newline(state, silent) {
  var pmax, max, pos = state.pos;
  if (state.src.charCodeAt(pos) !== 0x0A) {
    return false;
  }
  pmax = state.pending.length - 1;
  max = state.posMax;
  if (!silent) {
    if (pmax >= 0 && state.pending.charCodeAt(pmax) === 0x20) {
      if (pmax >= 1 && state.pending.charCodeAt(pmax - 1) === 0x20) {
        state.pending = state.pending.replace(/ +$/, '');
        state.push('hardbreak', 'br', 0);
      } else {
        state.pending = state.pending.slice(0, -1);
        state.push('softbreak', 'br', 0);
      }
    } else {
      state.push('softbreak', 'br', 0);
    }
  }
  pos++;
  while (pos < max && isSpace(state.src.charCodeAt(pos))) {
    pos++;
  }
  state.pos = pos;
  return true;
};

},

// node_modules/markdown-it/lib/rules_inline/escape.js @112
112: function(__fusereq, exports, module){
'use strict';
var isSpace = __fusereq(75).isSpace;
var ESCAPED = [];
for (var i = 0; i < 256; i++) {
  ESCAPED.push(0);
}
('\\!"#$%&\'()*+,./:;<=>?@[]^_`{|}~-').split('').forEach(function (ch) {
  ESCAPED[ch.charCodeAt(0)] = 1;
});
module.exports = function escape(state, silent) {
  var ch, pos = state.pos, max = state.posMax;
  if (state.src.charCodeAt(pos) !== 0x5C) {
    return false;
  }
  pos++;
  if (pos < max) {
    ch = state.src.charCodeAt(pos);
    if (ch < 256 && ESCAPED[ch] !== 0) {
      if (!silent) {
        state.pending += state.src[pos];
      }
      state.pos += 2;
      return true;
    }
    if (ch === 0x0A) {
      if (!silent) {
        state.push('hardbreak', 'br', 0);
      }
      pos++;
      while (pos < max) {
        ch = state.src.charCodeAt(pos);
        if (!isSpace(ch)) {
          break;
        }
        pos++;
      }
      state.pos = pos;
      return true;
    }
  }
  if (!silent) {
    state.pending += '\\';
  }
  state.pos++;
  return true;
};

},

// node_modules/markdown-it/lib/rules_inline/backticks.js @113
113: function(__fusereq, exports, module){
'use strict';
module.exports = function backtick(state, silent) {
  var start, max, marker, matchStart, matchEnd, token, pos = state.pos, ch = state.src.charCodeAt(pos);
  if (ch !== 0x60) {
    return false;
  }
  start = pos;
  pos++;
  max = state.posMax;
  while (pos < max && state.src.charCodeAt(pos) === 0x60) {
    pos++;
  }
  marker = state.src.slice(start, pos);
  matchStart = matchEnd = pos;
  while ((matchStart = state.src.indexOf('`', matchEnd)) !== -1) {
    matchEnd = matchStart + 1;
    while (matchEnd < max && state.src.charCodeAt(matchEnd) === 0x60) {
      matchEnd++;
    }
    if (matchEnd - matchStart === marker.length) {
      if (!silent) {
        token = state.push('code_inline', 'code', 0);
        token.markup = marker;
        token.content = state.src.slice(pos, matchStart).replace(/\n/g, ' ').replace(/^ (.+) $/, '$1');
      }
      state.pos = matchEnd;
      return true;
    }
  }
  if (!silent) {
    state.pending += marker;
  }
  state.pos += marker.length;
  return true;
};

},

// node_modules/markdown-it/lib/rules_inline/strikethrough.js @114
114: function(__fusereq, exports, module){
'use strict';
module.exports.tokenize = function strikethrough(state, silent) {
  var i, scanned, token, len, ch, start = state.pos, marker = state.src.charCodeAt(start);
  if (silent) {
    return false;
  }
  if (marker !== 0x7E) {
    return false;
  }
  scanned = state.scanDelims(state.pos, true);
  len = scanned.length;
  ch = String.fromCharCode(marker);
  if (len < 2) {
    return false;
  }
  if (len % 2) {
    token = state.push('text', '', 0);
    token.content = ch;
    len--;
  }
  for (i = 0; i < len; i += 2) {
    token = state.push('text', '', 0);
    token.content = ch + ch;
    state.delimiters.push({
      marker: marker,
      length: 0,
      jump: i,
      token: state.tokens.length - 1,
      end: -1,
      open: scanned.can_open,
      close: scanned.can_close
    });
  }
  state.pos += scanned.length;
  return true;
};
function postProcess(state, delimiters) {
  var i, j, startDelim, endDelim, token, loneMarkers = [], max = delimiters.length;
  for (i = 0; i < max; i++) {
    startDelim = delimiters[i];
    if (startDelim.marker !== 0x7E) {
      continue;
    }
    if (startDelim.end === -1) {
      continue;
    }
    endDelim = delimiters[startDelim.end];
    token = state.tokens[startDelim.token];
    token.type = 's_open';
    token.tag = 's';
    token.nesting = 1;
    token.markup = '~~';
    token.content = '';
    token = state.tokens[endDelim.token];
    token.type = 's_close';
    token.tag = 's';
    token.nesting = -1;
    token.markup = '~~';
    token.content = '';
    if (state.tokens[endDelim.token - 1].type === 'text' && state.tokens[endDelim.token - 1].content === '~') {
      loneMarkers.push(endDelim.token - 1);
    }
  }
  while (loneMarkers.length) {
    i = loneMarkers.pop();
    j = i + 1;
    while (j < state.tokens.length && state.tokens[j].type === 's_close') {
      j++;
    }
    j--;
    if (i !== j) {
      token = state.tokens[j];
      state.tokens[j] = state.tokens[i];
      state.tokens[i] = token;
    }
  }
}
module.exports.postProcess = function strikethrough(state) {
  var curr, tokens_meta = state.tokens_meta, max = state.tokens_meta.length;
  postProcess(state, state.delimiters);
  for (curr = 0; curr < max; curr++) {
    if (tokens_meta[curr] && tokens_meta[curr].delimiters) {
      postProcess(state, tokens_meta[curr].delimiters);
    }
  }
};

},

// node_modules/markdown-it/lib/rules_inline/emphasis.js @115
115: function(__fusereq, exports, module){
'use strict';
module.exports.tokenize = function emphasis(state, silent) {
  var i, scanned, token, start = state.pos, marker = state.src.charCodeAt(start);
  if (silent) {
    return false;
  }
  if (marker !== 0x5F && marker !== 0x2A) {
    return false;
  }
  scanned = state.scanDelims(state.pos, marker === 0x2A);
  for (i = 0; i < scanned.length; i++) {
    token = state.push('text', '', 0);
    token.content = String.fromCharCode(marker);
    state.delimiters.push({
      marker: marker,
      length: scanned.length,
      jump: i,
      token: state.tokens.length - 1,
      end: -1,
      open: scanned.can_open,
      close: scanned.can_close
    });
  }
  state.pos += scanned.length;
  return true;
};
function postProcess(state, delimiters) {
  var i, startDelim, endDelim, token, ch, isStrong, max = delimiters.length;
  for (i = max - 1; i >= 0; i--) {
    startDelim = delimiters[i];
    if (startDelim.marker !== 0x5F && startDelim.marker !== 0x2A) {
      continue;
    }
    if (startDelim.end === -1) {
      continue;
    }
    endDelim = delimiters[startDelim.end];
    isStrong = i > 0 && delimiters[i - 1].end === startDelim.end + 1 && delimiters[i - 1].token === startDelim.token - 1 && delimiters[startDelim.end + 1].token === endDelim.token + 1 && delimiters[i - 1].marker === startDelim.marker;
    ch = String.fromCharCode(startDelim.marker);
    token = state.tokens[startDelim.token];
    token.type = isStrong ? 'strong_open' : 'em_open';
    token.tag = isStrong ? 'strong' : 'em';
    token.nesting = 1;
    token.markup = isStrong ? ch + ch : ch;
    token.content = '';
    token = state.tokens[endDelim.token];
    token.type = isStrong ? 'strong_close' : 'em_close';
    token.tag = isStrong ? 'strong' : 'em';
    token.nesting = -1;
    token.markup = isStrong ? ch + ch : ch;
    token.content = '';
    if (isStrong) {
      state.tokens[delimiters[i - 1].token].content = '';
      state.tokens[delimiters[startDelim.end + 1].token].content = '';
      i--;
    }
  }
}
module.exports.postProcess = function emphasis(state) {
  var curr, tokens_meta = state.tokens_meta, max = state.tokens_meta.length;
  postProcess(state, state.delimiters);
  for (curr = 0; curr < max; curr++) {
    if (tokens_meta[curr] && tokens_meta[curr].delimiters) {
      postProcess(state, tokens_meta[curr].delimiters);
    }
  }
};

},

// node_modules/markdown-it/lib/rules_inline/link.js @116
116: function(__fusereq, exports, module){
'use strict';
var normalizeReference = __fusereq(75).normalizeReference;
var isSpace = __fusereq(75).isSpace;
module.exports = function link(state, silent) {
  var attrs, code, label, labelEnd, labelStart, pos, res, ref, title, token, href = '', oldPos = state.pos, max = state.posMax, start = state.pos, parseReference = true;
  if (state.src.charCodeAt(state.pos) !== 0x5B) {
    return false;
  }
  labelStart = state.pos + 1;
  labelEnd = state.md.helpers.parseLinkLabel(state, state.pos, true);
  if (labelEnd < 0) {
    return false;
  }
  pos = labelEnd + 1;
  if (pos < max && state.src.charCodeAt(pos) === 0x28) {
    parseReference = false;
    pos++;
    for (; pos < max; pos++) {
      code = state.src.charCodeAt(pos);
      if (!isSpace(code) && code !== 0x0A) {
        break;
      }
    }
    if (pos >= max) {
      return false;
    }
    start = pos;
    res = state.md.helpers.parseLinkDestination(state.src, pos, state.posMax);
    if (res.ok) {
      href = state.md.normalizeLink(res.str);
      if (state.md.validateLink(href)) {
        pos = res.pos;
      } else {
        href = '';
      }
    }
    start = pos;
    for (; pos < max; pos++) {
      code = state.src.charCodeAt(pos);
      if (!isSpace(code) && code !== 0x0A) {
        break;
      }
    }
    res = state.md.helpers.parseLinkTitle(state.src, pos, state.posMax);
    if (pos < max && start !== pos && res.ok) {
      title = res.str;
      pos = res.pos;
      for (; pos < max; pos++) {
        code = state.src.charCodeAt(pos);
        if (!isSpace(code) && code !== 0x0A) {
          break;
        }
      }
    } else {
      title = '';
    }
    if (pos >= max || state.src.charCodeAt(pos) !== 0x29) {
      parseReference = true;
    }
    pos++;
  }
  if (parseReference) {
    if (typeof state.env.references === 'undefined') {
      return false;
    }
    if (pos < max && state.src.charCodeAt(pos) === 0x5B) {
      start = pos + 1;
      pos = state.md.helpers.parseLinkLabel(state, pos);
      if (pos >= 0) {
        label = state.src.slice(start, pos++);
      } else {
        pos = labelEnd + 1;
      }
    } else {
      pos = labelEnd + 1;
    }
    if (!label) {
      label = state.src.slice(labelStart, labelEnd);
    }
    ref = state.env.references[normalizeReference(label)];
    if (!ref) {
      state.pos = oldPos;
      return false;
    }
    href = ref.href;
    title = ref.title;
  }
  if (!silent) {
    state.pos = labelStart;
    state.posMax = labelEnd;
    token = state.push('link_open', 'a', 1);
    token.attrs = attrs = [['href', href]];
    if (title) {
      attrs.push(['title', title]);
    }
    state.md.inline.tokenize(state);
    token = state.push('link_close', 'a', -1);
  }
  state.pos = pos;
  state.posMax = max;
  return true;
};

},

// node_modules/markdown-it/lib/rules_inline/image.js @117
117: function(__fusereq, exports, module){
'use strict';
var normalizeReference = __fusereq(75).normalizeReference;
var isSpace = __fusereq(75).isSpace;
module.exports = function image(state, silent) {
  var attrs, code, content, label, labelEnd, labelStart, pos, ref, res, title, token, tokens, start, href = '', oldPos = state.pos, max = state.posMax;
  if (state.src.charCodeAt(state.pos) !== 0x21) {
    return false;
  }
  if (state.src.charCodeAt(state.pos + 1) !== 0x5B) {
    return false;
  }
  labelStart = state.pos + 2;
  labelEnd = state.md.helpers.parseLinkLabel(state, state.pos + 1, false);
  if (labelEnd < 0) {
    return false;
  }
  pos = labelEnd + 1;
  if (pos < max && state.src.charCodeAt(pos) === 0x28) {
    pos++;
    for (; pos < max; pos++) {
      code = state.src.charCodeAt(pos);
      if (!isSpace(code) && code !== 0x0A) {
        break;
      }
    }
    if (pos >= max) {
      return false;
    }
    start = pos;
    res = state.md.helpers.parseLinkDestination(state.src, pos, state.posMax);
    if (res.ok) {
      href = state.md.normalizeLink(res.str);
      if (state.md.validateLink(href)) {
        pos = res.pos;
      } else {
        href = '';
      }
    }
    start = pos;
    for (; pos < max; pos++) {
      code = state.src.charCodeAt(pos);
      if (!isSpace(code) && code !== 0x0A) {
        break;
      }
    }
    res = state.md.helpers.parseLinkTitle(state.src, pos, state.posMax);
    if (pos < max && start !== pos && res.ok) {
      title = res.str;
      pos = res.pos;
      for (; pos < max; pos++) {
        code = state.src.charCodeAt(pos);
        if (!isSpace(code) && code !== 0x0A) {
          break;
        }
      }
    } else {
      title = '';
    }
    if (pos >= max || state.src.charCodeAt(pos) !== 0x29) {
      state.pos = oldPos;
      return false;
    }
    pos++;
  } else {
    if (typeof state.env.references === 'undefined') {
      return false;
    }
    if (pos < max && state.src.charCodeAt(pos) === 0x5B) {
      start = pos + 1;
      pos = state.md.helpers.parseLinkLabel(state, pos);
      if (pos >= 0) {
        label = state.src.slice(start, pos++);
      } else {
        pos = labelEnd + 1;
      }
    } else {
      pos = labelEnd + 1;
    }
    if (!label) {
      label = state.src.slice(labelStart, labelEnd);
    }
    ref = state.env.references[normalizeReference(label)];
    if (!ref) {
      state.pos = oldPos;
      return false;
    }
    href = ref.href;
    title = ref.title;
  }
  if (!silent) {
    content = state.src.slice(labelStart, labelEnd);
    state.md.inline.parse(content, state.md, state.env, tokens = []);
    token = state.push('image', 'img', 0);
    token.attrs = attrs = [['src', href], ['alt', '']];
    token.children = tokens;
    token.content = content;
    if (title) {
      attrs.push(['title', title]);
    }
  }
  state.pos = pos;
  state.posMax = max;
  return true;
};

},

// node_modules/markdown-it/lib/rules_inline/autolink.js @118
118: function(__fusereq, exports, module){
'use strict';
var EMAIL_RE = /^<([a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*)>/;
var AUTOLINK_RE = /^<([a-zA-Z][a-zA-Z0-9+.\-]{1,31}):([^<>\x00-\x20]*)>/;
module.exports = function autolink(state, silent) {
  var tail, linkMatch, emailMatch, url, fullUrl, token, pos = state.pos;
  if (state.src.charCodeAt(pos) !== 0x3C) {
    return false;
  }
  tail = state.src.slice(pos);
  if (tail.indexOf('>') < 0) {
    return false;
  }
  if (AUTOLINK_RE.test(tail)) {
    linkMatch = tail.match(AUTOLINK_RE);
    url = linkMatch[0].slice(1, -1);
    fullUrl = state.md.normalizeLink(url);
    if (!state.md.validateLink(fullUrl)) {
      return false;
    }
    if (!silent) {
      token = state.push('link_open', 'a', 1);
      token.attrs = [['href', fullUrl]];
      token.markup = 'autolink';
      token.info = 'auto';
      token = state.push('text', '', 0);
      token.content = state.md.normalizeLinkText(url);
      token = state.push('link_close', 'a', -1);
      token.markup = 'autolink';
      token.info = 'auto';
    }
    state.pos += linkMatch[0].length;
    return true;
  }
  if (EMAIL_RE.test(tail)) {
    emailMatch = tail.match(EMAIL_RE);
    url = emailMatch[0].slice(1, -1);
    fullUrl = state.md.normalizeLink('mailto:' + url);
    if (!state.md.validateLink(fullUrl)) {
      return false;
    }
    if (!silent) {
      token = state.push('link_open', 'a', 1);
      token.attrs = [['href', fullUrl]];
      token.markup = 'autolink';
      token.info = 'auto';
      token = state.push('text', '', 0);
      token.content = state.md.normalizeLinkText(url);
      token = state.push('link_close', 'a', -1);
      token.markup = 'autolink';
      token.info = 'auto';
    }
    state.pos += emailMatch[0].length;
    return true;
  }
  return false;
};

},

// node_modules/markdown-it/lib/rules_inline/html_inline.js @119
119: function(__fusereq, exports, module){
'use strict';
var HTML_TAG_RE = __fusereq(134).HTML_TAG_RE;
function isLetter(ch) {
  var lc = ch | 0x20;
  return lc >= 0x61 && lc <= 0x7a;
}
module.exports = function html_inline(state, silent) {
  var ch, match, max, token, pos = state.pos;
  if (!state.md.options.html) {
    return false;
  }
  max = state.posMax;
  if (state.src.charCodeAt(pos) !== 0x3C || pos + 2 >= max) {
    return false;
  }
  ch = state.src.charCodeAt(pos + 1);
  if (ch !== 0x21 && ch !== 0x3F && ch !== 0x2F && !isLetter(ch)) {
    return false;
  }
  match = state.src.slice(pos).match(HTML_TAG_RE);
  if (!match) {
    return false;
  }
  if (!silent) {
    token = state.push('html_inline', '', 0);
    token.content = state.src.slice(pos, pos + match[0].length);
  }
  state.pos += match[0].length;
  return true;
};

},

// node_modules/markdown-it/lib/rules_inline/entity.js @120
120: function(__fusereq, exports, module){
'use strict';
var entities = __fusereq(128);
var has = __fusereq(75).has;
var isValidEntityCode = __fusereq(75).isValidEntityCode;
var fromCodePoint = __fusereq(75).fromCodePoint;
var DIGITAL_RE = /^&#((?:x[a-f0-9]{1,6}|[0-9]{1,7}));/i;
var NAMED_RE = /^&([a-z][a-z0-9]{1,31});/i;
module.exports = function entity(state, silent) {
  var ch, code, match, pos = state.pos, max = state.posMax;
  if (state.src.charCodeAt(pos) !== 0x26) {
    return false;
  }
  if (pos + 1 < max) {
    ch = state.src.charCodeAt(pos + 1);
    if (ch === 0x23) {
      match = state.src.slice(pos).match(DIGITAL_RE);
      if (match) {
        if (!silent) {
          code = match[1][0].toLowerCase() === 'x' ? parseInt(match[1].slice(1), 16) : parseInt(match[1], 10);
          state.pending += isValidEntityCode(code) ? fromCodePoint(code) : fromCodePoint(0xFFFD);
        }
        state.pos += match[0].length;
        return true;
      }
    } else {
      match = state.src.slice(pos).match(NAMED_RE);
      if (match) {
        if (has(entities, match[1])) {
          if (!silent) {
            state.pending += entities[match[1]];
          }
          state.pos += match[0].length;
          return true;
        }
      }
    }
  }
  if (!silent) {
    state.pending += '&';
  }
  state.pos++;
  return true;
};

},

// node_modules/markdown-it/lib/rules_inline/balance_pairs.js @121
121: function(__fusereq, exports, module){
'use strict';
function processDelimiters(state, delimiters) {
  var closerIdx, openerIdx, closer, opener, minOpenerIdx, newMinOpenerIdx, isOddMatch, lastJump, openersBottom = {}, max = delimiters.length;
  for (closerIdx = 0; closerIdx < max; closerIdx++) {
    closer = delimiters[closerIdx];
    closer.length = closer.length || 0;
    if (!closer.close) continue;
    if (!openersBottom.hasOwnProperty(closer.marker)) {
      openersBottom[closer.marker] = [-1, -1, -1];
    }
    minOpenerIdx = openersBottom[closer.marker][closer.length % 3];
    newMinOpenerIdx = -1;
    openerIdx = closerIdx - closer.jump - 1;
    for (; openerIdx > minOpenerIdx; openerIdx -= opener.jump + 1) {
      opener = delimiters[openerIdx];
      if (opener.marker !== closer.marker) continue;
      if (newMinOpenerIdx === -1) newMinOpenerIdx = openerIdx;
      if (opener.open && opener.end < 0 && opener.level === closer.level) {
        isOddMatch = false;
        if (opener.close || closer.open) {
          if ((opener.length + closer.length) % 3 === 0) {
            if (opener.length % 3 !== 0 || closer.length % 3 !== 0) {
              isOddMatch = true;
            }
          }
        }
        if (!isOddMatch) {
          lastJump = openerIdx > 0 && !delimiters[openerIdx - 1].open ? delimiters[openerIdx - 1].jump + 1 : 0;
          closer.jump = closerIdx - openerIdx + lastJump;
          closer.open = false;
          opener.end = closerIdx;
          opener.jump = lastJump;
          opener.close = false;
          newMinOpenerIdx = -1;
          break;
        }
      }
    }
    if (newMinOpenerIdx !== -1) {
      openersBottom[closer.marker][(closer.length || 0) % 3] = newMinOpenerIdx;
    }
  }
}
module.exports = function link_pairs(state) {
  var curr, tokens_meta = state.tokens_meta, max = state.tokens_meta.length;
  processDelimiters(state, state.delimiters);
  for (curr = 0; curr < max; curr++) {
    if (tokens_meta[curr] && tokens_meta[curr].delimiters) {
      processDelimiters(state, tokens_meta[curr].delimiters);
    }
  }
};

},

// node_modules/markdown-it/lib/rules_inline/text_collapse.js @122
122: function(__fusereq, exports, module){
'use strict';
module.exports = function text_collapse(state) {
  var curr, last, level = 0, tokens = state.tokens, max = state.tokens.length;
  for (curr = last = 0; curr < max; curr++) {
    if (tokens[curr].nesting < 0) level--;
    tokens[curr].level = level;
    if (tokens[curr].nesting > 0) level++;
    if (tokens[curr].type === 'text' && curr + 1 < max && tokens[curr + 1].type === 'text') {
      tokens[curr + 1].content = tokens[curr].content + tokens[curr + 1].content;
    } else {
      if (curr !== last) {
        tokens[last] = tokens[curr];
      }
      last++;
    }
  }
  if (curr !== last) {
    tokens.length = last;
  }
};

},

// node_modules/markdown-it/lib/rules_inline/state_inline.js @123
123: function(__fusereq, exports, module){
'use strict';
var Token = __fusereq(132);
var isWhiteSpace = __fusereq(75).isWhiteSpace;
var isPunctChar = __fusereq(75).isPunctChar;
var isMdAsciiPunct = __fusereq(75).isMdAsciiPunct;
function StateInline(src, md, env, outTokens) {
  this.src = src;
  this.env = env;
  this.md = md;
  this.tokens = outTokens;
  this.tokens_meta = Array(outTokens.length);
  this.pos = 0;
  this.posMax = this.src.length;
  this.level = 0;
  this.pending = '';
  this.pendingLevel = 0;
  this.cache = {};
  this.delimiters = [];
  this._prev_delimiters = [];
}
StateInline.prototype.pushPending = function () {
  var token = new Token('text', '', 0);
  token.content = this.pending;
  token.level = this.pendingLevel;
  this.tokens.push(token);
  this.pending = '';
  return token;
};
StateInline.prototype.push = function (type, tag, nesting) {
  if (this.pending) {
    this.pushPending();
  }
  var token = new Token(type, tag, nesting);
  var token_meta = null;
  if (nesting < 0) {
    this.level--;
    this.delimiters = this._prev_delimiters.pop();
  }
  token.level = this.level;
  if (nesting > 0) {
    this.level++;
    this._prev_delimiters.push(this.delimiters);
    this.delimiters = [];
    token_meta = {
      delimiters: this.delimiters
    };
  }
  this.pendingLevel = this.level;
  this.tokens.push(token);
  this.tokens_meta.push(token_meta);
  return token;
};
StateInline.prototype.scanDelims = function (start, canSplitWord) {
  var pos = start, lastChar, nextChar, count, can_open, can_close, isLastWhiteSpace, isLastPunctChar, isNextWhiteSpace, isNextPunctChar, left_flanking = true, right_flanking = true, max = this.posMax, marker = this.src.charCodeAt(start);
  lastChar = start > 0 ? this.src.charCodeAt(start - 1) : 0x20;
  while (pos < max && this.src.charCodeAt(pos) === marker) {
    pos++;
  }
  count = pos - start;
  nextChar = pos < max ? this.src.charCodeAt(pos) : 0x20;
  isLastPunctChar = isMdAsciiPunct(lastChar) || isPunctChar(String.fromCharCode(lastChar));
  isNextPunctChar = isMdAsciiPunct(nextChar) || isPunctChar(String.fromCharCode(nextChar));
  isLastWhiteSpace = isWhiteSpace(lastChar);
  isNextWhiteSpace = isWhiteSpace(nextChar);
  if (isNextWhiteSpace) {
    left_flanking = false;
  } else if (isNextPunctChar) {
    if (!(isLastWhiteSpace || isLastPunctChar)) {
      left_flanking = false;
    }
  }
  if (isLastWhiteSpace) {
    right_flanking = false;
  } else if (isLastPunctChar) {
    if (!(isNextWhiteSpace || isNextPunctChar)) {
      right_flanking = false;
    }
  }
  if (!canSplitWord) {
    can_open = left_flanking && (!right_flanking || isLastPunctChar);
    can_close = right_flanking && (!left_flanking || isNextPunctChar);
  } else {
    can_open = left_flanking;
    can_close = right_flanking;
  }
  return {
    can_open: can_open,
    can_close: can_close,
    length: count
  };
};
StateInline.prototype.Token = Token;
module.exports = StateInline;

},

// node_modules/mdurl/encode.js @124
124: function(__fusereq, exports, module){
'use strict';
var encodeCache = {};
function getEncodeCache(exclude) {
  var i, ch, cache = encodeCache[exclude];
  if (cache) {
    return cache;
  }
  cache = encodeCache[exclude] = [];
  for (i = 0; i < 128; i++) {
    ch = String.fromCharCode(i);
    if ((/^[0-9a-z]$/i).test(ch)) {
      cache.push(ch);
    } else {
      cache.push('%' + ('0' + i.toString(16).toUpperCase()).slice(-2));
    }
  }
  for (i = 0; i < exclude.length; i++) {
    cache[exclude.charCodeAt(i)] = exclude[i];
  }
  return cache;
}
function encode(string, exclude, keepEscaped) {
  var i, l, code, nextCode, cache, result = '';
  if (typeof exclude !== 'string') {
    keepEscaped = exclude;
    exclude = encode.defaultChars;
  }
  if (typeof keepEscaped === 'undefined') {
    keepEscaped = true;
  }
  cache = getEncodeCache(exclude);
  for ((i = 0, l = string.length); i < l; i++) {
    code = string.charCodeAt(i);
    if (keepEscaped && code === 0x25 && i + 2 < l) {
      if ((/^[0-9a-f]{2}$/i).test(string.slice(i + 1, i + 3))) {
        result += string.slice(i, i + 3);
        i += 2;
        continue;
      }
    }
    if (code < 128) {
      result += cache[code];
      continue;
    }
    if (code >= 0xD800 && code <= 0xDFFF) {
      if (code >= 0xD800 && code <= 0xDBFF && i + 1 < l) {
        nextCode = string.charCodeAt(i + 1);
        if (nextCode >= 0xDC00 && nextCode <= 0xDFFF) {
          result += encodeURIComponent(string[i] + string[i + 1]);
          i++;
          continue;
        }
      }
      result += '%EF%BF%BD';
      continue;
    }
    result += encodeURIComponent(string[i]);
  }
  return result;
}
encode.defaultChars = ";/?:@&=+$,-_.!~*'()#";
encode.componentChars = "-_.!~*'()";
module.exports = encode;

},

// node_modules/mdurl/decode.js @125
125: function(__fusereq, exports, module){
'use strict';
var decodeCache = {};
function getDecodeCache(exclude) {
  var i, ch, cache = decodeCache[exclude];
  if (cache) {
    return cache;
  }
  cache = decodeCache[exclude] = [];
  for (i = 0; i < 128; i++) {
    ch = String.fromCharCode(i);
    cache.push(ch);
  }
  for (i = 0; i < exclude.length; i++) {
    ch = exclude.charCodeAt(i);
    cache[ch] = '%' + ('0' + ch.toString(16).toUpperCase()).slice(-2);
  }
  return cache;
}
function decode(string, exclude) {
  var cache;
  if (typeof exclude !== 'string') {
    exclude = decode.defaultChars;
  }
  cache = getDecodeCache(exclude);
  return string.replace(/(%[a-f0-9]{2})+/gi, function (seq) {
    var i, l, b1, b2, b3, b4, chr, result = '';
    for ((i = 0, l = seq.length); i < l; i += 3) {
      b1 = parseInt(seq.slice(i + 1, i + 3), 16);
      if (b1 < 0x80) {
        result += cache[b1];
        continue;
      }
      if ((b1 & 0xE0) === 0xC0 && i + 3 < l) {
        b2 = parseInt(seq.slice(i + 4, i + 6), 16);
        if ((b2 & 0xC0) === 0x80) {
          chr = b1 << 6 & 0x7C0 | b2 & 0x3F;
          if (chr < 0x80) {
            result += '\ufffd\ufffd';
          } else {
            result += String.fromCharCode(chr);
          }
          i += 3;
          continue;
        }
      }
      if ((b1 & 0xF0) === 0xE0 && i + 6 < l) {
        b2 = parseInt(seq.slice(i + 4, i + 6), 16);
        b3 = parseInt(seq.slice(i + 7, i + 9), 16);
        if ((b2 & 0xC0) === 0x80 && (b3 & 0xC0) === 0x80) {
          chr = b1 << 12 & 0xF000 | b2 << 6 & 0xFC0 | b3 & 0x3F;
          if (chr < 0x800 || chr >= 0xD800 && chr <= 0xDFFF) {
            result += '\ufffd\ufffd\ufffd';
          } else {
            result += String.fromCharCode(chr);
          }
          i += 6;
          continue;
        }
      }
      if ((b1 & 0xF8) === 0xF0 && i + 9 < l) {
        b2 = parseInt(seq.slice(i + 4, i + 6), 16);
        b3 = parseInt(seq.slice(i + 7, i + 9), 16);
        b4 = parseInt(seq.slice(i + 10, i + 12), 16);
        if ((b2 & 0xC0) === 0x80 && (b3 & 0xC0) === 0x80 && (b4 & 0xC0) === 0x80) {
          chr = b1 << 18 & 0x1C0000 | b2 << 12 & 0x3F000 | b3 << 6 & 0xFC0 | b4 & 0x3F;
          if (chr < 0x10000 || chr > 0x10FFFF) {
            result += '\ufffd\ufffd\ufffd\ufffd';
          } else {
            chr -= 0x10000;
            result += String.fromCharCode(0xD800 + (chr >> 10), 0xDC00 + (chr & 0x3FF));
          }
          i += 9;
          continue;
        }
      }
      result += '\ufffd';
    }
    return result;
  });
}
decode.defaultChars = ';/?:@&=+$,#';
decode.componentChars = '';
module.exports = decode;

},

// node_modules/mdurl/format.js @126
126: function(__fusereq, exports, module){
'use strict';
module.exports = function format(url) {
  var result = '';
  result += url.protocol || '';
  result += url.slashes ? '//' : '';
  result += url.auth ? url.auth + '@' : '';
  if (url.hostname && url.hostname.indexOf(':') !== -1) {
    result += '[' + url.hostname + ']';
  } else {
    result += url.hostname || '';
  }
  result += url.port ? ':' + url.port : '';
  result += url.pathname || '';
  result += url.search || '';
  result += url.hash || '';
  return result;
};

},

// node_modules/mdurl/parse.js @127
127: function(__fusereq, exports, module){
'use strict';
function Url() {
  this.protocol = null;
  this.slashes = null;
  this.auth = null;
  this.port = null;
  this.hostname = null;
  this.hash = null;
  this.search = null;
  this.pathname = null;
}
var protocolPattern = /^([a-z0-9.+-]+:)/i, portPattern = /:[0-9]*$/, simplePathPattern = /^(\/\/?(?!\/)[^\?\s]*)(\?[^\s]*)?$/, delims = ['<', '>', '"', '`', ' ', '\r', '\n', '\t'], unwise = ['{', '}', '|', '\\', '^', '`'].concat(delims), autoEscape = ['\''].concat(unwise), nonHostChars = ['%', '/', '?', ';', '#'].concat(autoEscape), hostEndingChars = ['/', '?', '#'], hostnameMaxLen = 255, hostnamePartPattern = /^[+a-z0-9A-Z_-]{0,63}$/, hostnamePartStart = /^([+a-z0-9A-Z_-]{0,63})(.*)$/, hostlessProtocol = {
  'javascript': true,
  'javascript:': true
}, slashedProtocol = {
  'http': true,
  'https': true,
  'ftp': true,
  'gopher': true,
  'file': true,
  'http:': true,
  'https:': true,
  'ftp:': true,
  'gopher:': true,
  'file:': true
};
function urlParse(url, slashesDenoteHost) {
  if (url && url instanceof Url) {
    return url;
  }
  var u = new Url();
  u.parse(url, slashesDenoteHost);
  return u;
}
Url.prototype.parse = function (url, slashesDenoteHost) {
  var i, l, lowerProto, hec, slashes, rest = url;
  rest = rest.trim();
  if (!slashesDenoteHost && url.split('#').length === 1) {
    var simplePath = simplePathPattern.exec(rest);
    if (simplePath) {
      this.pathname = simplePath[1];
      if (simplePath[2]) {
        this.search = simplePath[2];
      }
      return this;
    }
  }
  var proto = protocolPattern.exec(rest);
  if (proto) {
    proto = proto[0];
    lowerProto = proto.toLowerCase();
    this.protocol = proto;
    rest = rest.substr(proto.length);
  }
  if (slashesDenoteHost || proto || rest.match(/^\/\/[^@\/]+@[^@\/]+/)) {
    slashes = rest.substr(0, 2) === '//';
    if (slashes && !(proto && hostlessProtocol[proto])) {
      rest = rest.substr(2);
      this.slashes = true;
    }
  }
  if (!hostlessProtocol[proto] && (slashes || proto && !slashedProtocol[proto])) {
    var hostEnd = -1;
    for (i = 0; i < hostEndingChars.length; i++) {
      hec = rest.indexOf(hostEndingChars[i]);
      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd)) {
        hostEnd = hec;
      }
    }
    var auth, atSign;
    if (hostEnd === -1) {
      atSign = rest.lastIndexOf('@');
    } else {
      atSign = rest.lastIndexOf('@', hostEnd);
    }
    if (atSign !== -1) {
      auth = rest.slice(0, atSign);
      rest = rest.slice(atSign + 1);
      this.auth = auth;
    }
    hostEnd = -1;
    for (i = 0; i < nonHostChars.length; i++) {
      hec = rest.indexOf(nonHostChars[i]);
      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd)) {
        hostEnd = hec;
      }
    }
    if (hostEnd === -1) {
      hostEnd = rest.length;
    }
    if (rest[hostEnd - 1] === ':') {
      hostEnd--;
    }
    var host = rest.slice(0, hostEnd);
    rest = rest.slice(hostEnd);
    this.parseHost(host);
    this.hostname = this.hostname || '';
    var ipv6Hostname = this.hostname[0] === '[' && this.hostname[this.hostname.length - 1] === ']';
    if (!ipv6Hostname) {
      var hostparts = this.hostname.split(/\./);
      for ((i = 0, l = hostparts.length); i < l; i++) {
        var part = hostparts[i];
        if (!part) {
          continue;
        }
        if (!part.match(hostnamePartPattern)) {
          var newpart = '';
          for (var j = 0, k = part.length; j < k; j++) {
            if (part.charCodeAt(j) > 127) {
              newpart += 'x';
            } else {
              newpart += part[j];
            }
          }
          if (!newpart.match(hostnamePartPattern)) {
            var validParts = hostparts.slice(0, i);
            var notHost = hostparts.slice(i + 1);
            var bit = part.match(hostnamePartStart);
            if (bit) {
              validParts.push(bit[1]);
              notHost.unshift(bit[2]);
            }
            if (notHost.length) {
              rest = notHost.join('.') + rest;
            }
            this.hostname = validParts.join('.');
            break;
          }
        }
      }
    }
    if (this.hostname.length > hostnameMaxLen) {
      this.hostname = '';
    }
    if (ipv6Hostname) {
      this.hostname = this.hostname.substr(1, this.hostname.length - 2);
    }
  }
  var hash = rest.indexOf('#');
  if (hash !== -1) {
    this.hash = rest.substr(hash);
    rest = rest.slice(0, hash);
  }
  var qm = rest.indexOf('?');
  if (qm !== -1) {
    this.search = rest.substr(qm);
    rest = rest.slice(0, qm);
  }
  if (rest) {
    this.pathname = rest;
  }
  if (slashedProtocol[lowerProto] && this.hostname && !this.pathname) {
    this.pathname = '';
  }
  return this;
};
Url.prototype.parseHost = function (host) {
  var port = portPattern.exec(host);
  if (port) {
    port = port[0];
    if (port !== ':') {
      this.port = port.substr(1);
    }
    host = host.substr(0, host.length - port.length);
  }
  if (host) {
    this.hostname = host;
  }
};
module.exports = urlParse;

},

// node_modules/markdown-it/lib/common/entities.js @128
128: function(__fusereq, exports, module){
'use strict';
module.exports = __fusereq(135);

},

// node_modules/uc.micro/categories/P/regex.js @129
129: function(__fusereq, exports, module){
module.exports = /[!-#%-\*,-\/:;\?@\[-\]_\{\}\xA1\xA7\xAB\xB6\xB7\xBB\xBF\u037E\u0387\u055A-\u055F\u0589\u058A\u05BE\u05C0\u05C3\u05C6\u05F3\u05F4\u0609\u060A\u060C\u060D\u061B\u061E\u061F\u066A-\u066D\u06D4\u0700-\u070D\u07F7-\u07F9\u0830-\u083E\u085E\u0964\u0965\u0970\u09FD\u0A76\u0AF0\u0C84\u0DF4\u0E4F\u0E5A\u0E5B\u0F04-\u0F12\u0F14\u0F3A-\u0F3D\u0F85\u0FD0-\u0FD4\u0FD9\u0FDA\u104A-\u104F\u10FB\u1360-\u1368\u1400\u166D\u166E\u169B\u169C\u16EB-\u16ED\u1735\u1736\u17D4-\u17D6\u17D8-\u17DA\u1800-\u180A\u1944\u1945\u1A1E\u1A1F\u1AA0-\u1AA6\u1AA8-\u1AAD\u1B5A-\u1B60\u1BFC-\u1BFF\u1C3B-\u1C3F\u1C7E\u1C7F\u1CC0-\u1CC7\u1CD3\u2010-\u2027\u2030-\u2043\u2045-\u2051\u2053-\u205E\u207D\u207E\u208D\u208E\u2308-\u230B\u2329\u232A\u2768-\u2775\u27C5\u27C6\u27E6-\u27EF\u2983-\u2998\u29D8-\u29DB\u29FC\u29FD\u2CF9-\u2CFC\u2CFE\u2CFF\u2D70\u2E00-\u2E2E\u2E30-\u2E4E\u3001-\u3003\u3008-\u3011\u3014-\u301F\u3030\u303D\u30A0\u30FB\uA4FE\uA4FF\uA60D-\uA60F\uA673\uA67E\uA6F2-\uA6F7\uA874-\uA877\uA8CE\uA8CF\uA8F8-\uA8FA\uA8FC\uA92E\uA92F\uA95F\uA9C1-\uA9CD\uA9DE\uA9DF\uAA5C-\uAA5F\uAADE\uAADF\uAAF0\uAAF1\uABEB\uFD3E\uFD3F\uFE10-\uFE19\uFE30-\uFE52\uFE54-\uFE61\uFE63\uFE68\uFE6A\uFE6B\uFF01-\uFF03\uFF05-\uFF0A\uFF0C-\uFF0F\uFF1A\uFF1B\uFF1F\uFF20\uFF3B-\uFF3D\uFF3F\uFF5B\uFF5D\uFF5F-\uFF65]|\uD800[\uDD00-\uDD02\uDF9F\uDFD0]|\uD801\uDD6F|\uD802[\uDC57\uDD1F\uDD3F\uDE50-\uDE58\uDE7F\uDEF0-\uDEF6\uDF39-\uDF3F\uDF99-\uDF9C]|\uD803[\uDF55-\uDF59]|\uD804[\uDC47-\uDC4D\uDCBB\uDCBC\uDCBE-\uDCC1\uDD40-\uDD43\uDD74\uDD75\uDDC5-\uDDC8\uDDCD\uDDDB\uDDDD-\uDDDF\uDE38-\uDE3D\uDEA9]|\uD805[\uDC4B-\uDC4F\uDC5B\uDC5D\uDCC6\uDDC1-\uDDD7\uDE41-\uDE43\uDE60-\uDE6C\uDF3C-\uDF3E]|\uD806[\uDC3B\uDE3F-\uDE46\uDE9A-\uDE9C\uDE9E-\uDEA2]|\uD807[\uDC41-\uDC45\uDC70\uDC71\uDEF7\uDEF8]|\uD809[\uDC70-\uDC74]|\uD81A[\uDE6E\uDE6F\uDEF5\uDF37-\uDF3B\uDF44]|\uD81B[\uDE97-\uDE9A]|\uD82F\uDC9F|\uD836[\uDE87-\uDE8B]|\uD83A[\uDD5E\uDD5F]/;

},

// node_modules/uc.micro/index.js @130
130: function(__fusereq, exports, module){
'use strict';
exports.Any = __fusereq(136);
exports.Cc = __fusereq(137);
exports.Cf = __fusereq(138);
exports.P = __fusereq(129);
exports.Z = __fusereq(139);

},

// node_modules/linkify-it/lib/re.js @131
131: function(__fusereq, exports, module){
'use strict';
module.exports = function (opts) {
  var re = {};
  re.src_Any = __fusereq(136).source;
  re.src_Cc = __fusereq(137).source;
  re.src_Z = __fusereq(139).source;
  re.src_P = __fusereq(129).source;
  re.src_ZPCc = [re.src_Z, re.src_P, re.src_Cc].join('|');
  re.src_ZCc = [re.src_Z, re.src_Cc].join('|');
  var text_separators = '[><\uff5c]';
  re.src_pseudo_letter = '(?:(?!' + text_separators + '|' + re.src_ZPCc + ')' + re.src_Any + ')';
  re.src_ip4 = '(?:(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)';
  re.src_auth = '(?:(?:(?!' + re.src_ZCc + '|[@/\\[\\]()]).)+@)?';
  re.src_port = '(?::(?:6(?:[0-4]\\d{3}|5(?:[0-4]\\d{2}|5(?:[0-2]\\d|3[0-5])))|[1-5]?\\d{1,4}))?';
  re.src_host_terminator = '(?=$|' + text_separators + '|' + re.src_ZPCc + ')(?!-|_|:\\d|\\.-|\\.(?!$|' + re.src_ZPCc + '))';
  re.src_path = '(?:' + '[/?#]' + '(?:' + '(?!' + re.src_ZCc + '|' + text_separators + '|[()[\\]{}.,"\'?!\\-]).|' + '\\[(?:(?!' + re.src_ZCc + '|\\]).)*\\]|' + '\\((?:(?!' + re.src_ZCc + '|[)]).)*\\)|' + '\\{(?:(?!' + re.src_ZCc + '|[}]).)*\\}|' + '\\"(?:(?!' + re.src_ZCc + '|["]).)+\\"|' + "\\'(?:(?!" + re.src_ZCc + "|[']).)+\\'|" + "\\'(?=" + re.src_pseudo_letter + '|[-]).|' + '\\.{2,4}[a-zA-Z0-9%/]|' + '\\.(?!' + re.src_ZCc + '|[.]).|' + (opts && opts['---'] ? '\\-(?!--(?:[^-]|$))(?:-*)|' : '\\-+|') + '\\,(?!' + re.src_ZCc + ').|' + '\\!(?!' + re.src_ZCc + '|[!]).|' + '\\?(?!' + re.src_ZCc + '|[?]).' + ')+' + '|\\/' + ')?';
  re.src_email_name = '[\\-;:&=\\+\\$,\\.a-zA-Z0-9_][\\-;:&=\\+\\$,\\"\\.a-zA-Z0-9_]*';
  re.src_xn = 'xn--[a-z0-9\\-]{1,59}';
  re.src_domain_root = '(?:' + re.src_xn + '|' + re.src_pseudo_letter + '{1,63}' + ')';
  re.src_domain = '(?:' + re.src_xn + '|' + '(?:' + re.src_pseudo_letter + ')' + '|' + '(?:' + re.src_pseudo_letter + '(?:-|' + re.src_pseudo_letter + '){0,61}' + re.src_pseudo_letter + ')' + ')';
  re.src_host = '(?:' + '(?:(?:(?:' + re.src_domain + ')\\.)*' + re.src_domain + ')' + ')';
  re.tpl_host_fuzzy = '(?:' + re.src_ip4 + '|' + '(?:(?:(?:' + re.src_domain + ')\\.)+(?:%TLDS%))' + ')';
  re.tpl_host_no_ip_fuzzy = '(?:(?:(?:' + re.src_domain + ')\\.)+(?:%TLDS%))';
  re.src_host_strict = re.src_host + re.src_host_terminator;
  re.tpl_host_fuzzy_strict = re.tpl_host_fuzzy + re.src_host_terminator;
  re.src_host_port_strict = re.src_host + re.src_port + re.src_host_terminator;
  re.tpl_host_port_fuzzy_strict = re.tpl_host_fuzzy + re.src_port + re.src_host_terminator;
  re.tpl_host_port_no_ip_fuzzy_strict = re.tpl_host_no_ip_fuzzy + re.src_port + re.src_host_terminator;
  re.tpl_host_fuzzy_test = 'localhost|www\\.|\\.\\d{1,3}\\.|(?:\\.(?:%TLDS%)(?:' + re.src_ZPCc + '|>|$))';
  re.tpl_email_fuzzy = '(^|' + text_separators + '|"|\\(|' + re.src_ZCc + ')' + '(' + re.src_email_name + '@' + re.tpl_host_fuzzy_strict + ')';
  re.tpl_link_fuzzy = '(^|(?![.:/\\-_@])(?:[$+<=>^`|\uff5c]|' + re.src_ZPCc + '))' + '((?![$+<=>^`|\uff5c])' + re.tpl_host_port_fuzzy_strict + re.src_path + ')';
  re.tpl_link_no_ip_fuzzy = '(^|(?![.:/\\-_@])(?:[$+<=>^`|\uff5c]|' + re.src_ZPCc + '))' + '((?![$+<=>^`|\uff5c])' + re.tpl_host_port_no_ip_fuzzy_strict + re.src_path + ')';
  return re;
};

},

// node_modules/markdown-it/lib/token.js @132
132: function(__fusereq, exports, module){
'use strict';
function Token(type, tag, nesting) {
  this.type = type;
  this.tag = tag;
  this.attrs = null;
  this.map = null;
  this.nesting = nesting;
  this.level = 0;
  this.children = null;
  this.content = '';
  this.markup = '';
  this.info = '';
  this.meta = null;
  this.block = false;
  this.hidden = false;
}
Token.prototype.attrIndex = function attrIndex(name) {
  var attrs, i, len;
  if (!this.attrs) {
    return -1;
  }
  attrs = this.attrs;
  for ((i = 0, len = attrs.length); i < len; i++) {
    if (attrs[i][0] === name) {
      return i;
    }
  }
  return -1;
};
Token.prototype.attrPush = function attrPush(attrData) {
  if (this.attrs) {
    this.attrs.push(attrData);
  } else {
    this.attrs = [attrData];
  }
};
Token.prototype.attrSet = function attrSet(name, value) {
  var idx = this.attrIndex(name), attrData = [name, value];
  if (idx < 0) {
    this.attrPush(attrData);
  } else {
    this.attrs[idx] = attrData;
  }
};
Token.prototype.attrGet = function attrGet(name) {
  var idx = this.attrIndex(name), value = null;
  if (idx >= 0) {
    value = this.attrs[idx][1];
  }
  return value;
};
Token.prototype.attrJoin = function attrJoin(name, value) {
  var idx = this.attrIndex(name);
  if (idx < 0) {
    this.attrPush([name, value]);
  } else {
    this.attrs[idx][1] = this.attrs[idx][1] + ' ' + value;
  }
};
module.exports = Token;

},

// node_modules/markdown-it/lib/common/html_blocks.js @133
133: function(__fusereq, exports, module){
'use strict';
module.exports = ['address', 'article', 'aside', 'base', 'basefont', 'blockquote', 'body', 'caption', 'center', 'col', 'colgroup', 'dd', 'details', 'dialog', 'dir', 'div', 'dl', 'dt', 'fieldset', 'figcaption', 'figure', 'footer', 'form', 'frame', 'frameset', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head', 'header', 'hr', 'html', 'iframe', 'legend', 'li', 'link', 'main', 'menu', 'menuitem', 'meta', 'nav', 'noframes', 'ol', 'optgroup', 'option', 'p', 'param', 'section', 'source', 'summary', 'table', 'tbody', 'td', 'tfoot', 'th', 'thead', 'title', 'tr', 'track', 'ul'];

},

// node_modules/markdown-it/lib/common/html_re.js @134
134: function(__fusereq, exports, module){
'use strict';
var attr_name = '[a-zA-Z_:][a-zA-Z0-9:._-]*';
var unquoted = '[^"\'=<>`\\x00-\\x20]+';
var single_quoted = "'[^']*'";
var double_quoted = '"[^"]*"';
var attr_value = '(?:' + unquoted + '|' + single_quoted + '|' + double_quoted + ')';
var attribute = '(?:\\s+' + attr_name + '(?:\\s*=\\s*' + attr_value + ')?)';
var open_tag = '<[A-Za-z][A-Za-z0-9\\-]*' + attribute + '*\\s*\\/?>';
var close_tag = '<\\/[A-Za-z][A-Za-z0-9\\-]*\\s*>';
var comment = '<!---->|<!--(?:-?[^>-])(?:-?[^-])*-->';
var processing = '<[?].*?[?]>';
var declaration = '<![A-Z]+\\s+[^>]*>';
var cdata = '<!\\[CDATA\\[[\\s\\S]*?\\]\\]>';
var HTML_TAG_RE = new RegExp('^(?:' + open_tag + '|' + close_tag + '|' + comment + '|' + processing + '|' + declaration + '|' + cdata + ')');
var HTML_OPEN_CLOSE_TAG_RE = new RegExp('^(?:' + open_tag + '|' + close_tag + ')');
module.exports.HTML_TAG_RE = HTML_TAG_RE;
module.exports.HTML_OPEN_CLOSE_TAG_RE = HTML_OPEN_CLOSE_TAG_RE;

},

// node_modules/entities/lib/maps/entities.json @135
135: function(__fusereq, exports, module){
module.exports = { "Aacute": "\u00C1", "aacute": "\u00E1", "Abreve": "\u0102", "abreve": "\u0103", "ac": "\u223E", "acd": "\u223F", "acE": "\u223E\u0333", "Acirc": "\u00C2", "acirc": "\u00E2", "acute": "\u00B4", "Acy": "\u0410", "acy": "\u0430", "AElig": "\u00C6", "aelig": "\u00E6", "af": "\u2061", "Afr": "\uD835\uDD04", "afr": "\uD835\uDD1E", "Agrave": "\u00C0", "agrave": "\u00E0", "alefsym": "\u2135", "aleph": "\u2135", "Alpha": "\u0391", "alpha": "\u03B1", "Amacr": "\u0100", "amacr": "\u0101", "amalg": "\u2A3F", "amp": "&", "AMP": "&", "andand": "\u2A55", "And": "\u2A53", "and": "\u2227", "andd": "\u2A5C", "andslope": "\u2A58", "andv": "\u2A5A", "ang": "\u2220", "ange": "\u29A4", "angle": "\u2220", "angmsdaa": "\u29A8", "angmsdab": "\u29A9", "angmsdac": "\u29AA", "angmsdad": "\u29AB", "angmsdae": "\u29AC", "angmsdaf": "\u29AD", "angmsdag": "\u29AE", "angmsdah": "\u29AF", "angmsd": "\u2221", "angrt": "\u221F", "angrtvb": "\u22BE", "angrtvbd": "\u299D", "angsph": "\u2222", "angst": "\u00C5", "angzarr": "\u237C", "Aogon": "\u0104", "aogon": "\u0105", "Aopf": "\uD835\uDD38", "aopf": "\uD835\uDD52", "apacir": "\u2A6F", "ap": "\u2248", "apE": "\u2A70", "ape": "\u224A", "apid": "\u224B", "apos": "'", "ApplyFunction": "\u2061", "approx": "\u2248", "approxeq": "\u224A", "Aring": "\u00C5", "aring": "\u00E5", "Ascr": "\uD835\uDC9C", "ascr": "\uD835\uDCB6", "Assign": "\u2254", "ast": "*", "asymp": "\u2248", "asympeq": "\u224D", "Atilde": "\u00C3", "atilde": "\u00E3", "Auml": "\u00C4", "auml": "\u00E4", "awconint": "\u2233", "awint": "\u2A11", "backcong": "\u224C", "backepsilon": "\u03F6", "backprime": "\u2035", "backsim": "\u223D", "backsimeq": "\u22CD", "Backslash": "\u2216", "Barv": "\u2AE7", "barvee": "\u22BD", "barwed": "\u2305", "Barwed": "\u2306", "barwedge": "\u2305", "bbrk": "\u23B5", "bbrktbrk": "\u23B6", "bcong": "\u224C", "Bcy": "\u0411", "bcy": "\u0431", "bdquo": "\u201E", "becaus": "\u2235", "because": "\u2235", "Because": "\u2235", "bemptyv": "\u29B0", "bepsi": "\u03F6", "bernou": "\u212C", "Bernoullis": "\u212C", "Beta": "\u0392", "beta": "\u03B2", "beth": "\u2136", "between": "\u226C", "Bfr": "\uD835\uDD05", "bfr": "\uD835\uDD1F", "bigcap": "\u22C2", "bigcirc": "\u25EF", "bigcup": "\u22C3", "bigodot": "\u2A00", "bigoplus": "\u2A01", "bigotimes": "\u2A02", "bigsqcup": "\u2A06", "bigstar": "\u2605", "bigtriangledown": "\u25BD", "bigtriangleup": "\u25B3", "biguplus": "\u2A04", "bigvee": "\u22C1", "bigwedge": "\u22C0", "bkarow": "\u290D", "blacklozenge": "\u29EB", "blacksquare": "\u25AA", "blacktriangle": "\u25B4", "blacktriangledown": "\u25BE", "blacktriangleleft": "\u25C2", "blacktriangleright": "\u25B8", "blank": "\u2423", "blk12": "\u2592", "blk14": "\u2591", "blk34": "\u2593", "block": "\u2588", "bne": "=\u20E5", "bnequiv": "\u2261\u20E5", "bNot": "\u2AED", "bnot": "\u2310", "Bopf": "\uD835\uDD39", "bopf": "\uD835\uDD53", "bot": "\u22A5", "bottom": "\u22A5", "bowtie": "\u22C8", "boxbox": "\u29C9", "boxdl": "\u2510", "boxdL": "\u2555", "boxDl": "\u2556", "boxDL": "\u2557", "boxdr": "\u250C", "boxdR": "\u2552", "boxDr": "\u2553", "boxDR": "\u2554", "boxh": "\u2500", "boxH": "\u2550", "boxhd": "\u252C", "boxHd": "\u2564", "boxhD": "\u2565", "boxHD": "\u2566", "boxhu": "\u2534", "boxHu": "\u2567", "boxhU": "\u2568", "boxHU": "\u2569", "boxminus": "\u229F", "boxplus": "\u229E", "boxtimes": "\u22A0", "boxul": "\u2518", "boxuL": "\u255B", "boxUl": "\u255C", "boxUL": "\u255D", "boxur": "\u2514", "boxuR": "\u2558", "boxUr": "\u2559", "boxUR": "\u255A", "boxv": "\u2502", "boxV": "\u2551", "boxvh": "\u253C", "boxvH": "\u256A", "boxVh": "\u256B", "boxVH": "\u256C", "boxvl": "\u2524", "boxvL": "\u2561", "boxVl": "\u2562", "boxVL": "\u2563", "boxvr": "\u251C", "boxvR": "\u255E", "boxVr": "\u255F", "boxVR": "\u2560", "bprime": "\u2035", "breve": "\u02D8", "Breve": "\u02D8", "brvbar": "\u00A6", "bscr": "\uD835\uDCB7", "Bscr": "\u212C", "bsemi": "\u204F", "bsim": "\u223D", "bsime": "\u22CD", "bsolb": "\u29C5", "bsol": "\\", "bsolhsub": "\u27C8", "bull": "\u2022", "bullet": "\u2022", "bump": "\u224E", "bumpE": "\u2AAE", "bumpe": "\u224F", "Bumpeq": "\u224E", "bumpeq": "\u224F", "Cacute": "\u0106", "cacute": "\u0107", "capand": "\u2A44", "capbrcup": "\u2A49", "capcap": "\u2A4B", "cap": "\u2229", "Cap": "\u22D2", "capcup": "\u2A47", "capdot": "\u2A40", "CapitalDifferentialD": "\u2145", "caps": "\u2229\uFE00", "caret": "\u2041", "caron": "\u02C7", "Cayleys": "\u212D", "ccaps": "\u2A4D", "Ccaron": "\u010C", "ccaron": "\u010D", "Ccedil": "\u00C7", "ccedil": "\u00E7", "Ccirc": "\u0108", "ccirc": "\u0109", "Cconint": "\u2230", "ccups": "\u2A4C", "ccupssm": "\u2A50", "Cdot": "\u010A", "cdot": "\u010B", "cedil": "\u00B8", "Cedilla": "\u00B8", "cemptyv": "\u29B2", "cent": "\u00A2", "centerdot": "\u00B7", "CenterDot": "\u00B7", "cfr": "\uD835\uDD20", "Cfr": "\u212D", "CHcy": "\u0427", "chcy": "\u0447", "check": "\u2713", "checkmark": "\u2713", "Chi": "\u03A7", "chi": "\u03C7", "circ": "\u02C6", "circeq": "\u2257", "circlearrowleft": "\u21BA", "circlearrowright": "\u21BB", "circledast": "\u229B", "circledcirc": "\u229A", "circleddash": "\u229D", "CircleDot": "\u2299", "circledR": "\u00AE", "circledS": "\u24C8", "CircleMinus": "\u2296", "CirclePlus": "\u2295", "CircleTimes": "\u2297", "cir": "\u25CB", "cirE": "\u29C3", "cire": "\u2257", "cirfnint": "\u2A10", "cirmid": "\u2AEF", "cirscir": "\u29C2", "ClockwiseContourIntegral": "\u2232", "CloseCurlyDoubleQuote": "\u201D", "CloseCurlyQuote": "\u2019", "clubs": "\u2663", "clubsuit": "\u2663", "colon": ":", "Colon": "\u2237", "Colone": "\u2A74", "colone": "\u2254", "coloneq": "\u2254", "comma": ",", "commat": "@", "comp": "\u2201", "compfn": "\u2218", "complement": "\u2201", "complexes": "\u2102", "cong": "\u2245", "congdot": "\u2A6D", "Congruent": "\u2261", "conint": "\u222E", "Conint": "\u222F", "ContourIntegral": "\u222E", "copf": "\uD835\uDD54", "Copf": "\u2102", "coprod": "\u2210", "Coproduct": "\u2210", "copy": "\u00A9", "COPY": "\u00A9", "copysr": "\u2117", "CounterClockwiseContourIntegral": "\u2233", "crarr": "\u21B5", "cross": "\u2717", "Cross": "\u2A2F", "Cscr": "\uD835\uDC9E", "cscr": "\uD835\uDCB8", "csub": "\u2ACF", "csube": "\u2AD1", "csup": "\u2AD0", "csupe": "\u2AD2", "ctdot": "\u22EF", "cudarrl": "\u2938", "cudarrr": "\u2935", "cuepr": "\u22DE", "cuesc": "\u22DF", "cularr": "\u21B6", "cularrp": "\u293D", "cupbrcap": "\u2A48", "cupcap": "\u2A46", "CupCap": "\u224D", "cup": "\u222A", "Cup": "\u22D3", "cupcup": "\u2A4A", "cupdot": "\u228D", "cupor": "\u2A45", "cups": "\u222A\uFE00", "curarr": "\u21B7", "curarrm": "\u293C", "curlyeqprec": "\u22DE", "curlyeqsucc": "\u22DF", "curlyvee": "\u22CE", "curlywedge": "\u22CF", "curren": "\u00A4", "curvearrowleft": "\u21B6", "curvearrowright": "\u21B7", "cuvee": "\u22CE", "cuwed": "\u22CF", "cwconint": "\u2232", "cwint": "\u2231", "cylcty": "\u232D", "dagger": "\u2020", "Dagger": "\u2021", "daleth": "\u2138", "darr": "\u2193", "Darr": "\u21A1", "dArr": "\u21D3", "dash": "\u2010", "Dashv": "\u2AE4", "dashv": "\u22A3", "dbkarow": "\u290F", "dblac": "\u02DD", "Dcaron": "\u010E", "dcaron": "\u010F", "Dcy": "\u0414", "dcy": "\u0434", "ddagger": "\u2021", "ddarr": "\u21CA", "DD": "\u2145", "dd": "\u2146", "DDotrahd": "\u2911", "ddotseq": "\u2A77", "deg": "\u00B0", "Del": "\u2207", "Delta": "\u0394", "delta": "\u03B4", "demptyv": "\u29B1", "dfisht": "\u297F", "Dfr": "\uD835\uDD07", "dfr": "\uD835\uDD21", "dHar": "\u2965", "dharl": "\u21C3", "dharr": "\u21C2", "DiacriticalAcute": "\u00B4", "DiacriticalDot": "\u02D9", "DiacriticalDoubleAcute": "\u02DD", "DiacriticalGrave": "`", "DiacriticalTilde": "\u02DC", "diam": "\u22C4", "diamond": "\u22C4", "Diamond": "\u22C4", "diamondsuit": "\u2666", "diams": "\u2666", "die": "\u00A8", "DifferentialD": "\u2146", "digamma": "\u03DD", "disin": "\u22F2", "div": "\u00F7", "divide": "\u00F7", "divideontimes": "\u22C7", "divonx": "\u22C7", "DJcy": "\u0402", "djcy": "\u0452", "dlcorn": "\u231E", "dlcrop": "\u230D", "dollar": "$", "Dopf": "\uD835\uDD3B", "dopf": "\uD835\uDD55", "Dot": "\u00A8", "dot": "\u02D9", "DotDot": "\u20DC", "doteq": "\u2250", "doteqdot": "\u2251", "DotEqual": "\u2250", "dotminus": "\u2238", "dotplus": "\u2214", "dotsquare": "\u22A1", "doublebarwedge": "\u2306", "DoubleContourIntegral": "\u222F", "DoubleDot": "\u00A8", "DoubleDownArrow": "\u21D3", "DoubleLeftArrow": "\u21D0", "DoubleLeftRightArrow": "\u21D4", "DoubleLeftTee": "\u2AE4", "DoubleLongLeftArrow": "\u27F8", "DoubleLongLeftRightArrow": "\u27FA", "DoubleLongRightArrow": "\u27F9", "DoubleRightArrow": "\u21D2", "DoubleRightTee": "\u22A8", "DoubleUpArrow": "\u21D1", "DoubleUpDownArrow": "\u21D5", "DoubleVerticalBar": "\u2225", "DownArrowBar": "\u2913", "downarrow": "\u2193", "DownArrow": "\u2193", "Downarrow": "\u21D3", "DownArrowUpArrow": "\u21F5", "DownBreve": "\u0311", "downdownarrows": "\u21CA", "downharpoonleft": "\u21C3", "downharpoonright": "\u21C2", "DownLeftRightVector": "\u2950", "DownLeftTeeVector": "\u295E", "DownLeftVectorBar": "\u2956", "DownLeftVector": "\u21BD", "DownRightTeeVector": "\u295F", "DownRightVectorBar": "\u2957", "DownRightVector": "\u21C1", "DownTeeArrow": "\u21A7", "DownTee": "\u22A4", "drbkarow": "\u2910", "drcorn": "\u231F", "drcrop": "\u230C", "Dscr": "\uD835\uDC9F", "dscr": "\uD835\uDCB9", "DScy": "\u0405", "dscy": "\u0455", "dsol": "\u29F6", "Dstrok": "\u0110", "dstrok": "\u0111", "dtdot": "\u22F1", "dtri": "\u25BF", "dtrif": "\u25BE", "duarr": "\u21F5", "duhar": "\u296F", "dwangle": "\u29A6", "DZcy": "\u040F", "dzcy": "\u045F", "dzigrarr": "\u27FF", "Eacute": "\u00C9", "eacute": "\u00E9", "easter": "\u2A6E", "Ecaron": "\u011A", "ecaron": "\u011B", "Ecirc": "\u00CA", "ecirc": "\u00EA", "ecir": "\u2256", "ecolon": "\u2255", "Ecy": "\u042D", "ecy": "\u044D", "eDDot": "\u2A77", "Edot": "\u0116", "edot": "\u0117", "eDot": "\u2251", "ee": "\u2147", "efDot": "\u2252", "Efr": "\uD835\uDD08", "efr": "\uD835\uDD22", "eg": "\u2A9A", "Egrave": "\u00C8", "egrave": "\u00E8", "egs": "\u2A96", "egsdot": "\u2A98", "el": "\u2A99", "Element": "\u2208", "elinters": "\u23E7", "ell": "\u2113", "els": "\u2A95", "elsdot": "\u2A97", "Emacr": "\u0112", "emacr": "\u0113", "empty": "\u2205", "emptyset": "\u2205", "EmptySmallSquare": "\u25FB", "emptyv": "\u2205", "EmptyVerySmallSquare": "\u25AB", "emsp13": "\u2004", "emsp14": "\u2005", "emsp": "\u2003", "ENG": "\u014A", "eng": "\u014B", "ensp": "\u2002", "Eogon": "\u0118", "eogon": "\u0119", "Eopf": "\uD835\uDD3C", "eopf": "\uD835\uDD56", "epar": "\u22D5", "eparsl": "\u29E3", "eplus": "\u2A71", "epsi": "\u03B5", "Epsilon": "\u0395", "epsilon": "\u03B5", "epsiv": "\u03F5", "eqcirc": "\u2256", "eqcolon": "\u2255", "eqsim": "\u2242", "eqslantgtr": "\u2A96", "eqslantless": "\u2A95", "Equal": "\u2A75", "equals": "=", "EqualTilde": "\u2242", "equest": "\u225F", "Equilibrium": "\u21CC", "equiv": "\u2261", "equivDD": "\u2A78", "eqvparsl": "\u29E5", "erarr": "\u2971", "erDot": "\u2253", "escr": "\u212F", "Escr": "\u2130", "esdot": "\u2250", "Esim": "\u2A73", "esim": "\u2242", "Eta": "\u0397", "eta": "\u03B7", "ETH": "\u00D0", "eth": "\u00F0", "Euml": "\u00CB", "euml": "\u00EB", "euro": "\u20AC", "excl": "!", "exist": "\u2203", "Exists": "\u2203", "expectation": "\u2130", "exponentiale": "\u2147", "ExponentialE": "\u2147", "fallingdotseq": "\u2252", "Fcy": "\u0424", "fcy": "\u0444", "female": "\u2640", "ffilig": "\uFB03", "fflig": "\uFB00", "ffllig": "\uFB04", "Ffr": "\uD835\uDD09", "ffr": "\uD835\uDD23", "filig": "\uFB01", "FilledSmallSquare": "\u25FC", "FilledVerySmallSquare": "\u25AA", "fjlig": "fj", "flat": "\u266D", "fllig": "\uFB02", "fltns": "\u25B1", "fnof": "\u0192", "Fopf": "\uD835\uDD3D", "fopf": "\uD835\uDD57", "forall": "\u2200", "ForAll": "\u2200", "fork": "\u22D4", "forkv": "\u2AD9", "Fouriertrf": "\u2131", "fpartint": "\u2A0D", "frac12": "\u00BD", "frac13": "\u2153", "frac14": "\u00BC", "frac15": "\u2155", "frac16": "\u2159", "frac18": "\u215B", "frac23": "\u2154", "frac25": "\u2156", "frac34": "\u00BE", "frac35": "\u2157", "frac38": "\u215C", "frac45": "\u2158", "frac56": "\u215A", "frac58": "\u215D", "frac78": "\u215E", "frasl": "\u2044", "frown": "\u2322", "fscr": "\uD835\uDCBB", "Fscr": "\u2131", "gacute": "\u01F5", "Gamma": "\u0393", "gamma": "\u03B3", "Gammad": "\u03DC", "gammad": "\u03DD", "gap": "\u2A86", "Gbreve": "\u011E", "gbreve": "\u011F", "Gcedil": "\u0122", "Gcirc": "\u011C", "gcirc": "\u011D", "Gcy": "\u0413", "gcy": "\u0433", "Gdot": "\u0120", "gdot": "\u0121", "ge": "\u2265", "gE": "\u2267", "gEl": "\u2A8C", "gel": "\u22DB", "geq": "\u2265", "geqq": "\u2267", "geqslant": "\u2A7E", "gescc": "\u2AA9", "ges": "\u2A7E", "gesdot": "\u2A80", "gesdoto": "\u2A82", "gesdotol": "\u2A84", "gesl": "\u22DB\uFE00", "gesles": "\u2A94", "Gfr": "\uD835\uDD0A", "gfr": "\uD835\uDD24", "gg": "\u226B", "Gg": "\u22D9", "ggg": "\u22D9", "gimel": "\u2137", "GJcy": "\u0403", "gjcy": "\u0453", "gla": "\u2AA5", "gl": "\u2277", "glE": "\u2A92", "glj": "\u2AA4", "gnap": "\u2A8A", "gnapprox": "\u2A8A", "gne": "\u2A88", "gnE": "\u2269", "gneq": "\u2A88", "gneqq": "\u2269", "gnsim": "\u22E7", "Gopf": "\uD835\uDD3E", "gopf": "\uD835\uDD58", "grave": "`", "GreaterEqual": "\u2265", "GreaterEqualLess": "\u22DB", "GreaterFullEqual": "\u2267", "GreaterGreater": "\u2AA2", "GreaterLess": "\u2277", "GreaterSlantEqual": "\u2A7E", "GreaterTilde": "\u2273", "Gscr": "\uD835\uDCA2", "gscr": "\u210A", "gsim": "\u2273", "gsime": "\u2A8E", "gsiml": "\u2A90", "gtcc": "\u2AA7", "gtcir": "\u2A7A", "gt": ">", "GT": ">", "Gt": "\u226B", "gtdot": "\u22D7", "gtlPar": "\u2995", "gtquest": "\u2A7C", "gtrapprox": "\u2A86", "gtrarr": "\u2978", "gtrdot": "\u22D7", "gtreqless": "\u22DB", "gtreqqless": "\u2A8C", "gtrless": "\u2277", "gtrsim": "\u2273", "gvertneqq": "\u2269\uFE00", "gvnE": "\u2269\uFE00", "Hacek": "\u02C7", "hairsp": "\u200A", "half": "\u00BD", "hamilt": "\u210B", "HARDcy": "\u042A", "hardcy": "\u044A", "harrcir": "\u2948", "harr": "\u2194", "hArr": "\u21D4", "harrw": "\u21AD", "Hat": "^", "hbar": "\u210F", "Hcirc": "\u0124", "hcirc": "\u0125", "hearts": "\u2665", "heartsuit": "\u2665", "hellip": "\u2026", "hercon": "\u22B9", "hfr": "\uD835\uDD25", "Hfr": "\u210C", "HilbertSpace": "\u210B", "hksearow": "\u2925", "hkswarow": "\u2926", "hoarr": "\u21FF", "homtht": "\u223B", "hookleftarrow": "\u21A9", "hookrightarrow": "\u21AA", "hopf": "\uD835\uDD59", "Hopf": "\u210D", "horbar": "\u2015", "HorizontalLine": "\u2500", "hscr": "\uD835\uDCBD", "Hscr": "\u210B", "hslash": "\u210F", "Hstrok": "\u0126", "hstrok": "\u0127", "HumpDownHump": "\u224E", "HumpEqual": "\u224F", "hybull": "\u2043", "hyphen": "\u2010", "Iacute": "\u00CD", "iacute": "\u00ED", "ic": "\u2063", "Icirc": "\u00CE", "icirc": "\u00EE", "Icy": "\u0418", "icy": "\u0438", "Idot": "\u0130", "IEcy": "\u0415", "iecy": "\u0435", "iexcl": "\u00A1", "iff": "\u21D4", "ifr": "\uD835\uDD26", "Ifr": "\u2111", "Igrave": "\u00CC", "igrave": "\u00EC", "ii": "\u2148", "iiiint": "\u2A0C", "iiint": "\u222D", "iinfin": "\u29DC", "iiota": "\u2129", "IJlig": "\u0132", "ijlig": "\u0133", "Imacr": "\u012A", "imacr": "\u012B", "image": "\u2111", "ImaginaryI": "\u2148", "imagline": "\u2110", "imagpart": "\u2111", "imath": "\u0131", "Im": "\u2111", "imof": "\u22B7", "imped": "\u01B5", "Implies": "\u21D2", "incare": "\u2105", "in": "\u2208", "infin": "\u221E", "infintie": "\u29DD", "inodot": "\u0131", "intcal": "\u22BA", "int": "\u222B", "Int": "\u222C", "integers": "\u2124", "Integral": "\u222B", "intercal": "\u22BA", "Intersection": "\u22C2", "intlarhk": "\u2A17", "intprod": "\u2A3C", "InvisibleComma": "\u2063", "InvisibleTimes": "\u2062", "IOcy": "\u0401", "iocy": "\u0451", "Iogon": "\u012E", "iogon": "\u012F", "Iopf": "\uD835\uDD40", "iopf": "\uD835\uDD5A", "Iota": "\u0399", "iota": "\u03B9", "iprod": "\u2A3C", "iquest": "\u00BF", "iscr": "\uD835\uDCBE", "Iscr": "\u2110", "isin": "\u2208", "isindot": "\u22F5", "isinE": "\u22F9", "isins": "\u22F4", "isinsv": "\u22F3", "isinv": "\u2208", "it": "\u2062", "Itilde": "\u0128", "itilde": "\u0129", "Iukcy": "\u0406", "iukcy": "\u0456", "Iuml": "\u00CF", "iuml": "\u00EF", "Jcirc": "\u0134", "jcirc": "\u0135", "Jcy": "\u0419", "jcy": "\u0439", "Jfr": "\uD835\uDD0D", "jfr": "\uD835\uDD27", "jmath": "\u0237", "Jopf": "\uD835\uDD41", "jopf": "\uD835\uDD5B", "Jscr": "\uD835\uDCA5", "jscr": "\uD835\uDCBF", "Jsercy": "\u0408", "jsercy": "\u0458", "Jukcy": "\u0404", "jukcy": "\u0454", "Kappa": "\u039A", "kappa": "\u03BA", "kappav": "\u03F0", "Kcedil": "\u0136", "kcedil": "\u0137", "Kcy": "\u041A", "kcy": "\u043A", "Kfr": "\uD835\uDD0E", "kfr": "\uD835\uDD28", "kgreen": "\u0138", "KHcy": "\u0425", "khcy": "\u0445", "KJcy": "\u040C", "kjcy": "\u045C", "Kopf": "\uD835\uDD42", "kopf": "\uD835\uDD5C", "Kscr": "\uD835\uDCA6", "kscr": "\uD835\uDCC0", "lAarr": "\u21DA", "Lacute": "\u0139", "lacute": "\u013A", "laemptyv": "\u29B4", "lagran": "\u2112", "Lambda": "\u039B", "lambda": "\u03BB", "lang": "\u27E8", "Lang": "\u27EA", "langd": "\u2991", "langle": "\u27E8", "lap": "\u2A85", "Laplacetrf": "\u2112", "laquo": "\u00AB", "larrb": "\u21E4", "larrbfs": "\u291F", "larr": "\u2190", "Larr": "\u219E", "lArr": "\u21D0", "larrfs": "\u291D", "larrhk": "\u21A9", "larrlp": "\u21AB", "larrpl": "\u2939", "larrsim": "\u2973", "larrtl": "\u21A2", "latail": "\u2919", "lAtail": "\u291B", "lat": "\u2AAB", "late": "\u2AAD", "lates": "\u2AAD\uFE00", "lbarr": "\u290C", "lBarr": "\u290E", "lbbrk": "\u2772", "lbrace": "{", "lbrack": "[", "lbrke": "\u298B", "lbrksld": "\u298F", "lbrkslu": "\u298D", "Lcaron": "\u013D", "lcaron": "\u013E", "Lcedil": "\u013B", "lcedil": "\u013C", "lceil": "\u2308", "lcub": "{", "Lcy": "\u041B", "lcy": "\u043B", "ldca": "\u2936", "ldquo": "\u201C", "ldquor": "\u201E", "ldrdhar": "\u2967", "ldrushar": "\u294B", "ldsh": "\u21B2", "le": "\u2264", "lE": "\u2266", "LeftAngleBracket": "\u27E8", "LeftArrowBar": "\u21E4", "leftarrow": "\u2190", "LeftArrow": "\u2190", "Leftarrow": "\u21D0", "LeftArrowRightArrow": "\u21C6", "leftarrowtail": "\u21A2", "LeftCeiling": "\u2308", "LeftDoubleBracket": "\u27E6", "LeftDownTeeVector": "\u2961", "LeftDownVectorBar": "\u2959", "LeftDownVector": "\u21C3", "LeftFloor": "\u230A", "leftharpoondown": "\u21BD", "leftharpoonup": "\u21BC", "leftleftarrows": "\u21C7", "leftrightarrow": "\u2194", "LeftRightArrow": "\u2194", "Leftrightarrow": "\u21D4", "leftrightarrows": "\u21C6", "leftrightharpoons": "\u21CB", "leftrightsquigarrow": "\u21AD", "LeftRightVector": "\u294E", "LeftTeeArrow": "\u21A4", "LeftTee": "\u22A3", "LeftTeeVector": "\u295A", "leftthreetimes": "\u22CB", "LeftTriangleBar": "\u29CF", "LeftTriangle": "\u22B2", "LeftTriangleEqual": "\u22B4", "LeftUpDownVector": "\u2951", "LeftUpTeeVector": "\u2960", "LeftUpVectorBar": "\u2958", "LeftUpVector": "\u21BF", "LeftVectorBar": "\u2952", "LeftVector": "\u21BC", "lEg": "\u2A8B", "leg": "\u22DA", "leq": "\u2264", "leqq": "\u2266", "leqslant": "\u2A7D", "lescc": "\u2AA8", "les": "\u2A7D", "lesdot": "\u2A7F", "lesdoto": "\u2A81", "lesdotor": "\u2A83", "lesg": "\u22DA\uFE00", "lesges": "\u2A93", "lessapprox": "\u2A85", "lessdot": "\u22D6", "lesseqgtr": "\u22DA", "lesseqqgtr": "\u2A8B", "LessEqualGreater": "\u22DA", "LessFullEqual": "\u2266", "LessGreater": "\u2276", "lessgtr": "\u2276", "LessLess": "\u2AA1", "lesssim": "\u2272", "LessSlantEqual": "\u2A7D", "LessTilde": "\u2272", "lfisht": "\u297C", "lfloor": "\u230A", "Lfr": "\uD835\uDD0F", "lfr": "\uD835\uDD29", "lg": "\u2276", "lgE": "\u2A91", "lHar": "\u2962", "lhard": "\u21BD", "lharu": "\u21BC", "lharul": "\u296A", "lhblk": "\u2584", "LJcy": "\u0409", "ljcy": "\u0459", "llarr": "\u21C7", "ll": "\u226A", "Ll": "\u22D8", "llcorner": "\u231E", "Lleftarrow": "\u21DA", "llhard": "\u296B", "lltri": "\u25FA", "Lmidot": "\u013F", "lmidot": "\u0140", "lmoustache": "\u23B0", "lmoust": "\u23B0", "lnap": "\u2A89", "lnapprox": "\u2A89", "lne": "\u2A87", "lnE": "\u2268", "lneq": "\u2A87", "lneqq": "\u2268", "lnsim": "\u22E6", "loang": "\u27EC", "loarr": "\u21FD", "lobrk": "\u27E6", "longleftarrow": "\u27F5", "LongLeftArrow": "\u27F5", "Longleftarrow": "\u27F8", "longleftrightarrow": "\u27F7", "LongLeftRightArrow": "\u27F7", "Longleftrightarrow": "\u27FA", "longmapsto": "\u27FC", "longrightarrow": "\u27F6", "LongRightArrow": "\u27F6", "Longrightarrow": "\u27F9", "looparrowleft": "\u21AB", "looparrowright": "\u21AC", "lopar": "\u2985", "Lopf": "\uD835\uDD43", "lopf": "\uD835\uDD5D", "loplus": "\u2A2D", "lotimes": "\u2A34", "lowast": "\u2217", "lowbar": "_", "LowerLeftArrow": "\u2199", "LowerRightArrow": "\u2198", "loz": "\u25CA", "lozenge": "\u25CA", "lozf": "\u29EB", "lpar": "(", "lparlt": "\u2993", "lrarr": "\u21C6", "lrcorner": "\u231F", "lrhar": "\u21CB", "lrhard": "\u296D", "lrm": "\u200E", "lrtri": "\u22BF", "lsaquo": "\u2039", "lscr": "\uD835\uDCC1", "Lscr": "\u2112", "lsh": "\u21B0", "Lsh": "\u21B0", "lsim": "\u2272", "lsime": "\u2A8D", "lsimg": "\u2A8F", "lsqb": "[", "lsquo": "\u2018", "lsquor": "\u201A", "Lstrok": "\u0141", "lstrok": "\u0142", "ltcc": "\u2AA6", "ltcir": "\u2A79", "lt": "<", "LT": "<", "Lt": "\u226A", "ltdot": "\u22D6", "lthree": "\u22CB", "ltimes": "\u22C9", "ltlarr": "\u2976", "ltquest": "\u2A7B", "ltri": "\u25C3", "ltrie": "\u22B4", "ltrif": "\u25C2", "ltrPar": "\u2996", "lurdshar": "\u294A", "luruhar": "\u2966", "lvertneqq": "\u2268\uFE00", "lvnE": "\u2268\uFE00", "macr": "\u00AF", "male": "\u2642", "malt": "\u2720", "maltese": "\u2720", "Map": "\u2905", "map": "\u21A6", "mapsto": "\u21A6", "mapstodown": "\u21A7", "mapstoleft": "\u21A4", "mapstoup": "\u21A5", "marker": "\u25AE", "mcomma": "\u2A29", "Mcy": "\u041C", "mcy": "\u043C", "mdash": "\u2014", "mDDot": "\u223A", "measuredangle": "\u2221", "MediumSpace": "\u205F", "Mellintrf": "\u2133", "Mfr": "\uD835\uDD10", "mfr": "\uD835\uDD2A", "mho": "\u2127", "micro": "\u00B5", "midast": "*", "midcir": "\u2AF0", "mid": "\u2223", "middot": "\u00B7", "minusb": "\u229F", "minus": "\u2212", "minusd": "\u2238", "minusdu": "\u2A2A", "MinusPlus": "\u2213", "mlcp": "\u2ADB", "mldr": "\u2026", "mnplus": "\u2213", "models": "\u22A7", "Mopf": "\uD835\uDD44", "mopf": "\uD835\uDD5E", "mp": "\u2213", "mscr": "\uD835\uDCC2", "Mscr": "\u2133", "mstpos": "\u223E", "Mu": "\u039C", "mu": "\u03BC", "multimap": "\u22B8", "mumap": "\u22B8", "nabla": "\u2207", "Nacute": "\u0143", "nacute": "\u0144", "nang": "\u2220\u20D2", "nap": "\u2249", "napE": "\u2A70\u0338", "napid": "\u224B\u0338", "napos": "\u0149", "napprox": "\u2249", "natural": "\u266E", "naturals": "\u2115", "natur": "\u266E", "nbsp": "\u00A0", "nbump": "\u224E\u0338", "nbumpe": "\u224F\u0338", "ncap": "\u2A43", "Ncaron": "\u0147", "ncaron": "\u0148", "Ncedil": "\u0145", "ncedil": "\u0146", "ncong": "\u2247", "ncongdot": "\u2A6D\u0338", "ncup": "\u2A42", "Ncy": "\u041D", "ncy": "\u043D", "ndash": "\u2013", "nearhk": "\u2924", "nearr": "\u2197", "neArr": "\u21D7", "nearrow": "\u2197", "ne": "\u2260", "nedot": "\u2250\u0338", "NegativeMediumSpace": "\u200B", "NegativeThickSpace": "\u200B", "NegativeThinSpace": "\u200B", "NegativeVeryThinSpace": "\u200B", "nequiv": "\u2262", "nesear": "\u2928", "nesim": "\u2242\u0338", "NestedGreaterGreater": "\u226B", "NestedLessLess": "\u226A", "NewLine": "\n", "nexist": "\u2204", "nexists": "\u2204", "Nfr": "\uD835\uDD11", "nfr": "\uD835\uDD2B", "ngE": "\u2267\u0338", "nge": "\u2271", "ngeq": "\u2271", "ngeqq": "\u2267\u0338", "ngeqslant": "\u2A7E\u0338", "nges": "\u2A7E\u0338", "nGg": "\u22D9\u0338", "ngsim": "\u2275", "nGt": "\u226B\u20D2", "ngt": "\u226F", "ngtr": "\u226F", "nGtv": "\u226B\u0338", "nharr": "\u21AE", "nhArr": "\u21CE", "nhpar": "\u2AF2", "ni": "\u220B", "nis": "\u22FC", "nisd": "\u22FA", "niv": "\u220B", "NJcy": "\u040A", "njcy": "\u045A", "nlarr": "\u219A", "nlArr": "\u21CD", "nldr": "\u2025", "nlE": "\u2266\u0338", "nle": "\u2270", "nleftarrow": "\u219A", "nLeftarrow": "\u21CD", "nleftrightarrow": "\u21AE", "nLeftrightarrow": "\u21CE", "nleq": "\u2270", "nleqq": "\u2266\u0338", "nleqslant": "\u2A7D\u0338", "nles": "\u2A7D\u0338", "nless": "\u226E", "nLl": "\u22D8\u0338", "nlsim": "\u2274", "nLt": "\u226A\u20D2", "nlt": "\u226E", "nltri": "\u22EA", "nltrie": "\u22EC", "nLtv": "\u226A\u0338", "nmid": "\u2224", "NoBreak": "\u2060", "NonBreakingSpace": "\u00A0", "nopf": "\uD835\uDD5F", "Nopf": "\u2115", "Not": "\u2AEC", "not": "\u00AC", "NotCongruent": "\u2262", "NotCupCap": "\u226D", "NotDoubleVerticalBar": "\u2226", "NotElement": "\u2209", "NotEqual": "\u2260", "NotEqualTilde": "\u2242\u0338", "NotExists": "\u2204", "NotGreater": "\u226F", "NotGreaterEqual": "\u2271", "NotGreaterFullEqual": "\u2267\u0338", "NotGreaterGreater": "\u226B\u0338", "NotGreaterLess": "\u2279", "NotGreaterSlantEqual": "\u2A7E\u0338", "NotGreaterTilde": "\u2275", "NotHumpDownHump": "\u224E\u0338", "NotHumpEqual": "\u224F\u0338", "notin": "\u2209", "notindot": "\u22F5\u0338", "notinE": "\u22F9\u0338", "notinva": "\u2209", "notinvb": "\u22F7", "notinvc": "\u22F6", "NotLeftTriangleBar": "\u29CF\u0338", "NotLeftTriangle": "\u22EA", "NotLeftTriangleEqual": "\u22EC", "NotLess": "\u226E", "NotLessEqual": "\u2270", "NotLessGreater": "\u2278", "NotLessLess": "\u226A\u0338", "NotLessSlantEqual": "\u2A7D\u0338", "NotLessTilde": "\u2274", "NotNestedGreaterGreater": "\u2AA2\u0338", "NotNestedLessLess": "\u2AA1\u0338", "notni": "\u220C", "notniva": "\u220C", "notnivb": "\u22FE", "notnivc": "\u22FD", "NotPrecedes": "\u2280", "NotPrecedesEqual": "\u2AAF\u0338", "NotPrecedesSlantEqual": "\u22E0", "NotReverseElement": "\u220C", "NotRightTriangleBar": "\u29D0\u0338", "NotRightTriangle": "\u22EB", "NotRightTriangleEqual": "\u22ED", "NotSquareSubset": "\u228F\u0338", "NotSquareSubsetEqual": "\u22E2", "NotSquareSuperset": "\u2290\u0338", "NotSquareSupersetEqual": "\u22E3", "NotSubset": "\u2282\u20D2", "NotSubsetEqual": "\u2288", "NotSucceeds": "\u2281", "NotSucceedsEqual": "\u2AB0\u0338", "NotSucceedsSlantEqual": "\u22E1", "NotSucceedsTilde": "\u227F\u0338", "NotSuperset": "\u2283\u20D2", "NotSupersetEqual": "\u2289", "NotTilde": "\u2241", "NotTildeEqual": "\u2244", "NotTildeFullEqual": "\u2247", "NotTildeTilde": "\u2249", "NotVerticalBar": "\u2224", "nparallel": "\u2226", "npar": "\u2226", "nparsl": "\u2AFD\u20E5", "npart": "\u2202\u0338", "npolint": "\u2A14", "npr": "\u2280", "nprcue": "\u22E0", "nprec": "\u2280", "npreceq": "\u2AAF\u0338", "npre": "\u2AAF\u0338", "nrarrc": "\u2933\u0338", "nrarr": "\u219B", "nrArr": "\u21CF", "nrarrw": "\u219D\u0338", "nrightarrow": "\u219B", "nRightarrow": "\u21CF", "nrtri": "\u22EB", "nrtrie": "\u22ED", "nsc": "\u2281", "nsccue": "\u22E1", "nsce": "\u2AB0\u0338", "Nscr": "\uD835\uDCA9", "nscr": "\uD835\uDCC3", "nshortmid": "\u2224", "nshortparallel": "\u2226", "nsim": "\u2241", "nsime": "\u2244", "nsimeq": "\u2244", "nsmid": "\u2224", "nspar": "\u2226", "nsqsube": "\u22E2", "nsqsupe": "\u22E3", "nsub": "\u2284", "nsubE": "\u2AC5\u0338", "nsube": "\u2288", "nsubset": "\u2282\u20D2", "nsubseteq": "\u2288", "nsubseteqq": "\u2AC5\u0338", "nsucc": "\u2281", "nsucceq": "\u2AB0\u0338", "nsup": "\u2285", "nsupE": "\u2AC6\u0338", "nsupe": "\u2289", "nsupset": "\u2283\u20D2", "nsupseteq": "\u2289", "nsupseteqq": "\u2AC6\u0338", "ntgl": "\u2279", "Ntilde": "\u00D1", "ntilde": "\u00F1", "ntlg": "\u2278", "ntriangleleft": "\u22EA", "ntrianglelefteq": "\u22EC", "ntriangleright": "\u22EB", "ntrianglerighteq": "\u22ED", "Nu": "\u039D", "nu": "\u03BD", "num": "#", "numero": "\u2116", "numsp": "\u2007", "nvap": "\u224D\u20D2", "nvdash": "\u22AC", "nvDash": "\u22AD", "nVdash": "\u22AE", "nVDash": "\u22AF", "nvge": "\u2265\u20D2", "nvgt": ">\u20D2", "nvHarr": "\u2904", "nvinfin": "\u29DE", "nvlArr": "\u2902", "nvle": "\u2264\u20D2", "nvlt": "<\u20D2", "nvltrie": "\u22B4\u20D2", "nvrArr": "\u2903", "nvrtrie": "\u22B5\u20D2", "nvsim": "\u223C\u20D2", "nwarhk": "\u2923", "nwarr": "\u2196", "nwArr": "\u21D6", "nwarrow": "\u2196", "nwnear": "\u2927", "Oacute": "\u00D3", "oacute": "\u00F3", "oast": "\u229B", "Ocirc": "\u00D4", "ocirc": "\u00F4", "ocir": "\u229A", "Ocy": "\u041E", "ocy": "\u043E", "odash": "\u229D", "Odblac": "\u0150", "odblac": "\u0151", "odiv": "\u2A38", "odot": "\u2299", "odsold": "\u29BC", "OElig": "\u0152", "oelig": "\u0153", "ofcir": "\u29BF", "Ofr": "\uD835\uDD12", "ofr": "\uD835\uDD2C", "ogon": "\u02DB", "Ograve": "\u00D2", "ograve": "\u00F2", "ogt": "\u29C1", "ohbar": "\u29B5", "ohm": "\u03A9", "oint": "\u222E", "olarr": "\u21BA", "olcir": "\u29BE", "olcross": "\u29BB", "oline": "\u203E", "olt": "\u29C0", "Omacr": "\u014C", "omacr": "\u014D", "Omega": "\u03A9", "omega": "\u03C9", "Omicron": "\u039F", "omicron": "\u03BF", "omid": "\u29B6", "ominus": "\u2296", "Oopf": "\uD835\uDD46", "oopf": "\uD835\uDD60", "opar": "\u29B7", "OpenCurlyDoubleQuote": "\u201C", "OpenCurlyQuote": "\u2018", "operp": "\u29B9", "oplus": "\u2295", "orarr": "\u21BB", "Or": "\u2A54", "or": "\u2228", "ord": "\u2A5D", "order": "\u2134", "orderof": "\u2134", "ordf": "\u00AA", "ordm": "\u00BA", "origof": "\u22B6", "oror": "\u2A56", "orslope": "\u2A57", "orv": "\u2A5B", "oS": "\u24C8", "Oscr": "\uD835\uDCAA", "oscr": "\u2134", "Oslash": "\u00D8", "oslash": "\u00F8", "osol": "\u2298", "Otilde": "\u00D5", "otilde": "\u00F5", "otimesas": "\u2A36", "Otimes": "\u2A37", "otimes": "\u2297", "Ouml": "\u00D6", "ouml": "\u00F6", "ovbar": "\u233D", "OverBar": "\u203E", "OverBrace": "\u23DE", "OverBracket": "\u23B4", "OverParenthesis": "\u23DC", "para": "\u00B6", "parallel": "\u2225", "par": "\u2225", "parsim": "\u2AF3", "parsl": "\u2AFD", "part": "\u2202", "PartialD": "\u2202", "Pcy": "\u041F", "pcy": "\u043F", "percnt": "%", "period": ".", "permil": "\u2030", "perp": "\u22A5", "pertenk": "\u2031", "Pfr": "\uD835\uDD13", "pfr": "\uD835\uDD2D", "Phi": "\u03A6", "phi": "\u03C6", "phiv": "\u03D5", "phmmat": "\u2133", "phone": "\u260E", "Pi": "\u03A0", "pi": "\u03C0", "pitchfork": "\u22D4", "piv": "\u03D6", "planck": "\u210F", "planckh": "\u210E", "plankv": "\u210F", "plusacir": "\u2A23", "plusb": "\u229E", "pluscir": "\u2A22", "plus": "+", "plusdo": "\u2214", "plusdu": "\u2A25", "pluse": "\u2A72", "PlusMinus": "\u00B1", "plusmn": "\u00B1", "plussim": "\u2A26", "plustwo": "\u2A27", "pm": "\u00B1", "Poincareplane": "\u210C", "pointint": "\u2A15", "popf": "\uD835\uDD61", "Popf": "\u2119", "pound": "\u00A3", "prap": "\u2AB7", "Pr": "\u2ABB", "pr": "\u227A", "prcue": "\u227C", "precapprox": "\u2AB7", "prec": "\u227A", "preccurlyeq": "\u227C", "Precedes": "\u227A", "PrecedesEqual": "\u2AAF", "PrecedesSlantEqual": "\u227C", "PrecedesTilde": "\u227E", "preceq": "\u2AAF", "precnapprox": "\u2AB9", "precneqq": "\u2AB5", "precnsim": "\u22E8", "pre": "\u2AAF", "prE": "\u2AB3", "precsim": "\u227E", "prime": "\u2032", "Prime": "\u2033", "primes": "\u2119", "prnap": "\u2AB9", "prnE": "\u2AB5", "prnsim": "\u22E8", "prod": "\u220F", "Product": "\u220F", "profalar": "\u232E", "profline": "\u2312", "profsurf": "\u2313", "prop": "\u221D", "Proportional": "\u221D", "Proportion": "\u2237", "propto": "\u221D", "prsim": "\u227E", "prurel": "\u22B0", "Pscr": "\uD835\uDCAB", "pscr": "\uD835\uDCC5", "Psi": "\u03A8", "psi": "\u03C8", "puncsp": "\u2008", "Qfr": "\uD835\uDD14", "qfr": "\uD835\uDD2E", "qint": "\u2A0C", "qopf": "\uD835\uDD62", "Qopf": "\u211A", "qprime": "\u2057", "Qscr": "\uD835\uDCAC", "qscr": "\uD835\uDCC6", "quaternions": "\u210D", "quatint": "\u2A16", "quest": "?", "questeq": "\u225F", "quot": "\"", "QUOT": "\"", "rAarr": "\u21DB", "race": "\u223D\u0331", "Racute": "\u0154", "racute": "\u0155", "radic": "\u221A", "raemptyv": "\u29B3", "rang": "\u27E9", "Rang": "\u27EB", "rangd": "\u2992", "range": "\u29A5", "rangle": "\u27E9", "raquo": "\u00BB", "rarrap": "\u2975", "rarrb": "\u21E5", "rarrbfs": "\u2920", "rarrc": "\u2933", "rarr": "\u2192", "Rarr": "\u21A0", "rArr": "\u21D2", "rarrfs": "\u291E", "rarrhk": "\u21AA", "rarrlp": "\u21AC", "rarrpl": "\u2945", "rarrsim": "\u2974", "Rarrtl": "\u2916", "rarrtl": "\u21A3", "rarrw": "\u219D", "ratail": "\u291A", "rAtail": "\u291C", "ratio": "\u2236", "rationals": "\u211A", "rbarr": "\u290D", "rBarr": "\u290F", "RBarr": "\u2910", "rbbrk": "\u2773", "rbrace": "}", "rbrack": "]", "rbrke": "\u298C", "rbrksld": "\u298E", "rbrkslu": "\u2990", "Rcaron": "\u0158", "rcaron": "\u0159", "Rcedil": "\u0156", "rcedil": "\u0157", "rceil": "\u2309", "rcub": "}", "Rcy": "\u0420", "rcy": "\u0440", "rdca": "\u2937", "rdldhar": "\u2969", "rdquo": "\u201D", "rdquor": "\u201D", "rdsh": "\u21B3", "real": "\u211C", "realine": "\u211B", "realpart": "\u211C", "reals": "\u211D", "Re": "\u211C", "rect": "\u25AD", "reg": "\u00AE", "REG": "\u00AE", "ReverseElement": "\u220B", "ReverseEquilibrium": "\u21CB", "ReverseUpEquilibrium": "\u296F", "rfisht": "\u297D", "rfloor": "\u230B", "rfr": "\uD835\uDD2F", "Rfr": "\u211C", "rHar": "\u2964", "rhard": "\u21C1", "rharu": "\u21C0", "rharul": "\u296C", "Rho": "\u03A1", "rho": "\u03C1", "rhov": "\u03F1", "RightAngleBracket": "\u27E9", "RightArrowBar": "\u21E5", "rightarrow": "\u2192", "RightArrow": "\u2192", "Rightarrow": "\u21D2", "RightArrowLeftArrow": "\u21C4", "rightarrowtail": "\u21A3", "RightCeiling": "\u2309", "RightDoubleBracket": "\u27E7", "RightDownTeeVector": "\u295D", "RightDownVectorBar": "\u2955", "RightDownVector": "\u21C2", "RightFloor": "\u230B", "rightharpoondown": "\u21C1", "rightharpoonup": "\u21C0", "rightleftarrows": "\u21C4", "rightleftharpoons": "\u21CC", "rightrightarrows": "\u21C9", "rightsquigarrow": "\u219D", "RightTeeArrow": "\u21A6", "RightTee": "\u22A2", "RightTeeVector": "\u295B", "rightthreetimes": "\u22CC", "RightTriangleBar": "\u29D0", "RightTriangle": "\u22B3", "RightTriangleEqual": "\u22B5", "RightUpDownVector": "\u294F", "RightUpTeeVector": "\u295C", "RightUpVectorBar": "\u2954", "RightUpVector": "\u21BE", "RightVectorBar": "\u2953", "RightVector": "\u21C0", "ring": "\u02DA", "risingdotseq": "\u2253", "rlarr": "\u21C4", "rlhar": "\u21CC", "rlm": "\u200F", "rmoustache": "\u23B1", "rmoust": "\u23B1", "rnmid": "\u2AEE", "roang": "\u27ED", "roarr": "\u21FE", "robrk": "\u27E7", "ropar": "\u2986", "ropf": "\uD835\uDD63", "Ropf": "\u211D", "roplus": "\u2A2E", "rotimes": "\u2A35", "RoundImplies": "\u2970", "rpar": ")", "rpargt": "\u2994", "rppolint": "\u2A12", "rrarr": "\u21C9", "Rrightarrow": "\u21DB", "rsaquo": "\u203A", "rscr": "\uD835\uDCC7", "Rscr": "\u211B", "rsh": "\u21B1", "Rsh": "\u21B1", "rsqb": "]", "rsquo": "\u2019", "rsquor": "\u2019", "rthree": "\u22CC", "rtimes": "\u22CA", "rtri": "\u25B9", "rtrie": "\u22B5", "rtrif": "\u25B8", "rtriltri": "\u29CE", "RuleDelayed": "\u29F4", "ruluhar": "\u2968", "rx": "\u211E", "Sacute": "\u015A", "sacute": "\u015B", "sbquo": "\u201A", "scap": "\u2AB8", "Scaron": "\u0160", "scaron": "\u0161", "Sc": "\u2ABC", "sc": "\u227B", "sccue": "\u227D", "sce": "\u2AB0", "scE": "\u2AB4", "Scedil": "\u015E", "scedil": "\u015F", "Scirc": "\u015C", "scirc": "\u015D", "scnap": "\u2ABA", "scnE": "\u2AB6", "scnsim": "\u22E9", "scpolint": "\u2A13", "scsim": "\u227F", "Scy": "\u0421", "scy": "\u0441", "sdotb": "\u22A1", "sdot": "\u22C5", "sdote": "\u2A66", "searhk": "\u2925", "searr": "\u2198", "seArr": "\u21D8", "searrow": "\u2198", "sect": "\u00A7", "semi": ";", "seswar": "\u2929", "setminus": "\u2216", "setmn": "\u2216", "sext": "\u2736", "Sfr": "\uD835\uDD16", "sfr": "\uD835\uDD30", "sfrown": "\u2322", "sharp": "\u266F", "SHCHcy": "\u0429", "shchcy": "\u0449", "SHcy": "\u0428", "shcy": "\u0448", "ShortDownArrow": "\u2193", "ShortLeftArrow": "\u2190", "shortmid": "\u2223", "shortparallel": "\u2225", "ShortRightArrow": "\u2192", "ShortUpArrow": "\u2191", "shy": "\u00AD", "Sigma": "\u03A3", "sigma": "\u03C3", "sigmaf": "\u03C2", "sigmav": "\u03C2", "sim": "\u223C", "simdot": "\u2A6A", "sime": "\u2243", "simeq": "\u2243", "simg": "\u2A9E", "simgE": "\u2AA0", "siml": "\u2A9D", "simlE": "\u2A9F", "simne": "\u2246", "simplus": "\u2A24", "simrarr": "\u2972", "slarr": "\u2190", "SmallCircle": "\u2218", "smallsetminus": "\u2216", "smashp": "\u2A33", "smeparsl": "\u29E4", "smid": "\u2223", "smile": "\u2323", "smt": "\u2AAA", "smte": "\u2AAC", "smtes": "\u2AAC\uFE00", "SOFTcy": "\u042C", "softcy": "\u044C", "solbar": "\u233F", "solb": "\u29C4", "sol": "/", "Sopf": "\uD835\uDD4A", "sopf": "\uD835\uDD64", "spades": "\u2660", "spadesuit": "\u2660", "spar": "\u2225", "sqcap": "\u2293", "sqcaps": "\u2293\uFE00", "sqcup": "\u2294", "sqcups": "\u2294\uFE00", "Sqrt": "\u221A", "sqsub": "\u228F", "sqsube": "\u2291", "sqsubset": "\u228F", "sqsubseteq": "\u2291", "sqsup": "\u2290", "sqsupe": "\u2292", "sqsupset": "\u2290", "sqsupseteq": "\u2292", "square": "\u25A1", "Square": "\u25A1", "SquareIntersection": "\u2293", "SquareSubset": "\u228F", "SquareSubsetEqual": "\u2291", "SquareSuperset": "\u2290", "SquareSupersetEqual": "\u2292", "SquareUnion": "\u2294", "squarf": "\u25AA", "squ": "\u25A1", "squf": "\u25AA", "srarr": "\u2192", "Sscr": "\uD835\uDCAE", "sscr": "\uD835\uDCC8", "ssetmn": "\u2216", "ssmile": "\u2323", "sstarf": "\u22C6", "Star": "\u22C6", "star": "\u2606", "starf": "\u2605", "straightepsilon": "\u03F5", "straightphi": "\u03D5", "strns": "\u00AF", "sub": "\u2282", "Sub": "\u22D0", "subdot": "\u2ABD", "subE": "\u2AC5", "sube": "\u2286", "subedot": "\u2AC3", "submult": "\u2AC1", "subnE": "\u2ACB", "subne": "\u228A", "subplus": "\u2ABF", "subrarr": "\u2979", "subset": "\u2282", "Subset": "\u22D0", "subseteq": "\u2286", "subseteqq": "\u2AC5", "SubsetEqual": "\u2286", "subsetneq": "\u228A", "subsetneqq": "\u2ACB", "subsim": "\u2AC7", "subsub": "\u2AD5", "subsup": "\u2AD3", "succapprox": "\u2AB8", "succ": "\u227B", "succcurlyeq": "\u227D", "Succeeds": "\u227B", "SucceedsEqual": "\u2AB0", "SucceedsSlantEqual": "\u227D", "SucceedsTilde": "\u227F", "succeq": "\u2AB0", "succnapprox": "\u2ABA", "succneqq": "\u2AB6", "succnsim": "\u22E9", "succsim": "\u227F", "SuchThat": "\u220B", "sum": "\u2211", "Sum": "\u2211", "sung": "\u266A", "sup1": "\u00B9", "sup2": "\u00B2", "sup3": "\u00B3", "sup": "\u2283", "Sup": "\u22D1", "supdot": "\u2ABE", "supdsub": "\u2AD8", "supE": "\u2AC6", "supe": "\u2287", "supedot": "\u2AC4", "Superset": "\u2283", "SupersetEqual": "\u2287", "suphsol": "\u27C9", "suphsub": "\u2AD7", "suplarr": "\u297B", "supmult": "\u2AC2", "supnE": "\u2ACC", "supne": "\u228B", "supplus": "\u2AC0", "supset": "\u2283", "Supset": "\u22D1", "supseteq": "\u2287", "supseteqq": "\u2AC6", "supsetneq": "\u228B", "supsetneqq": "\u2ACC", "supsim": "\u2AC8", "supsub": "\u2AD4", "supsup": "\u2AD6", "swarhk": "\u2926", "swarr": "\u2199", "swArr": "\u21D9", "swarrow": "\u2199", "swnwar": "\u292A", "szlig": "\u00DF", "Tab": "\t", "target": "\u2316", "Tau": "\u03A4", "tau": "\u03C4", "tbrk": "\u23B4", "Tcaron": "\u0164", "tcaron": "\u0165", "Tcedil": "\u0162", "tcedil": "\u0163", "Tcy": "\u0422", "tcy": "\u0442", "tdot": "\u20DB", "telrec": "\u2315", "Tfr": "\uD835\uDD17", "tfr": "\uD835\uDD31", "there4": "\u2234", "therefore": "\u2234", "Therefore": "\u2234", "Theta": "\u0398", "theta": "\u03B8", "thetasym": "\u03D1", "thetav": "\u03D1", "thickapprox": "\u2248", "thicksim": "\u223C", "ThickSpace": "\u205F\u200A", "ThinSpace": "\u2009", "thinsp": "\u2009", "thkap": "\u2248", "thksim": "\u223C", "THORN": "\u00DE", "thorn": "\u00FE", "tilde": "\u02DC", "Tilde": "\u223C", "TildeEqual": "\u2243", "TildeFullEqual": "\u2245", "TildeTilde": "\u2248", "timesbar": "\u2A31", "timesb": "\u22A0", "times": "\u00D7", "timesd": "\u2A30", "tint": "\u222D", "toea": "\u2928", "topbot": "\u2336", "topcir": "\u2AF1", "top": "\u22A4", "Topf": "\uD835\uDD4B", "topf": "\uD835\uDD65", "topfork": "\u2ADA", "tosa": "\u2929", "tprime": "\u2034", "trade": "\u2122", "TRADE": "\u2122", "triangle": "\u25B5", "triangledown": "\u25BF", "triangleleft": "\u25C3", "trianglelefteq": "\u22B4", "triangleq": "\u225C", "triangleright": "\u25B9", "trianglerighteq": "\u22B5", "tridot": "\u25EC", "trie": "\u225C", "triminus": "\u2A3A", "TripleDot": "\u20DB", "triplus": "\u2A39", "trisb": "\u29CD", "tritime": "\u2A3B", "trpezium": "\u23E2", "Tscr": "\uD835\uDCAF", "tscr": "\uD835\uDCC9", "TScy": "\u0426", "tscy": "\u0446", "TSHcy": "\u040B", "tshcy": "\u045B", "Tstrok": "\u0166", "tstrok": "\u0167", "twixt": "\u226C", "twoheadleftarrow": "\u219E", "twoheadrightarrow": "\u21A0", "Uacute": "\u00DA", "uacute": "\u00FA", "uarr": "\u2191", "Uarr": "\u219F", "uArr": "\u21D1", "Uarrocir": "\u2949", "Ubrcy": "\u040E", "ubrcy": "\u045E", "Ubreve": "\u016C", "ubreve": "\u016D", "Ucirc": "\u00DB", "ucirc": "\u00FB", "Ucy": "\u0423", "ucy": "\u0443", "udarr": "\u21C5", "Udblac": "\u0170", "udblac": "\u0171", "udhar": "\u296E", "ufisht": "\u297E", "Ufr": "\uD835\uDD18", "ufr": "\uD835\uDD32", "Ugrave": "\u00D9", "ugrave": "\u00F9", "uHar": "\u2963", "uharl": "\u21BF", "uharr": "\u21BE", "uhblk": "\u2580", "ulcorn": "\u231C", "ulcorner": "\u231C", "ulcrop": "\u230F", "ultri": "\u25F8", "Umacr": "\u016A", "umacr": "\u016B", "uml": "\u00A8", "UnderBar": "_", "UnderBrace": "\u23DF", "UnderBracket": "\u23B5", "UnderParenthesis": "\u23DD", "Union": "\u22C3", "UnionPlus": "\u228E", "Uogon": "\u0172", "uogon": "\u0173", "Uopf": "\uD835\uDD4C", "uopf": "\uD835\uDD66", "UpArrowBar": "\u2912", "uparrow": "\u2191", "UpArrow": "\u2191", "Uparrow": "\u21D1", "UpArrowDownArrow": "\u21C5", "updownarrow": "\u2195", "UpDownArrow": "\u2195", "Updownarrow": "\u21D5", "UpEquilibrium": "\u296E", "upharpoonleft": "\u21BF", "upharpoonright": "\u21BE", "uplus": "\u228E", "UpperLeftArrow": "\u2196", "UpperRightArrow": "\u2197", "upsi": "\u03C5", "Upsi": "\u03D2", "upsih": "\u03D2", "Upsilon": "\u03A5", "upsilon": "\u03C5", "UpTeeArrow": "\u21A5", "UpTee": "\u22A5", "upuparrows": "\u21C8", "urcorn": "\u231D", "urcorner": "\u231D", "urcrop": "\u230E", "Uring": "\u016E", "uring": "\u016F", "urtri": "\u25F9", "Uscr": "\uD835\uDCB0", "uscr": "\uD835\uDCCA", "utdot": "\u22F0", "Utilde": "\u0168", "utilde": "\u0169", "utri": "\u25B5", "utrif": "\u25B4", "uuarr": "\u21C8", "Uuml": "\u00DC", "uuml": "\u00FC", "uwangle": "\u29A7", "vangrt": "\u299C", "varepsilon": "\u03F5", "varkappa": "\u03F0", "varnothing": "\u2205", "varphi": "\u03D5", "varpi": "\u03D6", "varpropto": "\u221D", "varr": "\u2195", "vArr": "\u21D5", "varrho": "\u03F1", "varsigma": "\u03C2", "varsubsetneq": "\u228A\uFE00", "varsubsetneqq": "\u2ACB\uFE00", "varsupsetneq": "\u228B\uFE00", "varsupsetneqq": "\u2ACC\uFE00", "vartheta": "\u03D1", "vartriangleleft": "\u22B2", "vartriangleright": "\u22B3", "vBar": "\u2AE8", "Vbar": "\u2AEB", "vBarv": "\u2AE9", "Vcy": "\u0412", "vcy": "\u0432", "vdash": "\u22A2", "vDash": "\u22A8", "Vdash": "\u22A9", "VDash": "\u22AB", "Vdashl": "\u2AE6", "veebar": "\u22BB", "vee": "\u2228", "Vee": "\u22C1", "veeeq": "\u225A", "vellip": "\u22EE", "verbar": "|", "Verbar": "\u2016", "vert": "|", "Vert": "\u2016", "VerticalBar": "\u2223", "VerticalLine": "|", "VerticalSeparator": "\u2758", "VerticalTilde": "\u2240", "VeryThinSpace": "\u200A", "Vfr": "\uD835\uDD19", "vfr": "\uD835\uDD33", "vltri": "\u22B2", "vnsub": "\u2282\u20D2", "vnsup": "\u2283\u20D2", "Vopf": "\uD835\uDD4D", "vopf": "\uD835\uDD67", "vprop": "\u221D", "vrtri": "\u22B3", "Vscr": "\uD835\uDCB1", "vscr": "\uD835\uDCCB", "vsubnE": "\u2ACB\uFE00", "vsubne": "\u228A\uFE00", "vsupnE": "\u2ACC\uFE00", "vsupne": "\u228B\uFE00", "Vvdash": "\u22AA", "vzigzag": "\u299A", "Wcirc": "\u0174", "wcirc": "\u0175", "wedbar": "\u2A5F", "wedge": "\u2227", "Wedge": "\u22C0", "wedgeq": "\u2259", "weierp": "\u2118", "Wfr": "\uD835\uDD1A", "wfr": "\uD835\uDD34", "Wopf": "\uD835\uDD4E", "wopf": "\uD835\uDD68", "wp": "\u2118", "wr": "\u2240", "wreath": "\u2240", "Wscr": "\uD835\uDCB2", "wscr": "\uD835\uDCCC", "xcap": "\u22C2", "xcirc": "\u25EF", "xcup": "\u22C3", "xdtri": "\u25BD", "Xfr": "\uD835\uDD1B", "xfr": "\uD835\uDD35", "xharr": "\u27F7", "xhArr": "\u27FA", "Xi": "\u039E", "xi": "\u03BE", "xlarr": "\u27F5", "xlArr": "\u27F8", "xmap": "\u27FC", "xnis": "\u22FB", "xodot": "\u2A00", "Xopf": "\uD835\uDD4F", "xopf": "\uD835\uDD69", "xoplus": "\u2A01", "xotime": "\u2A02", "xrarr": "\u27F6", "xrArr": "\u27F9", "Xscr": "\uD835\uDCB3", "xscr": "\uD835\uDCCD", "xsqcup": "\u2A06", "xuplus": "\u2A04", "xutri": "\u25B3", "xvee": "\u22C1", "xwedge": "\u22C0", "Yacute": "\u00DD", "yacute": "\u00FD", "YAcy": "\u042F", "yacy": "\u044F", "Ycirc": "\u0176", "ycirc": "\u0177", "Ycy": "\u042B", "ycy": "\u044B", "yen": "\u00A5", "Yfr": "\uD835\uDD1C", "yfr": "\uD835\uDD36", "YIcy": "\u0407", "yicy": "\u0457", "Yopf": "\uD835\uDD50", "yopf": "\uD835\uDD6A", "Yscr": "\uD835\uDCB4", "yscr": "\uD835\uDCCE", "YUcy": "\u042E", "yucy": "\u044E", "yuml": "\u00FF", "Yuml": "\u0178", "Zacute": "\u0179", "zacute": "\u017A", "Zcaron": "\u017D", "zcaron": "\u017E", "Zcy": "\u0417", "zcy": "\u0437", "Zdot": "\u017B", "zdot": "\u017C", "zeetrf": "\u2128", "ZeroWidthSpace": "\u200B", "Zeta": "\u0396", "zeta": "\u03B6", "zfr": "\uD835\uDD37", "Zfr": "\u2128", "ZHcy": "\u0416", "zhcy": "\u0436", "zigrarr": "\u21DD", "zopf": "\uD835\uDD6B", "Zopf": "\u2124", "Zscr": "\uD835\uDCB5", "zscr": "\uD835\uDCCF", "zwj": "\u200D", "zwnj": "\u200C" }
;
},

// node_modules/uc.micro/properties/Any/regex.js @136
136: function(__fusereq, exports, module){
module.exports = /[\0-\uD7FF\uE000-\uFFFF]|[\uD800-\uDBFF][\uDC00-\uDFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|(?:[^\uD800-\uDBFF]|^)[\uDC00-\uDFFF]/;

},

// node_modules/uc.micro/categories/Cc/regex.js @137
137: function(__fusereq, exports, module){
module.exports = /[\0-\x1F\x7F-\x9F]/;

},

// node_modules/uc.micro/categories/Cf/regex.js @138
138: function(__fusereq, exports, module){
module.exports = /[\xAD\u0600-\u0605\u061C\u06DD\u070F\u08E2\u180E\u200B-\u200F\u202A-\u202E\u2060-\u2064\u2066-\u206F\uFEFF\uFFF9-\uFFFB]|\uD804[\uDCBD\uDCCD]|\uD82F[\uDCA0-\uDCA3]|\uD834[\uDD73-\uDD7A]|\uDB40[\uDC01\uDC20-\uDC7F]/;

},

// node_modules/uc.micro/categories/Z/regex.js @139
139: function(__fusereq, exports, module){
module.exports = /[ \xA0\u1680\u2000-\u200A\u2028\u2029\u202F\u205F\u3000]/;

}
}, function(){
__fuse.r(1)
})