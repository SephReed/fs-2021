import "./portalPage.scss";

import { anchor, br, div, img, Innards, pre, Source, span, table, syncWithLocalStorage, Sourcify, el, UrlState } from "helium-ui";
import { md } from "helium-md";
import { FS } from "../../state/AppState";



export interface IPortal {
  id: string;
  link: Innards;
  info: Innards;
}

const portalState = Sourcify({
  signedWaiver: false
});
syncWithLocalStorage("portalState", portalState);


export function renderPortalPage() {
  FS.pointy.hide();

  const portals: IPortal[] = [
    { id: "principles", 
      link: [
        div("Image", { style: `background-image: url("/assets/img/howdy-fuckers-logo.png"); background-color: #02a652`}),
        div("Name", "Principles")
      ],
      info: [
        div("Title", "Basic Principles"),
        div("WhatFer", "Participant Expectations"),
        md(`
          1. **Decommodification**:  No for-profit ads.  No exchanging money.
          2. **Anti-Abuse**: Respect any boundaries that have been set.  The following boundaries are default:
            - *do not* publish visual records of a persons physical face *without their consent*
            - *do not* expose a persons private information (Dox) *without their consent*
            - *do not* push yourself onto someone romantically *without their consent*
            - *do not* express racial, gendered, or other non-character based observations of a person *without their consent*
            - basically, start off polite and work your way into being more... raw
          3. **Civic Responsibility**: Help others out.  Be the tech support you wish to see in the world.
          4. **Radical Inclusion**: There is no normal.  Always try to find a way to let people be who they truly are.
          5. **Immediacy**: Express yo-self.  Ease around your own personal identity.
            - the more people who let themselves be, the more people will feel comfortable being themselves
          6. **Participation**: If you want to share something, we'll find a place for it.  Everything you see is done by volunteers.
          7. **"Family Friendly"**: Please keep especially lewd behavior in private channels.
            - there is no way (nor attempt) to keep minors out
            - if you want a private channel, just ask
          8. **Edification**: Please do not share the direct links found *in* this portal.  Instead, share links *to* this portal.
            - we want people to have read these principles before entering any space
            - also, there's helpful instructions included, and they'll see all the other stuff they can do
          9. **The Un-principle**: Don't treat these principals as a weapon.  People fuck up.  People change.  Every "rule" has an exception.
        `),
        br(2),
        div({
          onPush: () => {
            console.log("HEY");
            portalState.signedWaiver = !portalState.signedWaiver
          },
        },[
          el("input", { 
            attr: { type: "checkbox", name: "waiver"},
            ddx: (ref) => ref.checked = portalState.signedWaiver
          }),
          el("label", { attr: { for: "waiver" }}, "I've read the stuff above")
        ])
      ]
    },
    { id: "discord", 
      link: [
        div("Image", { style: `background-image: url("/assets/img/discord-thumb.png")`}),
        div("Name", "Discord")
      ],
      info: [
        div("Title", "Discord"),
        div("WhatFer", "Main Communications and Support Hub"),
        img({src: "/assets/img/discord-thumb.png"}),
        anchor("btn", { href: `https://discord.gg/cwAdhQ5eDt` }, "https://discord.gg/cwAdhQ5eDt"),
        anchor({ href: `/survival-guide#discord` }, "Click here for help/instructions"),
      ]
    },

    { id: "minecraft", 
      link: [
        div("Image", { style: `background-image: url("/assets/img/minecraft-thumb.png")`}),
        div("Name", "Minecraft")
      ],
      info: [
        div("Title", "Minecraft"),
        div("WhatFer", "Building and Burning"),
        img({src: "/assets/img/minecraft-thumb.png"}),
        anchor("btn", { href: `/survival-guide#minecraft` }, "Click here for help/instructions"),
        table([
          ["Java Edition:", "howdy-fuckers.mc.gg"],
          ["Bedrock Edition:", "107.175.178.34 port 62402"]
        ]),
      ]
    },

    { id: "topia", 
      link: [
        div("Image", { style: `background-image: url("/assets/img/topia-thumb.png")`}),
        div("Name", "Topia")
      ],
      info: [
        div("Title", "Topia"),
        div("WhatFer", "Art Gallery and Hangout Space"),
        img({src: "/assets/img/topia-thumb.png"}),
        anchor("btn", { href: `https://topia.io/howdy-fuckers` }, "https://topia.io/howdy-fuckers"),
        anchor({ href: `/survival-guide#topia` }, "Click here for help/instructions"),
      ]
    },

    { id: "altspace", 
      link: [
        div("Image", { style: `background-image: url("https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-altspace--2d-play.png")`}),
        div("Name", "AltSpace")
      ],
      info: [
        div("Title", "Altspace"),
        div("WhatFer", "Dance Parties and Virtual Hangout"),
        img({src: "https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-altspace--2d-play.png"}),
        anchor("btn", { href: `/survival-guide#altspace` }, "Click here for help/instructions"),
        div([
          anchor({ href: `https://account.altvr.com/worlds/1742621755333149622/spaces/1742915064203051950` }, "World link"),
          " -- World Code: OAB052",
        ])
      ]
    },

    { id: "art-safari", 
      link: [
        div("Image", { style: `background-image: url("/assets/img/art-safari-thumb.png")`}),
        div("Name", "Art Safari")
      ],
      info: [
        div("Title", "Art Safari"),
        div("WhatFer", "Real World Outdoor Art Drive"),
        { hackableHTML: `<iframe src="https://www.google.com/maps/d/embed?mid=1RIAE7wbXFCqEEuhVHqTTO7JY3AUwvhDz" width="100%" height="400px"></iframe>`},
        anchor("btn", { href: `https://www.google.com/maps/d/u/0/viewer?mid=1RIAE7wbXFCqEEuhVHqTTO7JY3AUwvhDz&ll=30.274643949789976%2C-97.75821316064753&z=10` }, "See full map"),
      ]
    },

    { id: "calendar", 
      link: [
        div("Image", { style: `background-image: url("/assets/img/calendar-thumb.png")`}),
        div("Name", "Calendar")
      ],
      info: [
        div("Title", "Calendar"),
        div("WhatFer", "Schedule of Burns and Other Events"),
        { hackableHTML: `<iframe src="https://calendar.google.com/calendar/embed?src=howdy.fawker%40gmail.com" style="border: 0" width="100%" height="400" frameborder="0" scrolling="no"></iframe>`},
        anchor("btn", { href: `https://calendar.google.com/calendar/embed?src=howdy.fawker%40gmail.com
        ` }, "See full calendar"),
      ]
    },

    { id: "kflip", 
      link: [
        div("Image", { style: `background-image: url("/assets/img/kflip.png")`}),
        div("Name", "KFlip Radio")
      ],
      info: [
        div("Title", "KFlip Radio"),
        div("WhatFer", "The 24/7 Radio for this Burn"),
        img({src: "/assets/img/kflip.png"}),
        anchor("btn", { href: `http://www.kflipcamp.org/` }, "Tune in"),
      ]
    },
  ];

  const portalsById = new Map<string, IPortal>();
  portals.forEach((portal) => portalsById.set(portal.id, portal));

  function getViewId() {
    return FS.route.id.split(".")[1];
  }

  function renderPortalLink(portal: IPortal) {
    return anchor("Link", {
      ddxClass: () => [
        getViewId() === portal.id && "--selected",
        (portal.id !== "principles" && !portalState.signedWaiver) && "--no-access"
      ],
      href: `/portal/${portal.id}`,
      // onPush: () => {
      //   if (portal.id !== "principles" && !portalState.signedWaiver) { return; }
      //   linkView.set(portal.id)
      // },
      innards: portal.link
    })
  }

  return div("PortalPage", [
    div("Links", [
      portalsById.get("principles"),
      portalsById.get("discord"),
      portalsById.get("minecraft"),
      portalsById.get("art-safari"),
      
    ].map(renderPortalLink)),
    div("Info", () => {
      const viewId = getViewId();
      if (!viewId || (viewId !== "principles" && !portalState.signedWaiver)) {
        return portalsById.get("principles").info;
      }
      const portal = portalsById.get(viewId);
      return portal && portal.info;
    }),
    div("Links", [
      portalsById.get("topia"),
      portalsById.get("altspace"),
      
      portalsById.get("calendar"),
      portalsById.get("kflip"),
    ].map(renderPortalLink)),
  ])
}