import "./docsNav.scss";

import { anchor, div, Sourcified } from "helium-ui";
import { IDocSheetNavItem } from "../../state/DocsState";
import { FS } from "../../state/AppState";


export function renderDocsNav() {
  return div("DocsNav", [
    div("DocItemList", () => {
      return FS.docs.docList.renderMap((docItem) => renderDocItem(docItem));
    })
  ])
}

export function renderDocItem(item: Sourcified<IDocSheetNavItem>) {
  return div({
    class: [
      "DocItem",
      "href" in item ? "section" : "sheet"
    ],
    innards: [
      anchor("Label", {
        onPush: () => FS.selection.docItem = item.name,
        innards: item.name,
        href: () => "hash" in item ? item.hash : "#"
      }),
      "sections" in item && div("SubDocs", item.sections.renderMap((subDoc) =>
        renderDocItem(subDoc)
      ))
    ]
  })
}