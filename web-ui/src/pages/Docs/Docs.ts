import { md } from "helium-md";
import { div, Sourcified, awaitSource, append, anchor, addClass, byId } from "helium-ui";
import { FS } from "../../state/AppState";
import { IDocSheetNavItem } from "../../state/DocsState";

export function renderDocs() {
  return div("DocList", () => {
    const map = new Map<string, Sourcified<IDocSheetNavItem>>();
    const tryAddDoc = (doc: Sourcified<IDocSheetNavItem>) => {
      if (doc.filePath && map.has(doc.filePath.replace(/#.+/, "")) === false) {
        map.set(doc.filePath, doc);
      }
      if (doc.sections) {
        doc.sections.forEach(tryAddDoc);
      }
    }
    FS.docs.docList.forEach(tryAddDoc);

    setTimeout(() => {
      const hash = location.hash;
      const target = hash && byId(hash.slice(1));
      console.log("target", target);
      target && target.scrollIntoView();
    }, 1000)

    return Array.from(map.values()).map((doc) => {
      const docEl = div("Doc", {
        id: doc.hash.slice(1),
      }, () => {
        const docText = awaitSource("doc", () => FS.docs.fetchDocText(doc.filePath));
        if (!docText) { return "Loading Doc..."; }

        if (docEl) { addClass("--loaded", docEl); };
        
        const out = md(docText);
        const mainHeader = out.querySelector("h1");
        mainHeader && append(mainHeader, [
          anchor("HashLink", { href: doc.hash }, "#"),
          anchor("RawLink", {
            target: "_blank",
            href: `https://gitlab.com/SephReed/fs-2021/-/tree/master/docs${doc.filePath.slice(1)}`,
          })
        ]);

        const sections = out.querySelectorAll("div");
        if (sections) {
          sections.forEach((section) => {
            const subHeader = section.querySelector("h3");
            subHeader && append(subHeader, [
              anchor("HashLink", { href: `#${section.id}` }, "#")
            ]);
          })
        }

        const links = out.querySelectorAll("a");
        links.forEach((link) => {
          const href = link.getAttribute("href");
          const externalLink = /http/i.test(href);
          if (externalLink) {
            return link.target = "_blank";
          }

          const hashMatch = href.match(/#[\w-]+/g);
          if (hashMatch) {
            return link.href = hashMatch[0];
          }

          const fileNameMatch = href.match(/[\w-]+(?=\.md)/gi);
          if (fileNameMatch) {
            return link.href = `#${fileNameMatch[0]}`;
          }

          console.warn(`${href} matches none of the link use cases`);
        });

        if (doc.hash === "#volunteers" || doc.hash === "#creators-and-events") {
          links.forEach((link) => {
            if (
              link.href.includes("forms.gle") 
              || link.href.includes("announcements/art-safari")
            ) {
              link.classList.add("btn");
            }
          })
        }
        return out;
      });
      return docEl;
    })
    // FS.docs.docList.renderMap((doc) => 
    //   div("Doc", {
    //     id: doc.hash.slice(1),
    //   },() => {
    //     const docText = awaitSource("doc", () => FS.docs.fetchDocText(doc.filePath));
    //     if (!docText) { return "Loading Doc..."; }
    //     return md(docText);
    //   })
    // )
  })
}