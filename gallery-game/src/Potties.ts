import { Entity } from "./Entity";

export function generatePottyEnts() {
  return PottyPosList.map(([x, y]) => {
    const ent = new Entity({
      imgSrc: "/assets/img/potty.png",
      pos: {x, y},
      size: {
        height: 32,
        width: 16,
      },
      anchorPos: {
        dx: 16,
        dy: 32
      }
    });
    ent.warpToPos({x, y});
    return ent;
  }).sort((a, b) => a.pos.y - b.pos.y)
}



export const PottyPosList: Array<[number, number]> = [
  [279, 1051],
  [297, 1059],
  [320, 1066],

  // lava
  [778, 1222],
  [763, 1208],
  [744, 1195],

  // daft
  [1412, 1118],

  // ice
  [1496, 1236],
  [1523, 1226],
  [1543, 1214],
  [1568, 1203],

  [1080, 1457],
  [1052, 1446],
  [1037, 1436],
  [1453, 1462],
  [1470, 1465],
  [1495, 1467],
  [1881, 1287],
  [1905, 1286],
  [1927, 1286],
  [2101, 1256],
  [2122, 1257],
  [2140, 1258],

  // corn mid
  [2150, 960],
  [2175, 952],
  [2192, 943],

  // corn east
  [2710, 723],
  [2725, 717],
  [2739, 710],
  
  [3039, 819],
  [3021, 824],
  [3497, 780],
  [3516, 786],
  [2775, 1000],
  [2789, 989],
  [2808, 978],

  // tap
  [2582, 1353],
  [2561, 1351],
  [2538, 1354],

  [3000, 1359],
  [2982, 1359],
  [2964, 1357],

  // Calling artists/creatives who'd like to share something online for this years virtual burn!

  // We have a little retro-game version of apache pass that people will be able to wander around checking out links to art.  If you're interested in getting your art on the map, please fill out this form:
  // https://forms.gle/cazmudvvLBZLHvjA9

  // greeter
  [3339, 1363],
  [3356, 1363],

  // gate
  [3734, 1805]
]