import { IHeading, IPos } from "./BasicInterfaces";
import { cloneDeep } from "lodash";

export interface IEntityState<DATA = any> {
  imgSrc: string | null;
  pos: IPos;
  size: { height: number; width: number} | number;
  anchorPos: { dx: number, dy: number }
  targetPos: IPos | null;
  heading: IHeading | null;
  data: DATA;
}

export class Entity<DATA = any> {
  public static imgMap = new Map<string, HTMLImageElement>();
  public static getImage(src: string) {
    if (Entity.imgMap.has(src) === false) {
      const img = new Image();
      img.src = src;
      Entity.imgMap.set(src, img);
    }
    return Entity.imgMap.get(src);
  }

  protected state: IEntityState<DATA>;

  constructor(state?: Partial<IEntityState<DATA>>) {
    this.state = {
      imgSrc: null,
      pos: { x: 0, y: 0},
      size: 16,
      anchorPos: { dx: 8, dy: 8},
      targetPos: null,
      heading: null,
      data: null,
      ...state
    }
  }

  public get pos() { return this.state.pos; }
  public get size() { return "height" in this.size ? this.size : { height: this.size, width: this.size }};
  public get anchorPos() { return this.state.anchorPos; }
  public get imgSrc() { return this.state.imgSrc; }
  public get image() { return Entity.getImage(this.imgSrc); }
  public get data() { return this.state.data; }

  public warpToPos(targetPos: IPos) {
    this.state.pos = targetPos;
  }

  public goToPos(targetPos: IPos) {
    const { state } = this;
    state.targetPos = targetPos;

    const dx = state.targetPos.x - state.pos.x;
    const dy = state.targetPos.y - state.pos.y;
    const distance = Math.sqrt(dx*dx + dy*dy) || 0;
    const steps = distance / 3;
    state.heading = {
      dx: dx/steps || 0,
      dy: dy/steps || 0,
    }
    this.tweenPos();
  }

  public async loadImage() {
    if (!this.imgSrc) { return; }
    const img = Entity.getImage(this.imgSrc);
    if (img.complete) { return; }
    return new Promise((resolve) => {
      img.addEventListener("load", () => {
        console.log("image loaded");
        resolve(null);
      });
    })
  }

  protected dirty = true;
  public get isDirty() { return this.dirty; }
  public get isClean() { return !this.dirty; }
  public setClean() { this.dirty = false; }

  public quickDistanceTo(ent: Entity) {
    return Math.abs(ent.pos.x - this.pos.x) + Math.abs(ent.pos.y - this.pos.y) 
  }

  protected tweenPosInterval: any;
  protected tweenPos() {
    if (this.tweenPosInterval) { return; }
    this.tweenPosInterval = setInterval(() => {
      const { targetPos, pos, heading } = this.state;
      if (!targetPos) {
        clearInterval(this.tweenPosInterval);
        this.tweenPosInterval = null;
        return;
      }
      if (heading.dx !== null) {
        this.dirty = true;
        const newX = pos.x + heading.dx;
        if (targetPos.x < pos.x) {
          pos.x = Math.max(newX, targetPos.x);
        } else {
          pos.x = Math.min(newX, targetPos.x);
        }
        if (targetPos.x === pos.x) { heading.dx = null; }
      }
      if (heading.dy !== null) {
        this.dirty = true;
        const newY = pos.y + heading.dy;
        if (targetPos.y < pos.y) {
          pos.y = Math.max(newY, targetPos.y);
        } else {
          pos.y = Math.min(newY, targetPos.y);
        }
        if (targetPos.y === pos.y) { heading.dy = null; }
      }
      if (heading.dy === null && heading.dx === null) {
        this.state.targetPos = null;
        this.state.heading = null;
      }
    }, 40);
  }
}