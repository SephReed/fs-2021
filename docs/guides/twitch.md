# Guide: Twitch

*this section is not yet complete*

### Availability

Cost: Free

- ✅ Web Client
- ✅ Windows
- ✅ MacOS
- ✅ iOS
- ✅ Android
- ❌ Oculus


**Aditional Access **
- ✅ Fire TV
- ✅ Apple TV
- ✅ PlayStation 4
- ✅ XBox One

-------

### Installation

Totally not neccessary.  But if you want to install an app for watching twitch, [here's the link](https://www.twitch.tv/downloads)

*Note:* If you are intending to create your own stream on Twitch, you should know that
  1. You can livestream through Discord, and a "Station Manager" can highlight you on the Twitch stream
  2. If you don't want to do that, this guide does not have the information you need.

