import "./game.scss";

import { anchor, div, el, Source } from "helium-ui";
import { Entity } from "./Entity";
import { generateTreeEnts, TreePosList } from "./Trees";
import { generatePottyEnts } from "./Potties";

export interface IEntLinkData {
  name: string;
  link: string;
  embed: boolean;
}

export class GalleryGame {
  protected nodes: {
    root: HTMLDivElement;
    canvasStack: HTMLDivElement;
    locationReadout: HTMLDivElement;
    canvas: {
      bg: HTMLCanvasElement;
      ents: HTMLCanvasElement;
      transparentEnts: HTMLCanvasElement;
    }
  };

  protected currentLink = new Source<Entity<IEntLinkData>>();

  protected size = {
    height: 224,
    width: 256
  }

  protected ent = {
    player: new Entity({
      imgSrc: "/assets/img/howdy-head.png"
    }),
    walkingTarget: new Entity({
      imgSrc: "/assets/img/target.png"
    }),
    camera: new Entity(),
    trees: [] as Entity[],
    potties: [] as Entity[],
    billboards: [] as Entity[],
    links: [] as Entity<IEntLinkData>[],
    world: new Entity({
      imgSrc: "/assets/img/gallery-map.svg"
    })
  }

  constructor() {
    this.renderDomNode(),
    this.updateSize();

    this.ent.trees = generateTreeEnts();
    this.ent.potties = generatePottyEnts();

    const {player, walkingTarget, camera, world, trees, potties} = this.ent;

    player.warpToPos({x: 2357, y: 1228});
    camera.warpToPos({x: 2357, y: 1228});

    //{x: 3414, y: 1371}


    Promise.all([
      player.loadImage(),
      walkingTarget.loadImage(),
      world.loadImage(),
      this.fetchGalleryItems().then(() => this.ent.links[0].loadImage()),
      trees[0].loadImage(),
      potties[0].loadImage(),
    ]).then(() => {
      this.ent.billboards = [
        ...trees,
        ...potties
      ].sort((a, b) => a.pos.y - b.pos.y)
      this.start();
    })
  }

  public get domNode() {
    return this.nodes.root;
  }

  protected updateSize() {
    const {ents, bg, transparentEnts} = this.nodes.canvas;
    transparentEnts.width = ents.width = bg.width = this.size.width;
    transparentEnts.height = ents.height = bg.height = this.size.height;
  }

  protected renderDomNode() {
    this.nodes = {
      canvas: {}
    } as any;
    const nodes = this.nodes;

    const root = nodes.root = div("GalleryGame", [
      nodes.canvasStack = div("CanvasStack", {
        onTouch: (ev) => {
          let x = (ev.offsetX / root.offsetWidth) * this.size.width;
          let y = (ev.offsetY / root.offsetHeight) * this.size.height;
        
          const { camera, player, walkingTarget } = this.ent;
  
          const halfWidth = this.size.width/2;
          const halfHeight = this.size.height/2;

          x = (x + camera.pos.x) - halfWidth;
          y = (y + camera.pos.y) - halfHeight;
          player.goToPos({x, y});
          const cameraPos = {
            x: Math.min(Math.max(x, halfWidth), 4173 - halfWidth), 
            y: Math.min(Math.max(y, halfHeight), 2095 - halfHeight),
          };
          camera.goToPos(cameraPos);
          walkingTarget.warpToPos({x, y});
        }
      },[
        nodes.canvas.bg = el("canvas", {
          class: "Background"
        }),
        nodes.canvas.ents = el("canvas", {
          class: "Entities"
        }),
        nodes.canvas.transparentEnts = el("canvas", {
          class: "TransparentEntities"
        }),
      ]),
      div("UIOverlay", [
        nodes.locationReadout = div("Location"),
        () => {
          const link = this.currentLink.get();
          if (!link) { return; }
          return anchor("Link btn", {
            href: link.data.link,
            target: "_blank",
            innards: `View ${link.data.name}`,
          });
        }
      ]),
      // div("ControlPanel", [
      // ])
    ]);
  }

  protected async fetchGalleryItems() {
    const url = /localhost/i.test(location.host) ? `http://localhost:6969` : `https://api.howdyfuckers.online`;
    const items = await (await fetch(`${url}/gallery-items`)).json();
    this.ent.links = items.map((it: any) => new Entity<IEntLinkData>({
      imgSrc: "/assets/img/poi.png",
      pos: { x: it.x, y: it.y },
      data: {
        name: it.name,
        link: it.link,
        embed: it.embed
      }
    }))
  }
  

  public start() {
    this.drawNextFrame();
  }

  protected frameRequest: any;
  protected drawNextFrame() {
    if (this.frameRequest) { return; }
    this.frameRequest = requestAnimationFrame(() => {
      this.frameRequest = undefined;
      const { camera } = this.ent;
      if (camera.isDirty) {
        this.updateBackground();
      }
      this.updateEnts();

      camera.setClean();
      this.drawNextFrame();
    })
  }

  protected updateBackground() {
    const bgCtx = this.nodes.canvas.bg.getContext("2d");
    const { pos } = this.ent.camera;
    const { width, height } = this.size;
    const img = this.ent.world.image;
    bgCtx.clearRect(0, 0, width, height);
    bgCtx.drawImage(img, ~~(pos.x - width/2), ~~(pos.y - height/2), width, height, 0, 0, width, height);
  }

  protected updateEnts() {
    const {camera, walkingTarget, player, links, billboards} = this.ent;
    if (camera.isClean && walkingTarget.isClean && player.isClean) {
      return;
    }

    const entCtx = this.nodes.canvas.ents.getContext("2d");
    const { width, height } = this.size;
    entCtx.clearRect(0, 0, width, height);
    [
      ...links,
      walkingTarget,
      player
    ].forEach((ent) => {
      const { x, y } = ent.pos;
      const screenX = (x - camera.pos.x) + width/2;
      const screenY = (y - camera.pos.y) + height/2;

      entCtx.drawImage(ent.image, ~~screenX, ~~screenY);
      if (ent === player) {
        this.nodes.locationReadout.textContent = `${~~x} ${~~y}`;
      }
    });
    walkingTarget.setClean();
    player.setClean();

    this.currentLink.set(links.find((link) => link.quickDistanceTo(player) < 20));

    const transpCtx = this.nodes.canvas.transparentEnts.getContext("2d");
    transpCtx.clearRect(0, 0, width, height);
    const maxDistTry = width+height;
    billboards.forEach((ent) => {
      const { x, y } = ent.pos;
      if (ent.quickDistanceTo(camera) > maxDistTry) { return; }
      let screenX = (x - camera.pos.x) + width/2;
      screenX -= ent.anchorPos.dx;
      let screenY = (y - camera.pos.y) + height/2;
      screenY -= ent.anchorPos.dy;
      let ctx = entCtx;
      if (y >= camera.pos.y && ent.quickDistanceTo(player) < 100) {
        ctx = transpCtx;
      }
      ctx.drawImage(ent.image, ~~screenX, ~~screenY);
    })
  }
}