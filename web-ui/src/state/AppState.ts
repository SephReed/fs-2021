import { Sourcify } from "helium-ui";
import { Pointy } from "../components/Pointy/Pointy";
import { DocsState } from "./DocsState";
import { RouteState } from "./RouteState";



class _AppState {
  public readonly selection = Sourcify({
    docItem: null as string
  })

  public readonly docs = new DocsState();
  public readonly route = new RouteState();

  public readonly pointy = new Pointy();
}


export const FS = new _AppState();