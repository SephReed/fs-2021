# Guide: Discord
**The text / audio / video chat extravaganza**

<div id="DiscordAvailability">

### Availability

Cost: Free

- ✅ Web Client
- ✅ Windows
- ✅ MacOS
- ✅ iOS
- ✅ Android
- ❌ Oculus
</div>

-------

<div id="DiscordInstall">

### Installation

Download and install the mobile app or desktop app.  This is recommended over using the web browser for better performance.

[Download Discord Here](https://discord.com/download)

Once installed, open the application.  You may have to create an account, or want to find past account information.
</div>

-------

<div id="DiscordAddServer">

### Connect to the Event Server!
- Click the big `+` Plus Sign to add a server
  - On Desktop: this should be found on the sidebar
  - ![Discord Settings Gear](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-discord--add-server-1.png)
- A prompt will pop up.  At the bottom of it will be "Have an Invite Already?".  Select this option.
  - ![Discord Settings Gear](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-discord--add-server-2.png)
- Paste the following URL into the text field: OMMITTED
</div>

-------

<div id="DiscordVerification">

### Verification

At first, you will only have access to the "parking-lot" chat room.  Before you can gain access to the rest of the Discord server, you must verify
that you are human by telling a story about *"something interesting you once saw at an art festival"*.

It may take a few minutes (or hours??) before a greeter comes by to read your story, say hi, and grant you permissions.

We're trying to keep trolls out.

If nobody responds, you can come back later.  Eventually a greeter or ranger will see your message.
</div>

-------

<div id="DiscordPushToTalk">

### Push to Talk
By default, people will only hear you while you're holding the "UNKNOWN" button (ONLY ON DESKTOP??).  That's because this 
chat client is meant for gaming.

- Open up your settings by clicking the gear icon next to your username
  - On desktop: this should be near the bottom left of the application
  - ![Discord Settings Gear](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-discord--gear.png)
- Once in settings, navigate to `Voice & Video`
  - ![Voice and Video Setting](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-discord--push-to-talk-1.png)
- In the "Voice and Video" section, is a sub-section labeled `INPUT MODE`
  - Set your input mode to `Voice Activity`
  - Then test it by talking and seeing if the indicator bar below turns green
  - ![Voice Activity Setting and Test](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/guide-discord--push-to-talk-2.png)
</div>

-------

<div id="DiscordVolunteer">

### Volunteer

There are a few volunteer positions for Discord, which include:
1. Greeters - Saying hi to people who enter the `#parking-lot` channel, verifying they are human, and giving them entrance to the rest of the server
2. Rangers - Responding to trolls with kicks and bans.

</div>



