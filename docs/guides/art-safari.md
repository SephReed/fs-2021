# Guide: Art Safari

*this section is not yet complete*

<div id="ArtSafariAvailability">

### Availability

Cost: Free

- ✅ Web Client
- ❌ Windows
- ❌ MacOS
- ✅ iOS
- ✅ Android
- ❌ Oculus
</div>

-------

### Getting Started

- To read about the Burner Art Safari, visit: [http://burnerartsafari.org/](http://burnerartsafari.org/)
- To view the Art Safari map, visit: [https://www.google.com/maps/d/viewer?mid=1RIAE7wbXFCqEEuhVHqTTO7JY3AUwvhDz&ll=30.274643949789997%2C-97.75821316064753&z=10](https://www.google.com/maps/d/viewer?mid=1RIAE7wbXFCqEEuhVHqTTO7JY3AUwvhDz&ll=30.274643949789997%2C-97.75821316064753&z=10)
