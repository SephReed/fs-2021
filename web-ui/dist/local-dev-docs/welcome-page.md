# Welcome Home(page)


<div id="Announcements">

### Announcements!!
  - **Tech Upgrades**: If you were planning on getting a VR headset or upgrading your PC, here's your 
    reminder to get started.  Note that *expensive tech is not a requirement for participation*.  But 
    some experiences will be more immersive with it.  Also, this years Virtual "Big Burn" will be making heavy use of VR.

  - **Help us Ideate the Event**: There is so much room for ideas here.  We'd love to [get your input](https://discord.gg/v7zG9CaV5R).

  - **Mobile or Console Friendly Events**: Currently, there's a lack of virtual space (outside of chat apps) 
    available to participants without VR or PC.  [Suggest an idea here](./creators-and-events.md#Other)
</div>


--------

<div id="Overview">

### Overview
Welcome to the Virtual Event Survival Guide!  
  
Here's the basic gist of whats going on:
  * **The Burner Art Safari** - it's not technically virtual, but [the map part](./guides/art-safari.md) is
    ![The Burner Art Safari Map](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/photo-burner-art-map.jpg)
    *All the strange and interesting, from the comfort of your automobile! (last years map)*
  * **Video / Audio / Chat Streams** - whether chatting in [Discord](./guides/Discord.md), tuning in to KFlip, or watching [Twitch](./guides/Twitch.md) streams, anything streamable fits in
    ![Video Chatting](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/photo-silly-discord-call.png)
    *These are just some randos showing off Halloween costumes... but you get the idea*
  * **The Virtual Wander Gallery** - for things that are less live; an online gallery with a wandering, explorative, stumble-upon, no-particular-order aspect
    ![Mock-up of Wandering Gallery](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/photo-wandering-gallery.png)
    *This is just a mock-up of what the wandering gallery could be like*
  * **Virtual Game Spaces** - mostly [Minecraft](./guides/Minecraft.md) and [Altspace](./guides/Altspace.md), but others may pop up
    ![Minecraft Burn Night](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/photo-minecraft-burn-night.png)
    *2020 Flipside Weekend Minecraft on Burn Night*
    ![Altspace Monkeys](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/photo-altspace-monkeys.jpeg)
    *A bunch of monkeys hanging around the AltSpace version of their Burning Man art piece - credit: EspressoBuzz [(see more AltSpace Burner photos here)](https://www.espressobuzz.net/Events/2020/BRCvr/)*
  * **Events** - a calendar of whats happening when and how to connect to it
    ![Event Calendar](https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/img/photo-event-calendar.png)
    *Imagine something like this, but less organized*
  
Want to participate, or contribute?  Thank you so much!
  1. **Creators and Events** - Which covers pretty much anything you can [host on the internet or in your front yard](./creators-and-events.md).
  2. **Volunteers** - Which covers all the many, many niches that [could use your support](./volunteer.md).  Literally *anyone with an internet connection* can help.
</div>

--------

<div id="Schedule">

### Virtual Burn Schedule

The "Who What When Where" guide for this event is a Google calendar.  If you'd like to add an event, check
out the [Creators and Events section](#creators-and-events).
  
[Click here to view the (currently mostly empty) calendar](https://calendar.google.com/calendar/u/0?cid=b2lzZTJzajlmNmxncTk3N2dkYTNta2ttN2NAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ)
</div>

