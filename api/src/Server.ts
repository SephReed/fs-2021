import express from "express";
import fetch from "node-fetch";
import { csvTextToObjectArray } from "actual-csv-to-json-parser";

const port = 6969;
const app = express();


const docMap = new Map<string, string>();
const csvMap = new Map<string, string>();
const HOUR = 60 * 60 * 1000;
setInterval(() => {
  docMap.clear();
  csvMap.clear();
}, 2 * HOUR);

app.get(/^\/git-doc\//i, async (req, res) => {
  res.set("Access-Control-Allow-Origin", "*");

  const match = req.path.match(/git-doc\/(.+)/i);
  const docName = match && match[1].toLocaleLowerCase();
  if (!docName) { return res.send({ error: "No docName supplied" })};

  if (docMap.has(docName)) {
    console.log(`Using Cache for ${docName}`);
    return res.send(docMap.get(docName));
  }
  const url = `https://gitlab.com/SephReed/fs-2021/-/raw/master/docs/${docName}`;
  const doc = await fetch(url);
  const text = await doc.text();
  docMap.set(docName, text);
  res.send(text);
});

app.get("/refresh-docs", (req, res) => {
  res.set("Access-Control-Allow-Origin", "*");
  docMap.clear();
  res.send(`The docs cache has been cleared`);
})

app.get("/gallery-items", async (req, res) => {
  res.set("Access-Control-Allow-Origin", "*");
  
  const sheetUrl = `https://docs.google.com/spreadsheets/d/e/2PACX-1vT8A5WlztPQA2-lvknvwa53bJnS8ttKiLvciDOj6MXb2n6nZWV8mHKI3_5lN_M9i_zH6kwPS9ANVJq9/pub?gid=0&single=true&output=csv`;

  if (csvMap.has(sheetUrl) === false) {
    const rawSheet = await (await fetch(sheetUrl)).text();
    const items = csvTextToObjectArray(rawSheet).filter((it) => !!it.name);
    csvMap.set(sheetUrl, JSON.stringify(items, null, 2));
  }
  res.send(csvMap.get(sheetUrl));
})

let count = 0;
app.get(/./, (req, res) => {
  res.send(`Howdy Fucker!  This page has been loaded ${count++} time(s) since the api last restart.  Very interesting stuff`)
});


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})